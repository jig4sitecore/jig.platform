﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    using System.Text;

    using Newtonsoft.Json;

    [TestClass]
    public partial class UnitTest
    {
        /// <summary>
        /// Tests the exception data serialization to json.
        /// </summary>
        [TestMethod]
        public void TestExceptionDataSerializationToJson()
        {
            try
            {
                var exception = new Exception("Hello world");
                exception.Data["Id"] = "MyID";

                string json = JsonConvert.SerializeObject(exception);
                Assert.IsTrue(!string.IsNullOrWhiteSpace(json));
            }
            catch (Exception exception)
            {
                Assert.Fail(exception.Message);
            }
        }
    }
}
