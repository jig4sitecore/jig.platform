﻿namespace Jig.Sitecore.Logging
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading;
    using System.Web;
    using System.Web.Hosting;
    using System.Web.SessionState;

    using global::Sitecore;

    /// <summary>
    /// The log extensions.
    /// </summary>
    public static class LoggingExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Get the client's IP address, taking into account some proxies and clusters
        /// </summary>
        /// <param name="request">The HTTP values sent by a client during a Web request</param>
        /// <remarks>
        ///     Many proxies don't expose the real client's IP address. In such cases, the proxies IP address will be returned
        /// </remarks>
        /// <returns>The best guess at a client's IP address, or an empty string</returns>
        [NotNull, SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Suppression is OK here.")]
        public static string UserIpAddress([NotNull] this HttpRequest request)
        {
            var headers = new[]
                              {
                                  "HTTP_X_FORWARDED_FOR", 
                                  "HTTP_X_CLUSTER_CLIENT_IP", 
                                  "HTTP_CLIENT_IP",
                                  "REMOTE_ADDR", 
                                  "REMOTE_HOST"
                              };

            NameValueCollection serverVariables = request.ServerVariables;
            foreach (string ipAddress in headers.Select(t => serverVariables[t]).Where(ipAddress => !string.IsNullOrEmpty(ipAddress)))
            {
                return ipAddress;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the error source.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>the exception source text</returns>
        [NotNull]
        [DebuggerHidden]
        public static string GetExceptionSource([NotNull] this Exception exception)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (exception != null)
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                if (exception.InnerException != null)
                {
                    return exception.InnerException.Source;
                }

                return exception.Source;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the error error details.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>the exception detail text</returns>
        [NotNull]
        [DebuggerHidden]
        public static string GetExceptionDetails([NotNull] this Exception exception)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (exception != null)
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                if (exception.InnerException != null)
                {
                    return exception.InnerException.ToString();
                }

                return exception.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the name of the error type.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>the exception type name text</returns>
        [NotNull]
        [DebuggerHidden]
        public static string GetExceptionTypeName([NotNull] this Exception exception)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (exception != null)
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                if (exception.InnerException != null)
                {
                    return exception.InnerException.GetType().FullName;
                }

                return exception.GetType().FullName;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>The Exception Message text</returns>
        [NotNull]
        [DebuggerHidden]
        public static string GetExceptionMessage([NotNull] this Exception exception)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (exception != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                if (exception.InnerException != null)
                {
                    return exception.InnerException.Message;
                }

                return exception.Message;
            }

            return string.Empty;
        }

        /// <summary>
        /// Generates the formatted stack trace.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="exception">The httpException.</param>
        public static void AddExceptionStackData([NotNull] this IDictionary data, [NotNull] Exception exception)
        {
            try
            {
                data["Exception.Message"] = exception.GetExceptionMessage();
                data["Exception.TypeName"] = exception.GetExceptionTypeName();
                data["Exception.Source"] = exception.GetExceptionSource();

                data["Exception.HelpLink"] = exception.HelpLink;
                data["Environment.MachineName"] = Environment.MachineName;
                data["CurrentCulture.Name"] = Thread.CurrentThread.CurrentCulture.Name;

                data["Exception.Details"] = exception.GetExceptionDetails();

                /* The error should not occur till stack exception operation. The above properties will be logged always */
                var exceptionStack = new Stack<Exception>();

                // create httpException stack
                for (Exception e = exception; e != null; e = e.InnerException)
                {
                    exceptionStack.Push(e);
                }

                if (exceptionStack.Count != 0)
                {
                    // render httpException type and message
                    Exception ex = exceptionStack.Pop();

                    // Load stack trace
                    var stackTrace = new StackTrace(ex, true);
                    for (int frame = 0; frame < stackTrace.FrameCount; frame++)
                    {
                        StackFrame stackFrame = stackTrace.GetFrame(frame);
                        MethodBase method = stackFrame.GetMethod();
                        Type declaringType = method.DeclaringType;

                        if (stackFrame.GetFileLineNumber() > 0)
                        {
                            if (declaringType != null)
                            {
                                data["Type.GUID"] = declaringType.GUID.ToString();
                                data["Type.FullName"] = declaringType.FullName;
                                data["Type.AssemblyQualifiedName"] = declaringType.AssemblyQualifiedName;
                                data["Type.Namespace"] = declaringType.Namespace;

                                data["AssemblyLocation"] = Path.GetFileName(declaringType.Assembly.Location);
                                var assemblyName = declaringType.Assembly.GetName();
                                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                                if (assemblyName != null)
                                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                                {
                                    data["Assembly.Name"] = assemblyName.Name;
                                    if (assemblyName.Version != null)
                                    {
                                        var version = assemblyName.Version;
                                        data["Assembly.Version"] = string.Concat(version);
                                        data["Assembly.Version.Build"] = string.Concat(version.Build);
                                        data["Assembly.Version.MajorRevision"] = string.Concat(version.MajorRevision);
                                        data["Assembly.Version.MinorRevision"] = string.Concat(version.MinorRevision);
                                        data["Assembly.Version.Revision"] = string.Concat(version.Revision);
                                    }
                                }
                            }

                            var fileName = stackFrame.GetFileName();

                            if (!string.IsNullOrWhiteSpace(fileName))
                            {
                                data["StackFrame.FileName"] = stackFrame.GetFileName();
                                data["StackFrame.FileName.VirtualPath"] = fileName.Replace(HostingEnvironment.ApplicationPhysicalPath, "/").Replace('\\', '/');
                            }

                            data["StackFrame.FileLineNumber"] = stackFrame.GetFileLineNumber().ToString(CultureInfo.InvariantCulture);
                            data["StackFrame.ShortFileName"] = Path.GetFileName(stackFrame.GetFileName());

                            var trace = new StringBuilder(64);

                            if (declaringType != null)
                            {
                                trace.Append(declaringType.Namespace)
                                    .Append('.')
                                    .Append(declaringType.Name)
                                    .Append('.')
                                    .Append(method.Name);
                            }

                            data["StackFrame.MethodBaseName"] = trace.ToString();

                            trace.Append('(');

                            // get parameter information 
                            ParameterInfo[] parameters = method.GetParameters();
                            for (int paramIndex = 0; paramIndex < parameters.Length; paramIndex++)
                            {
                                trace.Append(
                                    ((paramIndex != 0) ? "," : string.Empty) + parameters[paramIndex].ParameterType.Name
                                    + " " + parameters[paramIndex].Name);
                            }

                            trace.Append(')');

                            data["StackFrame.MethodBaseFullName"] = trace.ToString();

                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceInformation(ex.ToString());
            }
        }

        /// <summary>
        /// Gets the server variables.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="request">The request.</param>
        public static void AddUrlVariables([NotNull] this IDictionary data, [NotNull] HttpRequest request)
        {
            try
            {
                data["Url.Host"] = request.Url.Host;
                data["Url"] = string.Concat(request.Url);
                data["Url.AbsolutePath"] = request.Url.AbsolutePath;
                data["Url.Query"] = request.Url.Query;
                data["Url.PathAndQuery"] = request.Url.PathAndQuery;
                data["Url.Referrer"] = string.Concat(request.UrlReferrer);

                data["Url.HttpMethod"] = request.HttpMethod;
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            {
                // ReSharper restore EmptyGeneralCatchClause
                /* If the httpException thrown before authentication, the property 'System.Web.HttpRequest.LogonUserIdentity' will throw the InvalidOperationException with message 'This method can only be called after the authentication event.'   */
            }
        }

        /// <summary>
        /// Add the user variables.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="request">The request.</param>
        public static void AddUserVariables([NotNull] this IDictionary data, [NotNull] HttpRequest request)
        {
            try
            {
                data["User.HostName"] = request.UserHostName;
                data["User.IpAddress"] = request.UserIpAddress();

                data["User.IsAuthenticated"] = string.Concat(request.IsAuthenticated);

                /* This will trow an exception if called before authenticate event */
                if (request.LogonUserIdentity != null && request.IsAuthenticated)
                {
                    data["User.Identity.Name"] = request.LogonUserIdentity.Name;
                    data["User.Identity.User"] = string.Concat(request.LogonUserIdentity.User);
                    data["User.Identity.AuthenticationType"] = request.LogonUserIdentity.AuthenticationType;
                }
            }

                // ReSharper disable EmptyGeneralCatchClause
            catch
            {
                // ReSharper restore EmptyGeneralCatchClause
                /* If the httpException thrown before authentication, the property 'System.Web.HttpRequest.LogonUserIdentity' will throw the InvalidOperationException with message 'This method can only be called after the authentication event.'   */
            }
        }

        /// <summary>
        /// Gets the session state variables.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="session">The session.</param>
        public static void AddSessionStateVariables([NotNull] this IDictionary data, [NotNull] HttpSessionState session)
        {
            try
            {
                data["Session.ID"] = session.SessionID;
                data["Session.IsNewSession"] = string.Concat(session.IsNewSession);
                data["Session.IsCookieless"] = string.Concat(session.IsCookieless);
                data["Session.IsReadOnly"] = string.Concat(session.IsReadOnly);
                data["Session.StateMode"] = string.Concat(session.Mode);
                data["Session.Timeout"] = string.Concat(session.Timeout);
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
            }
        }

        /// <summary>
        /// Adds the server variables.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="request">The request.</param>
        public static void AddServerVariables([NotNull] this IDictionary data, [NotNull] HttpRequest request)
        {
            try
            {
                data["Server.ID"] = request.ServerVariables["HTTP_X_SITE_DEPLOYMENT_ID"];
                data["Server.WebSiteName"] = Environment.GetEnvironmentVariable("WEBSITE_SITE_NAME");
                data["Server.WebSiteMode"] = Environment.GetEnvironmentVariable("WEBSITE_SKU");
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
            }
        }

        #endregion
    }
}