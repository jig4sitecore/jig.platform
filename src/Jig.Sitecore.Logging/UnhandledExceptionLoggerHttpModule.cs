﻿[assembly: System.Web.PreApplicationStartMethod(typeof(Jig.Sitecore.Logging.UnhandledExceptionLoggerHttpModule), "Start")]

namespace Jig.Sitecore.Logging
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.Web;
    using log4net;
    using Newtonsoft.Json;
    using global::Sitecore;

    /// <summary>
    /// The log exception http module.
    /// </summary>
    /// <remarks>The module is design to be used with JSON format loggers like LogEntries or Loggy, not regular error logging format.
    /// The JSON format makes custom queries, reports and dashboard implementation very easy and intuitive.
    /// </remarks>
    public sealed class UnhandledExceptionLoggerHttpModule : IHttpModule
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger("Jig.Sitecore.Logging.UnhandledExceptions");

        #region Public Methods and Operators

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public static void Start()
        {
            // Register module
            HttpApplication.RegisterModule(typeof(UnhandledExceptionLoggerHttpModule));
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// The init.
        /// </summary>
        /// <param name="context">The context.</param>
        public void Init(HttpApplication context)
        {
            context.Error += this.ContextOnError;
        }

        /// <summary>
        /// The context error.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void ContextOnError([NotNull] object sender, [NotNull] EventArgs e)
        {
            var httpApplication = sender as HttpApplication;
            try
            {
                if (httpApplication != null)
                {
                    Exception exception = httpApplication.Server.GetLastError();
                    if (exception != null)
                    {
                        var context = httpApplication.Context;

                        /* 500 and above exceptions only */
                        bool isFatalLevelException = false;

                        var httpException = exception as HttpException;
                        if (httpException != null)
                        {
                            /*
                               System.Web.HttpException (0x80004005): A potentially dangerous Request.Path value was detected from the client (:).
                                  at System.Web.HttpRequest.ValidateInputIfRequiredByConfig()
                                  at System.Web.HttpApplication.PipelineStepManager.ValidateHelper(HttpContext context)
             
                               System.Web.HttpException (0x80004005): A potentially dangerous Request.Path value was detected from the client (%).
                                  at System.Web.HttpRequest.ValidateInputIfRequiredByConfig()
                                  at System.Web.HttpApplication.PipelineStepManager.ValidateHelper(HttpContext context)
             
                               System.Web.HttpException (0x80004005): A potentially dangerous Request.Path value was detected from the client (?).
                                  at System.Web.HttpRequest.ValidateInputIfRequiredByConfig()
                                  at System.Web.HttpApplication.PipelineStepManager.ValidateHelper(HttpContext context)
                            */
                            if (httpException.Message.Contains("(0x80004005)"))
                            {
                                httpApplication.Server.ClearError();
                                return;
                            }

                            /* 
                             * Error caused by Search engine caching of ASP.NET assembly WebResource.axd file(s)
                               'WebResource.axd' Or 'ScriptResource.axd'
                             */
                            string url = httpApplication.Context.Request.Url.AbsolutePath;

                            if (url.EndsWith(".axd", StringComparison.OrdinalIgnoreCase))
                            {
                                httpApplication.Server.ClearError();
                                return;
                            }

                            var statusCode = httpException.GetHttpCode();
                            if (statusCode > 499)
                            {
                                isFatalLevelException = true;
                            }

                            exception.Data["StatusCode"] = statusCode.ToString(CultureInfo.InvariantCulture);

                            exception.Data.AddUrlVariables(context.Request);
                            exception.Data.AddUserVariables(context.Request);
                            exception.Data.AddSessionStateVariables(context.Session);
                            exception.Data.AddServerVariables(context.Request);
                        }

                        exception.Data.AddExceptionStackData(exception);

                        /* Major problem:
                         *      In the log4Net layout the exception is private and use Exception.ToString() method. 
                         *  This method ignores Data dictionary which stores all additional information. 
                         *  The only way to override this is implement custom AppenderSkeleton, but then it will be impossible to use out-of-box third party libraries.    
                         */
                        string json = JsonConvert.SerializeObject(exception);
                        if (isFatalLevelException)
                        {
                            Log.Fatal(json);
                        }
                        else
                        {
                            Log.Error(json);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error(exception.Message, exception);

                /* For Azure & Visual Studio */
                Trace.TraceInformation(exception.ToString());
                var message = string.Concat(exception);
                System.Diagnostics.Debugger.Log(0, "LogHttpException", message);
                System.Diagnostics.Trace.TraceInformation(message);
            }
        }

        #endregion
    }
}