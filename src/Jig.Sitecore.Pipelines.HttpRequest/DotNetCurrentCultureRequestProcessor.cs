﻿namespace Jig.Sitecore.Pipelines.HttpRequest
{
    using System;
    using System.Globalization;
    using System.Threading;
    using global::Sitecore;

    /// <summary>
    /// Class DotNetCurrent Culture RequestProcessor.
    /// </summary>
    public sealed class DotNetCurrentCultureRequestProcessor : HttpRequestProcessor
    {
        /// <summary>
        /// Processes the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        protected override void ProcessRequest([NotNull] global::Sitecore.Pipelines.HttpRequest.HttpRequestArgs args)
        {
            try
            {
                var language = global::Sitecore.Context.Language;
                if (language == null)
                {
                    return;
                }

                var culture = new CultureInfo(language.Name);
                if (!culture.Equals(Thread.CurrentThread.CurrentUICulture))
                {
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(language.Name);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(language.Name);
                }
            }
            catch (Exception ex)
            {
                global::Sitecore.Diagnostics.Log.Error(this.GetType().FullName + ". Details: " + ex.Message, this);
            }
        }
    }
}