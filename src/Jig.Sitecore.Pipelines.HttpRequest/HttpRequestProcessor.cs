﻿namespace Jig.Sitecore.Pipelines.HttpRequest
{
    using System;
    using global::Sitecore;
    using global::Sitecore.Pipelines.HttpRequest;

    /// <summary>
    /// The http request processor.
    /// </summary>
    public abstract class HttpRequestProcessor : global::Sitecore.Pipelines.HttpRequest.HttpRequestProcessor,
                                                 IHttpRequest
    {
        #region Public Methods and Operators

        /// <summary>
        /// Processes the specified arguments.
        /// </summary>
        /// <param name="args">
        /// The arguments.
        /// </param>
        public override void Process(
            [NotNull] HttpRequestArgs args)
        {
            /* ReSharper disable ConditionIsAlwaysTrueOrFalse */
            if (args == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                // ReSharper disable HeuristicUnreachableCode
                const string Msg = "The Sitecore.Pipelines.HttpRequest.HttpRequestArgs args is null!";
                global::Sitecore.Diagnostics.Log.Warn(Msg, this);
                System.Diagnostics.Trace.TraceWarning(Msg);

                return;
            }
            // ReSharper restore HeuristicUnreachableCode

            /* ReSharper restore HeuristicUnreachableCode */
            try
            {
                this.ProcessRequest(args);
            }
            catch (Exception ex)
            {
                global::Sitecore.Diagnostics.Log.Error(this.GetType().FullName + ". Details: " + ex.Message, ex, this);
                System.Diagnostics.Trace.TraceError(ex.ToString());
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The process request.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected abstract void ProcessRequest(
            [NotNull] HttpRequestArgs args);

        #endregion
    }
}