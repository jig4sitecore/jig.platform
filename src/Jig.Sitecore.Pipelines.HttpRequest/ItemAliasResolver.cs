﻿namespace Jig.Sitecore.Pipelines.HttpRequest
{
    using System.Linq;

    using Jig.Sitecore.Context;
    using Jig.Sitecore.Sites.Shared.Data.System.Interfaces;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Pipelines.HttpRequest;

    /// <summary>
    /// The item alias resolver.
    /// </summary>
    public class ItemAliasResolver : HttpRequestProcessor, ISitesToIgnore
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the sites to ignore.
        /// </summary>
        public string SitesToIgnore { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The process request.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected override void ProcessRequest(HttpRequestArgs args)
        {
            var site = global::Sitecore.Context.Site;
            if (site == null)
            {
                return;
            }

            // If the current item is found, there is no reason to go on, so we return.
            if (global::Sitecore.Context.Item != null)
            {
                return;
            }

            // If there is no context, we are not in a request from a web browser.
            if (args.Context == null)
            {
                return;
            }

            // If there is no context database, we won't be able to continue
            if (global::Sitecore.Context.Database == null)
            {
                return;
            }

            // We have to make sure we don't run for the website to make tasks working without failing.
            if (string.IsNullOrWhiteSpace(site.HostName))
            {
                return;
            }

            if (this.IsMatchSitesToIgnore(global::Sitecore.Context.Site))
            {
                return;
            }

            var aliasesFolder = global::Sitecore.Context.Database.GetItem(global::Sitecore.Context.Site.RootPath + Constants.SiteInfo.PropertyStandardValues.AliasFolder);
            if (aliasesFolder == null)
            {
                return;
            }

            var template = global::Sitecore.Context.Database.GetTemplate(typeof(IItemAlias));
            if (template == null)
            {
                return;
            }

            var aliasItems = aliasesFolder.Axes.GetDescendants().Where(d => d.IsDerived(template)).Select(i => i.As<IItemAlias>());

            IItemAlias itemAlias = null;
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var aliasItem in aliasItems)
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                if (aliasItem.IsMatch(args))
                {
                    itemAlias = aliasItem;
                    break;
                }
            }

            if (itemAlias == null)
            {
                return;
            }

            var action = itemAlias.Action.TargetItem.As<IItemAliasAction>();

            if (action == null)
            {
                Log.Warn("ItemAliasResolver - Alias: " + itemAlias.ID + " has no action assigned.", this);
                return;
            }

            action.Execute(itemAlias, args);
        }

        #endregion
    }
}