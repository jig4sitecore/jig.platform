﻿namespace Jig.Sitecore.Pipelines.HttpRequest
{
    using System;
    using System.IO;
    using System.Web;
    using Jig.Sitecore.Context;

    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Globalization;
    using global::Sitecore.Links;
    using global::Sitecore.Pipelines.HttpRequest;
    using global::Sitecore.Sites;
    using global::Sitecore.Web;

    /// <summary>
    /// Class SupportedLanguagesRequestProcessor.
    /// </summary>
    public class SupportedLanguagesRequestProcessor : HttpRequestProcessor, ISitesToIgnore, IDatabasesToIgnore
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SupportedLanguagesRequestProcessor" /> class.
        /// </summary>
        // ReSharper disable NotNullMemberIsNotInitialized
        public SupportedLanguagesRequestProcessor()
        // ReSharper restore NotNullMemberIsNotInitialized
        {
            this.FallBackLanguage = "en";
        }

        /// <summary>
        /// Gets or sets the fallback language.
        /// </summary>
        /// <value>The fallback language.</value>
        [NotNull]
        public string FallBackLanguage { get; set; }

        /// <summary>
        /// Gets or sets the databases to ignore.
        /// </summary>
        /// <value>The databases to ignore.</value>
        public string DatabasesToIgnore { get; set; }

        /// <summary>
        /// Gets or sets the sites to ignore.
        /// </summary>
        /// <value>The sites to ignore.</value>
        public string SitesToIgnore { get; set; }

        /// <summary>
        /// Gets or sets the site.
        /// </summary>
        /// <value>The site.</value>
        [NotNull]
        protected SiteContext ContextSite { get; set; }

        /// <summary>
        /// Gets or sets the database.
        /// </summary>
        /// <value>The database.</value>
        [NotNull]
        protected Database ContextDatabase { get; set; }

        /// <summary>
        /// Gets or sets the context language.
        /// </summary>
        /// <value>The context language.</value>
        [NotNull]
        protected Language ContextLanguage { get; set; }

        /// <summary>
        /// Gets or sets the context item.
        /// </summary>
        /// <value>The context item.</value>
        [NotNull]
        protected Item ContextItem { get; set; }

        /// <summary>
        /// Processes the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        protected override void ProcessRequest([NotNull] HttpRequestArgs args)
        {
            this.ContextItem = global::Sitecore.Context.Item;
            if (this.ContextItem == null || !global::Sitecore.Context.PageMode.IsNormal)
            {
                return;
            }

            /* Quick check */
            if (string.Equals(this.FallBackLanguage, this.ContextItem.Language.Name, StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }

            var requestUrl = args.Context.Request.Url;
            var path = requestUrl.AbsolutePath;
            var extension = Path.GetExtension(path);
            if (!string.IsNullOrWhiteSpace(extension))
            {
                return;
            }

            this.ContextSite = global::Sitecore.Context.Site;
            if (this.ContextSite == null)
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(this.ContextSite.HostName))
            {
                return;
            }

            if (this.IsMatchSitesToIgnore(this.ContextSite))
            {
                return;
            }

            this.ContextDatabase = global::Sitecore.Context.Database;
            if (this.ContextDatabase == null)
            {
                return;
            }

            if (this.IsMatchDatabasesToIgnore(this.ContextDatabase))
            {
                return;
            }

            var supportedLanguages = this.ContextSite.GetSupportedLanguages(this.ContextDatabase);
            if (supportedLanguages.Count == 0)
            {
                return;
            }

            /* Item exist. Verify the language logic */
            if (!this.ContextItem.Paths.FullPath.StartsWith(Jig.Sitecore.Constants.Paths.Sites.Content, StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }

            /* Last check */
            if (supportedLanguages.Contains(this.ContextItem.Language.Name.ToLowerInvariant()))
            {
                return;
            }

            /* Redirect logic to the same item but valid language like 'en' */
            var language = Language.Parse(this.FallBackLanguage);
            global::Sitecore.Context.Language = language;

            string location = null;

            /* Try get item with valid language */
            var newItem = this.ContextItem.Database.GetItem(this.ContextItem.ID, language);
            if (newItem != null)
            {
                /* Avoid loop redirects. Checking does not hurt! */
                var uri = LinkManager.GetItemUrl(newItem, this.ContextSite.GetDefaultUrlOptions());
                if (!string.Equals(uri, requestUrl.AbsolutePath, StringComparison.InvariantCultureIgnoreCase))
                {
                    location = uri;
                }
            }

            /* If specific language item not found do redirect to 404 logic */
            if (string.IsNullOrWhiteSpace(location))
            {
                var page = this.GeNotFoundPageItemPath(this.ContextSite);
                var rawUrl = global::Sitecore.Context.RawUrl;

                if (string.Equals(
                    this.ContextSite.Language,
                    this.FallBackLanguage,
                    StringComparison.InvariantCultureIgnoreCase))
                {
                    location =
                        string.Concat("/?page=", HttpUtility.UrlEncode(rawUrl))
                            .ToLowerInvariant();

                    this.SetLanguageCookie(language.Name);
                }
                else
                {
                    location =
                        string.Concat('/', this.FallBackLanguage, page, "?page=", HttpUtility.UrlEncode(rawUrl))
                            .ToLowerInvariant();

                    this.SetLanguageCookie(language.Name);
                }
            }
            else
            {
                this.SetLanguageCookie(language.Name);
            }

            args.Context.Response.Redirect(location, false);
            args.AbortPipeline();
        }

        /// <summary>
        /// Sets the language cookie.
        /// </summary>
        /// <param name="language">The language.</param>
        protected void SetLanguageCookie([NotNull] string language)
        {
            var cookieName = this.ContextSite.GetCookieKey("lang");
            WebUtil.SetCookieValue(cookieName, language, DateTime.MaxValue);
        }

        /// <summary>
        /// Gets the not found page item path.
        /// </summary>
        /// <param name="site">The site.</param>
        /// <param name="siteProperty">The site property.</param>
        /// <returns>Not found page path</returns>
        [NotNull]
        protected virtual string GeNotFoundPageItemPath(
            [NotNull]SiteContext site,
            [NotNull]string siteProperty = Jig.Sitecore.Constants.SiteInfo.PropertyNames.NotFoundPageItemPath)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (site != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                var property = site.Properties[siteProperty];
                if (!string.IsNullOrWhiteSpace(property))
                {
                    return property.Trim();
                }
            }

            return Jig.Sitecore.Constants.SiteInfo.PropertyStandardValues.NotFoundPageItemPath;
        }
    }
}