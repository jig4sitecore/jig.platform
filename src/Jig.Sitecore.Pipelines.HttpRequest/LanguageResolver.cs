﻿namespace Jig.Sitecore.Pipelines.HttpRequest
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Web;
    using Jig.Sitecore.Context;
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Globalization;
    using global::Sitecore.Pipelines.HttpRequest;
    using global::Sitecore.Sites;
    using global::Sitecore.Web;

    /// <summary>
    /// Improved  version of LanguageResolver with Browser Agent Accept languages detection.
    /// </summary>
    /// <remarks>Based on <![CDATA[https://github.com/sitecorerick/constellation.sitecore/blob/master/Pipelines/HttpRequest/HttpRequestProcessor.cs]]></remarks>
    public class LanguageResolver : global::Sitecore.Pipelines.HttpRequest.LanguageResolver, IDatabasesToIgnore
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageResolver"/> class.
        /// </summary>
        // ReSharper disable NotNullMemberIsNotInitialized
        public LanguageResolver()
        // ReSharper restore NotNullMemberIsNotInitialized
        {
            this.FallBackLanguage = "en";
        }

        // ReSharper disable NotNullMemberIsNotInitialized

        /// <summary>
        /// Gets or sets the fallback language.
        /// </summary>
        /// <value>The fallback language.</value>
        [NotNull]
        public string FallBackLanguage { get; set; }

        /// <summary>
        /// Gets or sets a comma-delimited list of Database names that should cause the Processor to ignore the request.
        /// </summary>
        /// <value>The databases to ignore.</value>
        public string DatabasesToIgnore { get; set; }

        /// <summary>
        /// Gets or sets the site.
        /// </summary>
        /// <value>The site.</value>
        [NotNull]
        protected SiteContext ContextSite { get; set; }

        /// <summary>
        /// Gets or sets the database.
        /// </summary>
        /// <value>The database.</value>
        [NotNull]
        protected Database ContextDatabase { get; set; }

        // ReSharper restore NotNullMemberIsNotInitialized

        /// <summary>
        /// Required by the HttpRequestProcessor contract. Called by Sitecore from the pipeline.
        /// </summary>
        /// <remarks>
        /// Language detection and setting is done from "most explicit" to "most implicit"
        /// Here is the order of determination:
        /// - Querystring (Explicit - typically a change request)
        /// - URL (Explicit - typically offered by the target content)
        /// - Sitecore Cookie (Explicit - typically set for the session)
        /// - Browser (Implicit - typically first visit)
        /// If no match is found from the above, the user is shunted to the site picker page.
        /// </remarks>
        /// <param name="args">
        /// The arguments for this request.
        /// </param>
        public override void Process([NotNull] HttpRequestArgs args)
        {
            this.ContextSite = Context.Site;
            this.ContextDatabase = Context.Database;

            if (this.Validate())
            {
                /* Check and re-check to avoid null exception as it might bring system down */
                var request = args.Context.Request;

                var language = this.GetQuerystringLanguage(request);
                string explicitLanguage = this.GetSupportableLanguage(language);
                if (!string.IsNullOrWhiteSpace(explicitLanguage))
                {
                    this.SetLanguage(explicitLanguage);
                    return;
                }

                var filePath = this.GetFilePathLanguage();
                explicitLanguage = this.GetSupportableLanguage(filePath);
                if (!string.IsNullOrWhiteSpace(explicitLanguage))
                {
                    this.SetLanguage(explicitLanguage);
                    return;
                }

                var cookieLanguage = this.GetCookieLanguage(request);
                explicitLanguage = this.GetSupportableLanguage(cookieLanguage);
                if (!string.IsNullOrWhiteSpace(explicitLanguage))
                {
                    this.SetLanguage(explicitLanguage);
                    return;
                }

                explicitLanguage = this.GetSupportableBrowserLanguage(request);
                if (!string.IsNullOrWhiteSpace(explicitLanguage))
                {
                    this.SetLanguage(explicitLanguage);
                    return;
                }
            }

            /* Execute Sitecore Default Language Resolver */
            base.Process(args);
        }

        /// <summary>
        /// Reduces the browser language string to the ISO Code.
        /// </summary>
        /// <param name="language">
        /// The string to parse.
        /// </param>
        /// <returns>
        /// The ISO code.
        /// </returns>
        [NotNull]
        protected static string TruncateBrowserLanguage([NotNull] string language)
        {
            if (language.IndexOf(';') > -1)
            {
                return language.Substring(0, language.IndexOf(';'));
            }

            return language;
        }

        /// <summary>
        /// Validates this instance.
        /// </summary>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        protected virtual bool Validate()
        {
            if (!global::Sitecore.Context.PageMode.IsNormal)
            {
                return false;
            }

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (this.ContextSite == null || string.IsNullOrWhiteSpace(this.ContextSite.HostName))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return false;
            }

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (this.ContextDatabase == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return false;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            if (this.IsMatchDatabasesToIgnore(this.ContextDatabase))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Retrieves the language code from a browser cookie.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The language code or null.
        /// </returns>
        [NotNull]
        protected virtual string GetCookieLanguage([NotNull] HttpRequest request)
        {
            // ReSharper disable PossibleNullReferenceException
            var key = this.ContextSite.GetCookieKey("lang");
            // ReSharper restore PossibleNullReferenceException
            var cookie = request.Cookies[key];

            if (cookie == null || string.IsNullOrWhiteSpace(cookie.Value))
            {
                return string.Empty;
            }

            return cookie.Value;
        }

        /// <summary>
        /// Retrieves the language code from the folder structure of the URL.
        /// </summary>
        /// <returns>The language code or null.</returns>
        [NotNull]
        protected virtual string GetFilePathLanguage()
        {
            if (Context.Data.FilePathLanguage == null)
            {
                return string.Empty;
            }

            return Context.Data.FilePathLanguage.Name;
        }

        /// <summary>
        /// Retrieves the language code from the "sc_lang" querystring parameter.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The language code or null.
        /// </returns>
        [NotNull]
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
        protected virtual string GetQuerystringLanguage([NotNull] HttpRequest request)
        {
            var lang = request.QueryString["sc_lang"];

            if (string.IsNullOrWhiteSpace(lang))
            {
                lang = request.QueryString["_lang"];
            }

            return lang ?? string.Empty;
        }

        /// <summary>
        /// Returns the first supportable ISO code based upon the browser's accepted language
        /// and the languages supported by the current site.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// A best fit ISO Code or null.
        /// </returns>
        [NotNull]
        protected virtual string GetSupportableBrowserLanguage([NotNull] HttpRequest request)
        {
            var languages = request.ServerVariables["HTTP_ACCEPT_LANGUAGE"];

            if (string.IsNullOrWhiteSpace(languages))
            {
                return this.ContextSite.Language;
            }

            var browserLanguages = StringUtil.Split(languages, ',', true);

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var browserCode in browserLanguages)
            {
                // ReSharper restore LoopCanBeConvertedToQuery
                var language = this.GetSupportableLanguage(TruncateBrowserLanguage(browserCode));
                if (!string.IsNullOrWhiteSpace(language))
                {
                    return language;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Returns a best-fit language code based on the supplied language code.
        /// </summary>
        /// <param name="langCode">
        /// The supplied code from browser, cookie, or querystring.
        /// </param>
        /// <returns>
        /// The best fit given the site's supported languages.
        /// </returns>
        [NotNull]
        protected virtual string GetSupportableLanguage([NotNull] string langCode)
        {
            if (string.IsNullOrWhiteSpace(langCode))
            {
                return string.Empty;
            }

            var siteLanguages = this.GetSupportedSiteLanguages();

            // Exact match
            if (siteLanguages.Contains(langCode))
            {
                return langCode; // matches Sitecore code.
            }

            // Match against languages configured without dialect
            foreach (var siteCode in siteLanguages)
            {
                if (siteCode.Length == 2 && langCode.StartsWith(siteCode))
                {
                    return siteCode; // use Sitecore code.
                }
            }

            // Match against a dialect when we ignore the browser dialect
            if (langCode.Length >= 2)
            {
                langCode = langCode.Substring(0, 2);

                // ReSharper disable LoopCanBeConvertedToQuery
                foreach (var siteCode in siteLanguages)
                {
                    // ReSharper restore LoopCanBeConvertedToQuery
                    if (siteCode.StartsWith(langCode))
                    {
                        return siteCode; // use Sitecore code.
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Retrieves the languages that are explicitly supported by the context site.
        /// </summary>
        /// <returns>An array of ISO Codes.</returns>
        [NotNull]
        protected virtual IList<string> GetSupportedSiteLanguages()
        {
            var languages = this.ContextSite.GetSupportedLanguages(this.ContextDatabase);

            if (languages.Count == 0)
            {
                return new[] { this.ContextSite.Language };
            }

            return languages;
        }

        /// <summary>
        /// Ensures that all language states are configured appropriately for this request, and the next if possible.
        /// </summary>
        /// <param name="isoCode">
        /// The language to use.
        /// </param>
        protected virtual void SetLanguage([NotNull] string isoCode)
        {
            Language targetLanguage = this.ResolveLanguageFromIsoCode(isoCode);
            if (targetLanguage != null)
            {
                this.SetCookieLanguage(targetLanguage);
                this.SetContextLanguage(targetLanguage);
            }
        }

        /// <summary>
        /// Sets the Sitecore language cookie for the session and our cookie for the next sessions.
        /// </summary>
        /// <param name="language">
        /// The language to use for subsequent requests.
        /// </param>
        protected virtual void SetCookieLanguage([NotNull] Language language)
        {
            var cookieName = this.ContextSite.GetCookieKey("lang");
            WebUtil.SetCookieValue(cookieName, language.Name, DateTime.MaxValue);
        }

        /// <summary>
        /// Sets the sitecore Context language for the request.
        /// </summary>
        /// <param name="language">
        /// The language to use for this request.
        /// </param>
        protected virtual void SetContextLanguage([NotNull] Language language)
        {
            Context.Language = language;
        }

        /// <summary>
        /// Uses an  ISO Code to obtain a Language instance from the Sitecore API.
        /// If no matching Language is found, the Site's default language is used.
        /// </summary>
        /// <param name="isoCode">
        /// The code to parse.
        /// </param>
        /// <returns>
        /// An instance of Sitecore.Globalization.Language.
        /// </returns>
        [CanBeNull]
        protected virtual Language ResolveLanguageFromIsoCode([NotNull] string isoCode)
        {
            Language language =
                this.ContextDatabase.Languages.FirstOrDefault(
                    systemLanguage => systemLanguage.Name.Equals(isoCode, StringComparison.InvariantCultureIgnoreCase));

            if (language == null)
            {
                if (Language.TryParse(this.ContextSite.Language, out language))
                {
                    return Language.Parse(this.FallBackLanguage);
                }
            }

            return language;
        }
    }
}
