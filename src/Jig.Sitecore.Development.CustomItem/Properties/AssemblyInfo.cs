﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: Guid("891a9971-654a-4db5-8728-7efda9261d79")]

[assembly: AssemblyTitle("Jig.Sitecore.Development")]

[assembly: AssemblyCompany("Jig4Sitecore Team")]
[assembly: AssemblyProduct("Jig4Sitecore Framework")]
[assembly: AssemblyCopyright("Copyright © Jig4Sitecore Team")]
[assembly: AssemblyTrademark("Jig4Sitecore Team")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyVersion("2.0.0")]
[assembly: AssemblyFileVersion("2.0.0")]
