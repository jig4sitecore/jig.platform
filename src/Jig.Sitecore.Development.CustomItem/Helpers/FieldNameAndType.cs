﻿namespace Jig.Sitecore.Development.CustomItems
{
    using System;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    /// <summary>
    /// Class FieldNameAndType.
    /// </summary>
    public class FieldNameAndType
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FieldNameAndType"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="type">The type.</param>
        public FieldNameAndType([NotNull] string name, [NotNull] Type type)
        {
            Assert.IsNotNullOrEmpty(name, "name");
            Assert.IsNotNull(type, "type");
            this.Name = name;
            this.Type = type;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        [NotNull]
        public string Name { get; private set; }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>The type.</value>
        [NotNull]
        public Type Type { get; private set; }
    }
}