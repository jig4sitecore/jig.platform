﻿namespace Jig.Sitecore.Development.CustomItems
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Web;
    using System.Xml;

    using global::Sitecore;

    using global::Sitecore.Configuration;

    using global::Sitecore.Data;

    using global::Sitecore.Diagnostics;

    /// <summary>
    /// The Custom Item class code generator handler.
    /// </summary>
    public sealed class CustomItemClassCodeGeneratorHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether is reusable.
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// The process request.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Buffer = true;
            context.Response.BufferOutput = true;
            context.Response.Clear();
            try
            {
                if (!context.Request.IsLocal)
                {
                    var ex = new HttpException("The service available for local request only for security reasons!");
                    Trace.TraceError(ex.ToString());
                    throw ex;
                }

                if (context.Request.QueryString.Count == 0)
                {
                    var url = context.Request.Url.AbsolutePath.ToLowerInvariant();
                    var ex = new HttpException(@"The expected request formats: 
                                                                    " + url + @"?id={Required:Guid}&namespace={Required:C# Namespace}
                                                                    " + url + @"?id={Required:Guid}&namespace={Required:C# Namespace}&className={Recommended/Optional}&baseClassName={Recommended/Optional}&databaseName={Optional}
                                            ");

                    Trace.TraceError(ex.ToString());
                    throw ex;
                }

                var itemId = context.Request.QueryString["id"];
                if (string.IsNullOrWhiteSpace(itemId))
                {
                    var ex = new HttpException("The item ID is not provided !");

                    Trace.TraceError(ex.ToString());
                    throw ex;
                }

                Guid guid;
                if (!Guid.TryParse(itemId.Trim(' ', '{', '}'), out guid))
                {
                    var ex = new HttpException("The item ID is not valid GUID !");

                    Trace.TraceError(ex.ToString());
                    throw ex;
                }

                var databaseName = context.Request.QueryString["databaseName"];
                if (string.IsNullOrWhiteSpace(databaseName))
                {
                    databaseName = "master";
                }

                var database = Factory.GetDatabase(databaseName.Trim());
                if (database == null)
                {
                    var ex = new HttpException("The database with name " + databaseName + " not found!");

                    Trace.TraceError(ex.ToString());
                    throw ex;
                }

                var item = database.GetItem(new ID(guid));
                if (item == null)
                {
                    var ex = new HttpException("The Item with ID " + guid + " not found!");

                    Trace.TraceError(ex.ToString());
                    throw ex;
                }

                var ns = context.Request.QueryString["namespace"];
                if (string.IsNullOrWhiteSpace(ns))
                {
                    var ex = new HttpException("The namespace parameter is required to generate code!");

                    Trace.TraceError(ex.ToString());
                    throw ex;
                }

                var model = new CustomItemCodeGenerationModel(item, ns.Trim(), context.Request.Url);

                var className = context.Request.QueryString["className"];
                if (!string.IsNullOrWhiteSpace(ns.Trim()))
                {
                    model.ClassName = className;
                }

                /* Recommended to pass by query string */
                var baseClassName = context.Request.QueryString["baseClassName"];
                if (!string.IsNullOrWhiteSpace(baseClassName))
                {
                    model.BaseClassName = baseClassName.Trim();
                }

                foreach (var fieldType in this.GetSitecoreFieldMappingToNetTypes())
                {
                    model.SitecoreFieldMappingToNetTypes[fieldType.Name] = fieldType.Type;
                }

                var template = new CustomItemCodeGenerationView
                                   {
                                       Model = model
                                   };

                var code = template.Render();

                context.Response.Write(code);
            }
            finally
            {
                context.ApplicationInstance.CompleteRequest();
            }
        }

        /// <summary>
        /// Gets the field type mapping.
        /// </summary>
        /// <returns>Collection field Sitecore types mapped to the .NET</returns>
        [NotNull]
        private IEnumerable<FieldNameAndType> GetSitecoreFieldMappingToNetTypes()
        {
            var fieldTypeNode = Factory.GetConfigNode("/sitecore/fieldTypes");
            foreach (XmlNode fieldType in fieldTypeNode.ChildNodes)
            {
                /* <fieldType name="Checkbox" type="Sitecore.Data.Fields.CheckboxField, Sitecore.Kernel" /> */
                if ("fieldType".Equals(fieldType.Name, StringComparison.InvariantCultureIgnoreCase) && fieldType.Attributes != null && fieldType.Attributes.Count > 1)
                {
                    var nameAttr = fieldType.Attributes["name"];
                    Assert.IsNotNull(nameAttr, "nameAttr != null");
                    Assert.IsNotNullOrEmpty(nameAttr.Value, "value!= null");

                    var typeAttr = fieldType.Attributes["type"];
                    Assert.IsNotNull(typeAttr, "typeAttr != null");
                    Assert.IsNotNullOrEmpty(typeAttr.Value, "value!= null");

                    var item = new FieldNameAndType(
                        nameAttr.Value.ToLowerInvariant(),
                        Type.GetType(typeName: typeAttr.Value, throwOnError: false, ignoreCase: true));

                    yield return item;
                }
            }
        }
    }
}