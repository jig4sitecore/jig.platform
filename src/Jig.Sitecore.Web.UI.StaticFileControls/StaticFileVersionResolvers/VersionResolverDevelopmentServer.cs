﻿namespace Jig.Sitecore.Web.UI.StaticFileControls.VersionResolvers
{
    using System;
    using Jig.Sitecore.Pipelines;

    /// <summary>
    /// The static file version resolver.
    /// </summary>
    public sealed class VersionResolverDevelopmentServer : IStaticFileVersionResolver
    {
        /// <summary>
        /// Resolves the static file version.
        /// </summary>
        /// <param name="args">The StaticFileVersionResolverPipelineArgs instance containing the event data.</param>
        public void Process(StaticFileVersionResolverPipelineArgs args)
        {
            if (!string.IsNullOrWhiteSpace(args.StaticFileVersion))
            {
                return;
            }

            var version = DateTime.Now.ToString("yyyyMMddHH");
            args.StaticFileVersion = version;
            args.HttpContext.Items[Constants.StaticFileVersion] = version;

            args.AbortPipeline();
        }
    }
}
