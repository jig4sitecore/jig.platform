﻿namespace Jig.Sitecore.Web.UI.StaticFileControls.VersionResolvers
{
    using Jig.Sitecore.Pipelines;

    using global::Sitecore;

    /// <summary>
    /// The static file version resolver.
    /// </summary>
    public sealed class VersionResolverHttpContextItem : IStaticFileVersionResolver
    {
        /// <summary>
        /// Resolves the static file version.
        /// </summary>
        /// <param name="args">The StaticFileVersionResolverPipelineArgs instance containing the event data.</param>
        public void Process([NotNull]StaticFileVersionResolverPipelineArgs args)
        {
            if (!string.IsNullOrWhiteSpace(args.StaticFileVersion))
            {
                return;
            }

            /* This is per request cache */
            var staticFileVersion = args.HttpContext.Items[Jig.Sitecore.Web.UI.Constants.StaticFileVersion] as string;
            if (!string.IsNullOrWhiteSpace(staticFileVersion))
            {
                args.StaticFileVersion = staticFileVersion;
                args.AbortPipeline();
            }
        }
    }
}
