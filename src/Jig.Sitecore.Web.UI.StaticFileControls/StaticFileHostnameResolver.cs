﻿namespace Jig.Sitecore.Web.UI
{
    using System.Text;
    using System.Web;

    using Jig.Sitecore.Pipelines;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Pipelines;

    /// <summary>
    /// Class StaticFileHostnameResolver.
    /// </summary>
    public static class StaticFileHostnameResolver
    {
        /// <summary>
        /// Runs the specified pipeline arguments.
        /// </summary>
        /// <param name="pipelineArgs">The pipeline arguments.</param>
        /// <returns>Static file Hostname</returns>
        /// <exception cref="System.Web.HttpException">Could not get pipeline:  + pipelineName</exception>
        [NotNull]
        public static string Run([NotNull] this StaticFileHostNameResolverPipelineArgs pipelineArgs)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (pipelineArgs != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var pipelineName = typeof(StaticFileHostnameResolver).FullName;

                CorePipeline pipeline = CorePipelineFactory.GetPipeline(pipelineName, string.Empty);
                if (pipeline == null)
                {
                    var msg = new StringBuilder(128);
                    msg.Append("Could not get pipeline: ").AppendLine(pipelineName);
                    msg.AppendLine("Missing config file: /App_Config/Include/_web/Jig.Sitecore.Web.UI.StaticFileControls.config");

                    throw new HttpException(msg.ToString());
                }

                pipeline.Run(pipelineArgs);

                var errors = pipelineArgs.GetMessages(PipelineMessageFilter.Errors);
                foreach (var error in errors)
                {
                    Log.Error(error.Text, typeof(StaticFileHostNameResolverPipelineArgs));
                }

                return pipelineArgs.StaticFileHostname ?? string.Empty;
            }

            return string.Empty;
        }
    }
}
