﻿namespace Jig.Sitecore.Web.UI
{
    using System.Text;
    using System.Web;

    using Jig.Sitecore.Configuration;
    using Jig.Sitecore.Pipelines;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Pipelines;

    /// <summary>
    /// Class StaticFileVersionResolver.
    /// </summary>
    public static class StaticFileVersionResolver
    {
        /// <summary>
        /// The is enabled
        /// </summary>
        private static readonly bool IsEnabled = ConfigurationSettingManager.RetrieveSettingAsBoolean("Jig.Sitecore.Web.UI.StaticFileVersionResolver.IsEnabled", true);

        /// <summary>
        /// Runs the specified pipeline arguments.
        /// </summary>
        /// <param name="pipelineArgs">The pipeline arguments.</param>
        /// <returns>Static file version</returns>
        [NotNull]
        public static string Run([NotNull] this StaticFileVersionResolverPipelineArgs pipelineArgs)
        {
            if (!IsEnabled)
            {
                return string.Empty;
            }

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (pipelineArgs != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var pipelineName = typeof(StaticFileVersionResolver).FullName;
                CorePipeline pipeline = CorePipelineFactory.GetPipeline(pipelineName, string.Empty);
                if (pipeline == null)
                {
                    var msg = new StringBuilder(128);
                    msg.Append("Could not get pipeline: ").AppendLine(pipelineName);
                    msg.AppendLine("Missing config file: /App_Config/Include/_web/Jig.Sitecore.Web.UI.StaticFileControls.config");

                    throw new HttpException(msg.ToString());
                }

                pipeline.Run(pipelineArgs);

                var errors = pipelineArgs.GetMessages(PipelineMessageFilter.Errors);
                foreach (var error in errors)
                {
                    Log.Error(error.Text, pipelineArgs.GetType());
                }

                return pipelineArgs.StaticFileVersion ?? string.Empty;
            }

            return string.Empty;
        }
    }
}
