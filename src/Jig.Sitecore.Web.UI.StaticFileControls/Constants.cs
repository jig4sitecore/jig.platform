namespace Jig.Sitecore.Web.UI
{
    /// <summary>
    /// The constants.
    /// </summary>
    public static class Constants
    {
        #region Constants

        /// <summary>
        /// The default query string, header, cookie name and HttpContextItem
        /// </summary>
        public const string StaticFileHostName = "staticFileHostName";

        /// <summary>
        /// The static file version (HttpContextItem)
        /// </summary>
        public const string StaticFileVersion = "staticFileVersion";

        /// <summary>
        /// The query string static file version
        /// </summary>
        public const string QueryStringStaticFileVersion = "version";

        #endregion
    }
}