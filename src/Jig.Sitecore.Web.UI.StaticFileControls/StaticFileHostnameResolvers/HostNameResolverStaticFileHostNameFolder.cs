﻿namespace Jig.Sitecore.Web.UI.StaticFileControls.HostnameResolvers
{
    using System;
    using System.Linq;

    using Jig.Sitecore.Pipelines;
    using Jig.Sitecore.Sites.Shared.Data.Folders;
    using global::Sitecore.Data.Items;

    /// <summary>
    /// The host name resolver.
    /// </summary>
    public sealed class HostNameResolverStaticFileHostNameFolder : IHostNameResolver
    {
        /// <summary>
        /// Resolves the host name mapping.
        /// </summary>
        /// <param name="args">The StaticFileHostnameResolverPipelineArgs instance containing the event data.</param>
        /// <remarks>Intented for development purposes only</remarks>
        public void Process(StaticFileHostNameResolverPipelineArgs args)
        {
            if (!string.IsNullOrWhiteSpace(args.StaticFileHostname) || args.SiteInfo == null || args.Database == null || args.HttpContext.Request.Url == null)
            {
                return;
            }

            /* Get Sitecore Folder Item */
            var folderPath = args.SiteInfo.GetStaticFilesHostNameFolderPath();
            Item item = args.Database.GetItem(folderPath);
            if (item == null)
            {
                return;
            }

            var folder = item.As<StaticFilesHostnameFolder>();
            if (folder == null)
            {
                return;
            }

            var records = folder.GetDescendants<Jig.Sitecore.Sites.Shared.Data.System.StaticFileHostName>();
            var record = records.FirstOrDefault(
                x =>
                string.Equals(
                    x.Hostname,
                    args.HttpContext.Request.Url.Host,
                    StringComparison.InvariantCultureIgnoreCase));

            if (record != null && !string.IsNullOrWhiteSpace(record.StaticFileHostNameWithOptionalPath.Value))
            {
                args.StaticFileHostname = record.StaticFileHostNameWithOptionalPath.Value;
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(folder.StaticFileHostNameWithOptionalPath))
                {
                    args.StaticFileHostname = folder.StaticFileHostNameWithOptionalPath;
                }
            }
        }
    }
}
