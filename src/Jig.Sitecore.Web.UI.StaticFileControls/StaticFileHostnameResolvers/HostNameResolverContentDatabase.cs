﻿namespace Jig.Sitecore.Web.UI.StaticFileControls.HostnameResolvers
{
    using System;

    using Jig.Sitecore.Pipelines;

    /// <summary>
    /// The host name resolver.
    /// </summary>
    public sealed class HostNameResolverContentDatabase : IHostNameResolver
    {
        /// <summary>
        /// Resolves the host name mapping.
        /// </summary>
        /// <param name="args">The IStaticFileHostnameResolverPipelineArgs instance containing the event data.</param>
        public void Process(StaticFileHostNameResolverPipelineArgs args)
        {
            if (!string.IsNullOrWhiteSpace(args.StaticFileHostname) || args.Database != null)
            {
                return;
            }

            var database = global::Sitecore.Context.Database;
            if (database != null && !string.Equals(database.Name, "core", StringComparison.InvariantCultureIgnoreCase))
            {
                args.Database = database;
            }
        }
    }
}
