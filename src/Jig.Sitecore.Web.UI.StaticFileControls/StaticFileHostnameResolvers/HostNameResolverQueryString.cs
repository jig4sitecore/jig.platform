﻿namespace Jig.Sitecore.Web.UI.StaticFileControls.HostnameResolvers
{
    using Jig.Sitecore.Configuration;
    using Jig.Sitecore.Pipelines;

    /// <summary>
    /// The host name resolver.
    /// </summary>
    public sealed class HostNameResolverQueryString : IHostNameResolver
    {
        /// <summary>
        /// The is enabled
        /// </summary>
        private static readonly bool IsEnabled = ConfigurationSettingManager.RetrieveSettingAsBoolean("Jig.Sitecore.Web.UI.StaticFileControls.HostnameResolvers.HostNameResolverQueryString.IsEnabled", true);
        
        /// <summary>
        /// Resolves the host name mapping.
        /// </summary>
        /// <param name="args">The StaticFileHostnameResolverPipelineArgs instance containing the event data.</param>
        /// <remarks>Intented for development purposes only</remarks>
        public void Process(StaticFileHostNameResolverPipelineArgs args)
        {
            if (IsEnabled)
            {
                if (!string.IsNullOrWhiteSpace(args.StaticFileHostname))
                {
                    return;
                }

                var staticFileHostname = args.HttpContext.Request.QueryString[Constants.StaticFileHostName];
                if (!string.IsNullOrWhiteSpace(staticFileHostname))
                {
                    args.StaticFileHostname = staticFileHostname;
                }
            }
        }
    }
}
