﻿namespace Jig.Sitecore.Web.UI.StaticFileControls.HostnameResolvers
{
    using Jig.Sitecore.Pipelines;

    /// <summary>
    /// The host name resolver.
    /// </summary>
    public sealed class HostNameResolverHttpContextItem : IHostNameResolver
    {
        /// <summary>
        /// Resolves the host name mapping.
        /// </summary>
        /// <param name="args">The StaticFileHostnameResolverPipelineArgs instance containing the event data.</param>
        /// <remarks>Intented as micro cache</remarks>
        public void Process(StaticFileHostNameResolverPipelineArgs args)
        {
            if (!string.IsNullOrWhiteSpace(args.StaticFileHostname))
            {
                return;
            }

            /* This is per request cache */
            var staticFileHostname = args.HttpContext.Items[Constants.StaticFileHostName] as string;
            if (!string.IsNullOrWhiteSpace(staticFileHostname))
            {
                args.StaticFileHostname = staticFileHostname;
            }
        }
    }
}
