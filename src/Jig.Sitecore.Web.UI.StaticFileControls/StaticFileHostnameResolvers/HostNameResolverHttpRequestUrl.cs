﻿namespace Jig.Sitecore.Web.UI.StaticFileControls.HostnameResolvers
{
    using Jig.Sitecore.Pipelines;

    /// <summary>
    /// The host name resolver.
    /// </summary>
    public sealed class HostNameResolverHttpRequestUrl : IHostNameResolver
    {
        /// <summary>
        /// Resolves the host name mapping.
        /// </summary>
        /// <param name="args">The StaticFileHostnameResolverPipelineArgs instance containing the event data.</param>
        public void Process(StaticFileHostNameResolverPipelineArgs args)
        {
            if (!string.IsNullOrWhiteSpace(args.StaticFileHostname))
            {
                return;
            }

            if (args.HttpContext.Request.Url != null)
            {
                args.StaticFileHostname = args.HttpContext.Request.Url.Host;
            }
        }
    }
}
