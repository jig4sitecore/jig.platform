﻿namespace Jig.Sitecore.Web.UI.StaticFileControls.HostnameResolvers
{
    using Jig.Sitecore.Configuration;
    using Jig.Sitecore.Pipelines;

    /// <summary>
    /// The host name resolver.
    /// </summary>
    public sealed class HostNameResolverHeader : IHostNameResolver
    {
        /// <summary>
        /// The is enabled
        /// </summary>
        private static readonly bool IsEnabled = ConfigurationSettingManager.RetrieveSettingAsBoolean("Jig.Sitecore.Web.UI.StaticFileControls.HostnameResolvers.HostNameResolverHeader.IsEnabled", true);

        /// <summary>
        /// Resolves the host name mapping.
        /// </summary>
        /// <param name="args">The IStaticFileHostnameResolverPipelineArgs instance containing the event data.</param>
        public void Process(StaticFileHostNameResolverPipelineArgs args)
        {
            if (IsEnabled)
            {
                if (!string.IsNullOrWhiteSpace(args.StaticFileHostname))
                {
                    return;
                }

                var staticFileHostname = args.HttpContext.Request.Headers[Constants.StaticFileHostName];
                if (!string.IsNullOrWhiteSpace(staticFileHostname))
                {
                    args.StaticFileHostname = staticFileHostname;
                }
            }
        }
    }
}
