﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: Guid("fa82a414-3baa-41f4-8f46-a62fc3d43696")]
[assembly: AssemblyTitle("Jig4Sitecore Framework")]
[assembly: AssemblyDescription("The library extending the Sitecore CMS.")]

[assembly: AssemblyCompany("Jig4Sitecore Team")]
[assembly: AssemblyProduct("Jig4Sitecore Framework")]
[assembly: AssemblyCopyright("Copyright © Jig4Sitecore Team")]
[assembly: AssemblyTrademark("Jig4Sitecore Team")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyVersion("2.0.0")]
[assembly: AssemblyFileVersion("2.0.0")]