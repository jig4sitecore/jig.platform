﻿namespace Jig.Sitecore.Web.UI.StaticFileControls.UriBuilders
{
    using System;

    using Jig.Sitecore.Pipelines;

    /// <summary>
    /// Class UriResolverExternalFile. This class cannot be inherited.
    /// </summary>
    public sealed class UriResolverExternalFile : IStaticFileUriResolver
    {
        /// <summary>
        /// Resolves the static file uri.
        /// </summary>
        /// <param name="args">The <see cref="StaticFileUriResolverPipelineArgs" /> instance containing the event data.</param>
        public void Process(StaticFileUriResolverPipelineArgs args)
        {
            if (args.Uri == null)
            {
                var filePath = args.StaticFileSource;

                /*
                * Skip handling of the external files paths like https://twitter.com/js/main.release.js
                */
                if (filePath.StartsWith("//"))
                {
                    args.Uri = args.StaticFileSource;

                    args.AbortPipeline();
                }
                else if (filePath.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
                {
                    var index = filePath.IndexOf("//", StringComparison.InvariantCultureIgnoreCase);
                    args.Uri = args.StaticFileSource.Substring(index);

                    args.AbortPipeline();
                }
            }
        }
    }
}
