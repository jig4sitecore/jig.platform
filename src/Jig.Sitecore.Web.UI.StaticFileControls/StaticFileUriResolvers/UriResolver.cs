﻿namespace Jig.Sitecore.Web.UI.StaticFileControls.UriBuilders
{
    using System.Text;
    using System.Web;

    using Jig.Sitecore.Pipelines;
    using Jig.Web;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;

    /// <summary>
    /// Class UriResolver. This class cannot be inherited.
    /// </summary>
    public sealed class UriResolver : IStaticFileUriResolver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UriResolver"/> class.
        /// </summary>
        public UriResolver()
        {
            this.VersionQueryStringKey = Jig.Sitecore.Web.UI.Constants.QueryStringStaticFileVersion;
        }

        /// <summary>
        /// Gets or sets the version query string key.
        /// </summary>
        /// <value>The version query string key.</value>
        [NotNull]
        public string VersionQueryStringKey { get; set; }

        /// <summary>
        /// Resolves the static file uri.
        /// </summary>
        /// <param name="args">The <see cref="StaticFileUriResolverPipelineArgs" /> instance containing the event data.</param>
        public void Process(StaticFileUriResolverPipelineArgs args)
        {
            if (!string.IsNullOrWhiteSpace(args.Uri))
            {
                return;
            }

            Assert.IsNotNull(args.StaticFileHostname, "The Static File Hostname");
            Assert.IsNotNull(args.StaticFileSource, "The Static File Source");

            var uriBuilder = new StringBuilder(128);

            uriBuilder.Append(string.IsNullOrWhiteSpace(args.UrlScheme) ? "//" : args.UrlScheme);

            uriBuilder.Append(args.StaticFileHostname.Trim(' ', '/').EnsureEndsWith("/"));

            uriBuilder.Append(args.StaticFileSource.Trim(' ', '/'));

            if (!string.IsNullOrWhiteSpace(args.StaticFileVersion))
            {
                uriBuilder.Append(args.StaticFileSource.IndexOf('?') > 1 ? '&' : '?');
                uriBuilder.Append(this.VersionQueryStringKey).Append('=').Append(HttpUtility.UrlEncode(args.StaticFileVersion));
            }

            args.Uri = uriBuilder.ToString();
            args.AbortPipeline();
        }
    }
}
