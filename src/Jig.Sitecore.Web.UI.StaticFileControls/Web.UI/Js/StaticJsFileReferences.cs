﻿namespace Jig.Sitecore.Web.UI
{
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Web.UI;

    /// <summary>
    /// The static javascript file references.
    /// </summary>
    [ToolboxData("<{0}:StaticJsFileReferences runat=server></{0}:StaticJsFileReferences>")]
    [Guid("375151CE-EE21-4C40-A4A0-7E3137FE4FFE")]
    [DefaultProperty("Src")]
    public class StaticJsFileReferences : StaticFilesViewBase<StaticJsFileReference>
    {
    }
}
