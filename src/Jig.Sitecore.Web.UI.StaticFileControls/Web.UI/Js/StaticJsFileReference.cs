﻿namespace Jig.Sitecore.Web.UI
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Xml.Linq;

    using Jig.Sitecore.Pipelines;

    using global::Sitecore;

    /// <summary>
    /// The js file.
    /// </summary>
    [ParseChildren(typeof(StaticJsFileReference))]
    [DefaultProperty("Src")]
    public class StaticJsFileReference : StaticFile
    {
        #region Static Fields

        /// <summary>
        ///     The source attribute name
        /// </summary>
        private static readonly XName SrcAttributeName = XName.Get("src");

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the StaticJsFileReference class.
        /// </summary>
        public StaticJsFileReference()
            : base(new XElement("script", string.Empty, new XAttribute(TypeAttributeName, "text/javascript")))
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the source.
        /// </summary>
        /// <value>
        ///     The source.
        /// </value>
        [NotNull]
        [Bindable(false)]
        [Localizable(false)]
        public string Src
        {
            get
            {
                return this.GetAttribute(SrcAttributeName);
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    var src = value.Trim(' ', '~', '.');
                    if (!src.StartsWith("http") || !src.StartsWith("//"))
                    {
                        /* Account for cases like that:
                         * Correct
                         *      //fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic 
                         * Wrong (case sensitive font name)
                         *      //fonts.googleapis.com/css?family=lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic
                         */
                        src = src.ToLowerInvariant();
                    }
                    
                    this.SetAttribute(SrcAttributeName, src);
                }
            }
        }

        /// <summary>
        /// Gets or sets the conditional comments.
        /// </summary>
        /// <value>
        /// The conditional comments.
        /// </value>
        [CanBeNull]
        [Bindable(false)]
        [Localizable(false)]
        [UsedImplicitly]
        public string Conditional { get; set; }

        #endregion

        /// <summary>
        /// Outputs server control content to a provided System.Web.UI.HtmlTextWriter object and stores
        /// tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
        /// <param name="context">The context.</param>
        public override void RenderView(HtmlTextWriter writer, [NotNull] HttpContextBase context)
        {
            /* Clone and override the Src attribute */
            var html = new XElement(this.InnerHtml);
            var src = html.Attribute(SrcAttributeName);
            if (!string.IsNullOrWhiteSpace(src.Value))
            {
                var versionArgs = new StaticFileVersionResolverPipelineArgs(context, this.Src, this.StaticFileHostname);
                var version = versionArgs.Run();

                var args = new StaticFileUriResolverPipelineArgs(context, this.Src, this.StaticFileHostname)
                {
                    StaticFileVersion = version
                };

                var uri = args.Run();

                html.SetAttributeValue(SrcAttributeName, uri);
            }

            if (string.IsNullOrWhiteSpace(this.Conditional))
            {
                writer.WriteLine(html);
            }
            else
            {
                // ReSharper disable PossibleNullReferenceException
                writer.WriteLine("<!--[" + this.Conditional.Trim() + "]>");
                // ReSharper restore PossibleNullReferenceException
                writer.WriteLine(html);
                writer.WriteLine("<![endif]-->");
            }
        }
    }
}
