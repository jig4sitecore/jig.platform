﻿namespace Jig.Sitecore.Web.UI
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Xml.Linq;
    using Jig.Sitecore.Pipelines;
    using global::Sitecore;

    /// <summary>
    /// The Icon file.
    /// </summary>
    [ParseChildren(typeof(StaticIconFileReference))]
    [DefaultProperty("Href")]
    public class StaticIconFileReference : StaticFile
    {
        #region Static Fields

        /// <summary>
        /// The href attribute name
        /// </summary>
        private static readonly XName HrefAttributeName = XName.Get("href");

        /// <summary>
        /// The sizes attribute name
        /// </summary>
        private static readonly XName SizesAttributeName = XName.Get("sizes");

        /// <summary>
        /// The relative attribute name
        /// </summary>
        private static readonly XName RelAttributeName = XName.Get("rel");

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the StaticIconFileReference class.
        /// </summary>
        public StaticIconFileReference()
            : base(new XElement("link"))
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the source.
        /// </summary>
        /// <value>
        ///     The source.
        /// </value>
        [NotNull]
        [Bindable(false)]
        [Localizable(false)]
        public string Href
        {
            get
            {
                return this.GetAttribute(HrefAttributeName);
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    var src = value.Trim(' ', '~', '.').ToLowerInvariant();
                    this.SetAttribute(HrefAttributeName, src);
                }
            }
        }

        /// <summary>
        /// Gets or sets the relative attribute.
        /// </summary>
        /// <value>
        /// The relative attribute value.
        /// </value>
        [NotNull]
        [Bindable(false)]
        [Localizable(false)]
        public string Rel
        {
            get
            {
                return this.GetAttribute(RelAttributeName);
            }

            set
            {
                this.SetAttribute(RelAttributeName, value);
            }
        }

        /// <summary>
        /// Gets or sets the sizes attribute.
        /// </summary>
        /// <value>
        /// The sizes attribute value.
        /// </value>
        [NotNull]
        [Bindable(false)]
        [Localizable(false)]
        public string Sizes
        {
            get
            {
                return this.GetAttribute(SizesAttributeName);
            }

            set
            {
                this.SetAttribute(SizesAttributeName, value);
            }
        }

        #endregion

        /// <summary>
        /// Outputs server control content to a provided System.Web.UI.HtmlTextWriter object and stores
        /// tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
        /// <param name="context">The context.</param>
        public override void RenderView(HtmlTextWriter writer, [NotNull] HttpContextBase context)
        {
            /* Clone and override the Src attribute */
            var html = new XElement(this.InnerHtml);
            var href = html.Attribute(HrefAttributeName);

            if (!string.IsNullOrWhiteSpace(href.Value))
            {
                var args = new StaticFileUriResolverPipelineArgs(context, this.Href, this.StaticFileHostname);

                var uri = args.Run();

                html.SetAttributeValue(HrefAttributeName, uri);
            }

            writer.WriteLine(html);
        }
    }
}
