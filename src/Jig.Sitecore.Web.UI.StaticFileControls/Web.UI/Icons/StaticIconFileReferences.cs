﻿namespace Jig.Sitecore.Web.UI
{
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Web.UI;

    /// <summary>
    /// The static icon file references.
    /// </summary>
    [ToolboxData("<{0}:StaticCssFileReferences runat=server></{0}:StaticCssFileReferences>")]
    [Guid("95989767-5B73-4038-958D-99127BB5DA00")]
    [DefaultProperty("Href")]
    public class StaticIconFileReferences : StaticFilesViewBase<StaticIconFileReference>
    {
    }
}
