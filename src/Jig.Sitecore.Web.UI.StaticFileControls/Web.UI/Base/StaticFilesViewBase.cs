﻿namespace Jig.Sitecore.Web.UI
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.Adapters;
    using System.Xml.Linq;
    using Jig.Sitecore.Pipelines;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;

    /// <summary>
    /// The Jig static files control base.
    /// </summary>
    /// <typeparam name="TFile">The type of the file.</typeparam>
    [ParseChildren(true)]
    [PersistChildren(true)]
    public abstract partial class StaticFilesViewBase<TFile> : Control, INamingContainer, IRenderView where TFile : class, IStaticFile, new()
    {
        /// <summary>
        /// The files
        /// </summary>
        private List<TFile> files;

        #region Public Properties

        /// <summary>
        /// Gets the files.
        /// </summary>
        /// <value>
        /// The files.
        /// </value>
        [NotNull]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public List<TFile> Files
        {
            get
            {
                return this.files ?? (this.files = new List<TFile>());
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether View state is enabled in control.
        /// </summary>
        /// <remarks>
        ///     This property is unsupported in this control.
        /// </remarks>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new bool EnableViewState
        {
            get
            {
                return false;
            }

            protected set
            {
                base.EnableViewState = value;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether EnableTheming is enabled in control.
        /// </summary>
        /// <remarks>
        ///     This property is unsupported in this control.
        /// </remarks>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new bool EnableTheming
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets or sets the static file hostname.
        /// </summary>
        /// <value>
        /// The static file hostname.
        /// </value>
        [CanBeNull]
        public string StaticFileHostname { get; set; }

        #endregion

        // ReSharper disable UnusedMember.Local
        #region Public Methods and Operators

        /// <summary>
        /// Outputs server control content to a provided System.Web.UI.HtmlTextWriter object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="output">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
        public sealed override void RenderControl(HtmlTextWriter output)
        {
            var name = this.GetType().Name;

            output.WriteLine();
            output.WriteLineNoTabs("<!-- Begin: " + name + "-->");

            if (this.Visible && !this.DesignMode)
            {
                try
                {
                    var contextWrapper = new HttpContextWrapper(this.Context);

                    this.RenderView(output, contextWrapper);
                }
                catch (Exception exception)
                {
                    Log.Error(exception.ToString(), this);
                    Trace.TraceError(exception.ToString());
                    var error = this.GetExceptionMessage(exception);
                    output.WriteLine(error);
                }
            }

            output.WriteLineNoTabs("<!-- END: " + name + "-->");
        }

        /// <summary>
        /// Renders the content of the control.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="context">The context.</param>
        public virtual void RenderView([NotNull] HtmlTextWriter writer, [NotNull] HttpContextBase context)
        {
            var hostname = this.ResolveHostname(context);
            var siteInfo = this.ResolveSiteInfo(context);

            var displayMode = siteInfo.DisplayMode;

            foreach (TFile file in this.GetStaticFileCollection())
            {
                if (file.DisplayMode != displayMode)
                {
                    continue;
                }

                if (string.IsNullOrWhiteSpace(file.StaticFileHostname))
                {
                    file.StaticFileHostname = hostname;
                }

                file.SiteInfo = siteInfo;

                file.RenderView(writer, context);
            }
        }

        /// <summary>
        /// Applies the style properties defined in the page style sheet to the control.
        /// </summary>
        /// <param name="page">
        /// The System.Web.UI.Page containing the control. 
        /// </param>
        /// <exception cref="T:System.InvalidOperationException">
        /// The style sheet is already applied.
        /// </exception>
        public sealed override void ApplyStyleSheetSkin([NotNull] Page page)
        {
            // Don't do anything
        }

        /// <summary>
        ///   Binds a data source to the invoked server control and all its child controls.
        /// </summary>
        public sealed override void DataBind()
        {
            throw new InvalidOperationException("The Method is not supported!");
        }

        /// <summary>
        /// Searches the current naming container for a server control with the specified id parameter.
        /// </summary>
        /// <param name="id">
        /// The identifier for the control to be found. 
        /// </param>
        /// <returns>
        /// The specified control, or null if the specified control does not exist. 
        /// </returns>
        [CanBeNull]
        public sealed override Control FindControl(string id)
        {
            throw new InvalidOperationException("The Method is not supported!");
        }

        /// <summary>
        ///   Sets input focus to a control.
        /// </summary>
        public sealed override void Focus()
        {
            throw new InvalidOperationException("The Method is not supported!");
        }

        #endregion

        #region Methods

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load" /> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            /* Hide then Page Editor mode */
            this.Visible = !global::Sitecore.Context.PageMode.IsPageEditor;
        }

        /// <summary>
        /// Resolves the hostname.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>Host Name</returns>
        [CanBeNull]
        protected virtual string ResolveHostname([NotNull] HttpContextBase context)
        {
            var args = new StaticFileHostNameResolverPipelineArgs(context)
                           {
                               SiteInfo = this.ResolveSiteInfo(context)
                           };

            var hostname = args.Run();

            return hostname;
        }

        // ReSharper disable UnusedParameter.Global

        /// <summary>
        /// Resolves the site information.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>Sitecore SiteContext.</returns>
        [NotNull]
        protected virtual global::Sitecore.Sites.SiteContext ResolveSiteInfo([NotNull] HttpContextBase context)
        {
            return global::Sitecore.Context.Site;
        }

        // ReSharper restore UnusedParameter.Global

        /// <summary>
        /// Gets the static file collection.
        /// </summary>
        /// <returns>The static file Collection</returns>
        [NotNull]
        // ReSharper disable ReturnTypeCanBeEnumerable.Global
        protected virtual IList<TFile> GetStaticFileCollection()
        // ReSharper restore ReturnTypeCanBeEnumerable.Global
        {
            return this.Files;
        }

        /// <summary>
        /// Sends server control content to a provided System.Web.UI.HtmlTextWriter object, which writes the content to be rendered on the client.
        /// </summary>
        /// <param name="writer">The System.Web.UI.HtmlTextWriter object that receives the server control content.</param>
        protected sealed override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
        }

        /// <summary>
        /// Binds a data source to the invoked server control and all its child controls with an option to raise the E:System.Web.UI.Control.DataBinding event.
        /// </summary>
        /// <param name="raiseOnDataBinding">
        /// true if the E:System.Web.UI.Control.DataBinding event is raised; otherwise, false. 
        /// </param>
        protected sealed override void DataBind(bool raiseOnDataBinding)
        {
            // Don't do anything
        }

        /// <summary>
        ///   Binds a data source to the server control's child controls.
        /// </summary>
        protected sealed override void DataBindChildren()
        {
            // Don't do anything
        }

        /// <summary>
        /// Restores control-state information from a previous page request that was saved by the M:System.Web.UI.Control.SaveControlState method.
        /// </summary>
        /// <param name="savedState">
        /// An System.Object that represents the control state to be restored. 
        /// </param>
        protected sealed override void LoadControlState([NotNull] object savedState)
        {
            // Don't do anything  
        }

        /// <summary>
        ///   Gets the control adapter responsible for rendering the specified control.
        /// </summary>
        /// <returns> A System.Web.UI.Adapters.ControlAdapter" /> that will render the control. </returns>
        [CanBeNull]
        protected sealed override ControlAdapter ResolveAdapter()
        {
            return null;
        }

        /// <summary>
        ///   Saves any server control view-state changes that have occurred since the time the page was posted back to the server.
        /// </summary>
        /// <returns> Returns the server control's current view state. If there is no view state associated with the control, this method returns null. </returns>
        protected sealed override object SaveViewState()
        {
            return null;
        }

        /// <summary>
        /// Causes tracking of view-state changes to the server control so they can be stored in the server control's System.Web.UI.StateBag object. 
        /// This object is accessible through the System.Web.UI.Control.ViewState property.
        /// </summary>
        protected sealed override void TrackViewState()
        {
            // Don't do anything
        }

        /// <summary>
        /// Gets the exception message.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>The exception message</returns>
        [NotNull]
        protected virtual XElement GetExceptionMessage([NotNull] Exception exception)
        {
            var attributes = new XElement("code", string.Empty);

            attributes.Add(new XAttribute("class", "exception"));
            attributes.Add(new XAttribute("data-exception-message", exception.Message));
            attributes.Add(new XAttribute("data-type-fullname", this.GetType().FullName));
            attributes.Add(new XAttribute("data-rawurl", global::Sitecore.Context.RawUrl));

            return attributes;
        }

        #endregion
    }
}
