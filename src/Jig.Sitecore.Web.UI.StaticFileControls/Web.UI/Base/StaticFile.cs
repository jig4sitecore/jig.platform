﻿namespace Jig.Sitecore.Web.UI
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Xml.Linq;

    using global::Sitecore;

    using global::Sitecore.Sites;

    /// <summary>
    /// The static file.
    /// </summary>
    public abstract class StaticFile : IStaticFile
    {
        /// <summary>
        ///     The type attribute name.
        /// </summary>
        protected static readonly XName TypeAttributeName = XName.Get("type");

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the StaticFile class.
        /// </summary>
        /// <param name="innerElement">
        /// The inner element.
        /// </param>
        /// <exception cref="System.Web.HttpException">
        /// The Inner Element is null!
        /// </exception>
        protected StaticFile([NotNull] XElement innerElement)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (innerElement == null)
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                throw new HttpException("The Inner Element is null!");
            }

            this.InnerHtml = innerElement;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the inner HTM.
        /// </summary>
        /// <value>
        ///     The inner HTM.
        /// </value>
        [Bindable(false)]
        [Browsable(false)]
        [Localizable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public XElement InnerHtml { get; private set; }

        /// <summary>
        ///     Gets or sets the type.
        /// </summary>
        [NotNull]
        [Bindable(false)]
        [Localizable(false)]
        public string Type
        {
            get
            {
                return this.GetAttribute(TypeAttributeName);
            }

            set
            {
                this.SetAttribute(TypeAttributeName, value);
            }
        }

        /// <summary>
        /// Gets or sets the static file hostname.
        /// </summary>
        /// <value>
        /// The static file hostname.
        /// </value>
        [CanBeNull]
        [Bindable(false)]
        [Localizable(false)]
        [UsedImplicitly]
        public string StaticFileHostname { get; set; }

        /// <summary>
        /// Gets or sets the site information.
        /// </summary>
        /// <value>The site information.</value>
        [CanBeNull]
        [Bindable(false)]
        [Localizable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [UsedImplicitly]
        public global::Sitecore.Sites.SiteContext SiteInfo { get; set; }

        /// <summary>
        /// Gets or sets the static file version.
        /// </summary>
        /// <value>The static file version.</value>
        [CanBeNull]
        [Bindable(false)]
        [Localizable(false)]
        [UsedImplicitly]
        public string StaticFileVersion { get; set; }

        /// <summary>
        /// Gets or sets the rendering behavior.
        /// </summary>
        /// <value>
        /// The rendering behavior.
        /// </value>
        [Bindable(false)]
        [Browsable(false)]
        [Localizable(false)]
        [UsedImplicitly]
        public DisplayMode DisplayMode { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        ///     A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return this.InnerHtml.ToString();
        }

        /// <summary>
        /// Outputs server control content to a provided System.Web.UI.HtmlTextWriter object and stores
        /// tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
        /// <param name="context">The context.</param>
        public abstract void RenderView(HtmlTextWriter writer, [NotNull] HttpContextBase context);

        /// <summary>
        /// Gets the attribute.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>The attribute value</returns>
        [NotNull]
        protected string GetAttribute([NotNull] XName name)
        {
            var attribute = this.InnerHtml.Attribute(name);
            if (attribute != null)
            {
                return attribute.Value;
            }

            return string.Empty;
        }

        /// <summary>
        /// Sets the attribute.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        protected void SetAttribute([NotNull] XName name, [NotNull] string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                var attribute = this.InnerHtml.Attribute(name);
                if (attribute == null)
                {
                    attribute = new XAttribute(name, value);
                    this.InnerHtml.Add(attribute);
                }
                else
                {
                    attribute.Value = value;
                }
            }
        }

        #endregion
    }
}
