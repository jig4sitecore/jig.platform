﻿namespace Jig.Sitecore.Web.UI
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Xml.Linq;

    using Jig.Sitecore.Pipelines;

    using global::Sitecore;

    /// <summary>
    /// The Css file.
    /// </summary>
    [ParseChildren(typeof(StaticCssFileReference))]
    [DefaultProperty("Href")]
    public class StaticCssFileReference : StaticFile
    {
        #region Static Fields

        /// <summary>
        /// The href attribute name
        /// </summary>
        private static readonly XName HrefAttributeName = XName.Get("href");

        /// <summary>
        /// The media attribute name
        /// </summary>
        private static readonly XName MediaAttributeName = XName.Get("media");

        /// <summary>
        /// The relative attribute name
        /// </summary>
        private static readonly XName RelAttributeName = XName.Get("rel");

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the StaticCssFileReference class.
        /// </summary>
        public StaticCssFileReference()
            : base(new XElement(
                                "link",
                                new XAttribute(TypeAttributeName, "text/css"),
                                new XAttribute(RelAttributeName, "stylesheet"),
                                new XAttribute(MediaAttributeName, "all")))
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the source.
        /// </summary>
        /// <value>
        ///     The source.
        /// </value>
        [NotNull]
        [Bindable(false)]
        [Localizable(false)]
        public string Href
        {
            get
            {
                return this.GetAttribute(HrefAttributeName);
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    var src = value.Trim(' ', '~', '.');
                    if (src.StartsWith("http") || src.StartsWith("//"))
                    {
                        /* Account for cases like that:
                         * Correct
                         *      //fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic 
                         * Wrong (case sensitive font name)
                         *      //fonts.googleapis.com/css?family=lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic
                         */
                        this.SetAttribute(HrefAttributeName, src);  
                    }
                    else
                    {
                      this.SetAttribute(HrefAttributeName, src.ToLowerInvariant());  
                    }                    
                }
            }
        }

        /// <summary>
        /// Gets or sets the relative attribute.
        /// </summary>
        /// <value>
        /// The relative attribute value.
        /// </value>
        [NotNull]
        [Bindable(false)]
        [Localizable(false)]
        public string Rel
        {
            get
            {
                return this.GetAttribute(RelAttributeName);
            }

            set
            {
                this.SetAttribute(RelAttributeName, value);
            }
        }

        /// <summary>
        /// Gets or sets the Media attribute.
        /// </summary>
        /// <value>
        /// The Media attribute value.
        /// </value>
        [NotNull]
        [Bindable(false)]
        [Localizable(false)]
        public string Media
        {
            get
            {
                return this.GetAttribute(MediaAttributeName);
            }

            set
            {
                this.SetAttribute(MediaAttributeName, value);
            }
        }

        /// <summary>
        /// Gets or sets the conditional comments.
        /// </summary>
        /// <value>
        /// The conditional comments.
        /// </value>
        [CanBeNull]
        [Bindable(false)]
        [Localizable(false)]
        [UsedImplicitly]
        public string Conditional { get; set; }

        #endregion

        /// <summary>
        /// Outputs server control content to a provided System.Web.UI.HtmlTextWriter object and stores
        /// tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
        /// <param name="context">The context.</param>
        public override void RenderView(HtmlTextWriter writer, [NotNull] HttpContextBase context)
        {
            /* Clone and override the Src attribute */
            var html = new XElement(this.InnerHtml);
            var href = html.Attribute(HrefAttributeName);

            if (!string.IsNullOrWhiteSpace(href.Value))
            {
                var versionArgs = new StaticFileVersionResolverPipelineArgs(context, this.Href, this.StaticFileHostname);
                var version = versionArgs.Run();
                var args = new StaticFileUriResolverPipelineArgs(context, this.Href, this.StaticFileHostname)
                               {
                                   StaticFileVersion = version
                               };

                var uri = args.Run();

                html.SetAttributeValue(HrefAttributeName, uri);
            }

            if (string.IsNullOrWhiteSpace(this.Conditional))
            {
                writer.WriteLine(html);
            }
            else
            {
                // ReSharper disable PossibleNullReferenceException
                writer.WriteLine("<!--[" + this.Conditional.Trim() + "]>");
                // ReSharper restore PossibleNullReferenceException
                writer.WriteLine(html);
                writer.WriteLine("<![endif]-->");
            }
        }
    }
}
