﻿namespace Jig.Sitecore.Web.UI
{
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Web.UI;

    /// <summary>
    /// The static Css file references.
    /// </summary>
    [ToolboxData("<{0}:StaticCssFileReferences runat=server></{0}:StaticCssFileReferences>")]
    [Guid("1BC36A55-ED9E-4585-9AF6-BAB990DAFD61")]
    [DefaultProperty("Href")]
    public class StaticCssFileReferences : StaticFilesViewBase<StaticCssFileReference>
    {
    }
}
