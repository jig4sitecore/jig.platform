﻿namespace Jig.Sitecore.Views
{
    using System;
    using System.Web;
    using System.Web.UI;

    using global::Sitecore;

    /// <summary>
    /// The diagnostics extensions.
    /// </summary>
    internal static class DiagnosticsExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The is diagnostics enabled.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public static bool IsCachingEnabled([NotNull] this HttpContext context)
        {
            var site = global::Sitecore.Context.Site;
            if (site == null)
            {
                return false;
            }

            return site.CacheHtml;
        }

        /// <summary>
        /// Render cache the diagnostics.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="cacheable">if set to <c>true</c> [cacheable].</param>
        /// <param name="varyByData">if set to <c>true</c> [vary by data].</param>
        /// <param name="varyByDevice">if set to <c>true</c> [vary by device].</param>
        /// <param name="varyByLogin">if set to <c>true</c> [vary by login].</param>
        /// <param name="varyByParm">if set to <c>true</c> [vary by parm].</param>
        /// <param name="varyByQueryString">if set to <c>true</c> [vary by query string].</param>
        /// <param name="varyByUser">if set to <c>true</c> [vary by user].</param>
        /// <param name="clearOnIndexUpdate">if set to <c>true</c> [clear on index update].</param>
        /// <param name="varyByCustom">The vary by custom.</param>
        public static void WriteCacheDiagnostics(
            [CanBeNull] this HtmlTextWriter writer,
            bool cacheable,
            bool varyByData,
            bool varyByDevice,
            bool varyByLogin,
            bool varyByParm,
            bool varyByQueryString,
            bool varyByUser,
            bool clearOnIndexUpdate,
            [CanBeNull] string varyByCustom)
        {
            if (writer == null)
            {
                return;
            }

            // <!-- Rendering was output cached at {datetime}, VaryByData, CachingID = "{ID}" -->
            if (!cacheable)
            {
                writer.WriteLine("<!-- Rendering was output cached disabled -->");
                return;
            }

            writer.Write("<!-- Rendering was output cached at ");
            writer.Write(DateTime.Now);

            if (clearOnIndexUpdate)
            {
                writer.Write(", ClearOnIndexUpdate");
            }

            if (varyByData)
            {
                writer.Write(", VaryByData");
            }

            if (varyByDevice)
            {
                writer.Write(", VaryByDevice");
            }

            if (varyByLogin)
            {
                writer.Write(", VaryByLogin");
            }

            if (varyByParm)
            {
                writer.Write(", VaryByParm");
            }

            if (varyByQueryString)
            {
                writer.Write(", VaryByQueryString");
            }

            if (varyByUser)
            {
                writer.Write(", VaryByUser");
            }

            if (!string.IsNullOrEmpty(varyByCustom))
            {
                writer.Write(", VaryByCustom=\"");
                writer.WriteEncodedText(varyByCustom);
                writer.Write("\"");
            }

            writer.WriteLine(" -->");
        }

        #endregion
    }
}
