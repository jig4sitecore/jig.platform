﻿namespace Jig.Sitecore.Views.WebForms
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.Adapters;
    using System.Web.UI.WebControls;
    using System.Xml.Linq;

    using Jig.Web.Collections;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    /// <summary>
    /// The Jig control base.
    /// </summary>
    [ParseChildren(false)]
    [PersistChildren(false)]
    [ControlBuilder(typeof(LiteralControlBuilder))]
    public partial class RazorViewBase : Control
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RazorViewBase"/> class.
        /// </summary>
        protected RazorViewBase()
        {
            base.EnableViewState = false;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets a value indicating whether EnableTheming is enabled in control.
        /// </summary>
        /// <remarks>
        ///     This property is unsupported in this control.
        /// </remarks>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new bool EnableTheming
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether View state is enabled in control.
        /// </summary>
        /// <remarks>
        ///     This property is unsupported in this control.
        /// </remarks>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new bool EnableViewState
        {
            get
            {
                return false;
            }

            protected set
            {
                base.EnableViewState = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Performs an implicit conversion from <see cref="RazorLiteralBase{TModel}"/> to <see cref="Control"/>.
        /// </summary>
        /// <param name="razorBase">The razor base.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator Literal(RazorViewBase razorBase)
        {
            if (razorBase == null)
            {
                return null;
            }

            var html = razorBase.Render();
            return new Literal
            {
                Mode = LiteralMode.PassThrough,
                Text = html
            };
        }

        /// <summary>
        /// Outputs server control content.
        /// </summary>
        /// <returns>Outputs server control content(html)</returns>
        [NotNull]
        public string Render()
        {
            using (var innerWriter = new HtmlTextWriter(new StringWriter(new StringBuilder(1024))))
            {
                this.RenderControl(innerWriter);

                var html = innerWriter.InnerWriter.ToString();
                return html;
            }
        }

        /// <summary>
        /// Applies the style properties defined in the page style sheet to the control.
        /// </summary>
        /// <param name="page">
        /// The System.Web.UI.Page containing the control.
        /// </param>
        public override sealed void ApplyStyleSheetSkin([NotNull] Page page)
        {
            // Don't do anything
        }

        /// <summary>
        ///     Binds a data source to the invoked server control and all its child controls.
        /// </summary>
        public override sealed void DataBind()
        {
            throw new InvalidOperationException("The Method is not supported!");
        }

        /// <summary>
        /// Searches the current naming container for a server control with the specified id parameter.
        /// </summary>
        /// <param name="id">The identifier for the control to be found.</param>
        /// <returns>
        /// The specified control, or null if the specified control does not exist.
        /// </returns>
        /// <exception cref="System.InvalidOperationException">The Method is not supported!</exception>
        [CanBeNull]
        public override sealed Control FindControl(string id)
        {
            throw new InvalidOperationException("The Method is not supported!");
        }

        /// <summary>
        ///     Sets input focus to a control.
        /// </summary>
        public override sealed void Focus()
        {
            throw new InvalidOperationException("The Method is not supported!");
        }

        /// <summary>
        ///     Determines if the server control contains any child controls.
        /// </summary>
        /// <returns> true if the control contains other controls; otherwise, false. </returns>
        public override sealed bool HasControls()
        {
            return false;
        }

        /// <summary>
        /// Outputs server control content to a provided System.Web.UI.HtmlTextWriter object and stores tracing information
        /// about the control if tracing is enabled.
        /// </summary>
        /// <param name="output">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
        public override sealed void RenderControl(HtmlTextWriter output)
        {
            var type = this.GetType();

            output.WriteLine();

            output.WriteLineNoTabs("<!-- Begin: " + type.Name + " -->");

            if (this.Visible && !this.DesignMode)
            {
                try
                {
                    /* Do proper exception handling, write to inner writer  */
                    using (var innerWriter = new HtmlTextWriter(new StringWriter(new StringBuilder(1024))))
                    {
                        this.RenderView(innerWriter);

                        output.WriteLine(innerWriter.InnerWriter.ToString());
                    }
                }
                catch (Exception exception)
                {
                    Log.Error(exception.ToString(), this);
                    Trace.TraceError(exception.ToString());
                    var error = this.GetExceptionMessage(exception);
                    output.WriteLine(error);
                }
            }

            output.WriteLineNoTabs("<!-- End: " + type.Name + " -->");
        }

        /// <summary>
        /// Outputs server control content to a provided System.Web.UI.HtmlTextWriter object and stores
        /// tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="output">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
        protected virtual void RenderView([NotNull] HtmlTextWriter output)
        {
            var code = new XElement("code", string.Empty);
            code.Add(new XAttribute("class", "error"));
            code.Add(
                new XAttribute(
                    "data-error-message",
                    "Override " + this.GetType().FullName + ".RenderView() method"));

            output.WriteLine(code);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Good programming practice
        /// </summary>
        /// <param name="obj">
        /// An Object that represents the parsed element.
        /// </param>
        protected override sealed void AddParsedSubObject(object obj)
        {
            // Don't do anything
        }

        /// <summary>
        ///     Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create
        ///     any child controls they contain in preparation for posting back or rendering.
        /// </summary>
        protected override sealed void CreateChildControls()
        {
            // Don't do anything
        }

        /// <summary>
        ///     Good programming practice
        /// </summary>
        /// <returns> Empty Control collection. No children. </returns>
        protected override sealed ControlCollection CreateControlCollection()
        {
            return new EmptyControlCollection(this);
        }

        /// <summary>
        /// Binds a data source to the invoked server control and all its child controls with an option to raise the
        ///     System.Web.UI.Control.DataBinding event.
        /// </summary>
        /// <param name="raiseOnDataBinding">
        /// true if the System.Web.UI.Control.DataBinding event is raised; otherwise, false.
        /// </param>
        protected override sealed void DataBind(bool raiseOnDataBinding)
        {
            // Don't do anything
        }

        /// <summary>
        ///     Binds a data source to the server control's child controls.
        /// </summary>
        protected override sealed void DataBindChildren()
        {
            // Don't do anything
        }

        /// <summary>
        ///     Determines whether the server control contains child controls. If it does not, it creates child controls.
        /// </summary>
        protected override sealed void EnsureChildControls()
        {
            // Don't do anything
        }

        /// <summary>
        ///     Determines if the server control holds only literal content.
        /// </summary>
        /// <returns> true if the server control contains solely literal content; otherwise false. </returns>
        protected new bool IsLiteralContent()
        {
            return true;
        }

        /// <summary>
        /// Restores control-state information from a previous page request that was saved by the
        /// M:System.Web.UI.Control.SaveControlState method.
        /// </summary>
        /// <param name="savedState">An System.Object that represents the control state to be restored.</param>
        protected override sealed void LoadControlState([NotNull] object savedState)
        {
            // Don't do anything  
        }

        /// <summary>
        /// Sends server control content to a provided System.Web.UI.HtmlTextWriter object, which writes the content to be
        ///     rendered on the client.
        /// </summary>
        /// <param name="writer">
        /// The System.Web.UI.HtmlTextWriter object that receives the server control content.
        /// </param>
        protected override sealed void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
        }

        /// <summary>
        ///     Resolve the absolute URI.
        /// </summary>
        /// <returns>The value for caching key</returns>
        [NotNull]
        protected virtual string ResolveAbsoluteUri()
        {
            return this.Context.Request.Url.AbsoluteUri;
        }

        /// <summary>
        ///     Gets the control adapter responsible for rendering the specified control.
        /// </summary>
        /// <returns> A System.Web.UI.Adapters.ControlAdapter" /> that will render the control. </returns>
        [CanBeNull]
        protected override sealed ControlAdapter ResolveAdapter()
        {
            return null;
        }

        /// <summary>
        /// Saves any server control view-state changes that have occurred since the time the page was posted back to the
        /// server.
        /// </summary>
        /// <returns>
        /// Returns the server control's current view state. If there is no view state associated with the control, this
        /// method returns null.
        /// </returns>
        protected override sealed object SaveViewState()
        {
            return null;
        }

        /// <summary>
        ///     Causes tracking of view-state changes to the server control so they can be stored in the server control's
        ///     System.Web.UI.StateBag object.
        ///     This object is accessible through the System.Web.UI.Control.ViewState property.
        /// </summary>
        protected override sealed void TrackViewState()
        {
            // Don't do anything
        }

        /// <summary>
        /// Gets the exception message.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>The exception message</returns>
        [NotNull]
        protected virtual XElement GetExceptionMessage([NotNull] Exception exception)
        {
            var attributes = new XElement("code", string.Empty);

            attributes.Add(new XAttribute("class", "exception"));
            attributes.Add(new XAttribute("data-exception-message", exception.Message));
            attributes.Add(new XAttribute("data-type-fullname", this.GetType().FullName));
            attributes.Add(new XAttribute("data-rawurl", global::Sitecore.Context.RawUrl));

            return attributes;
        }

        /// <summary>
        /// Gets the parameters as x attributes.
        /// </summary>
        /// <param name="standardValueAttributes">The standard value attributes.</param>
        /// <returns>
        /// The value of the Parameters field in XAttribute collection format.
        /// </returns>
        [NotNull]
        protected virtual XAttributeDictionary GetParametersAsXAttributes([NotNull] params XAttribute[] standardValueAttributes)
        {
            var attributes = new XAttributeDictionary(standardValueAttributes)
                                 {
                                     new XAttribute(
                                         "data-widget",
                                         this.GetType().Name)
                                 };

            return attributes;
        }

        #endregion
    }
}
