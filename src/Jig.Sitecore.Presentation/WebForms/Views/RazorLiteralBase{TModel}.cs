﻿namespace Jig.Sitecore.Views.WebForms
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Xml.Linq;

    using global::Sitecore;
    using global::Sitecore.Diagnostics;

    /// <summary>
    /// The Jig razor base.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    public partial class RazorLiteralBase<TModel> where TModel : class
    {
        /* ReSharper disable NotNullMemberIsNotInitialized */

        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        [NotNull]
        public TModel Model { get; [UsedImplicitly] set; }

        /* ReSharper restore NotNullMemberIsNotInitialized */

        #region Public Methods and Operators

        /// <summary>
        /// Performs an implicit conversion from <see cref="RazorLiteralBase{TModel}"/> to <see cref="System.String"/>.
        /// </summary>
        /// <param name="razorBase">The razor base.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator string(RazorLiteralBase<TModel> razorBase)
        {
            if (razorBase == null)
            {
                return string.Empty;
            }

            var html = razorBase.Render();
            return html;
        }

        /// <summary>
        /// Performs an implicit conversion from <see cref="RazorLiteralBase{TModel}"/> to <see cref="Control"/>.
        /// </summary>
        /// <param name="razorBase">The razor base.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator Control(RazorLiteralBase<TModel> razorBase)
        {
            if (razorBase == null)
            {
                return null;
            }

            var html = razorBase.Render();
            return new Literal
                       {
                           Mode = LiteralMode.PassThrough,
                           Text = html
                       };
        }

        /// <summary>
        /// Outputs server control content.
        /// </summary>
        /// <returns>Outputs server control content(html)</returns>
        [NotNull]
        public string Render()
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (this.Model != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                using (var innerWriter = new HtmlTextWriter(new StringWriter(new StringBuilder(512))))
                {
                    this.Render(innerWriter);

                    var html = innerWriter.InnerWriter.ToString();
                    return html;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Outputs server control content to a provided System.Web.UI.HtmlTextWriter object and stores tracing information
        /// about the control if tracing is enabled.
        /// </summary>
        /// <param name="output">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
        public void Render([NotNull]HtmlTextWriter output)
        {
            var type = this.GetType();

            output.WriteLine();

            output.WriteLineNoTabs("<!-- Begin: " + type.Name + " -->");

            try
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (this.Model != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    /* Do proper exception handling, write to inner writer  */
                    using (var innerWriter = new HtmlTextWriter(new StringWriter(new StringBuilder(512))))
                    {
                        this.RenderView(innerWriter);

                        output.WriteLine(innerWriter.InnerWriter.ToString());
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error(exception.ToString(), this);
                Trace.TraceError(exception.ToString());

                var msg = this.GetExceptionMessage(exception);
                output.WriteLine(msg);
            }

            output.WriteLineNoTabs("<!-- End: " + type.Name + " -->");
        }

        /// <summary>
        /// Outputs server control content to a provided System.Web.UI.HtmlTextWriter object and stores
        /// tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="output">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
        protected virtual void RenderView([NotNull] HtmlTextWriter output)
        {
            var code = new XElement("code", string.Empty);
            code.Add(new XAttribute("class", "error"));
            code.Add(
                new XAttribute(
                    "data-error-message",
                    "Override " + this.GetType().FullName + ".RenderView() method"));

            output.WriteLine(code);
        }

        /// <summary>
        /// Gets the exception message.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>
        /// The exception message
        /// </returns>
        [NotNull]
        protected virtual XElement GetExceptionMessage([NotNull] Exception exception)
        {
            var attributes = new XElement("code", string.Empty);

            attributes.Add(new XAttribute("class", "exception"));
            attributes.Add(new XAttribute("data-exception-message", exception.Message));
            attributes.Add(new XAttribute("data-rawurl", global::Sitecore.Context.RawUrl));
            attributes.Add(new XAttribute("data-type-fullname", this.GetType().FullName));
            attributes.Add(new XAttribute("data-model-fullname", typeof(TModel).FullName));

            return attributes;
        }

        #endregion
    }
}
