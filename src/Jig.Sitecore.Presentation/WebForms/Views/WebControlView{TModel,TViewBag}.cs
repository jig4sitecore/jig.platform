﻿namespace Jig.Sitecore.Views.WebForms
{
    using global::Sitecore;

    /// <summary>
    /// A View based on Sitecore's WebControl. T will be the Type used for the View's
    /// ViewModel, represented by the ViewModel property.
    /// </summary>
    /// <typeparam name="TModel">The Type to use for the View's ViewModel.</typeparam>
    /// <typeparam name="TViewBag">The type of the view bag.</typeparam>
    public abstract class WebControlView<TModel, TViewBag> : WebControlView<TModel>
        where TModel : class
        where TViewBag : class
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the view bag.
        /// </summary>
        /// <value>
        /// The view bag.
        /// </value>
        [CanBeNull]
        public TViewBag ViewBag { get; set; }

        #endregion
    }
}