﻿namespace Jig.Sitecore.Views.WebForms
{
    using System.Collections.Specialized;
    using System.Diagnostics.CodeAnalysis;
    using System.Web;
    using System.Web.UI;

    using Jig.Sitecore.Data;
    using Jig.Sitecore.Views.WebForms.Presenters;

    using global::Sitecore;

    using global::Sitecore.Data;

    using global::Sitecore.Data.Items;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Globalization;

    using global::Sitecore.Links;

    using global::Sitecore.Web.UI.WebControls;

    /// <summary>
    /// A View based on Microsoft's UserControl, compatible with Sitecore's Sublayout objects. 
    /// T will be the Type used for the View's ViewModel, represented by the ViewModel property.
    /// </summary>
    /// <typeparam name="TModel">The Type to use for the View's ViewModel.</typeparam>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public abstract class UserControlView<TModel> : UserControl, IView<TModel>
        where TModel : class
    {
        /// <summary>
        /// The actual Sitecore WebControl that we wrap the IView around.
        /// Required because of the way Sitecore implements Sublayouts.
        /// </summary>
        [CanBeNull]
        private Sublayout sublayout;

        /// <summary>
        /// The model relevant to the view's context.
        /// </summary>
        private TModel model;

        /// <summary>
        /// The presenter.
        /// </summary>
        private IPresenter<TModel> presenter;

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the output can be cached.
        /// </summary>
        public bool Cacheable
        {
            get { return this.Sublayout.Cacheable; }
            set { this.Sublayout.Cacheable = value; }
        }

        /// <summary>
        /// Gets or sets the path or query to the Sitecore Item that the View is assigned
        /// </summary>
        [NotNull]
        public string DataSource
        {
            get
            {
                return this.Sublayout.DataSource ?? string.Empty;
            }

            set
            {
                this.Sublayout.DataSource = value;
            }
        }

        /// <summary>
        /// Gets the Database to be used to retrieve Items rendered by the View.
        /// </summary>
        [NotNull]
        public Database Database
        {
            get
            {
                return this.GetContextItem().Database;
            }
        }

        /// <summary>
        /// Gets the Language to be used for Items rendered by the View.
        /// </summary>
        [NotNull]
        public Language Language
        {
            get
            {
                return this.GetContextItem().Language;
            }
        }

        /// <summary>
        /// Gets or sets an instance of T relevant to the view's context.
        /// </summary>
        /// <value>
        /// The view model.
        /// </value>
        [CanBeNull]
        public TModel Model
        {
            get
            {
                var instance = this.Presenter;
                if (instance != null)
                {
                    return this.model ?? (this.model = instance.GetModel(this, new HttpContextWrapper(this.Context)));
                }

                return null;
            }

            set
            {
                this.model = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether output caching should be discrete for the Datasource.
        /// </summary>
        public bool VaryByData
        {
            get { return this.Sublayout.VaryByData; }
            set { this.Sublayout.VaryByData = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether output caching should be discrete for the Device.
        /// </summary>
        public bool VaryByDevice
        {
            get { return this.Sublayout.VaryByDevice; }
            set { this.Sublayout.VaryByDevice = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether output caching should be discrete for the authenticated user.
        /// </summary>
        public bool VaryByLogin
        {
            get { return this.Sublayout.VaryByLogin; }
            set { this.Sublayout.VaryByLogin = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether output caching should be discrete for runtime rendering parameters.
        /// </summary>
        public bool VaryByParm
        {
            get { return this.Sublayout.VaryByParm; }
            set { this.Sublayout.VaryByParm = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether output caching should be discrete for query string parameters.
        /// </summary>
        public bool VaryByQueryString
        {
            get { return this.Sublayout.VaryByQueryString; }
            set { this.Sublayout.VaryByQueryString = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether output caching should be discrete for visitor sessions.
        /// </summary>
        public bool VaryByUser
        {
            get { return this.Sublayout.VaryByUser; }
            set { this.Sublayout.VaryByUser = value; }
        }

        /// <summary>
        /// Gets an instance of IPresenter capable of constructing an instance of TModel
        /// </summary>
        /// <remarks>
        /// This Property uses reflection internally to determine the correct presenter to
        /// load. If there is only one possible Presenter, override this property in your
        /// derived implementation and provide the correct presenter instance directly.
        /// </remarks>
        [CanBeNull]
        protected IPresenter<TModel> Presenter
        {
            get
            {
                return this.presenter ?? (this.presenter = PresenterFactory.GetPresenter<TModel>());
            }
        }

        /// <summary>
        /// Gets a Sitecore.Web.UI.WebControl we can use for this instance.
        /// When employed in a dynamically rendered user control, this is always the
        /// Parent control. If the user control is statically declared, we have to 
        /// create a control we can reference internally.
        /// </summary>
        [NotNull]
        protected Sublayout Sublayout
        {
            get
            {
                if (this.sublayout == null)
                {
                    var parentSublayout = this.Parent as Sublayout;
                    if (parentSublayout != null)
                    {
                        this.sublayout = parentSublayout;
                        return this.sublayout;
                    }

                    this.sublayout = new Sublayout();
                }

                return this.sublayout;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Either the resolved Datasource or the Context Item if the Datasource is empty.
        /// </summary>
        /// <returns>An Item.</returns>
        [CanBeNull]
        public Item GetItem()
        {
            if (DatasourceResolver.IsQuery(this.DataSource))
            {
                // ReSharper disable CSharpWarnings::CS0612
                Item[] items = this.GetItems();
                // ReSharper restore CSharpWarnings::CS0612
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (items != null && items.Length > 0)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    return items[0];
                }
            }

            var dataSource = this.DataSource;
            if (!string.IsNullOrWhiteSpace(dataSource) && dataSource.Contains("."))
            {
                var context = this.GetContextItem();

                return DatasourceResolver.Resolve(
                    dataSource.Replace(".", context.Paths.FullPath),
                    context.Database);
            }

            return this.GetSingleItemFromDataSource();
        }

        /// <summary>
        /// An array of either the resolved Datasource or the Context Item.
        /// </summary>
        /// <returns>An array of Items. The Array may be empty.</returns>
        [NotNull]
        public Item[] GetItems()
        {
            if (DatasourceResolver.IsQuery(this.DataSource))
            {
                var query = DatasourceResolver.EncodeQuery(this.DataSource);
                return this.GetContextItem().Database.SelectItems(query);
            }

            return new[] { this.GetItem() };
        }

        /// <summary>
        /// Returns the Context Item for the request.
        /// </summary>
        /// <returns>An Item.</returns>
        [NotNull]
        public Item GetContextItem()
        {
            return global::Sitecore.Context.Item;
        }

        /// <summary>
        /// Utilizes an appropriate LinkManager to resolve the item's browser URL.
        /// </summary>
        /// <param name="item">The item being linked.</param>
        /// <returns>A browser-ready URL.</returns>
        [NotNull]
        public string GetItemUrl([NotNull] Item item)
        {
            var stronglyTyped = item.AsStronglyTyped();
            if (stronglyTyped == null)
            {
                return string.Empty;
            }

            return this.GetItemUrl(stronglyTyped);
        }

        /// <summary>
        /// Utilizes an appropriate LinkManager to resolve the item's browser URL.
        /// </summary>
        /// <param name="item">The item being linked.</param>
        /// <param name="options">The options for constructing the URL.</param>
        /// <returns>A browser-ready URL.</returns>
        [NotNull]
        public string GetItemUrl([NotNull] Item item, [NotNull] UrlOptions options)
        {
            var stronglyTyped = item.AsStronglyTyped();
            if (stronglyTyped == null)
            {
                return string.Empty;
            }

            return this.GetItemUrl(stronglyTyped, options);
        }

        /// <summary>
        /// Utilizes an appropriate LinkManager to resolve the item's browser URL.
        /// </summary>
        /// <param name="item">The item being linked.</param>
        /// <returns>A browser-ready URL.</returns>
        [NotNull]
        public string GetItemUrl([NotNull] IStandardTemplate item)
        {
            var options = global::Sitecore.Context.Site.GetDefaultUrlOptions();
            options.Language = item.Language;
            return this.GetItemUrl(item, options);
        }

        /// <summary>
        /// Utilizes an appropriate LinkManager to resolve the item's browser URL.
        /// </summary>
        /// <param name="item">The item being linked.</param>
        /// <param name="options">The options for constructing the URL.</param>
        /// <returns>A browser-ready URL.</returns>
        [NotNull]
        public string GetItemUrl([NotNull] IStandardTemplate item, [NotNull] UrlOptions options)
        {
            var provider = this.GetLinkProvider();
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (provider != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var url = provider.GetItemUrl(item.InnerItem, options);
                return url;
            }

            return string.Empty;
        }

        /// <summary>
        /// Provides a quick way to access specific values in the query string style
        /// Parameters field on a Rendering definition.
        /// </summary>
        /// <returns>The value of the Parameters field in an indexable format.</returns>
        [NotNull]
        protected NameValueCollection GetParametersCollection()
        {
            var sub = this.Sublayout;
            if (string.IsNullOrEmpty(sub.Parameters))
            {
                return new NameValueCollection();
            }

            return HttpUtility.ParseQueryString(sub.Parameters);
        }

        /// <summary>
        /// Override to supply a LinkProvider other than the default LinkProvider.
        /// </summary>
        /// <returns>The link provider to use when resolving Item URLs.</returns>
        [NotNull]
        protected virtual LinkProvider GetLinkProvider()
        {
            return LinkManager.Provider;
        }

        /// <summary>
        /// Replicates the default functionality of WebControl.GetItem(), which is marked protected,
        /// so we can't access it from the Sublayout property.
        /// </summary>
        /// <returns>The Item to use as a rendering context.</returns>
        [CanBeNull]
        private Item GetSingleItemFromDataSource()
        {
            /*
             *  The following logic was pulled straight from Sitecore source code for WebControl.GetItem()
             *  IMHO the GetItem() method should be public rather than protected, but this is what
             *  we have to work with.
             */

            var item = this.GetContextItem();
            var dataSource = this.DataSource;

            if (!string.IsNullOrWhiteSpace(dataSource))
            {
                if (global::Sitecore.MainUtil.IsFullPath(dataSource))
                {
                    Database database = this.Sublayout.GetDatabase();
                    if (database != null)
                    {
                        item = database.GetItem(dataSource);
                    }
                }
                else
                {
                    Assert.IsNotNull(
                        item,
                        "Cannot resolve a relative data source path when the current item is null. Path is " + dataSource);
                    item = item.Axes.GetItem(dataSource);
                }
            }

            if (item == null)
            {
                Tracer.Warning(
                    "Rendering \"" + this.GetType().FullName + "\" will not be shown as the DataSource \"" + dataSource
                    + "\" was not found.");
            }

            return item;
        }

        #endregion
    }
}
