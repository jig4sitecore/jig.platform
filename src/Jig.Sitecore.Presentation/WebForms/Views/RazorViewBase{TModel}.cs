﻿namespace Jig.Sitecore.Views.WebForms
{
    using System;
    using System.Xml.Linq;

    using global::Sitecore;

    /// <summary>
    /// The razor control base.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    public abstract partial class RazorViewBase<TModel> : RazorViewBase
        where TModel : class
    {
        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        [CanBeNull]
        public TModel Model { get; set; }

        /// <summary>
        /// Gets the exception message.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>The exception message</returns>
        [NotNull]
        protected override XElement GetExceptionMessage([NotNull] Exception exception)
        {
            var attributes = base.GetExceptionMessage(exception);

            attributes.Add(new XAttribute("data-model-type", typeof(TModel).FullName));

            return attributes;
        }
    }
}