﻿namespace Jig.Sitecore.Views.WebForms
{
    using global::Sitecore;

    /// <summary>
    /// Razor View Base
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    /// <typeparam name="TViewBag">The type of the view bag.</typeparam>
    public abstract partial class RazorViewBase<TModel, TViewBag> : RazorViewBase
        where TModel : class 
        where TViewBag : class
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        [CanBeNull]
        public TModel Model { get; set; }

        /// <summary>
        /// Gets or sets the view bag.
        /// </summary>
        /// <value>
        /// The view bag.
        /// </value>
        [CanBeNull]
        public TViewBag ViewBag { get; set; }

        #endregion
    }
}