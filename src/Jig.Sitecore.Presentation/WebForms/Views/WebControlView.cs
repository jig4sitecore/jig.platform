﻿namespace Jig.Sitecore.Views.WebForms
{
    using System;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Xml.Linq;
    using Jig.Sitecore.Data;
    using Jig.Web.Collections;
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Globalization;
    using global::Sitecore.Links;
    using global::Sitecore.Web.UI;

    /// <summary>
    /// A View based on Sitecore's WebControl. T will be the Type used for the View's
    /// ViewModel, represented by the ViewModel property.
    /// </summary>
    public class WebControlView : WebControl
    {
        #region Properties

        /// <summary>
        /// Gets the Database to be used to retrieve Items rendered by the View.
        /// </summary>
        [NotNull]
        public new Database Database
        {
            get
            {
                return this.GetContextItem().Database;
            }
        }

        /// <summary>
        /// Gets the Language to be used for Items rendered by the View.
        /// </summary>
        [NotNull]
        public Language Language
        {
            get
            {
                return this.GetContextItem().Language;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The Sitecore Item referenced by the DataSource property of the control.
        /// If the DataSource is a query, the first Item in the result set will be returned.
        /// </summary>
        /// <returns>A Sitecore Item instance.</returns>
        [CanBeNull]
        public new Item GetItem()
        {
            if (DatasourceResolver.IsQuery(this.DataSource))
            {
                Item[] items = this.GetItems();
                if (items.Length > 0)
                {
                    return items[0];
                }
            }

            if (this.DataSource.Contains("."))
            {
                var context = this.GetContextItem();

                var path = this.DataSource.Replace(".", context.Paths.FullPath);
                var item = DatasourceResolver.Resolve(path, context.Database);
                return item;
            }

            var baseItem = base.GetItem();
            return baseItem;
        }

        /// <summary>
        /// The Sitecore Items referenced by the DataSource property of the control.
        /// If the DataSource references a single item, the resulting array will contain that
        /// item.
        /// </summary>
        /// <returns>An array of Sitecore Item instances.</returns>
        [NotNull]
        public Item[] GetItems()
        {
            if (DatasourceResolver.IsQuery(this.DataSource))
            {
                string query = DatasourceResolver.EncodeQuery(this.DataSource);
                var items = this.GetContextItem().Database.SelectItems(query);
                return items;
            }

            return new[] { this.GetItem() };
        }

        /// <summary>
        /// Utilizes an appropriate LinkManager to resolve the item's browser URL.
        /// </summary>
        /// <param name="item">The item being linked.</param>
        /// <returns>A browser-ready URL.</returns>
        [NotNull]
        public string GetItemUrl([NotNull] Item item)
        {
            var stronglyTypedItem = item.AsStronglyTyped();
            if (stronglyTypedItem == null)
            {
                return string.Empty;
            }

            var url = this.GetItemUrl(stronglyTypedItem);

            return url;
        }

        /// <summary>
        /// Utilizes an appropriate LinkManager to resolve the item's browser URL.
        /// </summary>
        /// <param name="item">The item being linked.</param>
        /// <param name="options">The options for constructing the URL.</param>
        /// <returns>A browser-ready URL.</returns>
        [NotNull]
        public string GetItemUrl([NotNull] Item item, [NotNull] UrlOptions options)
        {
            var stronglyTypedItem = item.AsStronglyTyped();
            if (stronglyTypedItem == null)
            {
                return string.Empty;
            }

            var url = this.GetItemUrl(stronglyTypedItem, options);

            return url;
        }

        /// <summary>
        /// Utilizes an appropriate LinkManager to resolve the item's browser URL.
        /// </summary>
        /// <param name="item">The item being linked.</param>
        /// <returns>A browser-ready URL.</returns>
        [NotNull]
        public string GetItemUrl([NotNull] IStandardTemplate item)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return string.Empty;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            var options = this.GetDefaultUrlOptions();
            options.Language = item.Language;
            var url = this.GetItemUrl(item, options);

            return url;
        }

        /// <summary>
        /// Gets the default URL options.
        /// </summary>
        /// <returns>The default Link manager options</returns>
        [NotNull]
        public UrlOptions GetDefaultUrlOptions()
        {
            var options = global::Sitecore.Context.Site.GetDefaultUrlOptions();

            return options;
        }

        /// <summary>
        /// Utilizes an appropriate LinkManager to resolve the item's browser URL.
        /// </summary>
        /// <param name="item">The item being linked.</param>
        /// <param name="options">The options for constructing the URL.</param>
        /// <returns>A browser-ready URL.</returns>
        [NotNull]
        public string GetItemUrl([NotNull] IStandardTemplate item, [NotNull] UrlOptions options)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null || options == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return string.Empty;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            var provider = this.GetLinkProvider();
            var url = provider.GetItemUrl(item.InnerItem, options);

            return url;
        }

        /// <summary>
        /// Gets the caching identifier.
        /// </summary>
        /// <returns>The cache key</returns>
        [NotNull]
        protected override string GetCachingID()
        {
            var value = string.Concat(base.GetCachingID(), '-', this.UniqueID, '-', this.GetType().FullName);

            return value;
        }

        /// <summary>
        /// Provides a quick way to access specific values in the query string style
        /// Parameters field on a Rendering definition.
        /// </summary>
        /// <returns>The value of the Parameters field in an indexable format.</returns>
        [NotNull]
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
        protected NameValueCollection GetParametersCollection()
        {
            if (string.IsNullOrEmpty(this.Parameters))
            {
                return new NameValueCollection();
            }

            var collection = HttpUtility.ParseQueryString(this.Parameters);
            return collection;
        }

        /// <summary>
        /// Gets the parameters as x attributes.
        /// </summary>
        /// <param name="standardValueAttributes">The standard value attributes.</param>
        /// <returns>
        /// The idea behind this method: To allow pass data-attributes to the control as well as allowing override 'standard values' passed from C# code
        /// </returns>
        [NotNull]
        protected virtual XAttributeDictionary GetParametersAsXAttributes([NotNull] params XAttribute[] standardValueAttributes)
        {
            return this.GetParametersAsXAttributes(true, true, standardValueAttributes);
        }

        /// <summary>
        /// Provides a quick way to access specific values in the query string style
        /// Parameters field on a Rendering definition.
        /// </summary>
        /// <param name="filter">if set to <c>true</c> [filter].</param>
        /// <param name="keyToLowerCase">if set to <c>true</c> [key to lower case].</param>
        /// <param name="standardValueAttributes">The standard value attributes.</param>
        /// <returns>
        /// The value of the Parameters field in XAttribute collection format.
        /// </returns>
        /// <remarks>
        /// The idea behind this method: To allow pass data-attributes to the control as well as allowing override 'standard values' passed from C# code
        /// </remarks>
        [NotNull]
        protected virtual XAttributeDictionary GetParametersAsXAttributes(bool filter = true, bool keyToLowerCase = true, [NotNull] params XAttribute[] standardValueAttributes)
        {
            var attributes = new XAttributeDictionary(standardValueAttributes)
                                 {
                                     new XAttribute(
                                         "data-widget",
                                         this.GetType().Name)
                                 };

            if (!string.IsNullOrEmpty(this.Parameters))
            {
                NameValueCollection parameters = HttpUtility.ParseQueryString(this.Parameters);

                foreach (string collectionKey in parameters.AllKeys.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray())
                {
                    var key = keyToLowerCase ? collectionKey.ToLowerInvariant() : collectionKey;
                    if (filter)
                    {
                        if (key.StartsWith("data", StringComparison.InvariantCultureIgnoreCase))
                        {
                            attributes.Add(key, parameters[key]);
                        }
                    }
                    else
                    {
                        attributes.Add(key, parameters[key]);
                    }
                }

                return attributes;
            }

            return attributes;
        }

        /// <summary>
        /// Override to supply a LinkProvider other than the default LinkProvider.
        /// </summary>
        /// <returns>The link provider to use when resolving Item URLs.</returns>
        [NotNull]
        protected virtual LinkProvider GetLinkProvider()
        {
            return LinkManager.Provider;
        }

        /// <summary>
        /// Implementation of Sitecore's contract from WebControl. This is decorated with logic
        /// that allows the control to gracefully hide if no data is available for rendering.
        /// </summary>
        /// <param name="output">The HtmlTextWriter for the response.</param>
        /// <remarks>The reason why the methods RenderView and RenderPageEditorView are not abstract is: Provide less friction with Resharper intellisense and razor file compilation using JigRazorGenerator tool</remarks>
        // ReSharper disable FunctionComplexityOverflow
        protected override void DoRender([NotNull] HtmlTextWriter output)
        // ReSharper restore FunctionComplexityOverflow
        {
            var type = this.GetType();

            output.WriteLine();

            output.WriteLineNoTabs("<!-- Begin: " + type.Name + " -->");

            if (this.Visible && this.IsEnabled && !this.DesignMode)
            {
                try
                {
                    /* Do proper exception handling, write to inner writer  */
                    using (var innerWriter = new HtmlTextWriter(new StringWriter(new StringBuilder(512))))
                    {
                        if (global::Sitecore.Context.PageMode.IsPageEditorEditing)
                        {
                            this.RenderPageEditorView(innerWriter);
                        }
                        else
                        {
                            innerWriter.WriteCacheDiagnostics(
                                    this.Cacheable,
                                    this.VaryByData,
                                    this.VaryByDevice,
                                    this.VaryByLogin,
                                    this.VaryByParm,
                                    this.VaryByQueryString,
                                    this.VaryByUser,
                                    clearOnIndexUpdate: false,
                                    varyByCustom: this.CacheKey);

                            this.RenderView(innerWriter);
                        }

                        output.WriteLine(innerWriter.InnerWriter.ToString());
                    }
                }
                catch (Exception exception)
                {
                    Log.Error(exception.ToString(), this);
                    Trace.TraceError(exception.ToString());
                    var error = this.GetExceptionMessage(exception);
                    output.WriteLine(error);
                }
            }

            output.WriteLineNoTabs("<!-- End: " + type.Name + " -->");
        }

        /// <summary>
        /// Called from Sitecore's DoRender() contract, this method will be executed if the control
        /// is not cached and the control has data to render, unless in Page Editor, in which case 
        /// the logic will execute regardless.
        /// </summary>
        /// <param name="output">The HtmlTextWriter for the response.</param>
        protected virtual void RenderView([NotNull] HtmlTextWriter output)
        {
            var code = new XElement("code", string.Empty);
            code.Add(new XAttribute("class", "error"));
            code.Add(
                new XAttribute(
                    "data-error-message",
                    "Override " + this.GetType().FullName + ".RenderView() method"));

            output.WriteLine(code);
        }

        /// <summary>
        /// Called from Sitecore's DoRender() contract if the site is in PageEditorEditing mode.
        /// By default, calls RenderModel without the null check. Override locally with appropriate
        /// functionality.
        /// </summary>
        /// <param name="output">The HtmlTextWriter for the response.</param>
        protected virtual void RenderPageEditorView([NotNull] HtmlTextWriter output)
        {
            this.RenderView(output);
        }

        /// <summary>
        /// Gets the exception message.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>The exception message</returns>
        [NotNull]
        protected virtual XElement GetExceptionMessage([NotNull] Exception exception)
        {
            var attributes = new XElement("code", string.Empty);

            attributes.Add(new XAttribute("class", "exception"));
            attributes.Add(new XAttribute("data-exception-message", exception.Message));
            attributes.Add(new XAttribute("data-type-fullname", this.GetType().FullName));
            attributes.Add(new XAttribute("data-raw-url", global::Sitecore.Context.RawUrl));
            attributes.Add(new XAttribute("data-item-datasource", this.DataSource));

            return attributes;
        }

        #endregion
    }
}
