﻿namespace Jig.Sitecore.Views.WebForms
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Xml.Linq;
    using Jig.Sitecore.Data;

    using Jig.Sitecore.Views.WebForms.Presenters;
    using Jig.Web;
    using Jig.Web.Collections;
    using Jig.Web.Html;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;

    /// <summary>
    /// A View based on Sitecore's WebControl. T will be the Type used for the View's
    /// ViewModel, represented by the ViewModel property.
    /// </summary>
    /// <typeparam name="TModel">The Type to use for the View's ViewModel.</typeparam>
    public abstract class WebControlView<TModel> : WebControlView, IView<TModel>
        where TModel : class
    {
        #region Fields
        /// <summary>
        /// The model relevant to the view's context.
        /// </summary>
        private TModel model;

        /// <summary>
        /// The presenter.
        /// </summary>
        private IPresenter<TModel> presenter;
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets an instance of T relevant to the view's context.
        /// </summary>
        /// <remarks>
        /// The name "ViewModel" was chosen to allow forward-compatibility with MVC, which 
        /// uses "Model". Also note that the MVC "Model" knows a bit more about the back-end 
        /// than our "ViewModel", which is truly a dumb bucket.
        /// </remarks>
        [global::Sitecore.NotNull]
        public TModel Model
        {
            get
            {
                /* The control code will not pass execute render method if Model is null */
                var instance = this.Presenter;
#if !DEBUG
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (instance != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
#endif
                    // ReSharper disable AssignNullToNotNullAttribute
                    return this.model ?? (this.model = instance.GetModel(this, new HttpContextWrapper(this.Context)));
                    // ReSharper restore AssignNullToNotNullAttribute
#if !DEBUG
                }

                // ReSharper disable AssignNullToNotNullAttribute
                return null;
                // ReSharper restore AssignNullToNotNullAttribute
#endif
            }

            set
            {
                this.model = value;
            }
        }

        /// <summary>
        /// Gets or sets an instance of IPresenter capable of constructing an instance of TModel
        /// </summary>
        /// <remarks>
        /// This Property uses reflection internally to determine the correct presenter to
        /// load. If there is only one possible Presenter, override this property in your
        /// derived implementation and provide the correct presenter instance directly.
        /// </remarks>
        [NotNull]
        protected IPresenter<TModel> Presenter
        {
            get
            {
                return this.presenter ?? (this.presenter = this.GetPresenter());
            }

            set
            {
                this.presenter = value;
            }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Override to supply a specific presenter other than what the PresenterFactory will dynamically inject.
        /// </summary>
        /// <returns>
        /// The presenter.
        /// </returns>
        [global::Sitecore.NotNull]
        protected virtual IPresenter<TModel> GetPresenter()
        {
            IPresenter<TModel> presenterFactory = PresenterFactory.GetPresenter<TModel>();
            return presenterFactory;
        }

        /// <summary>
        /// Implementation of Sitecore's contract from WebControl. This is decorated with logic
        /// that allows the control to gracefully hide if no data is available for rendering.
        /// </summary>
        /// <param name="output">The HtmlTextWriter for the response.</param>
        protected sealed override void DoRender([global::Sitecore.NotNull] HtmlTextWriter output)
        {
            var type = this.GetType();
            Log.Debug("Start Rendering: " + type.FullName, this);

            output.WriteLine();

            output.WriteLineNoTabs("<!-- Begin: " + type.Name + " -->");
            if (this.Visible && this.IsEnabled && !this.DesignMode)
            {
                try
                {
                    // ReSharper disable ConditionIsAlwaysTrueOrFalse
                    if (this.Model == null)
                    // ReSharper restore ConditionIsAlwaysTrueOrFalse
                    // ReSharper disable HeuristicUnreachableCode
                    {
                        var item = global::Sitecore.Context.Item;
                        var sourceString = this.DataSource;

                        if (string.IsNullOrEmpty(sourceString))
                        {
                            sourceString = "context item " + item;
                        }

                        var msg =
                            string.Format(
                                "[Rendering {0}, Language: {1}, Database: {2} is not compatible with datasource: {3} or the datasource string returned null.]",
                                type.Name,
                                this.Language,
                                this.Database,
                                sourceString);

                        /* View thought the Sitecore */
                        Log.Debug(msg, this);

                        /* Record to logs  */
                        Log.Error(msg, this);

                        var elem = new XElement(
                            "code",
                            string.Empty,
                            new XAttribute("class", "exception"),
                            new XAttribute("data-exception-message", msg));

                        elem.Add(new XAttribute("data-item-id", item));
                        elem.Add(new XAttribute("data-item-datasource", sourceString));
                        elem.Add(new XAttribute("data-type-fullname", type.FullName));

                        output.WriteLine(elem);

                        Trace.TraceWarning(msg);
                    }
                    // ReSharper restore HeuristicUnreachableCode
                    else
                    {
                        /* Do proper exception handling, write to inner writer  */
                        using (var innerWriter = new HtmlTextWriter(new StringWriter(new StringBuilder(512))))
                        {
                            if (global::Sitecore.Context.PageMode.IsPageEditorEditing)
                            {
                                using (innerWriter.RenderDiv(cssClass: "__experience_view"))
                                {
                                    using (innerWriter.RenderDiv(cssClass: "web-control control"))
                                    {
                                        var view = type.GetCustomAttribute<ViewAttribute>();

                                        var viewDisplayName = view != null ? view.DisplayName : type.Name.Remove("View");

                                        using (innerWriter.RenderSection(cssClass: "__experience_view_info"))
                                        {
                                            innerWriter.Span(viewDisplayName);
                                            var item = this.Model as IStandardTemplate;
                                            if (item != null)
                                            {
                                                innerWriter.Write(" -- ");
                                                innerWriter.Span(item.DisplayName);
                                            }
                                        }
                                    }

                                    this.RenderPageEditorView(innerWriter);
                                }
                            }
                            else
                            {
                                innerWriter.WriteCacheDiagnostics(
                                        this.Cacheable,
                                        this.VaryByData,
                                        this.VaryByDevice,
                                        this.VaryByLogin,
                                        this.VaryByParm,
                                        this.VaryByQueryString,
                                        this.VaryByUser,
                                        clearOnIndexUpdate: false,
                                        varyByCustom: this.CacheKey);

                                this.RenderView(innerWriter);
                            }

                            output.WriteLine(innerWriter.InnerWriter.ToString());
                        }
                    }
                }
                catch (Exception exception)
                {
                    Log.Error(exception.ToString(), this);
                    Trace.TraceError(exception.ToString());
                    var msg = this.GetExceptionMessage(exception);
                    output.WriteLine(msg);
                }
            }

            output.WriteLineNoTabs("<!-- End: " + type.Name + " -->");
            Log.Debug("End Rendering: " + type.FullName, this);
        }

        /// <summary>
        /// Gets the exception message.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>
        /// The exception message
        /// </returns>
        [NotNull]
        protected override XElement GetExceptionMessage([NotNull] Exception exception)
        {
            var attributes = base.GetExceptionMessage(exception);
            attributes.Add(new XAttribute("data-model-type", typeof(TModel).FullName));

            var standardTemplate = this.Model as IStandardTemplate;
            if (standardTemplate != null)
            {
                attributes.Add(new XAttribute("data-item-id", standardTemplate.ID.Guid));
            }

            return attributes;
        }

        /// <summary>
        /// Gets the parameters as attributes.
        /// </summary>
        /// <param name="standardValueAttributes">The standard value attributes.</param>
        /// <returns>
        /// The idea behind this method: To allow pass data-attributes to the control as well as allowing override 'standard values' passed from C# code
        /// </returns>
        [NotNull]
        protected override XAttributeDictionary GetParametersAsXAttributes([NotNull] params XAttribute[] standardValueAttributes)
        {
            return this.GetParametersAsXAttributes(
                filter: true,
                keyToLowerCase: true,
                standardValueAttributes: standardValueAttributes);
        }

        /// <summary>
        /// Provides a quick way to access specific values in the query string style
        /// Parameters field on a Rendering definition.
        /// </summary>
        /// <param name="filter">if set to <c>true</c> [filter].</param>
        /// <param name="keyToLowerCase">if set to <c>true</c> [key to lower case].</param>
        /// <param name="standardValueAttributes">The standard value attributes.</param>
        /// <returns>
        /// The value of the Parameters field in XAttribute collection format.
        /// </returns>
        /// <remarks>
        /// The idea behind this method: To allow pass data-attributes to the control as well as allowing override 'standard values' passed from C# code
        /// </remarks>
        [NotNull]
        protected override XAttributeDictionary GetParametersAsXAttributes(
            bool filter = true,
            bool keyToLowerCase = true,
            [NotNull] params XAttribute[] standardValueAttributes)
        {
            var attributes = base.GetParametersAsXAttributes(filter, keyToLowerCase, standardValueAttributes);

            var standardTemplate = this.Model as IStandardTemplate;
            if (standardTemplate != null)
            {
                attributes.Add(new XAttribute("data-item-id", standardTemplate.ID.Guid));
            }

            return attributes;
        }

        #endregion
    }
}
