﻿namespace Jig.Sitecore.Views.WebForms
{
    using global::Sitecore;

    /// <summary>
    /// The Jig razor literal base.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    /// <typeparam name="TViewBag">The type of the view bag.</typeparam>
    public partial class RazorLiteralBase<TModel, TViewBag> : RazorLiteralBase<TModel>
        where TModel : class
        where TViewBag : class 
    {
        /// <summary>
        /// Gets or sets the view bag.
        /// </summary>
        /// <value>
        /// The view bag.
        /// </value>
        [CanBeNull]
        public TViewBag ViewBag { get; set; }
    }
}
