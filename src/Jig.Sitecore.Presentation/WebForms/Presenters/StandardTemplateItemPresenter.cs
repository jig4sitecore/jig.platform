﻿namespace Jig.Sitecore.Views.WebForms.Presenters
{
    using System.Web;
    using Jig.Sitecore.Views;

    using global::Sitecore;

    /// <summary>
    /// Converts the View's Datasource into the requested strongly-typed-Item.
    /// </summary>
    /// <typeparam name="TModel">The Type of Item to return.</typeparam>
    public sealed class StandardTemplateItemPresenter<TModel> : IPresenter<TModel>
        where TModel : class
    {
        /// <summary>
        /// Coverts the results of View.GetItem() into the requested strongly-typed-item.
        /// </summary>
        /// <param name="view">The View requiring the Item.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        /// An instance of TModel or null.
        /// </returns>
        [global::Sitecore.CanBeNull]
        public TModel GetModel([NotNull] IView<TModel> view, [NotNull] HttpContextBase context)
        {
            var item = view.GetItem();

            if (item == null)
            {
                return null;
            }

            var model = item.AsStronglyTyped(view.Language) as TModel;

            return model;
        }
    }
}
