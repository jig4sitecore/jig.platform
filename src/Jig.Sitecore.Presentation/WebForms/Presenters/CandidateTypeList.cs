﻿namespace Jig.Sitecore.Views.WebForms.Presenters
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    /// <summary>
    /// Creates a list of Types that descend from the Required Ancestor Type.
    /// The Type list is limited to non-abstract classes.
    /// </summary>
    public class CandidateTypeList
    {
        #region Fields
        /// <summary>
        /// The internal list of candidate types. Do not reference this list directly.
        /// </summary>
        private readonly HashSet<Type> candidateTypes = new HashSet<Type>();

        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="CandidateTypeList"/> class.
        /// </summary>
        /// <param name="requiredAncestorType">The Type that all members of the list must inherit.</param>
        public CandidateTypeList([NotNull] Type requiredAncestorType)
        {
            this.RequiredAncestorType = requiredAncestorType;
            this.CreateCandidateList();
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets an initialized list of types.
        /// </summary>
        [NotNull]
        public IEnumerable<Type> CandidateTypes
        {
            get
            {
                return this.candidateTypes;
            }
        }

        /// <summary>
        /// Gets the Type that all members of the list must inherit.
        /// </summary>
        [NotNull]
        public Type RequiredAncestorType { get; private set; }
        #endregion

        /// <summary>
        /// Gets types that can actually be loaded by reflection. Handles the case where a
        /// type prerequisite isn't in the currently running application.
        /// </summary>
        /// <remarks>
        /// See http://haacked.com/archive/2012/07/23/get-all-types-in-an-assembly.aspx for details.
        /// </remarks>
        /// <param name="assembly">The assembly to inspect.</param>
        /// <returns>A list of Types that can be loaded.</returns>
        [NotNull]
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Phil Haack is his name.")]
        private static IEnumerable<Type> GetLoadableTypes([NotNull] Assembly assembly)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException("assembly");
            }

            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                var msg = new StringBuilder(512);
                msg.Append("The IEnumerable<Type> GetLoadableTypes(Assembly assembly) failed load assembly: ")
                    .AppendLine(assembly.FullName);
                msg.Append(e);

                Log.Debug(msg.ToString(), typeof(CandidateTypeList));

                System.Diagnostics.Trace.TraceInformation(msg.ToString());
                return e.Types.Where(t => t != null);
            }
        }

        /// <summary>
        /// Determines if the candidate Type descends from the supplied ancestor Type.
        /// </summary>
        /// <param name="type">The Type to test.</param>
        /// <param name="ancestor">The Type that must be inherited.</param>
        /// <returns>True if the candidate Type inherits from the ancestor Type.</returns>
        private static bool DescendsFromAncestorType([NotNull] Type type, [NotNull] Type ancestor)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (type == null || type == typeof(object) || ancestor == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return false; // walked all the way up the inheritance tree with no match.
            }

            if (ancestor.IsAssignableFrom(type))
            {
                return true; // works for basic interface and class inheritance.
            }

            if (ancestor.IsGenericType && DescendsFromGeneric(type, ancestor))
            {
                return true;
            }

            // ReSharper disable AssignNullToNotNullAttribute
            return DescendsFromAncestorType(type.BaseType, ancestor);
            // ReSharper restore AssignNullToNotNullAttribute
        }

        /// <summary>
        /// Determines if the candidate Type implements the ancestor Generic.
        /// </summary>
        /// <param name="type">The Type to test.</param>
        /// <param name="ancestor">The Generic type that must be implemented.</param>
        /// <returns>True if the candidate Type implements the ancestor Type.</returns>
        private static bool DescendsFromGeneric([NotNull] Type type, [NotNull] Type ancestor)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (type == null || type == typeof(object) || ancestor == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return false; // We've reached the top of the inheritance stack.
            }

            if (type.IsGenericType && type.GetGenericTypeDefinition() == ancestor)
            {
                return true;
            }

            var interfaceTypes = type.GetInterfaces();

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var i in interfaceTypes) // ReSharper restore LoopCanBeConvertedToQuery
            {
                if (i.IsGenericType && i.GetGenericTypeDefinition() == ancestor)
                {
                    return true;
                }
            }

            // ReSharper disable AssignNullToNotNullAttribute
            return DescendsFromAncestorType(type.BaseType, ancestor);
            // ReSharper restore AssignNullToNotNullAttribute
        }

        /// <summary>
        /// Creates a list of Types that descend from <see cref="RequiredAncestorType"/>
        /// based on the current running assemblies.
        /// </summary>
        private void CreateCandidateList()
        {
            Log.Debug("Starting executing CandidateTypeList.CreateCandidateList()", typeof(CandidateTypeList));
            var assemblies = this.GetNonSitecoreCmsAssemblies();

            foreach (var assembly in assemblies)
            {
                Log.Debug("Scanning the assembly custom attributes: " + assembly.FullName, typeof(CandidateTypeList));
                var types = GetLoadableTypes(assembly);

                foreach (var type in types)
                {
                    if (type.IsClass && !type.IsAbstract && DescendsFromAncestorType(type, this.RequiredAncestorType))
                    {
                        this.candidateTypes.Add(type);
                        var msg = "Adding CandidateType: " + type.FullName;
                        Log.Debug(msg, typeof(CandidateTypeList));
                        System.Diagnostics.Trace.TraceInformation(msg);
                    }
                }
            }

            Log.Debug("End executing CandidateTypeList.CreateCandidateList()", typeof(CandidateTypeList));
        }

        /// <summary>
        /// The get assemblies.
        /// </summary>
        /// <returns>
        /// The collection of non sitecore assemblies
        /// </returns>
        [NotNull]
        private IEnumerable<Assembly> GetNonSitecoreCmsAssemblies()
        {
            var assemblies =
                AppDomain.CurrentDomain.GetAssemblies()
                    .Where(
                        x =>
                        x.FullName.Contains("View") && !x.FullName.StartsWith("System", StringComparison.OrdinalIgnoreCase))
                    .ToArray();

            return assemblies;
        }
    }
}
