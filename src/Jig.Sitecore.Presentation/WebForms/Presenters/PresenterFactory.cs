﻿namespace Jig.Sitecore.Views.WebForms.Presenters
{
    using System;
    using System.Collections.Generic;

    using Jig.Sitecore.Data;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    /// <summary>
    /// Inspects the currently running application for implementations of IPresenter.
    /// </summary>
    public static class PresenterFactory
    {
        #region Fields
        /// <summary>
        /// The list of candidate IPresenter implementations.
        /// </summary>
        private static CandidateTypeList presenterTypes;

        /// <summary>
        /// Gets the list of candidate IPresenter implementations.
        /// </summary>
        [NotNull]
        public static IEnumerable<Type> PresenterTypes
        {
            get
            {
                if (presenterTypes == null)
                {
                    Log.Debug("Starting executing PresenterFactory.PresenterTypes", typeof(PresenterFactory));
                    presenterTypes = new CandidateTypeList(typeof(IPresenter<>));
                    Log.Debug("End executing PresenterFactory.PresenterTypes", typeof(PresenterFactory));
                }

                return presenterTypes.CandidateTypes;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Inspects the currently running application for implementations of IPresenter and
        /// returns an instance of IPresenter where the generic property matches the requirements
        /// of the view.
        /// </summary>
        /// <typeparam name="TModel">The type of the Model required by the View.</typeparam>
        /// <returns>An instance of IPresenter&lt;T&gt;.</returns>
        [global::Sitecore.NotNull]
        public static IPresenter<TModel> GetPresenter<TModel>()
            where TModel : class
        {
            /*
             * First figure out if it's an IStandardItem model, in which case, we can
             * just return the default presenter
             */
            if (typeof(IStandardTemplate).IsAssignableFrom(typeof(TModel)))
            {
                return new StandardTemplateItemPresenter<TModel>();
            }

            // We go wading through the IPresenter candidates looking for a match.
            var desiredType = typeof(IPresenter<TModel>);
            foreach (var availableType in PresenterTypes)
            {
                if (desiredType.IsAssignableFrom(availableType))
                {
                    var presenter = Activator.CreateInstance(availableType) as IPresenter<TModel>;
                    if (presenter != null)
                    {
                        return presenter;
                    }
                }
            }

            return new StandardTemplateItemPresenter<TModel>();
        }

        #endregion
    }
}
