﻿namespace Jig.Sitecore.Views
{
    using System;
    using System.IO;
    using System.Web.UI;

    using global::Sitecore.Diagnostics;
    using global::Sitecore.Web.UI.WebControls;

    /// <summary>
    /// The edit frame rendering.
    /// </summary>
    /// <see href="https://visionsincode.wordpress.com/2015/01/08/how-to-use-editframe-in-sitecore-mvc/"/>
    public class EditFrameRendering : IDisposable
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EditFrameRendering" /> class.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="dataSource">The data source.</param>
        /// <param name="buttons">The buttons.</param>
        /// <see href="https://visionsincode.wordpress.com/2015/01/08/how-to-use-editframe-in-sitecore-mvc/"/>
        public EditFrameRendering(TextWriter writer, string dataSource, string buttons)
        {
            Assert.IsNotNull(writer, "writer");

            this.HtmlWriter = new HtmlTextWriter(writer);
            this.EditFrame = new EditFrame
                                 {
                                     DataSource = dataSource,
                                     Buttons = buttons
                                 };

            this.EditFrame.RenderFirstPart(this.HtmlWriter);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the edit frame.
        /// </summary>
        public EditFrame EditFrame { get; protected set; }

        /// <summary>
        /// Gets or sets the html writer.
        /// </summary>
        public HtmlTextWriter HtmlWriter { get; protected set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.EditFrame.RenderLastPart(this.HtmlWriter);
            this.HtmlWriter.Dispose();
        }

        #endregion
    }
}