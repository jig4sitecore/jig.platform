﻿namespace Jig.Sitecore.Shell.VersionFromLanguage.Commands
{
    using System;
    using Jig.Sitecore.Shell.VersionFromLanguage.Helpers;
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Globalization;
    using global::Sitecore.Shell.Framework.Commands;
    using global::Sitecore.Web.UI.Sheer;

    /// <summary>
    /// The add version recursive command.
    /// </summary>
    [Serializable]
    public sealed class AddVersionRecursiveCommand : Command
    {
        #region Public Methods and Operators

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public override void Execute([NotNull] CommandContext context)
        {
            Assert.ArgumentNotNull(context, "context");
            if (context.Items != null && context.Items.Length == 1)
            {
                global::Sitecore.Context.ClientPage.Start(this, "Run", context.Parameters);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="args">The args.</param>
        private void Run([NotNull] ClientPipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            var id = args.Parameters["id"];
            var sourceLang = Language.Parse(args.Parameters["sourceLang"]);
            var targetLang = Language.Parse(args.Parameters["targetLang"]);

            if (SheerResponse.CheckModified())
            {
                AddVersionHelper.AddVersionRecursive(ID.Parse(id), sourceLang, targetLang);
            }
        }

        #endregion
    }
}