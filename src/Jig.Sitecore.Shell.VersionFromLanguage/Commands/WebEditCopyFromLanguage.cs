﻿namespace Jig.Sitecore.Shell.VersionFromLanguage.Commands
{
    using System;
    using System.Globalization;
    using System.Runtime.InteropServices;
    using Jig.Sitecore.Shell.VersionFromLanguage.Helpers;
    using global::Sitecore;
    using global::Sitecore.Collections;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Data.Managers;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Globalization;
    using global::Sitecore.Shell.Framework.Commands;
    using global::Sitecore.Web.UI.HtmlControls;
    using global::Sitecore.Web.UI.Sheer;

    /// <summary>
    /// The web edit copy from language.
    /// </summary>
    [Serializable]
    [Guid("A64E9213-E9C3-494A-8563-736907AA100B")]
    public class WebEditCopyFromLanguage : global::Sitecore.Shell.Applications.WebEdit.Commands.WebEditCommand
    {
        #region Public Methods and Operators

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public override void Execute([NotNull] CommandContext context)
        {
            Assert.ArgumentNotNull(context, "context");
            if (context.Items.Length == 1)
            {
                Item currentItem = context.Items[0];
                LanguageCollection languages = LanguageManager.GetLanguages(currentItem.Database);

                SheerResponse.DisableOutput();
                var menu = new Menu();
                foreach (Language language in languages)
                {
                    Item item = currentItem.Database.GetItem(currentItem.ID, language);
                    if (item != null && item.Versions.GetVersionNumbers(false).Length == 0)
                    {
                        continue;
                    }

                    string id = "L" + ShortID.NewId();
                    string languageName = GetLanguageName(language.CultureInfo);
                    string icon = LanguageService.GetIcon(language, currentItem.Database);
                    string click = this.GetClick(currentItem.ID, language, currentItem.Language);
                    menu.Add(id, languageName, icon, string.Empty, click, false, string.Empty, MenuItemType.Normal);
                }

                SheerResponse.EnableOutput();
                SheerResponse.ShowPopup("CopyFromLanguageButton", "below", menu);
            }
        }

        /// <summary>
        /// The query state.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="CommandState"/>.
        /// </returns>
        public override CommandState QueryState([NotNull] CommandContext context)
        {
            Assert.ArgumentNotNull(context, "context");
            return QueryStateHelper.GetQueryState(context, base.QueryState);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the click.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="sourceLang">The source language.</param>
        /// <param name="targetLang">The target language.</param>
        /// <returns>The click event string</returns>
        [NotNull]
        protected virtual string GetClick(
            [NotNull]ID id,
            [NotNull]Language sourceLang,
            [NotNull]Language targetLang)
        {
            return string.Format(
                "platform:item:addversionrecursive(id={0},sourceLang={1},targetLang={2})",
                id,
                sourceLang,
                targetLang);
        }

        /// <summary>
        /// The get language name.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <returns>The <see cref="string" />.</returns>
        [NotNull]
        private static string GetLanguageName([NotNull] CultureInfo info)
        {
            Assert.ArgumentNotNull(info, "info");
            if (info.IsNeutralCulture)
            {
                info = Language.CreateSpecificCulture(info.Name);
            }

            string englishName = info.EnglishName;
            if (englishName.IndexOf("(", StringComparison.Ordinal) > 0)
            {
                englishName = StringUtil.Left(englishName, englishName.IndexOf("(", StringComparison.Ordinal)).Trim();
            }

            return englishName;
        }

        #endregion
    }
}