﻿namespace Jig.Sitecore.Shell.VersionFromLanguage.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Globalization;

    /// <summary>
    /// The web edit copy with descendants from language.
    /// </summary>
    [Serializable]
    [Guid("D4F3CCAA-1FA2-45D7-A0B1-FF6B8BC13D4D")]
    public class WebEditCopyWithDescendantsFromLanguage : WebEditCopyFromLanguage
    {
        /// <summary>
        /// Gets the click.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="sourceLang">The source language.</param>
        /// <param name="targetLang">The target language.</param>
        /// <returns>The click event string</returns>
        [NotNull]
        protected override string GetClick(
            [NotNull]ID id,
            [NotNull]Language sourceLang,
            [NotNull]Language targetLang)
        {
            return string.Format(
                "platform:item:addversionwithdescendantsrecursive(id={0},sourceLang={1},targetLang={2})",
                id,
                sourceLang,
                targetLang);
        }
    }
}