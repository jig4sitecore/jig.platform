﻿namespace Jig.Sitecore.Shell.VersionFromLanguage.Commands
{
    using System;
    using System.Runtime.InteropServices;
    using Jig.Sitecore.Shell.VersionFromLanguage.Helpers;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Shell.Framework.Commands;

    /// <summary>
    /// The ribbon copy from language.
    /// </summary>
    [Serializable]
    [Guid("C1AE4FB0-3C44-4B42-A704-D972546039C5")]
    public sealed class RibbonCopyFromLanguage : Command
    {
        #region Public Methods and Operators

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">The context.</param>
        public override void Execute([NotNull] CommandContext context)
        {
        }

        /// <summary>
        /// The get click.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="click">The click.</param>
        /// <returns>
        /// The <see cref="string" />.
        /// </returns>
        [NotNull]
        public override string GetClick([NotNull] CommandContext context, [NotNull] string click)
        {
            return string.Empty;
        }

        /// <summary>
        /// The query state.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>
        /// The <see cref="CommandState" />.
        /// </returns>
        public override CommandState QueryState([NotNull] CommandContext context)
        {
            Assert.ArgumentNotNull(context, "context");
            return QueryStateHelper.GetQueryState(context, base.QueryState);
        }

        #endregion
    }
}