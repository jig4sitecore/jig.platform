﻿namespace Jig.Sitecore.Shell.VersionFromLanguage.GalleryForm
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using global::Sitecore;
    using global::Sitecore.Configuration;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Data.Managers;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Globalization;
    using global::Sitecore.Shell;
    using global::Sitecore.Web;
    using global::Sitecore.Web.UI.HtmlControls;
    using global::Sitecore.Web.UI.Sheer;
    using global::Sitecore.Web.UI.XmlControls;

    /// <summary>
    /// The gallery languages form.
    /// </summary>
    public class GalleryLanguagesForm : global::Sitecore.Shell.Applications.ContentManager.Galleries.GalleryForm
    {
        #region Fields

        /// <summary>
        /// The languages.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1306:FieldNamesMustBeginWithLowerCaseLetter", Justification = "Reviewed. Suppression is OK here."), SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Reviewed. Suppression is OK here."), UsedImplicitly]
        // ReSharper disable UnassignedField.Global
        protected Scrollbox Languages;
        // ReSharper restore UnassignedField.Global

        /// <summary>
        /// The options.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1306:FieldNamesMustBeginWithLowerCaseLetter", Justification = "Reviewed. Suppression is OK here."), SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Reviewed. Suppression is OK here."), UsedImplicitly]
        protected GalleryMenu Options;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The handle message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public override void HandleMessage([NotNull] Message message)
        {
            Assert.ArgumentNotNull(message, "message");
            if (message.Name != "event:click")
            {
                this.Invoke(message, true);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnLoad([NotNull] EventArgs e)
        {
            Assert.ArgumentNotNull(e, "e");
            base.OnLoad(e);
            if (!global::Sitecore.Context.ClientPage.IsEvent)
            {
                Item currentItem = GetCurrentItem();
                if (currentItem != null)
                {
                    foreach (Language language in currentItem.Languages)
                    {
                        ID languageItemId = LanguageManager.GetLanguageItemId(language, currentItem.Database);
                        if (!ItemUtil.IsNull(languageItemId))
                        {
                            Item languageItem = currentItem.Database.GetItem(languageItemId);
                            if (languageItem == null || !languageItem.Access.CanRead())
                            {
                                continue;
                            }

                            if (languageItem.Appearance.Hidden && !UserOptions.View.ShowHiddenItems)
                            {
                                continue;
                            }
                        }

                        Item item = currentItem.Database.GetItem(currentItem.ID, language);
                        if (item != null)
                        {
                            int length = item.Versions.GetVersionNumbers(false).Length;
                            if (length == 0)
                            {
                                continue;
                            }

                            var control = ControlFactory.GetControl("Gallery.Languages.Option") as XmlControl;
                            if (control != null)
                            {
                                control["Icon"] = LanguageService.GetIcon(language, currentItem.Database);
                                control["Header"] = Language.GetDisplayName(language.CultureInfo);
                                control["Description"] = (length == 1)
                                                             ? Translate.Text("1 version.")
                                                             : Translate.Text("{0} versions.", length);
                                control["Click"] = this.GetClick(currentItem.ID, language, currentItem.Language);

                                control["ClassName"] = language.Name.Equals(
                                    WebUtil.GetQueryString("la"),
                                    StringComparison.OrdinalIgnoreCase)
                                                           ? "scMenuPanelItemSelected"
                                                           : "scMenuPanelItem";
                                global::Sitecore.Context.ClientPage.AddControl(this.Languages, control);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the click.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="sourceLang">The source language.</param>
        /// <param name="targetLang">The target language.</param>
        /// <returns>The click event string</returns>
        [NotNull]
        protected virtual string GetClick(
            [NotNull]ID id,
            [NotNull] Language sourceLang,
            [NotNull]Language targetLang)
        {
            return string.Format(
                "platform:item:addversionrecursive(id={0},sourceLang={1},targetLang={2})",
                id,
                sourceLang,
                targetLang);
        }

        /// <summary>
        /// The get current item.
        /// </summary>
        /// <returns>
        /// The <see cref="Item"/>.
        /// </returns>
        [CanBeNull]
        private static Item GetCurrentItem()
        {
            string queryString = WebUtil.GetQueryString("db");
            string path = WebUtil.GetQueryString("id");

            Language language = Language.Parse(WebUtil.GetQueryString("la"));
            var version = global::Sitecore.Data.Version.Parse(WebUtil.GetQueryString("vs"));
            Database database = Factory.GetDatabase(queryString);

            Assert.IsNotNull(database, queryString);
            return database.GetItem(path, language, version);
        }

        #endregion
    }
}