﻿namespace Jig.Sitecore.Shell.VersionFromLanguage.GalleryForm
{
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Globalization;

    /// <summary>
    /// The gallery languages with Descendants form.
    /// </summary>
    public class GalleryLanguagesWithDescendantsForm : GalleryLanguagesForm
    {
        /// <summary>
        /// Gets the click.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="sourceLang">The source language.</param>
        /// <param name="targetLang">The target language.</param>
        /// <returns>The click event string</returns>
        [NotNull]
        protected override string GetClick(
            [NotNull] ID id,
            [NotNull] Language sourceLang, 
            [NotNull] Language targetLang)
        {
            return string.Format(
                "platform:item:addversionwithdescendantsrecursive(id={0},sourceLang={1},targetLang={2})",
                id,
                sourceLang,
                targetLang);
        }
    }
}