﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: Guid("6BC2EC49-9E59-4898-B025-4074027D3096")]
[assembly: AssemblyTitle("Jig4Sitecore Framework")]
[assembly: AssemblyDescription("The library extending the Sitecore CMS.")]
[assembly: AssemblyProduct("Jig4Sitecore Framework")]
[assembly: AssemblyCopyright("Copyright © Jig4Sitecore Team 2014")]
[assembly: AssemblyTrademark("Jig")]

[assembly: ComVisible(false)]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyVersion("2.0.0")]
[assembly: AssemblyFileVersion("2.0.0")]
