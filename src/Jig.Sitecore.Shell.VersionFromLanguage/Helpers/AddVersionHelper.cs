﻿namespace Jig.Sitecore.Shell.VersionFromLanguage.Helpers
{
    using System.Collections.Generic;
    using System.Linq;
    using Jig.Sitecore.Sites.Shared.Data.Interfaces.Rules;
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Globalization;
    using global::Sitecore.Layouts;

    /// <summary>
    /// Class AddVersionHelper.
    /// </summary>
    internal static class AddVersionHelper
    {
        /// <summary>
        /// Adds the version recursive.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="sourceLang">The source language.</param>
        /// <param name="targetLang">The target language.</param>
        public static void AddVersionRecursive([NotNull] ID id, [NotNull] Language sourceLang, [NotNull] Language targetLang)
        {
            var linkSet = new HashSet<ID>();

            Item item = Context.ContentDatabase.GetItem(id, targetLang);
            if (item == null)
            {
                return;
            }

            if (Context.IsAdministrator
                || (item.Access.CanWrite() && (item.Locking.CanLock() || item.Locking.HasLock())))
            {
                AddAllReferences(item, linkSet);

                LayoutField layoutField = item.Fields[FieldIDs.LayoutField];
                if (!string.IsNullOrEmpty(layoutField.Value))
                {
                    LayoutDefinition layout = LayoutDefinition.Parse(layoutField.Value);

                    foreach (DeviceDefinition device in layout.Devices)
                    {
                        foreach (RenderingDefinition rendering in device.Renderings)
                        {
                            Item datasourceItem = Context.ContentDatabase.GetItem(
                                rendering.Datasource ?? string.Empty,
                                targetLang);
                            if (datasourceItem == null)
                            {
                                continue;
                            }

                            AddAllReferences(datasourceItem, linkSet);
                        }
                    }
                }

                foreach (ID linkId in linkSet)
                {
                    CopyVersion(linkId, sourceLang, targetLang);
                }
            }
        }

        /// <summary>
        /// Adds the version with descendants recursive.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="sourceLang">The source language.</param>
        /// <param name="targetLang">The target language.</param>
        public static void AddVersionWithDescendantsRecursive([NotNull] ID id, [NotNull] Language sourceLang, [NotNull] Language targetLang)
        {
            var linkSet = new HashSet<ID>();

            Item rootItem = Context.ContentDatabase.GetItem(id, targetLang);
            if (rootItem == null)
            {
                return;
            }

            var items = new[] { rootItem }.Concat(rootItem.Axes.GetDescendants());

            foreach (var item in items)
            {
                if (Context.IsAdministrator
                    || (item.Access.CanWrite() && (item.Locking.CanLock() || item.Locking.HasLock())))
                {
                    AddAllReferences(item, linkSet);

                    LayoutField layoutField = item.Fields[FieldIDs.LayoutField];
                    if (!string.IsNullOrEmpty(layoutField.Value))
                    {
                        LayoutDefinition layout = LayoutDefinition.Parse(layoutField.Value);

                        foreach (DeviceDefinition device in layout.Devices)
                        {
                            foreach (RenderingDefinition rendering in device.Renderings)
                            {
                                Item datasourceItem =
                                    Context.ContentDatabase.GetItem(rendering.Datasource ?? string.Empty, targetLang);
                                if (datasourceItem == null)
                                {
                                    continue;
                                }

                                AddAllReferences(datasourceItem, linkSet);

                                foreach (Item childItem in datasourceItem.Children)
                                {
                                    AddAllReferences(childItem, linkSet);
                                }
                            }
                        }
                    }
                }
            }

            foreach (ID linkId in linkSet)
            {
                CopyVersion(linkId, sourceLang, targetLang);
            }
        }

        /// <summary>
        /// The add all references.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="linkSet">The link set</param>
        private static void AddAllReferences([NotNull] Item item, [NotNull] HashSet<ID> linkSet)
        {
            if (linkSet.Add(item.ID))
            {
                foreach (Item reference in item.Links.GetValidLinks(false).Select(il => il.GetTargetItem()))
                {
                    if (reference.Paths.IsContentItem || reference.Paths.IsMediaItem)
                    {
                        linkSet.Add(reference.ID);
                    }
                }
            }
        }

        /// <summary>
        /// The copy version.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="sourceLang">The source lang.</param>
        /// <param name="targetLang">The target lang.</param>
        private static void CopyVersion([NotNull] ID id, [NotNull] Language sourceLang, [NotNull] Language targetLang)
        {
            Item source = global::Sitecore.Context.ContentDatabase.GetItem(id, sourceLang);
            Item target = global::Sitecore.Context.ContentDatabase.GetItem(id, targetLang);

            if (source == null || target == null || target.Versions.Count > 0)
            {
                return;
            }

            var template = global::Sitecore.Context.ContentDatabase.GetTemplate(typeof(INotTranslatable));
            if (template != null && source.IsDerived(template))
            {
                return;
            }

            target.Versions.AddVersion();
            target.Editing.BeginEdit();
            foreach (Field field in source.Fields)
            {
                if (!field.Shared)
                {
                    target[field.Name] = source[field.Name];
                }
            }

            target.Editing.EndEdit();
        }
    }
}
