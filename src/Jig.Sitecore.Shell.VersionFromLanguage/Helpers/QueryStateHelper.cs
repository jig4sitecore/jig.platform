﻿﻿namespace Jig.Sitecore.Shell.VersionFromLanguage.Helpers
 {
     using System;
     using System.Linq;

     using global::Sitecore;
     using global::Sitecore.Data.Items;
     using global::Sitecore.Shell.Framework.Commands;

     using Jig.Sitecore.Sites.Shared.Data.Interfaces.Rules;

     /// <summary>
     /// Helper to determine query state
     /// </summary>
     internal static class QueryStateHelper
     {
         /// <summary>
         /// Gets the query state for the command.
         /// </summary>
         /// <param name="context">The context.</param>
         /// <param name="fallbackFunc">The fallback function.</param>
         /// <returns>The CommandState</returns>
         public static CommandState GetQueryState([NotNull] CommandContext context, [NotNull] Func<CommandContext, CommandState> fallbackFunc)
         {
             if (context.Items.Length != 1)
             {
                 return CommandState.Hidden;
             }

             var item = context.Items[0];
             if (item == null)
             {
                 return CommandState.Hidden;
             }

             CommandState commandState;
             if (IsItemCommandState(context.Items[0], out commandState))
             {
                 return commandState;
             }

             return fallbackFunc(context);
         }

         internal static bool IsItemCommandState([NotNull]Item item, out CommandState commandState)
         {
             if (
                 new[] { global::Sitecore.TemplateIDs.Template, global::Sitecore.TemplateIDs.TemplateSection, global::Sitecore.TemplateIDs.TemplateField }.Contains(
                     item.TemplateID))
             {
                 commandState = CommandState.Hidden;
                 return true;
             }

             if (item.Versions.Count > 0)
             {
                 commandState = CommandState.Disabled;
                 return true;
             }

             if (item.Appearance.ReadOnly)
             {
                 commandState = CommandState.Disabled;
                 return true;
             }

             if (!item.Access.CanWrite())
             {
                 commandState = CommandState.Disabled;
                 return true;
             }

             if (!item.Locking.CanLock() && !item.Locking.HasLock())
             {
                 commandState = CommandState.Disabled;
                 return true;
             }

             if (!item.Access.CanWriteLanguage())
             {
                 commandState = CommandState.Disabled;
                 return true;
             }

             var notTranslatableTemplate =
                 item.Template.BaseTemplates.Any(
                     x =>
                     x.ID.Guid == typeof(INotTranslatable).GUID
                     || x.BaseTemplates.Any(y => y.ID.Guid == typeof(INotTranslatable).GUID));

             if (notTranslatableTemplate)
             {
                 commandState = CommandState.Disabled;
                 return true;
             }

             commandState= CommandState.Enabled;
             return false;
         }
     }
 }
