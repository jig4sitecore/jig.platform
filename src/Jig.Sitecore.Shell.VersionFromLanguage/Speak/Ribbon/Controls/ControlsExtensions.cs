﻿namespace Jig.Sitecore.Shell.VersionFromLanguage.Speak.Ribbon.Controls
{
    using System.Web;

    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Mvc;
    using global::Sitecore.Mvc.Presentation;

    /// <summary>
    /// Class ControlsExtensions.
    /// </summary>
    public static class ControlsExtensions
    {
        /// <summary>
        /// Larges the drop down button.
        /// </summary>
        /// <param name="controls">The controls.</param>
        /// <param name="rendering">The rendering.</param>
        /// <returns>Html String.</returns>
        [NotNull]
        public static HtmlString LargeDropDownButton([NotNull] this Controls controls, [NotNull] Rendering rendering)
        {
            Assert.ArgumentNotNull(controls, "controls");
            Assert.ArgumentNotNull(rendering, "rendering");
            return new HtmlString(new LargeDropDownButton(controls.GetParametersResolver(rendering)).Render());
        }
    }
}
