﻿namespace Jig.Sitecore.Shell.VersionFromLanguage.Speak.Ribbon.Controls
{
    using System.Web.UI;

    using global::Sitecore;
    using global::Sitecore.Configuration;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.ExperienceEditor.Speak.Caches;
    using global::Sitecore.ExperienceEditor.Speak.Ribbon;
    using global::Sitecore.Resources;
    using global::Sitecore.Web.UI;

    /// <summary>
    /// The select options command item.
    /// </summary>
    public class SelectOptionsCommandItem : RibbonComponentControlBase
    {
        /// <summary>
        /// The default data bind.
        /// </summary>
        protected const string DefaultDataBind = "visible: isVisible, click: click, enabled: isEnabled";

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectOptionsCommandItem" /> class.
        /// </summary>
        /// <param name="dataSourceItem">The data source item.</param>
        /// <param name="click">The click.</param>
        /// <param name="command">The command.</param>
        /// <param name="renderAsThumbnail">The render as thumbnail.</param>
        public SelectOptionsCommandItem([NotNull] Item dataSourceItem, [NotNull] string click, [NotNull] string command, bool renderAsThumbnail)
        {
            Assert.IsNotNull(dataSourceItem, "SelectOptionsCommandItem - dataSourceItem");
            this.SourceItem = dataSourceItem;
            this.Click = click;
            this.Command = command;
            this.RenderAsThumbnail = renderAsThumbnail;
            this.InitializeControl();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectOptionsCommandItem"/> class.
        /// </summary>
        protected SelectOptionsCommandItem()
        {
        }

        /// <summary>
        /// Gets or sets the click.
        /// </summary>
        [NotNull]
        protected string Click { get; set; }

        /// <summary>
        /// Gets or sets the command.
        /// </summary>
        [NotNull]
        protected string Command { get; set; }

        /// <summary>
        /// Gets or sets the source item.
        /// </summary>
        [NotNull]
        protected Item SourceItem { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether render as thumbnail.
        /// </summary>
        protected bool RenderAsThumbnail { get; set; }

        /// <summary>
        /// The initialize control.
        /// </summary>
        protected void InitializeControl()
        {
            Assert.IsNotNull(this.SourceItem, "SourceItem on {0} is missing", (object)this.ControlId);
            this.Class = "sc-insertItem-container";
            this.DataBind = DefaultDataBind;

            ResourcesCache.RequireJs(this, "command", this.Command + ".js");
            ResourcesCache.RequireJs(this, "ribbon", "SelectOptionsCommandItem.js");
        }

        /// <summary>
        /// The pre render.
        /// </summary>
        protected override void PreRender()
        {
            base.PreRender();
            this.Attributes["data-sc-click"] = this.Click;
            this.Attributes["data-sc-command"] = this.Command;
            this.Attributes["data-sc-displayname"] = this.SourceItem.DisplayName;
            this.Attributes["data-sc-itemid"] = this.SourceItem.ID.ToShortID().ToString();
        }

        /// <summary>
        /// The render.
        /// </summary>
        /// <param name="output">
        /// The output.
        /// </param>
        protected override void Render([NotNull] HtmlTextWriter output)
        {
            base.Render(output);
            this.AddAttributes(output);
            output.AddAttribute(HtmlTextWriterAttribute.Class, "sc-insertItem-container");
            output.AddAttribute(HtmlTextWriterAttribute.Href, "#");
            output.RenderBeginTag(HtmlTextWriterTag.A);
            output.AddAttribute(HtmlTextWriterAttribute.Class, "sc-insertItem-imageContainer");
            output.RenderBeginTag(HtmlTextWriterTag.Div);
            output.AddAttribute(HtmlTextWriterAttribute.Alt, this.SourceItem.DisplayName);
            output.AddAttribute(HtmlTextWriterAttribute.Src, this.GetIconPath(this.SourceItem));
            output.RenderBeginTag(HtmlTextWriterTag.Img);
            output.RenderEndTag();
            output.RenderEndTag();
            output.AddAttribute(HtmlTextWriterAttribute.Class, "sc-insertItem-displayNameContainer");
            output.RenderBeginTag(HtmlTextWriterTag.Div);
            output.AddAttribute(HtmlTextWriterAttribute.Class, "sc-display-name");
            output.RenderBeginTag(HtmlTextWriterTag.Span);
            output.Write(this.SourceItem.DisplayName);
            output.RenderEndTag();
            output.RenderEndTag();
            output.RenderEndTag();
        }

        /// <summary>
        /// The get icon path.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>The <see cref="string" />.</returns>
        [NotNull]
        protected string GetIconPath([NotNull] Item item)
        {
            if (!this.RenderAsThumbnail)
            {
                return Images.GetThemedImageSource(item.Appearance.Icon, ImageDimension.id48x48);
            }

            if (item.Appearance.Thumbnail == Settings.DefaultThumbnail)
            {
                return Images.GetThemedImageSource(item.Appearance.Icon, ImageDimension.id48x48);
            }

            return UIUtil.GetThumbnailSrc(item, 128, 128);
        }
    }
}