﻿namespace Jig.Sitecore.Shell.VersionFromLanguage.Speak.Ribbon.Controls
{
    using System.Web.UI;

    using global::Sitecore;
    using global::Sitecore.Configuration;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.ExperienceEditor.Speak.Caches;
    using global::Sitecore.ExperienceEditor.Speak.Ribbon.Controls;
    using global::Sitecore.Mvc.Presentation;
    using global::Sitecore.Resources;
    using global::Sitecore.Web.UI;

    /// <summary>
    /// Class LargeDropDownButton.
    /// </summary>
    public class LargeDropDownButton : RibbonIconButtonBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LargeDropDownButton" /> class.
        /// </summary>
        public LargeDropDownButton()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LargeDropDownButton"/> class.
        /// </summary>
        /// <param name="renderingParametersResolver">The rendering parameters resolver.</param>
        public LargeDropDownButton([NotNull] RenderingParametersResolver renderingParametersResolver)
            : base(renderingParametersResolver)
        {
        }

        /// <summary>
        /// Gets or sets the list data source item.
        /// </summary>
        /// <value>The list data source item.</value>
        [NotNull]
        protected Item ListDataSourceItem { get; set; }

        /// <summary>
        /// Gets or sets the name of the datasource database.
        /// </summary>
        /// <value>The name of the datasource database.</value>
        [NotNull]
        protected string DatasourceDatabaseName { get; set; }

        /// <summary>
        /// Initializes the control.
        /// </summary>
        /// <param name="renderingParametersResolver">The rendering parameters resolver.</param>
        protected override void InitializeControl([NotNull] RenderingParametersResolver renderingParametersResolver)
        {
            if (string.IsNullOrEmpty(this.DataSource))
            {
                this.DataSource = renderingParametersResolver.GetString("DataSource", string.Empty);
            }

            Assert.IsNotNullOrEmpty(this.DataSource, "Datasource on {0} is missing", (object)this.ControlId);
            this.ControlId = this.DataSourceItem[global::Sitecore.ExperienceEditor.Speak.Ribbon.Constants.Fields.Id]
                             + "_" + new ID();
            this.Class = "sc-chunk-button-dropdown";
            this.DataBind =
                "ispressed: isPressed, visible: isVisible, click: click, command: command, enabled: isEnabled";

            ResourcesCache.RequireJs(this, "ribbon", "LargeDropDownButton.js");
            var @string = renderingParametersResolver.GetString("Database", string.Empty);
            Assert.IsNotNullOrEmpty(@string, "Datasource database is null or empty.");

            this.DatasourceDatabaseName = @string;
            var obj =
                Factory.GetDatabase(this.DatasourceDatabaseName)
                    .GetItem(renderingParametersResolver.GetId("ListDataSourceItemId", ID.Null).ToString());

            Assert.IsNotNull(obj, "List datasource item is null.");
            this.ListDataSourceItem = obj;
            this.Label = this.DataSourceItem[global::Sitecore.ExperienceEditor.Speak.Ribbon.Constants.Fields.Header];
            this.IconPath =
                Images.GetThemedImageSource(
                    this.DataSourceItem[global::Sitecore.ExperienceEditor.Speak.Ribbon.Constants.Fields.Icon],
                    ImageDimension.id24x24);

            ResourcesCache.Add(this.IconPath);
            this.Tooltip = this.DataSourceItem[global::Sitecore.ExperienceEditor.Speak.Ribbon.Constants.Fields.Tooltip];
            this.HasNestedComponents = false;
            this.InitializeAttributes(renderingParametersResolver);
        }

        /// <summary>
        /// Initializes the attributes.
        /// </summary>
        /// <param name="renderingParametersResolver">The rendering parameters resolver.</param>
        protected void InitializeAttributes([NotNull] RenderingParametersResolver renderingParametersResolver)
        {
            var string1 = renderingParametersResolver.GetString("IconAndLabelRequest", string.Empty);
            var string2 = renderingParametersResolver.GetString("DefaultListItemCommand", string.Empty);
            var string3 = renderingParametersResolver.GetString("RetrieveListItemsRequest", string.Empty);
            var @bool = renderingParametersResolver.GetBool("ShowIcon", true);
            this.Attributes["data-sc-iconlabelrequest"] = string1;
            this.Attributes["data-sc-listdatasourceid"] = this.ListDataSourceItem.ID.ToString();
            this.Attributes["data-sc-datasourcedatabase"] = this.DatasourceDatabaseName;
            this.Attributes["data-sc-defaultlistitemcommand"] = string2;
            this.Attributes["data-sc-retrievelistitemsrequest"] = string3;
            this.Attributes["data-sc-showicon"] = @bool ? "1" : "0";
        }

        /// <summary>
        /// Renders the specified output.
        /// </summary>
        /// <param name="output">The output.</param>
        protected override void Render([NotNull] HtmlTextWriter output)
        {
            base.Render(output);
            this.AddAttributes(output);
            output.AddAttribute(HtmlTextWriterAttribute.Href, "#");
            output.AddAttribute(HtmlTextWriterAttribute.Title, this.Tooltip);
            output.RenderBeginTag(HtmlTextWriterTag.A);
            output.AddAttribute(HtmlTextWriterAttribute.Src, this.IconPath);
            output.AddAttribute(HtmlTextWriterAttribute.Alt, this.Tooltip);
            output.RenderBeginTag(HtmlTextWriterTag.Img);
            output.RenderEndTag();
            output.AddAttribute(HtmlTextWriterAttribute.Class, "sc-chunk-button-label");
            output.RenderBeginTag(HtmlTextWriterTag.Span);
            output.Write(ClientHost.Globalization.Translate(this.Label));
            output.RenderEndTag();
            output.RenderEndTag();
        }
    }
}