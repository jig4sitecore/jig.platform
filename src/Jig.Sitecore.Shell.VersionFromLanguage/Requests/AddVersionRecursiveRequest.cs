﻿namespace Jig.Sitecore.Shell.VersionFromLanguage.Requests
{
    using Jig.Sitecore.Shell.VersionFromLanguage.Helpers;

    using global::Sitecore;

    using global::Sitecore.Data;

    using global::Sitecore.ExperienceEditor.Speak.Server.Contexts;

    using global::Sitecore.ExperienceEditor.Speak.Server.Requests;

    using global::Sitecore.ExperienceEditor.Speak.Server.Responses;

    using global::Sitecore.Globalization;

    /// <summary>
    /// Class AddVersionRecursiveRequest.
    /// </summary>
    public class AddVersionRecursiveRequest : PipelineProcessorRequest<ItemContext>
    {
        /// <summary>
        /// Processes the request.
        /// </summary>
        /// <returns>Pipeline Processor Response Value.</returns>
        [NotNull]
        public override PipelineProcessorResponseValue ProcessRequest()
        {
            var langId = this.RequestContext.Argument;
            var langItem = this.RequestContext.Item.Database.GetItem(ID.Parse(langId));
            var sourceLang = Language.Parse(langItem.Name);
            var targetLang = Language.Parse(this.RequestContext.Language);

            AddVersionHelper.AddVersionRecursive(this.RequestContext.Item.ID, sourceLang, targetLang);

            return new PipelineProcessorResponseValue();
        }
    }
}
