﻿namespace Jig.Sitecore.Shell.VersionFromLanguage.Requests
{
    using System.Collections.Generic;

    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Data.Managers;
    using global::Sitecore.ExperienceEditor.Speak.Ribbon.Requests.LargeDropDownButton;
    using global::Sitecore.ExperienceEditor.Speak.Server.Responses;
    using global::Sitecore.Globalization;
    using global::Sitecore.Shell;

    /// <summary>
    /// Class GetLanguagesRequest.
    /// </summary>
    public class GetLanguagesRequest : LargeDropDownButtonChildItems
    {
        /// <summary>
        /// Processes the request.
        /// </summary>
        /// <returns>Pipeline Processor Response Value.</returns>
        [NotNull]
        public override PipelineProcessorResponseValue ProcessRequest()
        {
            var currentItem = this.RequestContext.Item;
            var languages = new List<object>();

            foreach (Language language in currentItem.Languages)
            {
                ID languageItemId = LanguageManager.GetLanguageItemId(language, currentItem.Database);
                Item languageItem = currentItem.Database.GetItem(languageItemId);

                if (languageItem == null || !languageItem.Access.CanRead())
                {
                    continue;
                }

                if (languageItem.Appearance.Hidden && !UserOptions.View.ShowHiddenItems)
                {
                    continue;
                }

                Item item = currentItem.Database.GetItem(currentItem.ID, language);
                if (item != null)
                {
                    int length = item.Versions.GetVersionNumbers(false).Length;
                    if (length == 0)
                    {
                        continue;
                    }

                    languages.Add(
                        new
                            {
                                Title =
                                    Language.GetDisplayName(language.CultureInfo) + " "
                                    + ((length == 1)
                                           ? Translate.Text("1 version.")
                                           : Translate.Text("{0} versions.", length)),
                                ItemId = languageItemId.ToString(),
                                IconPath = this.GetIcon(languageItem)
                            });
                }
            }

            return new PipelineProcessorResponseValue { Value = languages };
        }
    }
}
