﻿namespace Jig.Sitecore.Shell.VersionFromLanguage.Requests
{
    using Jig.Sitecore.Shell.VersionFromLanguage.Helpers;

    using global::Sitecore;

    using global::Sitecore.ExperienceEditor.Speak.Server.Contexts;

    using global::Sitecore.ExperienceEditor.Speak.Server.Requests;

    using global::Sitecore.ExperienceEditor.Speak.Server.Responses;

    using global::Sitecore.Shell.Framework.Commands;

    /// <summary>
    /// Class CanExecuteRequest.
    /// </summary>
    public class CanExecuteRequest : PipelineProcessorRequest<ItemContext>
    {
        /// <summary>
        /// Processes the request.
        /// </summary>
        /// <returns>Pipeline Processor Response Value.</returns>
        [NotNull]
        public override PipelineProcessorResponseValue ProcessRequest()
        {
            this.RequestContext.ValidateContextItem();

            CommandState commandState;
            if (QueryStateHelper.IsItemCommandState(this.RequestContext.Item, out commandState))
            {
                return new PipelineProcessorResponseValue { Value = false };
            }

            return new PipelineProcessorResponseValue { Value = true };
        }
    }
}
