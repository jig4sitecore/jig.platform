﻿namespace Jig.Sitecore.Data.Fields.Pipelines.LinkPropertyRenders
{
    /// <summary>
    /// The link property external link render.
    /// </summary>
    public sealed class ExternalLinkRender : ILinkPropertyRender
    {
        #region Public Methods and Operators

        /// <summary>
        /// The render.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process(LinkPropertyRenderPipelineArgs args)
        {
            if (!args.Aborted && string.IsNullOrWhiteSpace(args.Href) && args.LinkProperty.IsExternal)
            {
                var href = args.LinkProperty.GetLinkUrl();
                args.Href = args.GetLinkWithQueryStringAndAnchor(href);

                if (string.IsNullOrWhiteSpace(args.Target))
                {
                    args.Target = "_blank";
                }
            }
        }

        #endregion
    }
}