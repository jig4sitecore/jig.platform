﻿namespace Jig.Sitecore.Data.Fields.Pipelines.LinkPropertyRenders
{
    using Jig.Web.Html;

    /// <summary>
    /// Link Html Output Render. This class cannot be inherited.
    /// </summary>
    public sealed class LinkHtmlOutputRender : ILinkPropertyRender
    {
        #region Public Methods and Operators

        /// <summary>
        /// The render.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process(LinkPropertyRenderPipelineArgs args)
        {
            if (!args.Aborted && string.IsNullOrWhiteSpace(args.Output))
            {
                using (var output = new XhtmlTagTextWriter())
                {
                    output.A(
                               innerHtml: args.InnerHtml,
                               href: string.IsNullOrWhiteSpace(args.Href) ? "#empty-link" : args.Href,
                               cssClass: args.CssClass,
                               target: args.Target,
                               title: args.Title,
                               attributes: args.Attributes.Values);

                    args.Output = output.Html;
                }
            }
        }

        #endregion
    }
}