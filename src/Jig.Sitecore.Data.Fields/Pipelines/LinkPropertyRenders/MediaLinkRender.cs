﻿namespace Jig.Sitecore.Data.Fields.Pipelines.LinkPropertyRenders
{
    using global::Sitecore.Data.Items;

    /// <summary>
    /// The link property media link render.
    /// </summary>
    public sealed class MediaLinkRender : ILinkPropertyRender
    {
        #region Public Methods and Operators

        /// <summary>
        /// The render.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process(LinkPropertyRenderPipelineArgs args)
        {
            if (!args.Aborted && string.IsNullOrWhiteSpace(args.Href) && args.LinkProperty.IsMediaLink)
            {
                MediaItem mediaItem = args.LinkProperty.TargetItem;
                if (mediaItem == null)
                {
                    return;
                }

                args.InnerHtml = string.IsNullOrEmpty(args.InnerHtml)
                                   ? (string.IsNullOrWhiteSpace(args.LinkProperty.Text) ? mediaItem.Description : args.LinkProperty.Text)
                                   : args.InnerHtml;

                var href = args.LinkProperty.GetLinkUrl();
                args.Href = args.GetLinkWithQueryStringAndAnchor(href);
            }
        }

        #endregion
    }
}