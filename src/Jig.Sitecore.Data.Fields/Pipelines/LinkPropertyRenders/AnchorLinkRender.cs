﻿namespace Jig.Sitecore.Data.Fields.Pipelines.LinkPropertyRenders
{
    /// <summary>
    /// The link property anchor link render.
    /// </summary>
    public sealed class AnchorLinkRender : ILinkPropertyRender
    {
        #region Public Methods and Operators

        /// <summary>
        /// The render.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process(LinkPropertyRenderPipelineArgs args)
        {
            if (!args.Aborted && string.IsNullOrWhiteSpace(args.Href) && args.LinkProperty.IsAnchor)
            {
                args.Href = args.Anchor;
            }
        }

        #endregion
    }
}