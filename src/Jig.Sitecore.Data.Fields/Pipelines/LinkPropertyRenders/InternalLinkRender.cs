﻿namespace Jig.Sitecore.Data.Fields.Pipelines.LinkPropertyRenders
{
    using System;

    using Jig.Web.Html;

    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;

    /// <summary>
    /// The link property internal link render.
    /// </summary>
    /// <remarks>The field Id is static for performance as this field is heavy hitter. If the logic doesn't match clone the pipe, modify code and replace in config</remarks>
    public sealed class InternalLinkRender : ILinkPropertyRender
    {
        /// <summary> The Field Info:
        ///     <para>Field Type: Single-Line Text</para>
        ///     <para>Field Name: BrowserTitle</para>
        ///     <para>Field Path: /sitecore/templates/Sites/Shared/Metadata/Interfaces/Page Browser Title/Page - Information and Metadata/BrowserTitle</para>
        /// </summary>
        public static readonly ID BrowserTitleId = new ID("8e27ce2b-a4cb-420f-8d11-bee3bbedbcd8");

        /// <summary> The Field Info:
        ///     <para>Field Type: Single-Line Text</para>
        ///     <para>Field Name: Page Menu Title</para>
        ///     <para>Field Path: /sitecore/templates/Sites/Shared/Metadata/Interfaces/Page Menu Title/Page - Information and Metadata/Page Menu Title</para>
        /// </summary>
        public static readonly ID PageMenuTitleId = new ID("8f95e656-d6d5-45c7-9634-3a291638f816");

        #region Public Methods and Operators

        /// <summary>
        /// The render.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process(LinkPropertyRenderPipelineArgs args)
        {
            if (!args.Aborted && string.IsNullOrWhiteSpace(args.Href) && args.LinkProperty.IsInternal)
            {
                if (string.IsNullOrWhiteSpace(args.InnerHtml))
                {
                    args.InnerHtml = this.GetBestNavigationTitle(args.LinkProperty.TargetItem);
                }

                var href = args.LinkProperty.GetLinkUrl();
                args.Href = args.GetLinkWithQueryStringAndAnchor(href);

                /* Info attribute */
                args.Attributes.Add(new DataAttribute("target-id", (args.LinkProperty.TargetID ?? new ID(Guid.Empty)).Guid.ToString("D")));
            }
        }

        /// <summary>
        /// Determines the browser title based on all title fields available on the item.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <returns>The browser title.</returns>
        [NotNull]
        private string GetBestNavigationTitle([CanBeNull] Item page)
        {
            if (page == null)
            {
                return string.Empty;
            }

            var title = page[PageMenuTitleId];
            if (!string.IsNullOrEmpty(title))
            {
                return title;
            }

            title = page[BrowserTitleId];
            if (!string.IsNullOrEmpty(title))
            {
                return title;
            }

            return page.DisplayName;
        }

        #endregion
    }
}