﻿namespace Jig.Sitecore.Data.Fields.Pipelines.LinkPropertyRenders
{
    /// <summary>
    /// The link property telephone link render.
    /// </summary>
    public sealed class TelephoneLinkRender : ILinkPropertyRender
    {
        #region Public Methods and Operators

        /// <summary>
        /// The render.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process(LinkPropertyRenderPipelineArgs args)
        {
            if (!args.Aborted && string.IsNullOrWhiteSpace(args.Href) && args.LinkProperty.IsTelephone)
            {
                args.InnerHtml = args.LinkProperty.Text;

                args.Href = args.LinkProperty.Url;
                if (string.IsNullOrEmpty(args.Title))
                {
                    args.Title = args.Href;
                }

                if (string.IsNullOrWhiteSpace(args.Target))
                {
                    args.Target = "_blank";
                }
            }
        }

        #endregion
    }
}