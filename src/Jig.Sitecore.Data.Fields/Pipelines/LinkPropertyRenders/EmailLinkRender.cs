﻿namespace Jig.Sitecore.Data.Fields.Pipelines.LinkPropertyRenders
{
    /// <summary>
    /// The link property email link render.
    /// </summary>
    public sealed class EmailLinkRender : ILinkPropertyRender
    {
        #region Public Methods and Operators

        /// <summary>
        /// The render.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process(LinkPropertyRenderPipelineArgs args)
        {
            if (!args.Aborted && string.IsNullOrWhiteSpace(args.Href) && args.LinkProperty.IsEmail)
            {
                args.Href = args.LinkProperty.Url;

                if (string.IsNullOrWhiteSpace(args.Target))
                {
                    args.Target = "_blank";
                }
            }
        }

        #endregion
    }
}