﻿namespace Jig.Sitecore.Data.Fields.Pipelines.LinkPropertyRenders
{
    using global::Sitecore;

    /// <summary>
    /// The link render extensions.
    /// </summary>
    public static class LinkRenderExtensions
    {
        /// <summary>
        /// The get link with query string and anchor.
        /// </summary>
        /// <param name="args">The args.</param>
        /// <param name="href">The href.</param>
        /// <returns>The formated link</returns>
        public static string GetLinkWithQueryStringAndAnchor(this LinkPropertyRenderPipelineArgs args, [CanBeNull] string href)
        {
            if (string.IsNullOrWhiteSpace(href))
            {
                href = string.Empty;
            }

            if (!string.IsNullOrWhiteSpace(args.QueryString))
            {
                var queryString = args.QueryString.TrimStart('?');
                if (href.Contains("?"))
                {
                    href += '&' + queryString;
                }
                else
                {
                    href += '?' + queryString;
                }
            }

            if (!string.IsNullOrWhiteSpace(args.Anchor))
            {
                href += '#' + args.Anchor.TrimStart('#');
            }

            return href;
        }
    }
}