﻿namespace Jig.Sitecore.Data.Fields
{
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Globalization;
    using global::Sitecore.Links;

    /// <summary>
    /// Facade for Sitecore File Field
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    public class FileProperty : XmlProperty
    {
        /// <summary>
        /// The file field to wrap.
        /// </summary>
        private readonly FileField fileField;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FileProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The field to wrap.
        /// </param>
        public FileProperty([NotNull] Field field)
            : base(field)
        {
            this.fileField = field;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the database containing the the media.
        /// </summary>
        /// <value>The media database.</value>
        /// <contract><requires name="value" condition="not null" /><ensures condition="not null" /></contract>
        [NotNull]
        public Database MediaDatabase
        {
            get
            {
                return this.fileField.MediaDatabase;
            }
        }

        /// <summary>
        /// Gets the language of the media item to use. 
        /// </summary>
        /// <value>
        /// The media language.
        /// </value>
        /// <contract><requires name="value" condition="not null"/><ensures condition="not null"/></contract>
        [NotNull]
        public Language MediaLanguage
        {
            get
            {
                return this.fileField.MediaLanguage;
            }
        }

        /// <summary>
        /// Gets the ID of the media item.
        /// </summary>
        /// <value>
        /// The media ID.
        /// </value>
        /// <contract><requires name="value" condition="not null"/><ensures condition="not null"/></contract>
        // ReSharper disable InconsistentNaming
        [NotNull]
        public ID MediaID
        {
            // ReSharper restore InconsistentNaming
            get
            {
                return this.fileField.MediaID;
            }
        }

        /// <summary>
        /// Gets the media item.
        /// </summary>
        /// <value>
        /// The media item.
        /// </value>
        [NotNull]
        public Item MediaItem
        {
            get
            {
                return this.fileField.MediaItem;
            }
        }

        /// <summary>
        /// Gets the version of the media item to use.
        /// </summary>
        /// <value>
        /// The media version.
        /// </value>
        /// <contract><requires name="value" condition="not null"/><ensures condition="nullable"/></contract>
        [NotNull]
        public Version MediaVersion
        {
            get
            {
                return this.fileField.MediaVersion;
            }
        }

        /// <summary>
        /// Gets the source path.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        /// <contract><requires name="value" condition="not null"/><ensures condition="not null"/></contract>
        [NotNull]
        public string Src
        {
            get
            {
                return this.fileField.Src;
            }
        }
        #endregion

        #region Operators

        /// <summary>
        /// Allows interoperability with Sitecore FileField.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        /// <returns>A new instance of FileProperty based on the supplied field.</returns>
        public static implicit operator FileProperty([NotNull]FileField field)
        {
            return new FileProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore FileField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.FileField([NotNull]FileProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator FileProperty([NotNull]Field field)
        {
            return new FileProperty(field);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Re-links the specified item.
        /// </summary>
        /// <param name="itemLink">The item link.</param>
        /// <param name="newLink">The new link.</param>
        /// <contract><requires name="itemLink" condition="not null" /><requires name="newLink" condition="not null" /></contract>
        public override void Relink([NotNull] ItemLink itemLink, [NotNull] Item newLink)
        {
            this.fileField.Relink(itemLink, newLink);
        }

        /// <summary>
        /// Removes the link.
        /// </summary>
        /// <param name="itemLink">
        /// The item link.
        /// </param>
        /// <contract><requires name="itemLink" condition="not null"/></contract>
        public override void RemoveLink([NotNull] ItemLink itemLink)
        {
            this.fileField.RemoveLink(itemLink);
        }

        /// <summary>
        /// Updates the link.
        /// </summary>
        /// <param name="itemLink">
        /// The link.
        /// </param>
        /// <contract><requires name="itemLink" condition="not null"/></contract>
        public override void UpdateLink([NotNull] ItemLink itemLink)
        {
            this.fileField.UpdateLink(itemLink);
        }

        /// <summary>
        /// Validates the links.
        /// </summary>
        /// <param name="result">
        /// The result.
        /// </param>
        /// <contract><requires name="result" condition="not null"/></contract>
        public override void ValidateLinks([NotNull] LinksValidationResult result)
        {
            this.fileField.ValidateLinks(result);
        }

        #endregion
    }
}
