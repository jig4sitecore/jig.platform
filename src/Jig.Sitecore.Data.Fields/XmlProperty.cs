﻿namespace Jig.Sitecore.Data.Fields
{
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Linq;

    using global::Sitecore;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Web.UI.WebControls;

    /// <summary>
    /// Wraps a Sitecore XmlField.
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    public class XmlProperty : FieldProperty
    {
        /// <summary>
        /// The field to wrap.
        /// </summary>
        private readonly XmlField xmlField;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The field to wrap.
        /// </param>
        public XmlProperty([NotNull] Field field)
            : base(field)
        {
            this.xmlField = field;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the root tag.
        /// </summary>
        /// <value>
        /// The root.
        /// </value>
        [CanBeNull]
        public string Root
        {
            get
            {
                return this.xmlField.Root;
            }

            set
            {
                this.xmlField.Root = value;
            }
        }

        /// <summary>
        /// Gets the XML document contained in this field value.
        /// </summary>
        /// <value>
        /// The XML.
        /// </value>
        [NotNull]
        public XmlDocument Xml
        {
            get { return this.xmlField.Xml; }
        }
        #endregion

        #region Operators

        /// <summary>
        /// Allows interoperability with XmlDocument.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The Property's XML value.</returns>
        public static implicit operator XmlDocument([NotNull]XmlProperty property)
        {
            return property.Xml;
        }

        /// <summary>
        /// Allows interoperability with Sitecore XmlField.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A new instance of XmlProperty.</returns>
        public static implicit operator XmlProperty([NotNull]XmlField field)
        {
            return new XmlProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore XmlField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.XmlField([NotNull]XmlProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator XmlProperty([NotNull]Field field)
        {
            return new XmlProperty(field);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the attribute.
        /// </summary>
        /// <param name="name">
        /// The attribute name.
        /// </param>
        /// <returns>
        /// The attribute.
        /// </returns>
        [NotNull]
        public string GetAttribute([NotNull] string name)
        {
            return this.xmlField.GetAttribute(name);
        }

        /// <summary>
        /// Sets the attribute.
        /// </summary>
        /// <param name="name">
        /// The attribute name.
        /// </param>
        /// <param name="value">
        /// The attribute value.
        /// </param>
        public void SetAttribute([NotNull] string name, [NotNull] string value)
        {
            this.xmlField.SetAttribute(name, value);
        }

        /// <summary>
        /// Gets the data attributes.
        /// </summary>
        /// <returns>The data attributes</returns>
        [NotNull]
        public IEnumerable<XAttribute> GetDataAttributes()
        {
            if (!string.IsNullOrWhiteSpace(this.xmlField.Value))
            {
                var root = this.xmlField.Xml.FirstChild;
                if (root != null)
                {
                    XmlAttributeCollection attributes = root.Attributes;
                    if (attributes != null && attributes.Count > 0)
                    {
                        foreach (XmlAttribute attr in attributes)
                        {
                            if (!string.IsNullOrWhiteSpace(attr.Value) && attr.Name.StartsWith("data-"))
                            {
                                var dataAttr = new XAttribute(attr.Name, attr.Value.Trim());
                                yield return dataAttr;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The field render.
        /// </summary>
        /// <returns>The Sitecore Non-Page Editor Mode Html</returns>
        [NotNull]
        public virtual global::Jig.Sitecore.MvcHtmlString Render()
        {
            if (this.HasValue)
            {
                var html = FieldRenderer.Render(this.Item, this.Name, "disable-web-editing=true");

                return html;
            }

            return string.Empty;
        }

        #endregion
    }
}
