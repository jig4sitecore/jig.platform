﻿namespace Jig.Sitecore.Data.Fields.RenderField
{
    using System;

    using global::Sitecore;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Pipelines.RenderField;

    /// <summary>
    /// The set href if telephone link.
    /// </summary>
    public class SetHrefIfTelephoneLink
    {
        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process([NotNull] RenderFieldArgs args)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (args != null && args.FieldTypeKey.IndexOf("link", StringComparison.InvariantCultureIgnoreCase) > -1)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                LinkField linkField = args.Item.Fields[args.FieldName];
                if (!string.IsNullOrEmpty(linkField.Url) && linkField.LinkType == "telephone")
                {
                    args.Parameters["href"] = linkField.Url;
                }
            }
        }
    }
}