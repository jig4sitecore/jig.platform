﻿namespace Jig.Sitecore.Data.Fields
{
    using System.Diagnostics.CodeAnalysis;

    using global::Sitecore;
    using global::Sitecore.Data.Fields;

    /// <summary>
    /// Wraps a Sitecore GroupedDroplinkField
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Name of Sitecore field.")]
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    public class GroupedDroplinkProperty : LookupProperty
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupedDroplinkProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The field to wrap.
        /// </param>
        public GroupedDroplinkProperty([NotNull] Field field)
            : base(field)
        {
            // nothing to do!
        }

        #endregion

        #region Operators

        /// <summary>
        /// Allows interoperability with Sitecore GroupedDroplinkField.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        /// <returns>A new instance of GroupedDroplinkProperty using the supplied field.</returns>
        public static implicit operator GroupedDroplinkProperty([NotNull] GroupedDroplinkField field)
        {
            return new GroupedDroplinkProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore GroupedDroplinkField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.GroupedDroplinkField([NotNull]GroupedDroplinkProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator GroupedDroplinkProperty(Field field)
        {
            return new GroupedDroplinkProperty(field);
        }

        #endregion
    }
}
