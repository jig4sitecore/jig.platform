﻿namespace Jig.Sitecore.Data.Fields
{
    using System;

    using global::Sitecore;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Web.UI.WebControls;

    /// <summary>
    /// Facade for the Sitecore DateField.
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    public class DateProperty : FieldProperty
    {
        /// <summary>
        /// The Sitecore field to wrap.
        /// </summary>
        private readonly DateField dateField;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DateProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The Field to wrap.
        /// </param>
        public DateProperty([NotNull] Field field)
            : base(field)
        {
            this.dateField = field;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the value as a <see cref="T:System.DateTime"/>.
        /// </summary>
        /// <value>
        /// The date time.
        /// </value>
        public DateTime DateTime
        {
            get { return this.dateField.DateTime; }
        }

        #endregion

        #region Operators

        /// <summary>
        /// Converts a DateProperty into a DateTime.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.DateTime value.</returns>
        public static implicit operator DateTime(DateProperty property)
        {
            return property.DateTime;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore DateFields.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A new instance of DateProperty based on the field.</returns>
        public static implicit operator DateProperty([NotNull]DateField field)
        {
            return new DateProperty(field.InnerField);
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore DateFields.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator global::Sitecore.Data.Fields.DateField([NotNull]DateProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator DateProperty([NotNull]Field field)
        {
            return new DateProperty(field);
        }

        #endregion

        /// <summary>
        /// Gets the date.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <returns>The date with format</returns>
        [NotNull]
        public string GetDate([NotNull] string format = "d")
        {
            if (this.HasValue)
            {
                return this.DateTime.ToString(format);
            }

            return string.Empty;
        }

        /// <summary>
        /// The field render.
        /// </summary>
        /// <returns>The Sitecore Non-Page Editor Mode Html</returns>
        [NotNull]
        public virtual global::Jig.Sitecore.MvcHtmlString Render()
        {
            if (this.HasValue)
            {
                var html = FieldRenderer.Render(this.Item, this.Name, "disable-web-editing=true");

                return html;
            }

            return string.Empty;
        }
    }
}
