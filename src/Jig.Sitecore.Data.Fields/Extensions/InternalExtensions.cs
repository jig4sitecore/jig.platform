﻿namespace Jig.Sitecore.Data.Fields.Pipelines
{
    using global::Sitecore;

    /// <summary>
    /// The internal extensions.
    /// </summary>
    internal static class InternalExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The ensure starts with if not empty.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="character">The character.</param>
        /// <returns>The input with specific char as prefix.</returns>
        [NotNull]
        public static string EnsureStartsWithIfNotEmpty([NotNull] this string value, char character)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                if (value[0] != character)
                {
                    var newValue = character + value;
                    return newValue;
                }
            }

            return string.Empty;
        }

        #endregion
    }
}