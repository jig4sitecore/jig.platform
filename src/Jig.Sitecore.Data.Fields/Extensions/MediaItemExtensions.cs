﻿namespace Jig.Sitecore.Data
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Xml.Linq;
    using Jig.Web.Html;
    using global::Sitecore;
    using global::Sitecore.Data.Items;

    /// <summary>
    /// Media Item extensions
    /// </summary>
    public static class MediaItemViewExtensions
    {
        /// <summary>
        /// Renders the specified output.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="output">The output.</param>
        /// <param name="dataAttributes">The attributes.</param>
        public static void Render(
            [NotNull] this MediaItem mediaItem,
            [NotNull] HtmlTextWriter output,
            [NotNull] params XAttribute[] dataAttributes)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (output != null)
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse

                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (mediaItem != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    var mediaUrl = mediaItem.GetMediaItemUrl();
                    var alt = string.IsNullOrWhiteSpace(mediaItem.Alt) ? mediaItem.InnerItem.DisplayName : mediaItem.Alt;

                    output.Img(src: mediaUrl, alt: alt, title: alt, attributes: dataAttributes);
                }
                else
                // ReSharper disable HeuristicUnreachableCode
                {
                    var code = new XElement("code", string.Empty);

                    code.Add(new XAttribute("class", "error"));
                    code.Add(new XAttribute("data-error-message", "The media resource is missing!"));
                    output.Write(code);
                }
                // ReSharper restore HeuristicUnreachableCode
            }
        }

        /// <summary>
        /// Renders the specified output.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="output">The output.</param>
        /// <param name="dataAttributes">The data attributes.</param>
        public static void Render(
            [NotNull] this MediaItem mediaItem,
            [NotNull] HtmlTextWriter output,
            [NotNull] IEnumerable<XAttribute> dataAttributes)
        {
            // ReSharper disable ConstantNullCoalescingCondition
            mediaItem.Render(output, (dataAttributes ?? new XAttribute[] { }).ToArray());
            // ReSharper restore ConstantNullCoalescingCondition
        }

        /// <summary>
        /// Renders the specified output.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="output">The output.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="dataAttributes">The attributes.</param>
        public static void Render(
            [NotNull] this MediaItem mediaItem,
            [NotNull] HtmlTextWriter output,
            int width,
            int height,
            [NotNull] params XAttribute[] dataAttributes)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (output != null)
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse

                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (mediaItem != null) // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    var mediaUrl = mediaItem.GetMediaItemUrl(width, height);
                    var alt = string.IsNullOrWhiteSpace(mediaItem.Alt) ? mediaItem.InnerItem.DisplayName : mediaItem.Alt;

                    output.Img(src: mediaUrl, alt: alt, title: alt, attributes: dataAttributes);
                }
                else
                // ReSharper disable HeuristicUnreachableCode
                {
                    var code = new XElement("code", string.Empty);

                    code.Add(new XAttribute("class", "error"));
                    code.Add(new XAttribute("data-error-message", "The media resource is missing!"));
                    output.Write(code);
                }
                // ReSharper restore HeuristicUnreachableCode
            }
        }

        /// <summary>
        /// Renders the specified output.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="output">The output.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="dataAttributes">The attributes.</param>
        public static void Render(
            [NotNull] this MediaItem mediaItem,
            [NotNull] HtmlTextWriter output,
            int width,
            int height,
            [NotNull] IEnumerable<XAttribute> dataAttributes)
        {
            // ReSharper disable ConstantNullCoalescingCondition
            mediaItem.Render(output, width, height, (dataAttributes ?? new XAttribute[] { }).ToArray());
            // ReSharper restore ConstantNullCoalescingCondition
        }

        /// <summary>
        /// Renders the specified data attributes.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="dataAttributes">The data attributes.</param>
        /// <returns>
        /// The Html Output
        /// </returns>
        [NotNull]
        public static MvcHtmlString Render([NotNull] this MediaItem mediaItem, int width, int height, [NotNull] params XAttribute[] dataAttributes)
        {
            using (var output = new XhtmlTagTextWriter())
            {
                mediaItem.Render(output, width, height, dataAttributes);
                return output.Html;
            }
        }

        /// <summary>
        /// Renders the specified data attributes.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="dataAttributes">The data attributes.</param>
        /// <returns>
        /// The Html Output
        /// </returns>
        [NotNull]
        public static MvcHtmlString Render(
            [NotNull] this MediaItem mediaItem,
            int width,
            int height,
            [NotNull] IEnumerable<XAttribute> dataAttributes)
        {
            using (var output = new XhtmlTagTextWriter())
            {
                mediaItem.Render(output, width, height, dataAttributes);
                return output.Html;
            }
        }

        /// <summary>
        /// Renders the specified data attributes.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="dataAttributes">The data attributes.</param>
        /// <returns>
        /// The Html Output
        /// </returns>
        [NotNull]
        public static MvcHtmlString Render([NotNull] this MediaItem mediaItem, [NotNull] params XAttribute[] dataAttributes)
        {
            using (var output = new XhtmlTagTextWriter())
            {
                mediaItem.Render(output, dataAttributes);
                return output.Html;
            }
        }

        /// <summary>
        /// Renders the specified data attributes.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="dataAttributes">The data attributes.</param>
        /// <returns>
        /// The Html Output
        /// </returns>
        [NotNull]
        public static MvcHtmlString Render([NotNull] this MediaItem mediaItem, [NotNull] IEnumerable<XAttribute> dataAttributes)
        {
            using (var output = new XhtmlTagTextWriter())
            {
                mediaItem.Render(output, dataAttributes);
                return output.Html;
            }
        }
    }
}
