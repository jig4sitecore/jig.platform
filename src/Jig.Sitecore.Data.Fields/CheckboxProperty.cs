﻿namespace Jig.Sitecore.Data.Fields
{
    using global::Sitecore;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Web.UI.WebControls;

    /// <summary>
    /// Facade for the Sitecore CheckboxField.
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    // ReSharper disable ClassWithVirtualMembersNeverInherited.Global
    public class CheckboxProperty : FieldProperty
    // ReSharper restore ClassWithVirtualMembersNeverInherited.Global
    {
        /// <summary>
        /// The Field to wrap.
        /// </summary>
        private readonly CheckboxField checkboxField;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckboxProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The Sitecore field to wrap.
        /// </param>
        public CheckboxProperty([NotNull] Field field)
            : base(field)
        {
            this.checkboxField = field;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Jig.Sitecore.Data.Fields.CheckboxProperty"/> is checked.
        /// </summary>
        /// <value>
        /// <c>true</c> if checked; otherwise, <c>false</c>.
        /// </value>
        public bool Checked
        {
            get { return this.checkboxField.Checked; }
        }
        #endregion

        #region Operators

        /// <summary>
        /// Converts a checkbox property to a Boolean value.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The value of property.Checked.</returns>
        public static implicit operator bool(CheckboxProperty property)
        {
            return property.Checked;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore CheckboxFields.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A new instance of CheckboxProperty based on the Field.</returns>
        public static implicit operator CheckboxProperty([NotNull]CheckboxField field)
        {
            return new CheckboxProperty(field.InnerField);
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore CheckboxFields.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator global::Sitecore.Data.Fields.CheckboxField([NotNull]CheckboxProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator CheckboxProperty([NotNull]Field field)
        {
            return new CheckboxProperty(field);
        }

        /// <summary>
        /// The field render.
        /// </summary>
        /// <returns>The Sitecore Non-Page Editor Mode Html</returns>
        [NotNull]
        public virtual global::Jig.Sitecore.MvcHtmlString Render()
        {
            if (this.HasValue)
            {
                var html = FieldRenderer.Render(this.Item, this.Name, "disable-web-editing=true");

                return html;
            }

            return string.Empty;
        }

        #endregion
    }
}
