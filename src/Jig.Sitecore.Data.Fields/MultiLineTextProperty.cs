﻿namespace Jig.Sitecore.Data.Fields
{
    using global::Sitecore;
    using global::Sitecore.Data.Fields;

    /// <summary>
    /// Wraps a Sitecore TextField.
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)] 
    public partial class MultilineTextProperty : FieldProperty
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MultilineTextProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The field to wrap.
        /// </param>
        public MultilineTextProperty([NotNull] Field field)
            : base(field)
        {
            // Nothing to do.
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the FieldRenderer output for this property.
        /// </summary>
        [NotNull]
        public string Text
        {
            get
            {
                return this.InnerField.Value;
            }
        }
        #endregion

        #region Operators

        /// <summary>
        /// Allows interoperability with string.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.Text value.</returns>
        public static implicit operator string(MultilineTextProperty property)
        {
            return property.Text;
        }

        /// <summary>
        /// Allows interoperability with Sitecore TextField.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        /// <returns>A new instance of MultilineTextProperty using the supplied field.</returns>
        public static implicit operator MultilineTextProperty([NotNull] TextField field)
        {
            return new MultilineTextProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore TextField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.TextField([NotNull]MultilineTextProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator MultilineTextProperty([NotNull] Field field)
        {
            return new MultilineTextProperty(field);
        }

        #endregion

        /// <summary>
        /// The field render.
        /// </summary>
        /// <returns>The Sitecore Non-Page Editor Mode Html</returns>
        [NotNull]
        public MvcHtmlString Render()
        {
            if (this.HasValue)
            {
                var html = this.Value;

                return html;
            }

            return string.Empty;
        }
    }
}
