﻿namespace Jig.Sitecore.Data.Fields
{
    using System.Collections.Generic;

    using global::Sitecore;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Links;
    using global::Sitecore.Web.UI.WebControls;

    /// <summary>
    /// Wraps a Sitecore HtmlField.
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)] 
    public class HtmlProperty : FieldProperty
    {
        /// <summary>
        /// The HtmlField to wrap.
        /// </summary>
        private readonly HtmlField htmlField;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The field to wrap.
        /// </param>
        public HtmlProperty([NotNull] Field field)
            : base(field)
        {
            this.htmlField = field;
        }

        #endregion

        #region Operators

        /// <summary>
        /// Allows interoperability with Sitecore HtmlField.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        /// <returns>A new instance of HtmlProperty using the supplied field.</returns>
        public static implicit operator HtmlProperty([NotNull]HtmlField field)
        {
            return new HtmlProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore HtmlField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.HtmlField([NotNull]HtmlProperty property)
        {
            return property.InnerField;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Tightens the relative image links.
        /// </summary>
        /// <param name="html">The HTML.</param>
        /// <returns>The relative image links.</returns>
        /// <contract><requires name="html" condition="not empty" /><ensures condition="not null" /></contract>
        [NotNull]
        public static string TightenRelativeImageLinks([NotNull] string html)
        {
            return global::Sitecore.Data.Fields.HtmlField.TightenRelativeImageLinks(html);
        }

        /// <summary>
        /// Gets the plain text.
        /// </summary>
        /// <returns>The value stripped of all HTML elements.</returns>
        [NotNull]
        public virtual string GetPlainText()
        {
            return this.htmlField.GetPlainText();
        }

        /// <summary>
        /// Gets the web edit buttons.
        /// </summary>
        /// <returns>The web edit buttons.</returns>
        /// <contract><ensures condition="nullable" /></contract>
        [NotNull]
        public override List<WebEditButton> GetWebEditButtons()
        {
            return this.htmlField.GetWebEditButtons();
        }

        /// <summary>
        /// Re-links the specified item.
        /// </summary>
        /// <param name="itemLink">The item link.</param>
        /// <param name="newLink">The new link.</param>
        /// <contract><requires name="itemLink" condition="not null" /><requires name="newLink" condition="not null" /></contract>
        public override void Relink([NotNull] ItemLink itemLink, [NotNull] Item newLink)
        {
            this.htmlField.Relink(itemLink, newLink);
        }

        /// <summary>
        /// Removes the link.
        /// </summary>
        /// <param name="itemLink">
        /// The item link.
        /// </param>
        /// <contract><requires name="itemLink" condition="not null"/></contract>
        public override void RemoveLink([NotNull] ItemLink itemLink)
        {
            this.htmlField.RemoveLink(itemLink);
        }

        /// <summary>
        /// Validates the links.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <contract><requires name="result" condition="not null" /></contract>
        public override void ValidateLinks([NotNull] LinksValidationResult result)
        {
            this.htmlField.ValidateLinks(result);
        }

        /// <summary>
        /// The field render.
        /// </summary>
        /// <returns>The Sitecore Non-Page Editor Mode Html</returns>
        [NotNull]
        public virtual global::Jig.Sitecore.MvcHtmlString Render()
        {
            if (this.HasValue)
            {
                var html = FieldRenderer.Render(this.Item, this.Name, "disable-web-editing=true");

                return html;
            }

            return string.Empty;
        }

        #endregion
    }
}
