﻿namespace Jig.Sitecore.Data.Fields.JsonConverters
{
    using System;

    using Newtonsoft.Json;

    using global::Sitecore;

    /// <summary>
    /// The field property json converter.
    /// </summary>
    public sealed class FieldPropertyJsonConverter : JsonConverter
    {
        #region Public Methods and Operators

        /// <summary>
        /// The can convert.
        /// </summary>
        /// <param name="objectType">The object type.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public override bool CanConvert([NotNull] Type objectType)
        {
            if (objectType.IsClass && !objectType.IsAbstract)
            {
                var canConvert = typeof(IFieldProperty).IsAssignableFrom(objectType);
                return canConvert;
            }

            return false;
        }

        /// <summary>
        /// The read json.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="objectType">The object type.</param>
        /// <param name="existingValue">The existing value.</param>
        /// <param name="serializer">The serializer.</param>
        /// <returns>The <see cref="object" />.</returns>
        /// <exception cref="NotSupportedException">This functionality is not supported!</exception>
        [NotNull]
        public override object ReadJson(
            [NotNull] JsonReader reader, 
            [NotNull] Type objectType, 
            [NotNull] object existingValue, 
            [NotNull] JsonSerializer serializer)
        {
            throw new NotSupportedException("This functionality is not supported!");
        }

        /// <summary>
        /// The write json.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The serializer.</param>
        public override void WriteJson(
            [NotNull] JsonWriter writer, 
            [NotNull] object value, 
            [NotNull] JsonSerializer serializer)
        {
            var field = value as IFieldProperty;

            if (field != null)
            {
                serializer.Serialize(writer, field.FieldRender());
            }
        }

        #endregion
    }
}