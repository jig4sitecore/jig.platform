﻿namespace Jig.Sitecore.Data.Fields
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    using Jig.Sitecore.Data;

    using Newtonsoft.Json;

    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Globalization;

    /// <summary>
    /// Facade for a Sitecore Multilist Field.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Name of Sitecore field.")]
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class MultilistProperty : DelimitedProperty
    {
        /// <summary>
        /// The field to wrap.
        /// </summary>
        private readonly MultilistField multilistField;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MultilistProperty" /> class.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        public MultilistProperty([NotNull] Field field)
            : base(field)
        {
            this.multilistField = field;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the list of target IDs.
        /// </summary>
        // ReSharper disable ReturnTypeCanBeEnumerable.Global
        [NotNull]
        public IList<ID> TargetIDs
        {
            // ReSharper restore ReturnTypeCanBeEnumerable.Global
            get
            {
                return this.multilistField.TargetIDs;
            }
        }

        #endregion

        #region Operators

        /// <summary>
        /// Allows interoperability with Sitecore MultilistField.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        /// <returns>A new instance of MultilistProperty using the supplied field.</returns>
        public static implicit operator MultilistProperty([NotNull]MultilistField field)
        {
            return new MultilistProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore MultilistField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.MultilistField([NotNull]MultilistProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator MultilistProperty([NotNull]Field field)
        {
            return new MultilistProperty(field);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <returns>The items.</returns>
        [NotNull]
        public Item[] GetItems()
        {
            return this.multilistField.GetItems();
        }

        /// <summary>
        /// Gets a collection of <see cref="IStandardTemplate" /> items.
        /// </summary>
        /// <param name="language">The language.</param>
        /// <returns>A collection of IStandardTemplateItem, the collection may be empty.</returns>
        [NotNull]
        public IList<IStandardTemplate> GetTargetItems([NotNull] Language language)
        {
            return this.TargetIDs.GetTargetItems(this.Database, language);
        }

        /// <summary>
        /// Gets a collection of <see cref="IStandardTemplate"/> items.
        /// </summary>
        /// <returns>A collection of IStandardTemplateItem, the collection may be empty.</returns>
        [NotNull]
        public IList<IStandardTemplate> GetTargetItems()
        {
            return this.TargetIDs.GetTargetItems(this.Database, global::Sitecore.Context.Language);
        }

        /// <summary>
        /// Gets a collection TItem based on the field's target items. Only Items that are of the
        /// supplied Type are returned.
        /// </summary>
        /// <typeparam name="TItem">The Item type to filter for.</typeparam>
        /// <param name="language">The language.</param>
        /// <returns>A collection of TItem. The collection  may be empty.</returns>
        [NotNull]
        public IList<TItem> GetTargetItems<TItem>([NotNull] Language language) where TItem : class, IStandardTemplate
        {
            // ReSharper disable InvokeAsExtensionMethod
            var result = ItemIdCollectionExtensions.GetTargetItems<TItem>(this.TargetIDs, this.Database, language);

            // ReSharper restore InvokeAsExtensionMethod
            return result;
        }

        /// <summary>
        /// Gets a collection TItem based on the field's target items. Only Items that are of the
        /// supplied Type are returned.
        /// </summary>
        /// <typeparam name="TItem">The Item type to filter for.</typeparam>
        /// <returns>A collection of TItem. The collection  may be empty.</returns>
        [NotNull]
        public IList<TItem> GetTargetItems<TItem>() where TItem : class, IStandardTemplate
        {
            // ReSharper disable InvokeAsExtensionMethod
            var result = ItemIdCollectionExtensions.GetTargetItems<TItem>(
                this.TargetIDs, 
                this.Database, 
                global::Sitecore.Context.Language);

            // ReSharper restore InvokeAsExtensionMethod
            return result;
        }

        #endregion
    }
}
