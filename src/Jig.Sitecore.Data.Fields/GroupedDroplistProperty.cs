﻿namespace Jig.Sitecore.Data.Fields
{
    using System.Diagnostics.CodeAnalysis;

    using global::Sitecore;
    using global::Sitecore.Data.Fields;

    /// <summary>
    /// Wraps a Sitecore GroupedDroplistField
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    public class GroupedDroplistProperty : ValueLookupProperty
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupedDroplistProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The field to wrap.
        /// </param>
        public GroupedDroplistProperty([NotNull] Field field)
            : base(field)
        {
            // Nothing to do.
        }

        #endregion

        #region Operators

        /// <summary>
        /// Allows interoperability with Sitecore GroupedDroplistField.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        /// <returns>A new instance of GroupedDroplistProperty using the supplied field.</returns>
        public static implicit operator GroupedDroplistProperty([NotNull]GroupedDroplistField field)
        {
            return new GroupedDroplistProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore GroupedDroplistField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.GroupedDroplistField([NotNull]GroupedDroplistProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator GroupedDroplistProperty([NotNull]Field field)
        {
            return new GroupedDroplistProperty(field);
        }

        #endregion
    }
}
