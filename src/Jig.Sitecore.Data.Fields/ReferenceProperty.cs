﻿namespace Jig.Sitecore.Data.Fields
{
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Links;
    using global::Sitecore.Web.UI.WebControls;

    /// <summary>
    /// Wraps a Sitecore ReferenceField
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    public class ReferenceProperty : FieldProperty
    {
        /// <summary>
        /// The field to wrap.
        /// </summary>
        private readonly ReferenceField referenceField;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ReferenceProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The field to wrap.
        /// </param>
        public ReferenceProperty([NotNull] Field field)
            : base(field)
        {
            this.referenceField = field;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the path to the target.
        /// </summary>
        /// <value>
        /// The path.
        /// </value>
        [NotNull]
        public string Path
        {
            get
            {
                return this.referenceField.Path;
            }
        }

        /// <summary>
        /// Gets the target ID.
        /// </summary>
        /// <value>
        /// The target ID.
        /// </value>
        [NotNull]

        // ReSharper disable InconsistentNaming
        public ID TargetID
        {
            // ReSharper restore InconsistentNaming
            get
            {
                return this.referenceField.TargetID;
            }
        }

        /// <summary>
        /// Gets the target item.
        /// </summary>
        /// <value>
        /// The target item.
        /// </value>
        [NotNull]
        public Item TargetItem
        {
            get { return this.referenceField.TargetItem; }
        }
        #endregion

        #region Operators

        /// <summary>
        /// Allows interoperability with Sitecore ReferenceField.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        /// <returns>A new instance of ReferenceProperty using the supplied field.</returns>
        public static implicit operator ReferenceProperty([NotNull]ReferenceField field)
        {
            return new ReferenceProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore ReferenceField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.ReferenceField([NotNull]ReferenceProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator ReferenceProperty([NotNull]Field field)
        {
            return new ReferenceProperty(field);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Re-links the specified item.
        /// </summary>
        /// <param name="itemLink">
        /// The item link.
        /// </param>
        /// <param name="newLink">
        /// The new link.
        /// </param>
        public override void Relink([NotNull] ItemLink itemLink, [NotNull] Item newLink)
        {
            this.referenceField.Relink(itemLink, newLink);
        }

        /// <summary>
        /// Removes the link.
        /// </summary>
        /// <param name="itemLink">
        /// The item link.
        /// </param>
        public override void RemoveLink([NotNull] ItemLink itemLink)
        {
            this.referenceField.RemoveLink(itemLink);
        }

        /// <summary>
        /// Validates the links.
        /// </summary>
        /// <param name="result">
        /// The result.
        /// </param>
        public override void ValidateLinks([NotNull] LinksValidationResult result)
        {
            this.referenceField.ValidateLinks(result);
        }

        /// <summary>
        /// The field render.
        /// </summary>
        /// <returns>The Sitecore Non-Page Editor Mode Html</returns>
        [NotNull]
        public virtual global::Jig.Sitecore.MvcHtmlString Render()
        {
            if (this.HasValue)
            {
                var html = FieldRenderer.Render(this.Item, this.Name, "disable-web-editing=true");

                return html;
            }

            return string.Empty;
        }

        #endregion
    }
}
