﻿namespace Jig.Sitecore.Data.Fields
{
    using global::Sitecore;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Web.UI.WebControls;

    /// <summary>
    /// Wraps a Sitecore TextField.
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    // ReSharper disable ClassWithVirtualMembersNeverInherited.Global
    public class TextProperty : FieldProperty
    // ReSharper restore ClassWithVirtualMembersNeverInherited.Global
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TextProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The field to wrap.
        /// </param>
        public TextProperty([NotNull]Field field)
            : base(field)
        {
            // Nothing to do.
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the FieldRenderer output for this property.
        /// </summary>
        [NotNull]
        public string Text
        {
            get
            {
                return global::Sitecore.Web.UI.WebControls.FieldRenderer.Render(this.InnerField.Item, this.Name);
            }
        }

        #endregion

        #region Operators

        /// <summary>
        /// Allows interoperability with string.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.Text value.</returns>
        public static implicit operator string(TextProperty property)
        {
            return property.Text;
        }

        /// <summary>
        /// Allows interoperability with Sitecore TextField.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        /// <returns>A new instance of TextProperty using the supplied field.</returns>
        public static implicit operator TextProperty([NotNull]TextField field)
        {
            return new TextProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore TextField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.TextField([NotNull]TextProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator TextProperty([NotNull]Field field)
        {
            return new TextProperty(field);
        }

        /// <summary>
        /// The field render.
        /// </summary>
        /// <returns>The Sitecore Non-Page Editor Mode Html</returns>
        [NotNull]
        public virtual global::Jig.Sitecore.MvcHtmlString Render()
        {
            if (this.HasValue)
            {
                var html = FieldRenderer.Render(this.Item, this.Name, "disable-web-editing=true");

                return html;
            }

            return string.Empty;
        }

        #endregion
    }
}
