﻿namespace Jig.Sitecore.Data.Fields
{
    using global::Sitecore;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Links;
    using global::Sitecore.Web.UI.WebControls;

    /// <summary>
    /// Wraps a Sitecore ValueLookupField
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    public class ValueLookupProperty : FieldProperty
    {
        /// <summary>
        /// The field to wrap.
        /// </summary>
        private readonly ValueLookupField valueLookupField;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ValueLookupProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The field to wrap.
        /// </param>
        public ValueLookupProperty([NotNull] Field field)
            : base(field)
        {
            this.valueLookupField = field;
        }

        #endregion

        #region Operators

        /// <summary>
        /// Allows interoperability with Sitecore ValueLookupField.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        /// <returns>A new instance of ValueLookupProperty using the supplied field.</returns>
        public static implicit operator ValueLookupProperty([NotNull]ValueLookupField field)
        {
            return new ValueLookupProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore ValueLookupField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.ValueLookupField([NotNull]ValueLookupProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator ValueLookupProperty([NotNull]Field field)
        {
            return new ValueLookupProperty(field);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Re-links the specified item.
        /// </summary>
        /// <param name="itemLink">
        /// The item link.
        /// </param>
        /// <param name="newLink">
        /// The new link.
        /// </param>
        public override void Relink([NotNull] ItemLink itemLink, [NotNull] Item newLink)
        {
            this.valueLookupField.Relink(itemLink, newLink);
        }

        /// <summary>
        /// Removes the link.
        /// </summary>
        /// <param name="itemLink">
        /// The item link.
        /// </param>
        public override void RemoveLink([NotNull] ItemLink itemLink)
        {
            this.valueLookupField.RemoveLink(itemLink);
        }

        /// <summary>
        /// Updates the link.
        /// </summary>
        /// <param name="itemLink">
        /// The link.
        /// </param>
        public override void UpdateLink([NotNull] ItemLink itemLink)
        {
            this.valueLookupField.UpdateLink(itemLink);
        }

        /// <summary>
        /// Validates the links.
        /// </summary>
        /// <param name="result">
        /// The result.
        /// </param>
        public override void ValidateLinks([NotNull] LinksValidationResult result)
        {
            this.valueLookupField.ValidateLinks(result);
        }

        /// <summary>
        /// The field render.
        /// </summary>
        /// <returns>The Sitecore Non-Page Editor Mode Html</returns>
        [NotNull]
        public virtual global::Jig.Sitecore.MvcHtmlString Render()
        {
            if (this.HasValue)
            {
                var html = FieldRenderer.Render(this.Item, this.Name, "disable-web-editing=true");

                return html;
            }

            return string.Empty;
        }

        #endregion
    }
}
