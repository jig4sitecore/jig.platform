﻿namespace Jig.Sitecore.Data.Fields
{
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Links;
    using global::Sitecore.Web.UI.WebControls;

    /// <summary>
    /// Wraps the Sitecore WordDocumentField
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    public class WordDocumentProperty : FieldProperty
    {
        /// <summary>
        /// The field to wrap.
        /// </summary>
        private readonly WordDocumentField wordDocumentField;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WordDocumentProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The field to wrap.
        /// </param>
        public WordDocumentProperty([NotNull] Field field)
            : base(field)
        {
            this.wordDocumentField = field;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the plain text.
        /// </summary>
        /// <value>
        /// The plain text.
        /// </value>
        [NotNull]
        public string PlainText
        {
            get
            {
                return this.wordDocumentField.PlainText;
            }
        }

        /// <summary>
        /// Gets the HTML.
        /// </summary>
        /// <value>
        /// The HTML.
        /// </value>
        [NotNull]
        public string Html
        {
            get
            {
                return this.wordDocumentField.Html;
            }
        }

        /// <summary>
        /// Gets the styles.
        /// </summary>
        /// <value>
        /// The styles.
        /// </value>
        [NotNull]
        public string Styles
        {
            get
            {
                return this.wordDocumentField.Styles;
            }
        }

        /// <summary>
        /// Gets the BLOB id.
        /// </summary>
        /// <value>
        /// The BLOB id.
        /// </value>
        [NotNull]
        public ID BlobId
        {
            get
            {
                return this.wordDocumentField.BlobId;
            }
        }
        #endregion

        #region Operators

        /// <summary>
        /// Allows interoperability with Sitecore WordDocumentField.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        /// <returns>A new instance of WordDocumentProperty using the supplied field.</returns>
        public static implicit operator WordDocumentProperty([NotNull]WordDocumentField field)
        {
            return new WordDocumentProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore WordDocumentField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.WordDocumentField([NotNull]WordDocumentProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator WordDocumentProperty([NotNull]Field field)
        {
            return new WordDocumentProperty(field);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates the links.
        /// </summary>
        /// <param name="result">
        /// The result.
        /// </param>
        /// <contract><requires name="result" condition="not null"/></contract>
        public override void ValidateLinks([NotNull] LinksValidationResult result)
        {
            this.wordDocumentField.ValidateLinks(result);
        }

        /// <summary>
        /// The field render.
        /// </summary>
        /// <returns>The Sitecore Non-Page Editor Mode Html</returns>
        [NotNull]
        public virtual global::Jig.Sitecore.MvcHtmlString Render()
        {
            if (this.HasValue)
            {
                var html = FieldRenderer.Render(this.Item, this.Name, "disable-web-editing=true");

                return html;
            }

            return string.Empty;
        }

        #endregion
    }
}
