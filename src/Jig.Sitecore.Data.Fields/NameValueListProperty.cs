﻿namespace Jig.Sitecore.Data.Fields
{
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Web;

    using global::Sitecore;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Web.UI.WebControls;

    /// <summary>
    /// Wraps a Sitecore NameValueListField
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    public class NameValueListProperty : FieldProperty
    {
        /// <summary>
        /// The field to wrap.
        /// </summary>
        private readonly NameValueListField nameValueListField;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NameValueListProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The field to wrap.
        /// </param>
        public NameValueListProperty([NotNull] Field field)
            : base(field)
        {
            this.nameValueListField = field;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Sitecore.Data.Fields.CheckboxField"/> is checked.
        /// </summary>
        /// <value>
        /// <c>true</c> if checked; otherwise, <c>false</c>.
        /// </value>
        [NotNull]
        public NameValueCollection NameValues
        {
            get
            {
                return this.nameValueListField.NameValues;
            }
        }
        #endregion

        #region Operators

        /// <summary>
        /// Allows interoperability with Sitecore NameValueListField.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        /// <returns>A new instance of NameValueListProperty using the supplied field.</returns>
        public static implicit operator NameValueListProperty([NotNull]NameValueListField field)
        {
            return new NameValueListProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore ImageField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.NameValueListField([NotNull]NameValueListProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator NameValueListProperty([NotNull]Field field)
        {
            return new NameValueListProperty(field);
        }

        /// <summary>
        /// The get values.
        /// </summary>
        /// <returns>
        /// The list of values.
        /// </returns>
        [NotNull]
        public IList<string> GetValues()
        {
            var collection = this.nameValueListField.NameValues;

            return
                collection.AllKeys.Select(key => collection[key])
                    .Where(value => !string.IsNullOrWhiteSpace(value))
                    .ToArray();
        }

        /// <summary>
        /// Gets the key value pairs.
        /// </summary>
        /// <param name="ignoreValueEmpty">
        /// if set to <c>true</c> [ignore value empty].
        /// </param>
        /// <param name="urlDecodeValues">
        /// if set to <c>true</c> [URL decode values].
        /// </param>
        /// <returns>
        /// The key value collection
        /// </returns>
        [NotNull]
        public IList<KeyValuePair<string, string>> GetKeyValuePairs(bool ignoreValueEmpty = true, bool urlDecodeValues = true)
        {
            var collection = this.nameValueListField.NameValues;
            var keys = collection.AllKeys;
            var list = new List<KeyValuePair<string, string>>();
            if (ignoreValueEmpty)
            {
                for (int i = 0; i < collection.Count; i++)
                {
                    var key = keys[i];
                    var value = collection[i];
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        if (urlDecodeValues)
                        {
                            value = HttpUtility.UrlDecode(value);
                        }

                        list.Add(new KeyValuePair<string, string>(key, value));
                    }
                }
            }
            else
            {
                for (int i = 0; i < collection.Count; i++)
                {
                    var key = keys[i];
                    var value = collection[i];

                    if (urlDecodeValues)
                    {
                        value = string.IsNullOrWhiteSpace(value) ? string.Empty : HttpUtility.UrlDecode(value);
                    }

                    list.Add(new KeyValuePair<string, string>(key, value));
                }
            }

            return list.ToArray();
        }

        /// <summary>
        /// The field render.
        /// </summary>
        /// <returns>The Sitecore Non-Page Editor Mode Html</returns>
        [NotNull]
        public virtual global::Jig.Sitecore.MvcHtmlString Render()
        {
            if (this.HasValue)
            {
                var html = FieldRenderer.Render(this.Item, this.Name, "disable-web-editing=true");

                return html;
            }

            return string.Empty;
        }

        #endregion
    }
}
