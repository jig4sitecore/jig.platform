﻿namespace Jig.Sitecore.Data.Fields
{
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Links;
    using global::Sitecore.Web.UI.WebControls;

    /// <summary>
    /// Wraps a Sitecore LookupField
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    public class LookupProperty : FieldProperty
    {
        /// <summary>
        /// The field to wrap.
        /// </summary>
        private readonly LookupField lookupField;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LookupProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The field to wrap.
        /// </param>
        public LookupProperty([NotNull] Field field)
            : base(field)
        {
            this.lookupField = field;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the target ID.
        /// </summary>
        /// <value>
        /// The target ID.
        /// </value>
        /// <contract><ensures condition="not null"/></contract>
        // ReSharper disable InconsistentNaming
        [NotNull]
        public ID TargetID
        {
            // ReSharper restore InconsistentNaming
            get
            {
                return this.lookupField.TargetID;
            }
        }

        /// <summary>
        /// Gets the target item.
        /// </summary>
        /// <value>
        /// The target item.
        /// </value>
        [NotNull]
        public Item TargetItem
        {
            get
            {
                return this.lookupField.TargetItem;
            }
        }
        #endregion

        #region Operators

        /// <summary>
        /// Allows interoperability with Sitecore LookupField.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        /// <returns>A new instance of LookupProperty using the supplied field.</returns>
        public static implicit operator LookupProperty([NotNull]LookupField field)
        {
            return new LookupProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore LookupField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.ImageField([NotNull]LookupProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator LookupProperty([NotNull]Field field)
        {
            return new LookupProperty(field);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Re-links the specified item.
        /// </summary>
        /// <param name="itemLink">
        /// The item link.
        /// </param>
        /// <param name="newLink">
        /// The new link.
        /// </param>
        /// <contract><requires name="itemLink" condition="not null"/><requires name="newLink" condition="not null"/></contract>
        public override void Relink([NotNull] ItemLink itemLink, [NotNull] Item newLink)
        {
            this.lookupField.Relink(itemLink, newLink);
        }

        /// <summary>
        /// Removes the link.
        /// </summary>
        /// <param name="itemLink">
        /// The item link.
        /// </param>
        /// <contract><requires name="itemLink" condition="not null"/></contract>
        public override void RemoveLink([NotNull] ItemLink itemLink)
        {
            this.lookupField.RemoveLink(itemLink);
        }

        /// <summary>
        /// Validates the links. 
        /// </summary>
        /// <param name="result">
        /// The result.
        /// </param>
        /// <contract><requires name="result" condition="not null"/></contract>
        public override void ValidateLinks([NotNull] LinksValidationResult result)
        {
            this.lookupField.ValidateLinks(result);
        }

        /// <summary>
        /// The field render.
        /// </summary>
        /// <returns>The Sitecore Non-Page Editor Mode Html</returns>
        [NotNull]
        public virtual global::Jig.Sitecore.MvcHtmlString Render()
        {
            if (this.HasValue)
            {
                var html = FieldRenderer.Render(this.Item, this.Name, "disable-web-editing=true");

                return html;
            }

            return string.Empty;
        }

        #endregion
    }
}
