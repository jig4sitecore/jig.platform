﻿namespace Jig.Sitecore.Data.Fields
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Xml.Linq;
    using Jig.Sitecore.Data.Fields.Pipelines;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Pipelines;
    using global::Sitecore.Resources.Media;

    /// <summary>
    /// Wraps a Sitecore LinkField
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    public class LinkProperty : XmlProperty
    {
        /// <summary>
        /// The Link field to wrap.
        /// </summary>
        private readonly global::Sitecore.Data.Fields.LinkField linkField;

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="LinkProperty"/> class.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        public LinkProperty([NotNull] global::Sitecore.Data.Fields.Field field)
            : base(field)
        {
            this.linkField = field;
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets the Anchor.
        /// </summary>
        [NotNull]
        public string Anchor
        {
            get
            {
                return this.linkField.Anchor;
            }
        }

        /// <summary>
        /// Gets the HTML Class attribute value.
        /// </summary>
        [NotNull]
        public string Class
        {
            get
            {
                return this.linkField.Class;
            }
        }

        /// <summary>
        /// Gets the path to the Sitecore Item.
        /// </summary>
        [NotNull]
        public string InternalPath
        {
            get
            {
                return this.linkField.InternalPath;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the link is internal.
        /// </summary>
        /// <value>
        /// <c>true</c> if this link is internal; otherwise, <c>false</c>.
        /// </value>
        public bool IsInternal
        {
            get
            {
                return this.linkField.LinkType.IndexOf("internal", StringComparison.InvariantCultureIgnoreCase) > -1;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this link is external.
        /// </summary>
        /// <value><c>true</c> if this instance is external; otherwise, <c>false</c>.</value>
        public bool IsExternal
        {
            get
            {
                return this.linkField.LinkType.IndexOf("external", StringComparison.InvariantCultureIgnoreCase) > -1;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is media link.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is media link; otherwise, <c>false</c>.
        /// </value>
        public bool IsMediaLink
        {
            get
            {
                return this.linkField.LinkType.IndexOf("media", StringComparison.InvariantCultureIgnoreCase) > -1;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is anchor.
        /// </summary>
        /// <value><c>true</c> if this instance is anchor; otherwise, <c>false</c>.</value>
        public bool IsAnchor
        {
            get
            {
                return this.linkField.LinkType.IndexOf("anchor", StringComparison.InvariantCultureIgnoreCase) > -1;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is email.
        /// </summary>
        /// <value><c>true</c> if this instance is email; otherwise, <c>false</c>.</value>
        public bool IsEmail
        {
            get
            {
                return this.linkField.LinkType.IndexOf("mailto", StringComparison.InvariantCultureIgnoreCase) > -1;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is telephone.
        /// </summary>
        /// <value><c>true</c> if this instance is telephone; otherwise, <c>false</c>.</value>
        public bool IsTelephone
        {
            get
            {
                return this.linkField.LinkType.IndexOf("telephone", StringComparison.InvariantCultureIgnoreCase) > -1;
            }
        }

        /// <summary>
        /// Gets the link type.
        /// </summary>
        /// <value>
        /// The type of the link.
        /// </value>
        [NotNull]
        public string LinkType
        {
            get
            {
                return this.linkField.LinkType;
            }
        }

        /// <summary>
        /// Gets the path to the Media Item.
        /// </summary>
        /// <value>
        /// The internal path.
        /// </value>
        [NotNull]
        public string MediaPath
        {
            get
            {
                return this.linkField.MediaPath;
            }
        }

        /// <summary>
        /// Gets the query string in internal links.
        /// </summary>
        /// <value>
        /// The query string.
        /// </value>
        [NotNull]
        public string QueryString
        {
            get
            {
                return this.linkField.QueryString;
            }
        }

        /// <summary>
        /// Gets the HTML target attribute value.
        /// </summary>
        /// <value>
        /// The value of the target HTML attribute.
        /// </value>
        [NotNull]
        public string Target
        {
            get
            {
                return this.linkField.Target;
            }
        }

        /// <summary>
        /// Gets the target ID.
        /// </summary>
        /// <value>
        /// The target ID.
        /// </value>
        // ReSharper disable InconsistentNaming
        [CanBeNull]
        public global::Sitecore.Data.ID TargetID
        // ReSharper restore InconsistentNaming
        {
            get
            {
                return this.linkField.TargetID;
            }
        }

        /// <summary>
        /// Gets the target item.
        /// </summary>
        /// <value>
        /// The target item.
        /// </value>
        [CanBeNull]
        public global::Sitecore.Data.Items.Item TargetItem
        {
            get
            {
                return this.linkField.TargetItem;
            }
        }

        /// <summary>
        /// Gets the inner text of the tag.
        /// </summary>
        /// <value>
        /// The inner text.
        /// </value>
        [NotNull]
        public string Text
        {
            get
            {
                return this.linkField.Text;
            }
        }

        /// <summary>
        /// Gets the title attribute value.
        /// </summary>
        /// <value>
        /// The title attribute value.
        /// </value>
        [NotNull]
        public new string Title
        {
            get
            {
                return this.linkField.Title;
            }
        }

        /// <summary>
        /// Gets the URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        [NotNull]
        public string Url
        {
            get
            {
                return this.linkField.Url;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the field's value is not null or empty.
        /// </summary>
        public override bool HasValue
        {
            get
            {
                var flag = base.HasValue &&
                    (!string.IsNullOrWhiteSpace(this.Url) || !string.IsNullOrWhiteSpace(this.MediaPath) || this.TargetItem != null || !string.IsNullOrWhiteSpace(this.Anchor));

                return flag;
            }
        }

        #endregion

        #region Operators
        /// <summary>
        /// Allows interoperability with Sitecore LinkField.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        /// <returns>A new instance of LinkProperty using the supplied field.</returns>
        public static implicit operator LinkProperty([NotNull]global::Sitecore.Data.Fields.LinkField field)
        {
            return new LinkProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore LinkField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.LinkField([NotNull]LinkProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator LinkProperty([NotNull]global::Sitecore.Data.Fields.Field field)
        {
            return new LinkProperty(field);
        }

        #endregion

        /// <summary>
        /// Renders the pipeline.
        /// </summary>
        /// <param name="pipelineName">Name of the pipeline.</param>
        /// <param name="args">The arguments.</param>
        /// <returns>The Link Html</returns>
        [NotNull]
        public static MvcHtmlString RenderPipeline([NotNull] string pipelineName, [NotNull]LinkPropertyRenderPipelineArgs args)
        {
            Assert.IsNotNull(args, "args");
            Assert.IsNotNullOrEmpty(pipelineName, "pipelineName");

            if (!args.LinkProperty.HasValue)
            {
                return string.Empty;
            }

            CorePipeline pipeline = CorePipelineFactory.GetPipeline(pipelineName, string.Empty);
            if (pipeline == null)
            {
                var msg = new StringBuilder(128);
                msg.Append("Could not get pipeline: ").AppendLine(pipelineName);
                msg.AppendLine("Missing config file or related file to: /App_Config/Include/Jig4Sitecore/core/Jig.Sitecore.Data.Fields.Render.config");

                throw new HttpException(msg.ToString());
            }

            pipeline.Run(args);

            return args.Output;
        }

        #region Methods

        /// <summary>
        /// Inspects the Link's properties to determine the best URL.
        /// </summary>
        /// <returns>The URL for the rendering.</returns>
        [NotNull]
        public string GetLinkUrl()
        {
            if (this.IsMediaLink)
            {
                global::Sitecore.Data.Items.MediaItem mediaItem = this.TargetItem;
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (mediaItem != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    var url = mediaItem.GetMediaItemUrl();

                    return url;
                }

                // ReSharper disable HeuristicUnreachableCode
                return "#media-item-not-found";
                // ReSharper restore HeuristicUnreachableCode
            }

            if (this.IsInternal)
            {
                var item = this.TargetItem;

                if (item != null)
                {
                    var url = item.GetUrl();

                    return url;
                }

                return "#item-not-found";
            }

            return this.Url;
        }

        /// <summary>
        /// Gets the link URL.
        /// </summary>
        /// <param name="mediaUrlOptions">The media URL options.</param>
        /// <returns>The URL for the rendering.</returns>
        [NotNull]
        public string GetLinkUrl([NotNull] global::Sitecore.Resources.Media.MediaUrlOptions mediaUrlOptions)
        {
            if (this.IsMediaLink)
            {
                global::Sitecore.Data.Items.MediaItem mediaItem = this.TargetItem;
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (mediaItem != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    var url = mediaItem.GetMediaItemUrl(mediaUrlOptions);

                    return url;
                }

                // ReSharper disable HeuristicUnreachableCode
                return string.Empty;
                // ReSharper restore HeuristicUnreachableCode
            }

            if (this.IsInternal)
            {
                var item = this.TargetItem;

                if (item != null)
                {
                    var url = item.GetUrl();

                    return url;
                }

                return "#not-found";
            }

            return this.Url;
        }

        /// <summary>
        /// Re-links the specified item link.
        /// </summary>
        /// <param name="itemLink">The item link.</param><param name="newLink">The new link.</param>
        public override void Relink([NotNull] global::Sitecore.Links.ItemLink itemLink, [NotNull] global::Sitecore.Data.Items.Item newLink)
        {
            this.linkField.Relink(itemLink, newLink);
        }

        /// <summary>
        /// Removes the link.
        /// </summary>
        /// <param name="itemLink">The item link.</param>
        public override void RemoveLink([NotNull] global::Sitecore.Links.ItemLink itemLink)
        {
            this.linkField.RemoveLink(itemLink);
        }

        /// <summary>
        /// Updates the link.
        /// </summary>
        /// <param name="itemLink">The link.</param>
        public override void UpdateLink([NotNull] global::Sitecore.Links.ItemLink itemLink)
        {
            this.linkField.UpdateLink(itemLink);
        }

        /// <summary>
        /// Validates the links.
        /// </summary>
        /// <param name="result">The result.</param>
        public override void ValidateLinks([NotNull] global::Sitecore.Links.LinksValidationResult result)
        {
            this.linkField.ValidateLinks(result);
        }

        /// <summary>
        /// Gets the media item URL.
        /// </summary>
        /// <returns>
        /// The Media url
        /// </returns>
        [NotNull]
        public string GetMediaItemUrl()
        {
            if (this.IsMediaLink && this.HasValue)
            {
                global::Sitecore.Data.Items.MediaItem mediaItem = this.TargetItem;
                if (mediaItem != null)
                {
                    var mediaUrlOptions = new MediaUrlOptions
                    {
                        LowercaseUrls = true,
                        AbsolutePath = false,
                        IncludeExtension = true,
                        UseItemPath = false,
                        RequestExtension = mediaItem.Extension
                    };

                    var mediaUrl = MediaManager.GetMediaUrl(mediaItem, mediaUrlOptions);

                    if (string.IsNullOrWhiteSpace(mediaUrl))
                    {
                        return string.Empty;
                    }

                    if (mediaUrl.StartsWith("http:"))
                    {
                        return mediaUrl.Remove(0, "http:".Length);
                    }

                    if (mediaUrl.StartsWith("https:"))
                    {
                        return mediaUrl.Remove(0, "https:".Length);
                    }

                    return mediaUrl;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the media item URL.
        /// </summary>
        /// <param name="mediaUrlOptions">The media URL options.</param>
        /// <returns>The Media url</returns>
        [NotNull]
        public string GetMediaItemUrl([NotNull] global::Sitecore.Resources.Media.MediaUrlOptions mediaUrlOptions)
        {
            if (this.IsMediaLink && this.HasValue)
            {
                global::Sitecore.Data.Items.MediaItem mediaItem = this.TargetItem;
                if (mediaItem != null)
                {
                    var mediaUrl = MediaManager.GetMediaUrl(mediaItem, mediaUrlOptions);
                    if (string.IsNullOrWhiteSpace(mediaUrl))
                    {
                        return string.Empty;
                    }

                    if (mediaUrl.StartsWith("http:"))
                    {
                        return mediaUrl.Remove(0, "http:".Length);
                    }

                    if (mediaUrl.StartsWith("https:"))
                    {
                        return mediaUrl.Remove(0, "https:".Length);
                    }

                    return mediaUrl;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the media item.
        /// </summary>
        /// <returns>The media item</returns>
        [CanBeNull]
        public global::Sitecore.Data.Items.MediaItem GetMediaItem()
        {
            if (this.IsMediaLink && this.HasValue)
            {
                global::Sitecore.Data.Items.MediaItem mediaItem = this.TargetItem;

                return mediaItem;
            }

            return null;
        }

        /// <summary>
        /// Renders the specified link.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        /// <returns>The link output</returns>
        [NotNull]
        public MvcHtmlString Render([NotNull] params XAttribute[] attributes)
        {
            var html = this.Render(string.Empty, attributes);
            return html;
        }

        /// <summary>
        /// Renders the specified link.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        /// <returns>The link output</returns>
        [NotNull]
        public MvcHtmlString Render([NotNull] IEnumerable<XAttribute> attributes)
        {
            var html = this.Render(string.Empty, attributes);
            return html;
        }

        /// <summary>
        /// Renders the specified link.
        /// </summary>
        /// <param name="innerHtml">The inner HTML.</param>
        /// <param name="attributes">The attributes.</param>
        /// <returns>The link output</returns>
        [NotNull]
        public MvcHtmlString Render([NotNull] string innerHtml, [NotNull] params XAttribute[] attributes)
        {
            var html = this.Render(
                innerHtml: innerHtml,
                cssClass: null,
                queryString: null,
                anchor: null,
                attributes: attributes);

            return html;
        }

        /// <summary>
        /// Renders the specified link.
        /// </summary>
        /// <param name="innerHtml">The inner HTML.</param>
        /// <param name="attributes">The attributes.</param>
        /// <returns>The link output</returns>
        [NotNull]
        public MvcHtmlString Render([NotNull] string innerHtml, [NotNull] IEnumerable<XAttribute> attributes)
        {
            var html = this.Render(
                innerHtml: innerHtml,
                cssClass: null,
                queryString: null,
                anchor: null,
                attributes: attributes);

            return html;
        }

        /// <summary>
        /// Renders the specified link.
        /// </summary>
        /// <param name="innerHtml">The inner HTML.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="attributes">The attributes.</param>
        /// <returns>The link output</returns>
        [NotNull]
        public MvcHtmlString Render([NotNull] string innerHtml, [CanBeNull] string cssClass, [NotNull] params XAttribute[] attributes)
        {
            var html = this.Render(
                innerHtml: innerHtml,
                cssClass: cssClass,
                queryString: null,
                anchor: null,
                attributes: attributes);

            return html;
        }

        /// <summary>
        /// Renders the specified link.
        /// </summary>
        /// <param name="innerHtml">The inner HTML.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="attributes">The attributes.</param>
        /// <returns>The link output</returns>
        [NotNull]
        public MvcHtmlString Render([NotNull] string innerHtml, [CanBeNull] string cssClass, [NotNull] IEnumerable<XAttribute> attributes)
        {
            var html = this.Render(
                innerHtml: innerHtml,
                cssClass: cssClass,
                queryString: null,
                anchor: null,
                attributes: attributes);

            return html;
        }

        /// <summary>
        /// Renders the specified link.
        /// </summary>
        /// <param name="innerHtml">The inner HTML.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="queryString">The query string.</param>
        /// <param name="attributes">The attributes.</param>
        /// <returns>The link output</returns>
        [NotNull]
        public MvcHtmlString Render([NotNull] string innerHtml, [NotNull] string cssClass, [NotNull] string queryString, [NotNull] params XAttribute[] attributes)
        {
            var html = this.Render(
                innerHtml: innerHtml,
                cssClass: cssClass,
                queryString: queryString,
                anchor: null,
                attributes: attributes);

            return html;
        }

        /// <summary>
        /// Renders the specified link.
        /// </summary>
        /// <param name="innerHtml">The inner HTML.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="queryString">The query string.</param>
        /// <param name="attributes">The attributes.</param>
        /// <returns>The link output</returns>
        [NotNull]
        public MvcHtmlString Render([NotNull] string innerHtml, [NotNull] string cssClass, [NotNull] string queryString, [NotNull] IEnumerable<XAttribute> attributes)
        {
            var html = this.Render(
                innerHtml: innerHtml,
                cssClass: cssClass,
                queryString: queryString,
                anchor: null,
                attributes: attributes);

            return html;
        }

        /// <summary>
        /// Renders the specified attributes.
        /// </summary>
        /// <param name="innerHtml">The inner HTML.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="queryString">The query string.</param>
        /// <param name="anchor">The anchor.</param>
        /// <param name="attributes">The attributes.</param>
        /// <returns>The link output</returns>
        [NotNull]
        public MvcHtmlString Render(
            [CanBeNull] string innerHtml,
            [CanBeNull] string cssClass,
            [CanBeNull] string queryString,
            [CanBeNull] string anchor,
            [NotNull] IEnumerable<XAttribute> attributes)
        {
            // ReSharper disable ConstantNullCoalescingCondition
            return this.Render(innerHtml, cssClass, queryString, anchor, (attributes ?? Enumerable.Empty<XAttribute>()).ToArray());
            // ReSharper restore ConstantNullCoalescingCondition
        }

        /// <summary>
        /// Renders the specified attributes.
        /// </summary>
        /// <param name="innerHtml">The inner HTML.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="queryString">The query string.</param>
        /// <param name="anchor">The anchor.</param>
        /// <param name="attributes">The attributes.</param>
        /// <returns>The link output</returns>
        [NotNull]
        public MvcHtmlString Render([CanBeNull] string innerHtml, [CanBeNull] string cssClass, [CanBeNull] string queryString, [CanBeNull] string anchor, [NotNull] params XAttribute[] attributes)
        {
            if (!this.HasValue)
            {
                return string.Empty;
            }

            var args = new Jig.Sitecore.Data.Fields.Pipelines.LinkPropertyRenderPipelineArgs(this, attributes)
            {
                /* Merge Css classes if code and link is not empty */
                CssClass = string.IsNullOrWhiteSpace(cssClass) ? this.Class : (this.Class + " " + cssClass).Trim(),
                /* ReSharper disable AssignNullToNotNullAttribute */
                InnerHtml = string.IsNullOrEmpty(innerHtml) ? this.Text : innerHtml,
                QueryString = string.IsNullOrWhiteSpace(queryString) ? this.QueryString.EnsureStartsWithIfNotEmpty('?') : queryString.EnsureStartsWithIfNotEmpty('?'),
                Anchor = string.IsNullOrWhiteSpace(anchor) ? this.Anchor.EnsureStartsWithIfNotEmpty('#') : anchor.EnsureStartsWithIfNotEmpty('#')
                /* ReSharper restore AssignNullToNotNullAttribute */
            };

            /* Target and titles should always be set in CMS */
            var result = LinkProperty.RenderPipeline(typeof(LinkProperty).FullName, args);
            return result;
        }

        #endregion
    }
}
