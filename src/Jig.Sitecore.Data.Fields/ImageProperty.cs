﻿namespace Jig.Sitecore.Data.Fields
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Xml.Linq;

    using Jig.Web.Html;

    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Globalization;
    using global::Sitecore.Links;
    using global::Sitecore.Resources.Media;

    /// <summary>
    /// Wraps a Sitecore ImageField
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    public class ImageProperty : XmlProperty
    {
        /// <summary>
        /// The field identifier
        /// /sitecore/templates/System/Media/Unversioned/File/Information/Extension
        /// {C06867FE-9A43-4C7D-B739-48780492D06F}
        /// </summary>
        private static readonly ID FieldId = new global::Sitecore.Data.ID("C06867FE-9A43-4C7D-B739-48780492D06F");

        /// <summary>
        /// The Image Field to wrap.
        /// </summary>
        private readonly ImageField imageField;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageProperty" /> class.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        public ImageProperty([global::Sitecore.NotNull] Field field)
            : base(field)
        {
            this.imageField = field;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Alt text.
        /// </summary>
        /// <value>The alt text.</value>
        [NotNull]
        public string Alt
        {
            get
            {
                return this.imageField.Alt;
            }
        }

        /// <summary>
        /// Gets the HTML class.
        /// </summary>
        /// <value>The class.</value>
        [NotNull]
        public string Class
        {
            get
            {
                return this.imageField.Class;
            }
        }

        /// <summary>
        /// Gets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        [NotNull]
        public string Height
        {
            get
            {
                return this.imageField.Height;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the image is internal.
        /// </summary>
        /// <value>
        /// <c>true</c> if the image is internal; otherwise, <c>false</c>.
        /// </value>
        public bool IsInternal
        {
            get { return this.imageField.IsInternal; }
        }

        /// <summary>
        /// Gets the link type.
        /// </summary>
        /// <value>The type of the link.</value>
        [NotNull]
        public string LinkType
        {
            get
            {
                return this.imageField.LinkType;
            }
        }

        /// <summary>
        /// Gets the database containing the the media.
        /// </summary>
        /// <value>The media database.</value>
        [NotNull]
        public Database MediaDatabase
        {
            get
            {
                return this.imageField.MediaDatabase;
            }
        }

        /// <summary>
        /// Gets the ID of the media item.
        /// </summary>
        /// <value>
        /// The media ID.
        /// </value>
        // ReSharper disable InconsistentNaming
        [NotNull]
        public ID MediaID
        {
            // ReSharper restore InconsistentNaming
            get
            {
                return this.imageField.MediaID;
            }
        }

        /// <summary>
        /// Gets the media item.
        /// </summary>
        /// <value>The media item.</value>
        [NotNull]
        public Item MediaItem
        {
            get
            {
                return this.imageField.MediaItem;
            }
        }

        /// <summary>
        /// Gets the language of the media item to use.
        /// </summary>
        /// <value>The media language.</value>
        [NotNull]
        public Language MediaLanguage
        {
            get
            {
                return this.imageField.MediaLanguage;
            }
        }

        /// <summary>
        /// Gets the version of the media item to use.
        /// </summary>
        /// <value>The media version.</value>
        [NotNull]
        public Version MediaVersion
        {
            get
            {
                return this.imageField.MediaVersion;
            }
        }

        /// <summary>
        /// Gets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        [NotNull]
        public string Width
        {
            get
            {
                return this.imageField.Width;
            }
        }
        #endregion

        #region Operators

        /// <summary>
        /// Allows interoperability with Sitecore ImageField.
        /// </summary>
        /// <param name="field">The field to wrap.</param>
        /// <returns>A new instance of ImageProperty using the supplied field.</returns>
        public static implicit operator ImageProperty([NotNull]ImageField field)
        {
            return new ImageProperty(field.InnerField);
        }

        /// <summary>
        /// Allows interoperability with Sitecore ImageField.
        /// </summary>
        /// <param name="property">The property to convert.</param>
        /// <returns>The property.InnerField.</returns>
        public static implicit operator global::Sitecore.Data.Fields.ImageField([NotNull]ImageProperty property)
        {
            return property.InnerField;
        }

        /// <summary>
        /// Allows automatic interoperability with Sitecore Field.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>A Sitecore Field.</returns>
        public static implicit operator ImageProperty([NotNull]Field field)
        {
            return new ImageProperty(field);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the media item as base64.
        /// </summary>
        /// <returns>The media asset as Base64 string src attribute value</returns>
        [global::Sitecore.NotNull]
        public string GetMediaItemAsBase64()
        {
            MediaItem mediaItem = this.imageField.MediaItem;
            if (mediaItem != null)
            {
                using (var stream = mediaItem.GetMediaStream())
                {
                    /* The image might be not published but content item could be. Verify */
                    if (stream != null)
                    {
                        var bytes = new byte[stream.Length];
                        stream.Read(bytes, 0, bytes.Length);
                        var src = string.Concat("data:", mediaItem.MimeType, ";base64,", System.Convert.ToBase64String(bytes));

                        return src;
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Re-links the specified item.
        /// </summary>
        /// <param name="itemLink">
        /// The item link.
        /// </param>
        /// <param name="newLink">
        /// The new link.
        /// </param>
        /// <contract><requires name="itemLink" condition="not null"/><requires name="newLink" condition="not null"/></contract>
        public override void Relink([NotNull] ItemLink itemLink, [NotNull] Item newLink)
        {
            this.imageField.Relink(itemLink, newLink);
        }

        /// <summary>
        /// Removes the link.
        /// </summary>
        /// <param name="itemLink">
        /// The item link.
        /// </param>
        /// <contract><requires name="itemLink" condition="not null"/></contract>
        public override void RemoveLink([NotNull] ItemLink itemLink)
        {
            this.imageField.RemoveLink(itemLink);
        }

        /// <summary>
        /// Updates the link.
        /// </summary>
        /// <param name="itemLink">
        /// The link.
        /// </param>
        /// <contract><requires name="itemLink" condition="not null"/></contract>
        public override void UpdateLink([NotNull] ItemLink itemLink)
        {
            this.imageField.UpdateLink(itemLink);
        }

        /// <summary>
        /// Validates the links.
        /// </summary>
        /// <param name="result">
        /// The result.
        /// </param>
        /// <contract><requires name="result" condition="not null"/></contract>
        public override void ValidateLinks([NotNull] LinksValidationResult result)
        {
            this.imageField.ValidateLinks(result);
        }

        /// <summary>
        /// Gets the width.
        /// </summary>
        /// <returns>The with of the image</returns>
        public int? GetWidth()
        {
            if (!string.IsNullOrWhiteSpace(this.Width))
            {
                int value;
                if (int.TryParse(this.Width, out value) && value > 0)
                {
                    return value;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the height.
        /// </summary>
        /// <returns>The Height of the media item</returns>
        public int? GetHeight()
        {
            if (!string.IsNullOrWhiteSpace(this.Height))
            {
                int value;
                if (int.TryParse(this.Height, out value) && value > 0)
                {
                    return value;
                }
            }

            return null;
        }

        /// <summary>
        /// The get media url.
        /// </summary>
        /// <returns>
        /// The Media Url.
        /// </returns>
        [NotNull]
        public string GetMediaItemUrl()
        {
            if (this.HasValue)
            {
                var mediaOptions = new MediaUrlOptions
                {
                    LowercaseUrls = true,
                    AbsolutePath = false,
                    IncludeExtension = true,
                    UseItemPath = false
                };

                /* /sitecore/templates/System/Media/Unversioned/File/Information/Extension
                 * {C06867FE-9A43-4C7D-B739-48780492D06F}
                 */
                var extension = this.MediaItem[FieldId];
                if (!string.IsNullOrWhiteSpace(extension))
                {
                    mediaOptions.RequestExtension = extension;
                }

                var mediaUrl = global::Sitecore.Resources.Media.MediaManager.GetMediaUrl(this.MediaItem, mediaOptions);

                if (string.IsNullOrWhiteSpace(mediaUrl))
                {
                    return string.Empty;
                }

                if (mediaUrl.StartsWith("http:"))
                {
                    return mediaUrl.Remove(0, "http:".Length);
                }

                if (mediaUrl.StartsWith("https:"))
                {
                    return mediaUrl.Remove(0, "https:".Length);
                }

                return mediaUrl;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the media URL.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns>The Media Url</returns>
        [NotNull]
        public string GetMediaItemUrl(int width, int height)
        {
            if (this.HasValue)
            {
                var mediaOptions = new MediaUrlOptions
                {
                    LowercaseUrls = true,
                    AbsolutePath = false,
                    IncludeExtension = true,
                    UseItemPath = false
                };

                /* /sitecore/templates/System/Media/Unversioned/File/Information/Extension
                 * {C06867FE-9A43-4C7D-B739-48780492D06F}
                 */
                var extension = this.MediaItem[FieldId];
                if (!string.IsNullOrWhiteSpace(extension))
                {
                    mediaOptions.RequestExtension = extension;
                }

                if (width > 1)
                {
                    mediaOptions.Width = width;
                }

                if (height > 1)
                {
                    mediaOptions.Height = height;
                }

                var mediaUrl = global::Sitecore.Resources.Media.MediaManager.GetMediaUrl(this.MediaItem, mediaOptions);

                if (string.IsNullOrWhiteSpace(mediaUrl))
                {
                    return string.Empty;
                }

                if (mediaUrl.StartsWith("http:"))
                {
                    return mediaUrl.Remove(0, "http:".Length);
                }

                if (mediaUrl.StartsWith("https:"))
                {
                    return mediaUrl.Remove(0, "https:".Length);
                }

                return mediaUrl;
            }

            return string.Empty;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Renders the specified data attributes.
        /// </summary>
        /// <returns>The Html Output</returns>
        [NotNull]
        public override MvcHtmlString Render()
        {
            using (var output = new XhtmlTagTextWriter())
            {
                this.Render(output);
                return output.Html;
            }
        }

        /// <summary>
        /// Renders the specified output.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="dataAttributes">The attributes.</param>
        public void Render(
            [NotNull] HtmlTextWriter output,
            [NotNull] params XAttribute[] dataAttributes)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (output != null)
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (this.HasValue && this.MediaItem != null && !this.MediaItem.Empty)
                {
                    // ReSharper restore ConditionIsAlwaysTrueOrFalse
                    var mediaUrl = this.GetMediaItemUrl();
                    var alt = string.IsNullOrWhiteSpace(this.Alt)
                                  ? this.Item.DisplayName
                                  : this.Alt;

                    output.Img(src: mediaUrl, alt: alt, title: alt, attributes: dataAttributes);
                }
                else
                {
                    var code = new XElement("code", string.Empty);

                    code.Add(new XAttribute("class", "error"));
                    code.Add(new XAttribute("data-error-message", "The media resource is missing!"));
                    code.Add(new XAttribute("data-media-item-id", this.Item.ID));
                    code.Add(new XAttribute("data-media-item-field-id", this.InnerField.ID));
                    output.Write(code);
                }
            }
        }

        /// <summary>
        /// Renders the specified output.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="dataAttributes">The attributes.</param>
        public void Render([NotNull] HtmlTextWriter output, [NotNull] IEnumerable<XAttribute> dataAttributes)
        {
            // ReSharper disable ConstantNullCoalescingCondition
            this.Render(output, (dataAttributes ?? new XAttribute[] { }).ToArray());
            // ReSharper restore ConstantNullCoalescingCondition
        }

        /// <summary>
        /// Renders the specified output.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="dataAttributes">The attributes.</param>
        public void Render(
            [NotNull] HtmlTextWriter output,
            int width,
            int height,
            [NotNull] params XAttribute[] dataAttributes)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (output != null)
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (this.HasValue && this.MediaItem != null && !this.MediaItem.Empty)
                {
                    // ReSharper restore ConditionIsAlwaysTrueOrFalse
                    var mediaUrl = this.GetMediaItemUrl(width, height);
                    var alt = string.IsNullOrWhiteSpace(this.Alt)
                                  ? this.Item.DisplayName
                                  : this.Alt;

                    output.Img(src: mediaUrl, alt: alt, title: alt, attributes: dataAttributes);
                }
                else
                {
                    var code = new XElement("code", string.Empty);

                    code.Add(new XAttribute("class", "error"));
                    code.Add(new XAttribute("data-error-message", "The media resource is missing!"));
                    code.Add(new XAttribute("data-media-item-id", this.Item.ID));
                    code.Add(new XAttribute("data-media-item-field-id", this.InnerField.ID));
                    output.Write(code);
                }
            }
        }

        /// <summary>
        /// Renders the specified output.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="dataAttributes">The attributes.</param>
        public void Render(
            [NotNull] HtmlTextWriter output,
            int width,
            int height,
            [NotNull] IEnumerable<XAttribute> dataAttributes)
        {
            // ReSharper disable ConstantNullCoalescingCondition
            this.Render(output, width, height, (dataAttributes ?? new XAttribute[] { }).ToArray());
            // ReSharper restore ConstantNullCoalescingCondition
        }

        /// <summary>
        /// Renders the specified data attributes.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="dataAttributes">The data attributes.</param>
        /// <returns>The Html Output</returns>
        [NotNull]
        public MvcHtmlString Render(int width, int height, [NotNull] params XAttribute[] dataAttributes)
        {
            using (var output = new XhtmlTagTextWriter())
            {
                this.Render(output, width, height, dataAttributes);
                return output.Html;
            }
        }

        /// <summary>
        /// Renders the specified data attributes.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="dataAttributes">The data attributes.</param>
        /// <returns>The Html Output</returns>
        [NotNull]
        public MvcHtmlString Render(int width, int height, [NotNull] IEnumerable<XAttribute> dataAttributes)
        {
            using (var output = new XhtmlTagTextWriter())
            {
                // ReSharper disable ConstantNullCoalescingCondition
                this.Render(output, width, height, (dataAttributes ?? new XAttribute[] { }).ToArray());
                // ReSharper restore ConstantNullCoalescingCondition
                return output.Html;
            }
        }

        /// <summary>
        /// Renders the specified data attributes.
        /// </summary>
        /// <param name="dataAttributes">The data attributes.</param>
        /// <returns>The Html Output</returns>
        [NotNull]
        public MvcHtmlString Render([NotNull] params XAttribute[] dataAttributes)
        {
            using (var output = new XhtmlTagTextWriter())
            {
                this.Render(output, dataAttributes);
                return output.Html;
            }
        }

        /// <summary>
        /// Renders the specified data attributes.
        /// </summary>
        /// <param name="dataAttributes">The data attributes.</param>
        /// <returns>The Html Output</returns>
        [NotNull]
        public MvcHtmlString Render([NotNull] IEnumerable<XAttribute> dataAttributes)
        {
            using (var output = new XhtmlTagTextWriter())
            {
                // ReSharper disable ConstantNullCoalescingCondition
                this.Render(output, (dataAttributes ?? new XAttribute[] { }).ToArray());
                // ReSharper restore ConstantNullCoalescingCondition
                return output.Html;
            }
        }

        #endregion
    }
}
