﻿namespace Jig.Sitecore.Data.Fields
{
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Data.Templates;
    using global::Sitecore.Globalization;
    using global::Sitecore.Web.UI.WebControls;

    /// <summary>
    /// Provides a facade for Sitecore CustomField.
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptIn)]
    [Newtonsoft.Json.JsonConverter(typeof(Jig.Sitecore.Data.Fields.JsonConverters.FieldPropertyJsonConverter))]
    public abstract class FieldProperty : global::Sitecore.Data.Fields.CustomField, IFieldProperty
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldProperty"/> class.
        /// </summary>
        /// <param name="field">
        /// The field to wrap.
        /// </param>
        protected FieldProperty([NotNull] Field field)
            : base(field)
        {
            // Nothing to do Stylecop.
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether the field can be read in the current context.
        /// </summary>
        public bool CanRead
        {
            get { return this.InnerField.CanRead; }
        }

        /// <summary>
        /// Gets a value indicating whether the field can be written in the current context.
        /// </summary>
        public bool CanWrite
        {
            get { return this.InnerField.CanWrite; }
        }

        /// <summary>
        /// Gets a value indicating whether the field's value is a Standard Value.
        /// </summary>
        public bool ContainsStandardValue
        {
            get { return this.InnerField.ContainsStandardValue; }
        }

        /// <summary>
        /// Gets a value indicating whether the field's value is inherited from another Item.
        /// </summary>
        public bool InheritsValueFromOtherItem
        {
            get { return this.InnerField.InheritsValueFromOtherItem; }
        }

        /// <summary>
        /// Gets the Database for the field.
        /// </summary>
        [NotNull]
        public Database Database
        {
            get { return this.InnerField.Database; }
        }

        /// <summary>
        /// Gets the field definition.
        /// </summary>
        [NotNull]
        public TemplateField Definition
        {
            get { return this.InnerField.Definition; }
        }

        /// <summary>
        /// Gets the field description.
        /// </summary>
        [NotNull]
        public string Description
        {
            get { return this.InnerField.Description; }
        }

        /// <summary>
        /// Gets the field display name. (Includes field Title.)
        /// </summary>
        [NotNull]
        public string DisplayName
        {
            get { return this.InnerField.DisplayName; }
        }

        /// <summary>
        /// Gets the field's Section Name.
        /// </summary>
        [NotNull]
        public string SectionDisplayName
        {
            get { return this.InnerField.SectionDisplayName; }
        }

        /// <summary>
        /// Gets a value indicating whether the field has a blob stream.
        /// </summary>
        public bool HasBlobStream
        {
            get { return this.InnerField.HasBlobStream; }
        }

        /// <summary>
        /// Gets a value indicating whether the field's value is not null or empty.
        /// </summary>
        public virtual bool HasValue
        {
            get
            {
                var value = this.InnerField.HasValue;

                if (!value)
                {
                    value = !string.IsNullOrEmpty(this.InnerField.InheritedValue);
                }

                return value;
            }
        }

        /// <summary>
        /// Gets the URL for external help text.
        /// </summary>
        [NotNull]
        public string HelpLink
        {
            get { return this.InnerField.HelpLink; }
        }

        /// <summary>
        /// Gets the field's ID.
        /// </summary>
        // ReSharper disable InconsistentNaming
        [NotNull]
        public ID ID
        {
            // ReSharper restore InconsistentNaming
            get { return this.InnerField.ID; }
        }

        /// <summary>
        /// Gets the field's inherited value.
        /// </summary>
        [NotNull]
        public string InheritedValue
        {
            get { return this.InnerField.InheritedValue; }
        }

        /// <summary>
        /// Gets a value indicating whether the field is a Blob.
        /// </summary>
        public bool IsBlobField
        {
            get { return this.InnerField.IsBlobField; }
        }

        /// <summary>
        /// Gets a value indicating whether the field's value has been modified for this instance.
        /// </summary>
        public bool IsModified
        {
            get { return this.InnerField.IsModified; }
        }

        /// <summary>
        /// Gets the Item that the field belongs to.
        /// </summary>
        [NotNull]
        public Item Item
        {
            get { return this.InnerField.Item; }
        }

        /// <summary>
        /// Gets the field Item's Key.
        /// </summary>
        [NotNull]
        public string Key
        {
            get { return this.InnerField.Key; }
        }

        /// <summary>
        /// Gets the field Item's Language.
        /// </summary>
        [NotNull]
        public Language Language
        {
            get { return this.InnerField.Language; }
        }

        /// <summary>
        /// Gets the field Item's Name (also used as an index in the Item's Fields collection).
        /// </summary>
        [NotNull]
        public string Name
        {
            get { return this.InnerField.Name; }
        }

        /// <summary>
        /// Gets a value indicating whether the field has been reset to blank.
        /// </summary>
        public bool ResetBlank
        {
            get { return this.InnerField.ResetBlank; }
        }

        /// <summary>
        /// Gets the Field's Section Item.
        /// </summary>
        [NotNull]
        public string Section
        {
            get { return this.InnerField.Section; }
        }

        /// <summary>
        /// Gets the translated name of the field's section.
        /// </summary>
        // ReSharper disable InconsistentNaming
        [NotNull]
        public string SectionNameByUILocale
        {
            // ReSharper restore InconsistentNaming
            get { return this.InnerField.SectionNameByUILocale; }
        }

        /// <summary>
        /// Gets the Sort Order for the field's section.
        /// </summary>
        public int SectionSortOrder
        {
            get { return this.InnerField.SectionSortorder; }
        }

        /// <summary>
        /// Gets a value indicating whether the field's value is shared across languages.
        /// </summary>
        public bool Shared
        {
            get { return this.InnerField.Shared; }
        }

        /// <summary>
        /// Gets a value indicating whether the field's value should be translated.
        /// </summary>
        public bool ShouldBeTranslated
        {
            get { return this.InnerField.ShouldBeTranslated; }
        }

        /// <summary>
        /// Gets the field's Sort Order used within the scope of the Field's Section.
        /// </summary>
        public int SortOrder
        {
            get { return this.InnerField.Sortorder; }
        }

        /// <summary>
        /// Gets the field's data source.
        /// </summary>
        [NotNull]
        public string Source
        {
            get { return this.InnerField.Source; }
        }

        /// <summary>
        /// Gets the field's CSS settings, used in the Content Editor.
        /// </summary>
        [NotNull]
        public string Style
        {
            get { return this.InnerField.Style; }
        }

        /// <summary>
        /// Gets the field's Title. (Rarely used.)
        /// </summary>
        [NotNull]
        public string Title
        {
            get { return this.InnerField.Title; }
        }

        /// <summary>
        /// Gets the field's ToolTip. (Field's Long Description help text.)
        /// </summary>
        [NotNull]
        public string ToolTip
        {
            get { return this.InnerField.ToolTip; }
        }

        /// <summary>
        /// Gets a value indicating whether the field's value is translatable.
        /// </summary>
        public bool Translatable
        {
            get { return this.InnerField.Translatable; }
        }

        /// <summary>
        /// Gets the Sitecore field "type", which is a string.
        /// </summary>
        [NotNull]
        public string Type
        {
            get { return this.InnerField.Type; }
        }

        /// <summary>
        /// Gets the field type key (believed to be the lowercase name of the Type).
        /// </summary>
        [NotNull]
        public string TypeKey
        {
            get { return this.InnerField.TypeKey; }
        }

        /// <summary>
        /// Gets a value indicating whether the field's value applies to all versions of the Item 
        /// for a given language.
        /// </summary>
        public bool Unversioned
        {
            get { return this.InnerField.Unversioned; }
        }

        /// <summary>
        /// Gets a RegEx string for old-style validation.
        /// </summary>
        [NotNull]
        public string Validation
        {
            get { return this.InnerField.Validation; }
        }

        /// <summary>
        /// Gets a validation message applying to old-style RegEx validation.
        /// </summary>
        [NotNull]
        public string ValidationText
        {
            get { return this.InnerField.ValidationText; }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Removes the current value from the field.
        /// </summary>
        public void Clear()
        {
            this._innerField.Value = string.Empty;
        }

        /// <summary>
        /// Updates the value of the field.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="force">Normally, if the value is the same
        /// as the current value, the field is not updated.
        /// Force causes the field to be marked
        /// as updated anyway.</param>
        public void SetValue([NotNull] string value, bool force = false)
        {
            this._innerField.SetValue(value, force);
        }

        /// <summary>
        /// The field render.
        /// </summary>
        /// <returns>The Sitecore Page Editor Mode Html</returns>
        [NotNull]
        public virtual string FieldRender()
        {
            var html = FieldRenderer.Render(this.Item, this.Name);

            return html;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (this.HasValue)
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                var html = FieldRenderer.Render(this.Item, this.Name);

                return html;
            }

            return string.Empty;
        }

        #endregion
    }
}
