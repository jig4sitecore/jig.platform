﻿namespace Jig.Sitecore.Data.Fields.Pipelines.LinkPropertyRenders
{
    using global::Sitecore;

    /// <summary>
    /// The LinkPropertyRender interface.
    /// </summary>
    public interface ILinkPropertyRender
    {
        #region Public Methods and Operators

        /// <summary>
        /// The render.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        void Process([NotNull] LinkPropertyRenderPipelineArgs args);

        #endregion
    }
}