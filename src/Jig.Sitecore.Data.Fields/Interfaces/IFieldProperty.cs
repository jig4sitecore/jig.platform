﻿namespace Jig.Sitecore.Data.Fields
{
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Data.Templates;
    using global::Sitecore.Globalization;

    /// <summary>
    /// The FieldProperty interface.
    /// </summary>
    public interface IFieldProperty
    {
        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether the field can be read in the current context.
        /// </summary>
        bool CanRead { get; }

        /// <summary>
        /// Gets a value indicating whether the field can be written in the current context.
        /// </summary>
        bool CanWrite { get; }

        /// <summary>
        /// Gets a value indicating whether the field's value is a Standard Value.
        /// </summary>
        bool ContainsStandardValue { get; }

        /// <summary>
        /// Gets the Database for the field.
        /// </summary>
        [NotNull]
        Database Database { get; }

        /// <summary>
        /// Gets the field definition.
        /// </summary>
        [NotNull]
        TemplateField Definition { get; }

        /// <summary>
        /// Gets the field description.
        /// </summary>
        [NotNull]
        string Description { get; }

        /// <summary>
        /// Gets the field display name. (Includes field Title.)
        /// </summary>
        [NotNull]
        string DisplayName { get; }

        /// <summary>
        /// Gets a value indicating whether the field has a blob stream.
        /// </summary>
        bool HasBlobStream { get; }

        /// <summary>
        /// Gets a value indicating whether the field's value is not null or empty.
        /// </summary>
        bool HasValue { get; }

        /// <summary>
        /// Gets the URL for external help text.
        /// </summary>
        [NotNull]
        string HelpLink { get; }

        /// <summary>
        /// Gets the field's ID.
        /// </summary>
        // ReSharper disable InconsistentNaming
        [NotNull]
        ID ID { get; }

        // ReSharper restore InconsistentNaming

        /// <summary>
        /// Gets the field's inherited value.
        /// </summary>
        [NotNull]
        string InheritedValue { get; }

        /// <summary>
        /// Gets a value indicating whether the field's value is inherited from another Item.
        /// </summary>
        bool InheritsValueFromOtherItem { get; }

        /// <summary>
        /// Gets the inner field.
        /// </summary>
        [NotNull]
        Field InnerField { get; }

        /// <summary>
        /// Gets a value indicating whether the field is a Blob.
        /// </summary>
        bool IsBlobField { get; }

        /// <summary>
        /// Gets a value indicating whether the field's value has been modified for this instance.
        /// </summary>
        bool IsModified { get; }

        /// <summary>
        /// Gets the Item that the field belongs to.
        /// </summary>
        [NotNull]
        Item Item { get; }

        /// <summary>
        /// Gets the field Item's Key.
        /// </summary>
        [NotNull]
        string Key { get; }

        /// <summary>
        /// Gets the field Item's Language.
        /// </summary>
        [NotNull]
        Language Language { get; }

        /// <summary>
        /// Gets the field Item's Name (also used as an index in the Item's Fields collection).
        /// </summary>
        [NotNull]
        string Name { get; }

        /// <summary>
        /// Gets a value indicating whether the field has been reset to blank.
        /// </summary>
        bool ResetBlank { get; }

        /// <summary>
        /// Gets the Field's Section Item.
        /// </summary>
        [NotNull]
        string Section { get; }

        /// <summary>
        /// Gets the field's Section Name.
        /// </summary>
        [NotNull]
        string SectionDisplayName { get; }

        /// <summary>
        /// Gets the translated name of the field's section.
        /// </summary>
        // ReSharper disable InconsistentNaming
        [NotNull]
        string SectionNameByUILocale { get; }

        // ReSharper restore InconsistentNaming

        /// <summary>
        /// Gets the Sort Order for the field's section.
        /// </summary>
        int SectionSortOrder { get; }

        /// <summary>
        /// Gets a value indicating whether the field's value is shared across languages.
        /// </summary>
        bool Shared { get; }

        /// <summary>
        /// Gets a value indicating whether the field's value should be translated.
        /// </summary>
        bool ShouldBeTranslated { get; }

        /// <summary>
        /// Gets the field's Sort Order used within the scope of the Field's Section.
        /// </summary>
        int SortOrder { get; }

        /// <summary>
        /// Gets the field's data source.
        /// </summary>
        [NotNull]
        string Source { get; }

        /// <summary>
        /// Gets the field's Title. (Rarely used.)
        /// </summary>
        [NotNull]
        string Title { get; }

        /// <summary>
        /// Gets the field's ToolTip. (Field's Long Description help text.)
        /// </summary>
        [NotNull]
        string ToolTip { get; }

        /// <summary>
        /// Gets a value indicating whether the field's value is translatable.
        /// </summary>
        bool Translatable { get; }

        /// <summary>
        /// Gets the Sitecore field "type", which is a string.
        /// </summary>
        [NotNull]
        string Type { get; }

        /// <summary>
        /// Gets the field type key (believed to be the lowercase name of the Type).
        /// </summary>
        [NotNull]
        string TypeKey { get; }

        /// <summary>
        /// Gets a value indicating whether the field's value applies to all versions of the Item 
        /// for a given language.
        /// </summary>
        bool Unversioned { get; }

        /// <summary>
        /// Gets a RegEx string for old-style validation.
        /// </summary>
        [NotNull]
        string Validation { get; }

        /// <summary>
        /// Gets a validation message applying to old-style RegEx validation.
        /// </summary>
        [NotNull]
        string ValidationText { get; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        [NotNull]
        string Value { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Removes the current value from the field.
        /// </summary>
        void Clear();

        /// <summary>
        /// The field render.
        /// </summary>
        /// <returns>
        /// The Sitecore Page Editor Mode Html
        /// </returns>
        [NotNull]
        string FieldRender();

        /// <summary>
        /// Updates the value of the field.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="force">
        /// Normally, if the value is the same
        /// as the current value, the field is not updated.
        /// Force causes the field to be marked
        /// as updated anyway.
        /// </param>
        void SetValue([NotNull] string value, bool force = false);

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        [NotNull]
        string ToString();

        #endregion
    }
}