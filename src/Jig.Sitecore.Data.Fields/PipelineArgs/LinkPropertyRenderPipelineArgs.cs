﻿namespace Jig.Sitecore.Data.Fields.Pipelines
{
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Xml.Linq;
    using Jig.Sitecore.Data.Fields;
    using Jig.Web.Collections;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;

    /// <summary>
    /// The link property render pipeline args.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable", Justification = "The serialization never used any more. Legacy"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2240:ImplementISerializableCorrectly", Justification = "No unmanaged resources were allocated")]
    public sealed class LinkPropertyRenderPipelineArgs : global::Sitecore.Pipelines.PipelineArgs, ISerializable
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkPropertyRenderPipelineArgs"/> class.
        /// </summary>
        /// <param name="linkProperty">
        /// The link property.
        /// </param>
        public LinkPropertyRenderPipelineArgs([NotNull] LinkProperty linkProperty)
            : this(linkProperty, new XAttribute[] { })
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkPropertyRenderPipelineArgs"/> class.
        /// </summary>
        /// <param name="linkProperty">
        /// The link property.
        /// </param>
        /// <param name="attributes">
        /// The attributes.
        /// </param>
        public LinkPropertyRenderPipelineArgs([NotNull] LinkProperty linkProperty, [NotNull] XAttribute[] attributes)
        {
            Assert.IsNotNull(linkProperty, "LinkProperty");
            Assert.IsNotNull(attributes, "attributes");

            this.LinkProperty = linkProperty;
            this.InnerHtml = linkProperty.Text;
            this.Title = linkProperty.Title;
            this.Target = linkProperty.Target;
            this.Anchor = linkProperty.Anchor;
            this.QueryString = linkProperty.QueryString;
            this.CssClass = linkProperty.Class;
            this.Attributes = new XAttributeDictionary(attributes);

            /* Override code attributes with CMS  */
            var dataAttr = linkProperty.GetDataAttributes().ToArray();
            this.Attributes.Add(dataAttr);
            this.Output = string.Empty;
       } 

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the href.
        /// </summary>
        [CanBeNull]
        public string Href { get; set; }

        /// <summary>
        /// Gets or sets the anchor.
        /// </summary>
        [CanBeNull]
        public string Anchor { get; set; }

        /// <summary>
        /// Gets the attributes.
        /// </summary>
        [NotNull]
        public XAttributeDictionary Attributes { get; private set; }

        /// <summary>
        /// Gets or sets the css class.
        /// </summary>
        [CanBeNull]
        public string CssClass { get; set; }

        /// <summary>
        /// Gets or sets the inner html.
        /// </summary>
        [CanBeNull]
        public string InnerHtml { get; set; }

        /// <summary>
        /// Gets the link property.
        /// </summary>
        [NotNull]
        public LinkProperty LinkProperty { get; private set; }

        /// <summary>
        /// Gets or sets the query string.
        /// </summary>
        [CanBeNull]
        public string QueryString { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the target.
        /// </summary>
        /// <value>The target.</value>
        [CanBeNull]
        public string Target { get; set; }

        /// <summary>
        /// Gets or sets the output.
        /// </summary>
        /// <value>The output.</value>
        [NotNull]
        public string Output { get; set; }

        #endregion
    }
}