﻿namespace Jig.Sitecore.Data.Validators.FieldValidators
{
    using System.Text.RegularExpressions;

    using global::Sitecore;
    using global::Sitecore.Data.Validators;

    /// <summary>
    /// The is valid regular expression validator.
    /// </summary>
    public class IsValidRegularExpressionValidator : global::Jig.Sitecore.Data.Validators.StandardValidator
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        [NotNull]
        public override string Name
        {
            get
            {
                return "Is Valid Regular Expression";
            }
        }

        /// <summary>
        /// The validate.
        /// </summary>
        /// <returns>
        /// The <see cref="ValidatorResult"/>.
        /// </returns>
        protected override ValidatorResult Validate()
        {
            var item = this.GetItem();
            if (item == null)
            {
                return ValidatorResult.Valid;
            }

            var field = this.GetField();
            if (field == null)
            {
                return ValidatorResult.Valid;
            }

            var value = this.ControlValidationValue;

            try
            {
                // ReSharper disable UnusedVariable
                var regex = new Regex(value);
                // ReSharper restore UnusedVariable
                return ValidatorResult.Valid;
            }
            catch
            {
                return ValidatorResult.CriticalError;
            }
        }
    }
}