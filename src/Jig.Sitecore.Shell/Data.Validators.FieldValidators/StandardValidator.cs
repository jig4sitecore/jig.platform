﻿namespace Jig.Sitecore.Data.Validators
{
    using System;
    using System.Runtime.Serialization;
    using log4net;
    using global::Sitecore;
    using global::Sitecore.Data.Validators;

    /// <summary>
    /// The standard validator.
    /// </summary>
    public abstract class StandardValidator : global::Sitecore.Data.Validators.StandardValidator
    {
        /// <summary>
        /// The log
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger("FieldValidators");

        /// <summary>
        /// Initializes a new instance of the <see cref="StandardValidator"/> class.
        /// </summary>
        protected StandardValidator()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StandardValidator"/> class.
        /// </summary>
        /// <param name="info">
        /// The information.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        protected StandardValidator([NotNull] SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #region Methods

        /// <summary>
        /// The evaluate.
        /// </summary>
        /// <returns>
        /// The <see cref="ValidatorResult" />.
        /// </returns>
        protected override sealed ValidatorResult Evaluate()
        {
            try
            {
                var result = this.Validate();
                return result;
            }
            catch (Exception exception)
            {
                Log.Error(exception.Message, exception);
            }

            return ValidatorResult.Unknown;
        }

        /// <summary>
        /// The get max validator result.
        /// </summary>
        /// <returns>
        /// The <see cref="ValidatorResult" />.
        /// </returns>
        protected override ValidatorResult GetMaxValidatorResult()
        {
            return this.GetFailedResult(ValidatorResult.Error);
        }

        /// <summary>
        /// The validate.
        /// </summary>
        /// <returns>
        /// The <see cref="ValidatorResult" />.
        /// </returns>
        protected abstract ValidatorResult Validate();

        #endregion
    }
}