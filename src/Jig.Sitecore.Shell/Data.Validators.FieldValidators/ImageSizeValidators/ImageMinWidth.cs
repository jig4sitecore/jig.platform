﻿namespace Jig.Sitecore.Data.Validators.FieldValidators.ImageSizeValidators
{
    using System.Drawing;

    using global::Sitecore.Data.Items;

    using global::Sitecore.Data.Validators;

    /// <summary>
    /// The min image width validator.
    /// </summary>
    public sealed class ImageMinWidth : ImageSizeValidator
    {
        #region Methods

        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="image">The image.</param>
        /// <returns>
        /// The <see cref="ValidatorResult" />.
        /// </returns>
        protected override ValidatorResult Validate(
            MediaItem mediaItem, 
            Image image)
        {
            string value;
            if (this.Parameters.TryGetValue("MinWidth", out value))
            {
                int size;
                if (int.TryParse(value, out size))
                {
                    if (image.PhysicalDimension.Width < size)
                    {
                        return ValidatorResult.Error;
                    }

                    return ValidatorResult.Valid;
                }
            }

            return ValidatorResult.Unknown;
        }

        #endregion
    }
}