﻿namespace Jig.Sitecore.Data.Validators.FieldValidators.ImageSizeValidators
{
    using System;
    using System.Text;
    using System.Windows.Media.Imaging;

    using global::Sitecore;
    using global::Sitecore.Data.Items;

    using global::Sitecore.Data.Validators;

    /// <summary>
    /// The image metadata validator.
    /// </summary>
    public sealed class ImageMetadata : Jig.Sitecore.Data.Validators.FieldValidators.ImageValidators.ImageValidator
    {
        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="mediaItem">The media item.</param>
        /// <returns>
        /// The <see cref="ValidatorResult" />.
        /// </returns>
        protected override ValidatorResult Validate(System.IO.Stream stream, MediaItem mediaItem)
        {
            BitmapMetadata meta = this.GetBitmapMetadata(stream, mediaItem);
            if (meta == null)
            {
                /* Not JPEG */
                return ValidatorResult.Valid;
            }

            if (!string.IsNullOrWhiteSpace(meta.ApplicationName))
            {
                this.AddError(mediaItem, "ApplicationName");
            }

            if (meta.Author != null)
            {
                this.AddError(mediaItem, "Author");
            }

            if (!string.IsNullOrWhiteSpace(meta.CameraManufacturer))
            {
                this.AddError(mediaItem, "CameraManufacturer");
            }

            if (!string.IsNullOrWhiteSpace(meta.CameraModel))
            {
                this.AddError(mediaItem, "CameraModel");
            }

            if (!string.IsNullOrWhiteSpace(meta.Comment))
            {
                this.AddError(mediaItem, "Comment");
            }

            if (!string.IsNullOrWhiteSpace(meta.Title))
            {
                this.AddError(mediaItem, "Title");
            }

            if (!string.IsNullOrWhiteSpace(meta.Subject))
            {
                this.AddError(mediaItem, "Subject");
            }

            if (meta.Keywords != null)
            {
                this.AddError(mediaItem, "Subject");
            }

            if (!string.IsNullOrWhiteSpace(meta.DateTaken))
            {
                this.AddError(mediaItem, "DateTaken");
            }

            if (this.Errors.Count == 0)
            {
                return ValidatorResult.Valid;
            }

            var msg = this.GetValidationText();

            this.Text = msg;
            this.LogError(msg, mediaItem);

            return ValidatorResult.CriticalError;
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <param name="mediaItem">The media item.</param>
        private void LogError([NotNull] string error, [NotNull] MediaItem mediaItem)
        {
            var message = new StringBuilder(error, error.Length + 128);
            message.Append("Media ID: ").AppendLine(mediaItem.ID.ToString());
            message.Append("Media Path: ").AppendLine(mediaItem.Path);
            message.Append("User: ").AppendLine(Context.GetUserName());

            Log.Error(message);
        }

        /// <summary>
        /// Adds the error.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="metadataFieldName">Name of the metadata field.</param>
        private void AddError([NotNull] MediaItem mediaItem, [NotNull] string metadataFieldName)
        {
            var msg = string.Format(
                "The Image '{0}' is not web optimized. It has metadata - {1}",
                mediaItem.DisplayName,
                metadataFieldName);

            this.Errors.Add(msg);
        }

        /// <summary>
        /// Gets the bitmap metadata.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="mediaItem">The media item.</param>
        /// <returns>The Bitmap Metadata</returns>
        [CanBeNull]
        private BitmapMetadata GetBitmapMetadata([NotNull] System.IO.Stream stream, [NotNull] MediaItem mediaItem)
        {
            if (string.Equals(mediaItem.Extension, "jpg", StringComparison.InvariantCultureIgnoreCase)
                || string.Equals(mediaItem.Extension, "jpeg", StringComparison.InvariantCultureIgnoreCase))
            {
                var img = System.Windows.Media.Imaging.BitmapFrame.Create(stream);

                var meta = (BitmapMetadata)img.Metadata;

                return meta;
            }

            return null;
        }

        /// <summary>
        /// Gets the text.
        /// </summary>
        /// <returns>The Error Message</returns>
        [NotNull]
        private string GetValidationText()
        {
            var builder = new StringBuilder(256);
            foreach (var error in this.Errors)
            {
                builder.AppendLine(error);
            }

            return builder.ToString();
        }
    }
}