﻿namespace Jig.Sitecore.Data.Validators.FieldValidators.ImageValidators
{
    using System;
    using System.IO;
    using System.Runtime.Serialization;
    using global::Sitecore;
    using global::Sitecore.Configuration;

    using global::Sitecore.Data;

    using global::Sitecore.Data.Fields;

    using global::Sitecore.Data.Items;

    using global::Sitecore.Data.Validators;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Shell.Applications.ContentEditor;

    /// <summary>
    /// The image validator.
    /// </summary>
    public abstract class ImageValidator : Jig.Sitecore.Data.Validators.StandardValidator
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageValidator"/> class.
        /// </summary>
        protected ImageValidator()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageValidator" /> class.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="context">The context.</param>
        protected ImageValidator([NotNullAttribute] SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the name.
        /// </summary>
        [NotNull]
        public override string Name
        {
            get
            {
                var name = this.GetType().Name;
                return name;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get max validator result.
        /// </summary>
        /// <returns>
        /// The <see cref="ValidatorResult"/>.
        /// </returns>
        protected override ValidatorResult GetMaxValidatorResult()
        {
            return this.GetFailedResult(ValidatorResult.Error);
        }

        /// <summary>
        /// The validate.
        /// </summary>
        /// <returns>
        /// The <see cref="ValidatorResult"/>.
        /// </returns>
        protected sealed override ValidatorResult Validate()
        {
            ItemUri itemUri = this.ItemUri;
            if (itemUri == null)
            {
                return ValidatorResult.Valid;
            }

            Field field = this.GetField();
            if (field == null)
            {
                return ValidatorResult.Valid;
            }

            string controlValidationValue = this.ControlValidationValue;
            if (string.IsNullOrEmpty(controlValidationValue)
                || string.Compare(controlValidationValue, "<image />", StringComparison.InvariantCulture) == 0)
            {
                return ValidatorResult.Valid;
            }

            string attribute = new XmlValue(controlValidationValue, "image").GetAttribute("mediaid");
            if (string.IsNullOrEmpty(attribute))
            {
                return ValidatorResult.Valid;
            }

            Database database = Factory.GetDatabase(itemUri.DatabaseName);
            Assert.IsNotNull(database, itemUri.DatabaseName);

            MediaItem mediaItem = database.GetItem(attribute);
            if (mediaItem == null)
            {
                return ValidatorResult.Valid;
            }

            using (Stream stream = this.GetMediaStream(mediaItem))
            {
                if (stream != null)
                {
                    try
                    {
                        Log.Debug(string.Concat("Executing validator '", this.GetType().FullName, "' for media item => ", mediaItem.DisplayName, " with id ", mediaItem.ID));

                        ValidatorResult result = this.Validate(stream, mediaItem);
                        return result;
                    }
                    catch (Exception exception)
                    {
                        Log.Error(exception.Message, exception);
                        return ValidatorResult.Unknown;
                    }
                }
            }

            return ValidatorResult.Valid;
        }

        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="mediaItem">The media item.</param>
        /// <returns>
        /// The <see cref="ValidatorResult" />.
        /// </returns>
        protected abstract ValidatorResult Validate([NotNull] Stream stream, [NotNull]MediaItem mediaItem);

        /// <summary>
        /// Gets the media stream.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <returns>The media stream</returns>
        [CanBeNull]
        protected virtual Stream GetMediaStream([NotNull]MediaItem mediaItem)
        {
            return mediaItem.GetMediaStream();
        }

        #endregion
    }
}