﻿namespace Jig.Sitecore.Data.Validators.FieldValidators.ImageSizeValidators
{
    using System.Drawing;
    using System.IO;
    using System.Runtime.Serialization;
    using global::Sitecore;
    using global::Sitecore.Data.Items;

    using global::Sitecore.Data.Validators;

    /// <summary>
    /// The image size validator.
    /// </summary>
    public abstract class ImageSizeValidator :
        Jig.Sitecore.Data.Validators.FieldValidators.ImageValidators.ImageValidator
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageSizeValidator"/> class.
        /// </summary>
        protected ImageSizeValidator()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageSizeValidator" /> class.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="context">The context.</param>
        protected ImageSizeValidator([NotNull] SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="image">The image.</param>
        /// <returns>
        /// The <see cref="ValidatorResult" />.
        /// </returns>
        protected abstract ValidatorResult Validate(
            [NotNull] MediaItem mediaItem,
            [NotNull] Image image);

        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="mediaItem">The media item.</param>
        /// <returns>
        /// The <see cref="ValidatorResult" />.
        /// </returns>
        protected override ValidatorResult Validate(
            [NotNull] Stream stream,
            [NotNull] MediaItem mediaItem)
        {
            using (Image image = System.Drawing.Image.FromStream(stream))
            {
                ValidatorResult result = this.Validate(mediaItem, image);
                return result;
            }
        }

        #endregion
    }
}