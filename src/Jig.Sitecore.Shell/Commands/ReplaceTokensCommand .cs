﻿namespace Jig.Sitecore.Shell.Commands
{
    using System.Diagnostics;
    using global::Sitecore;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Shell.Framework.Commands;

    /// <summary>
    /// Class ReplaceTokensCommand.
    /// </summary>
    /// <remarks>Expand $token command button in the content editor ribbon</remarks>
    /// <author>
    /// Original code https://github.com/sobek1985/TokenReplacementCommand
    /// </author>
    public sealed class ReplaceTokensCommand : Command
    {
        /// <summary>
        /// Executes the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        public override void Execute([NotNull]CommandContext context)
        {
            Item item = context.Items[0];

            this.ReplaceTokens(item);

            global::Sitecore.Context.ClientPage.SendMessage(this, "item:load(id=" + item.ID + ")");
        }

        /// <summary>
        /// Queries the state.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>Command State</returns>
        public override CommandState QueryState([NotNull]CommandContext context)
        {
            Assert.ArgumentNotNull(context, "context");

            if (context.Items.Length != 1)
            {
                return CommandState.Hidden;
            }

            Item items = context.Items[0];
            if (items.Appearance.ReadOnly)
            {
                return CommandState.Disabled;
            }

            if (items.Versions.Count == 0)
            {
                return CommandState.Disabled;
            }

            if (Command.IsLockedByOther(items))
            {
                return CommandState.Disabled;
            }

            return base.QueryState(context);
        }

        /// <summary>
        /// Replaces the tokens.
        /// </summary>
        /// <param name="item">The item.</param>
        private void ReplaceTokens([NotNull]Item item)
        {
            try
            {
                item.Editing.BeginEdit();

                item.Fields.ReadAll();

                foreach (Field field in item.Fields)
                {
                    if (field.Value.Contains("$"))
                    {
                        global::Sitecore.Data.MasterVariablesReplacer replacer = global::Sitecore.Configuration.Factory.GetMasterVariablesReplacer();
                        global::Sitecore.Diagnostics.Assert.IsNotNull(replacer, "replacer");
                        replacer.ReplaceItem(item);
                    }
                }

                item.Editing.AcceptChanges(true, false);
            }
            catch (System.Exception ex)
            {
                item.Editing.CancelEdit();
                Trace.TraceError(ex.Message);
                Log.Error(ex.Message, ex, this.GetType());
            }
        }
    }
}
