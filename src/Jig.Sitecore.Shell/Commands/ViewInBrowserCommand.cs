﻿namespace Jig.Sitecore.Shell.Commands
{
    using System;
    using System.Collections.Specialized;
    using System.Linq;
    using global::Sitecore;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Links;
    using global::Sitecore.Shell.Framework.Commands;
    using global::Sitecore.Sites;
    using global::Sitecore.Web;
    using global::Sitecore.Web.UI.Sheer;

    /// <summary>
    /// The view in browser command
    /// </summary>
    public sealed partial class ViewInBrowserCommand : Command
    {
        /// <summary>
        /// Executes the command in the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        public override void Execute([NotNull]CommandContext context)
        {
            Assert.IsNotNull(context, "context");
            if (context.Items.Length == 1)
            {
                var parameters = new NameValueCollection();
                parameters["items"] = this.SerializeItems(context.Items);
                Context.ClientPage.Start(this, "Run", parameters);
            }
        }

        /// <summary>
        /// Queries the state.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The CommandState</returns>
        public override CommandState QueryState([NotNull]CommandContext context)
        {
            if (context.Items.Length != 1)
            {
                return CommandState.Hidden;
            }

            var item = context.Items[0];
            if (item == null)
            {
                return CommandState.Hidden;
            }

            if (!item.Paths.FullPath.StartsWith(global::Jig.Sitecore.Constants.Paths.Sites.Content, StringComparison.InvariantCultureIgnoreCase))
            {
                return CommandState.Hidden;
            }

            if (item.Key.StartsWith("_", StringComparison.OrdinalIgnoreCase))
            {
                return CommandState.Disabled;
            }

            if (item.Parent != null && item.Parent.Key.StartsWith("_", StringComparison.OrdinalIgnoreCase))
            {
                return CommandState.Disabled;
            }

            if (!item.HasLayout())
            {
                return CommandState.Disabled;
            }

            return base.QueryState(context);
        }

        /// <summary>
        /// Runs the pipeline.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <remarks>this will be run from button in ribbon</remarks>
        // ReSharper disable UnusedMember.Local
        private void Run([NotNull]ClientPipelineArgs args)
        // ReSharper restore UnusedMember.Local
        {
            Assert.IsNotNull(args, "args");
            SheerResponse.CheckModified(false);
            var item = this.DeserializeItems(args.Parameters["items"])[0];
            if (item != null)
            {
                var url = this.ResolveUrl(item);

                SheerResponse.Eval(string.Format("window.open('{0}','_blank');", url));
            }
        }

        /// <summary>
        /// Resolves the URL.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>The Item Url</returns>
        [NotNull]
        private string ResolveUrl([NotNull] Item item)
        {
            SiteInfo siteInfo = global::Sitecore.Configuration.Factory.GetSiteInfoList()
                                    .Where(x => !string.IsNullOrWhiteSpace(x.HostName) && item.Paths.FullPath.StartsWith(x.RootPath + x.StartItem, StringComparison.InvariantCultureIgnoreCase))
                                    .OrderByDescending(x => x.HostName)
                                    .FirstOrDefault() ?? global::Sitecore.Configuration.Factory.GetSiteInfo("website");

            var site = new SiteContext(siteInfo);

            using (new SiteContextSwitcher(site))
            {
                var urlOptions = site.GetDefaultUrlOptions();

                urlOptions.SiteResolving = true;

                var url = LinkManager.GetItemUrl(item, urlOptions);
                return url;
            }
        }
    }
}