﻿namespace Jig.Sitecore.Shell.Commands
{
    using System.Collections.Specialized;

    using global::Sitecore;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Shell.Framework.Commands;
    using global::Sitecore.Web.UI.Sheer;

    /// <summary>
    /// The expand descendants command
    /// </summary>
    public sealed partial class ExpandDescendants : Command
    {
        /// <summary>
        /// Executes the command in the specified context.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public override void Execute([NotNull]CommandContext context)
        {
            Assert.IsNotNull(context, "context");
            if (context.Items.Length == 1)
            {
                var parameters = new NameValueCollection();
                parameters["items"] = this.SerializeItems(context.Items);
                Context.ClientPage.Start(this, "Run", parameters);
            }
        }

        /// <summary>
        /// The query state.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="CommandState"/>.
        /// </returns>
        public override CommandState QueryState([NotNull]CommandContext context)
        {
            if (context.Items.Length != 1)
            {
                return CommandState.Hidden;
            }

            var item = context.Items[0];

            if (item.IsDerived(item.Database.GetTemplate(TemplateIDs.MainSection)) || item.ID == ItemIDs.RootID)
            {
                return CommandState.Hidden;
            }

            return base.QueryState(context);
        }

        /// <summary>
        /// The refresh children.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        private void RefreshChildren([CanBeNull]Item parent)
        {
            if (parent != null)
            {
                Context.ClientPage.SendMessage(this, string.Format("item:refreshchildren(id={0})", parent.ID));

                var children = parent.GetChildren();

                foreach (Item child in children)
                {
                    this.RefreshChildren(child);
                }
            }
        }

        /// <summary>
        /// Runs the pipeline.
        /// </summary>
        /// <param name="args">
        /// The arguments.
        /// </param>
        /// <remarks>
        /// this will be run from button in ribbon
        /// </remarks>
        private void Run([NotNull]ClientPipelineArgs args)
        {
            // ReSharper restore UnusedMember.Local
            Assert.IsNotNull(args, "args");
            SheerResponse.CheckModified(false);
            var item = this.DeserializeItems(args.Parameters["items"])[0];

            this.RefreshChildren(item);
        }
    }
}