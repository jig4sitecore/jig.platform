﻿namespace Jig.Sitecore.Shell.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using Jig.Sitecore.CustomItems;
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Shell.Framework.Commands;
    using global::Sitecore.Text;
    using global::Sitecore.Web.UI.Sheer;

    /// <summary>
    /// The create and select experience editor button command
    /// </summary>
    public partial class CreateAndSelectExperienceEditorButtonCommand : Command
    {
        /// <summary>
        /// Executes the command in the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        public override void Execute([NotNull] CommandContext context)
        {
            Assert.IsNotNull(context, "context");
            if (context.Items.Length == 1)
            {
                var parameters = new NameValueCollection();
                parameters["items"] = this.SerializeItems(context.Items);
                global::Sitecore.Context.ClientPage.Start(this, "Run", parameters);
            }
        }

        /// <summary>
        /// Queries the state.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The CommandState</returns>
        public override CommandState QueryState([NotNull]CommandContext context)
        {
            if (context.Items.Length != 1)
            {
                return CommandState.Hidden;
            }

            var item = context.Items[0];
            if (item == null)
            {
                return CommandState.Hidden;
            }

            if (!item.Paths.FullPath.StartsWith(global::Jig.Sitecore.Constants.Paths.Sites.Renderings))
            {
                return CommandState.Disabled;
            }

            if (!item.IsDerived(typeof(WebControlItem)))
            {
                return CommandState.Hidden;
            }

            return base.QueryState(context);
        }

        /// <summary>
        /// Runs the pipeline.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <remarks>this will be run from button in ribbon</remarks>
        // ReSharper disable UnusedMember.Local
        protected void Run([NotNull]ClientPipelineArgs args)
        // ReSharper restore UnusedMember.Local
        {
            Assert.IsNotNull(args, "args");
            SheerResponse.CheckModified(false);
            var item = this.DeserializeItems(args.Parameters["items"])[0];
            WebControlItem webControlItem = item;

            if (webControlItem != null)
            {
                var siteName = string.Empty;
                if (webControlItem.InnerItem.Parent != null
                    && string.Equals(webControlItem.InnerItem.Key, "Sites", StringComparison.InvariantCultureIgnoreCase))
                {
                    siteName = webControlItem.InnerItem.DisplayName;
                }
                else
                {
                    foreach (var parentItem in webControlItem.InnerItem.Axes.GetAncestors())
                    {
                        if (parentItem != null && string.Equals(parentItem.Key, "Sites", StringComparison.InvariantCultureIgnoreCase))
                        {
                            siteName = parentItem.DisplayName;
                            break;
                        }
                    }
                }

                if (string.IsNullOrWhiteSpace(siteName))
                {
                    Log.Warn("The CreateAndSelectExperienceEditorButtonCommand: couldnot resolve Site Name! + WebControlId: " + webControlItem.ID, this);
                    return;
                }

                TemplateItem template = webControlItem.DatasourceTemplate.TargetItem;
                if (template == null)
                {
                    Log.Warn("The CreateAndSelectExperienceEditorButtonCommand => webControlItem.DatasourceTemplate.TargetItem not found! + WebControlId: " + webControlItem.ID, this);
                    return;
                }

                /* The template.OwnFields does not work for multi inheritance */
                List<string> fieldNames = this.GetPropertyNames(template);

                if (fieldNames.Count > 0)
                {
                    var cebSitesItem = global::Sitecore.Context.Database.GetItem(Jig.Sitecore.Constants.Id.Sites.CustomExperienceButtons);

                    if (cebSitesItem != null)
                    {
                        var cebSiteItem = cebSitesItem.FindOrCreateChildItem(
                            siteName,
                            new ID("A87A00B1-E6DB-45AB-8B54-636FEC3B5523"));

                        var ceb = cebSiteItem.FindOrCreateChildItem(
                            item.DisplayName + " Button",
                            new ID("1AB4F9AD-B004-413C-8924-3E07143A614B"));

                        ceb.Editing.BeginEdit();

                        ceb["Header"] = ceb.DisplayName;
                        ceb["Icon"] = "Applications/32x32/notebook_edit.png";
                        ceb["Fields"] = new ListString(fieldNames.ToList()).ToString();
                        ceb["Tooltip"] = "Edit " + item.DisplayName;

                        ceb.Editing.EndEdit();

                        webControlItem.BeginEdit();

                        webControlItem.PageEditorButtons.Add(ceb.ID.ToString());

                        webControlItem.EndEdit();
                    }
                }
            }
        }

        /// <summary>
        /// Gets the property names.
        /// </summary>
        /// <param name="templateItem">The template item.</param>
        /// <returns>The list of property names</returns>
        protected virtual List<string> GetPropertyNames(TemplateItem templateItem)
        {
            var dictionary = new Dictionary<string, int>();

            foreach (global::Sitecore.Data.Items.TemplateFieldItem field in templateItem.OwnFields)
            {
                if (field.Key.StartsWith("_"))
                {
                    continue;
                }

                var key = field.Name;
                dictionary[key] = field.Sortorder;
            }

            foreach (TemplateItem baseTemplateItem in templateItem.BaseTemplates)
            {
                foreach (global::Sitecore.Data.Items.TemplateFieldItem field in baseTemplateItem.OwnFields)
                {
                    if (field.Key.StartsWith("_"))
                    {
                        continue;
                    }

                    var key = field.Name;
                    dictionary[key] = field.Sortorder;
                }
            }

            return dictionary.ToArray().OrderBy(x => x.Value).Select(x => x.Key).ToList();
        }
    }
}
