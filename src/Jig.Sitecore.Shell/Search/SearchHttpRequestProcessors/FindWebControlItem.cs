﻿namespace Jig.Sitecore.Shell.Search.SearchHttpRequestProcessors
{
    using System;
    using System.Linq;

    using Jig.Sitecore.CustomItems;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Globalization;

    using global::Sitecore.Pipelines.Search;

    using global::Sitecore.Search;

    /// <summary>
    ///  The Sitecore editor search
    /// </summary>
    public sealed class FindWebControlItem
    {
        /// <summary>
        /// Processes the specified args.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process([CanBeNull] SearchArgs args)
        {
            if (args != null && !string.IsNullOrWhiteSpace(args.TextQuery) && args.Database != null)
            {
                var input = args.TextQuery.Trim();
                if (input.IndexOf("View", 0, StringComparison.InvariantCultureIgnoreCase) > 0)
                {
                    try
                    {
                        var item = args.Database.GetItem(Jig.Sitecore.Constants.Id.Sites.Renderings);
                        if (item != null)
                        {
                            var hits = item.Axes.GetDescendants()
                                .Where(
                                    x =>
                                    x.TemplateID.Guid == typeof(WebControlItem).GUID
                                    && !string.IsNullOrWhiteSpace(x[WebControlItem.FieldNames.Tag])
                                    && x[WebControlItem.FieldNames.Tag].IndexOf(
                                        input,
                                        0,
                                        StringComparison.InvariantCultureIgnoreCase) > 0)
                                .ToArray();

                            foreach (var hit in hits)
                            {
                                SearchResult searchResult = SearchResult.FromItem(hit);
                                args.Result.AddResultToCategory(searchResult, Translate.Text("Direct Hit"));  
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        Log.Error(exception.Message, exception, this);
                    }
                }
            }
        }
    }
}