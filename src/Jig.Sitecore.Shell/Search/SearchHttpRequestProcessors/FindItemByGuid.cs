﻿namespace Jig.Sitecore.Shell.Search.SearchHttpRequestProcessors
{
    using System;
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Globalization;
    using global::Sitecore.Pipelines.Search;
    using global::Sitecore.Search;

    /// <summary>
    ///  The Sitecore editor search
    /// </summary>
    public sealed class FindItemByGuid
    {
        /// <summary>
        /// The unique identifier length
        /// </summary>
        /// <remarks>Random GUID Length</remarks>
        private static readonly int GuidLength = "b618716c1f254d4f8a786eda78ca00a2".Length;

        /// <summary>
        /// Processes the specified args.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process([CanBeNull] SearchArgs args)
        {
            if (args != null && !string.IsNullOrWhiteSpace(args.TextQuery) && args.Database != null)
            {
                var input = args.TextQuery.Trim();
                if (input.Length == GuidLength)
                {
                    Guid id;
                    if (Guid.TryParse(input, out id))
                    {
                        try
                        {
                            var item = args.Database.GetItem(new ID(id));
                            if (item != null)
                            {
                                SearchResult searchResult = SearchResult.FromItem(item);
                                args.Result.AddResultToCategory(searchResult, Translate.Text("Direct Hit"));
                            }
                        }
                        catch (Exception exception)
                        {
                            Log.Error(exception.Message, exception, this);
                        }
                    }
                }
            }
        }
    }
}