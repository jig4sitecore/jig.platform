﻿namespace Jig.Sitecore.Shell.Search.SearchHttpRequestProcessors
{
    using System;
    using System.IO;
    using System.Linq;

    using global::Sitecore;
    using global::Sitecore.Configuration;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Globalization;
    using global::Sitecore.Pipelines.Search;
    using global::Sitecore.Search;

    /// <summary>
    ///  The Sitecore editor search
    /// </summary>
    public sealed class FindItemByUrlPath
    {
        /// <summary>
        /// Processes the specified args.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process([CanBeNull] SearchArgs args)
        {
            if (args != null && !string.IsNullOrWhiteSpace(args.TextQuery) && args.Database != null)
            {
                var input = args.TextQuery.Trim();
                if (input.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
                {
                    Uri uri;
                    if (Uri.TryCreate(input, UriKind.Absolute, out uri))
                    {
                        var path = uri.AbsolutePath;
                        var extension = Path.GetExtension(path);
                        if (!string.IsNullOrWhiteSpace(extension))
                        {
                            path = path.Replace(extension, string.Empty);
                        }

                        var siteList = Factory.GetSiteInfoList();
                        var sites = (from site in siteList
                                     where
                                         !string.IsNullOrWhiteSpace(site.HostName)
                                         && !string.IsNullOrWhiteSpace(site.RootPath)
                                         && !string.IsNullOrWhiteSpace(site.StartItem)
                                         && !string.IsNullOrWhiteSpace(site.VirtualFolder)
                                         && !site.VirtualFolder.StartsWith(
                                             "/sitecore",
                                             StringComparison.InvariantCultureIgnoreCase)
                                     select site).ToArray();
                        try
                        {
                            foreach (var site in sites)
                            {
                                var itemPath = site.RootPath + site.StartItem + path;
                                var item = args.Database.GetItem(itemPath);
                                if (item != null)
                                {
                                    SearchResult searchResult = SearchResult.FromItem(item);
                                    args.Result.AddResultToCategory(searchResult, Translate.Text("Direct Hit"));
                                }
                            }
                        }
                        catch (Exception exception)
                        {
                            Log.Error(exception.Message, exception, this);
                        }
                    }
                }
            }
        }
    }
}