﻿namespace Jig.Sitecore.Shell.Applications.ContentEditor
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Web.UI.HtmlControls;
    using global::Sitecore.Web.UI.Pages;
    using global::Sitecore.Web.UI.Sheer;

    /// <summary>
    /// The rich text telephone link form.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate",
    Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1306:FieldNamesMustBeginWithLowerCaseLetter",
        Justification = "Reviewed. Suppression is OK here.")]
    public class RichTextTelephoneLinkForm : DialogForm
    {
        /* ReSharper disable UnassignedField.Global */

        /// <summary>
        /// The class.
        /// </summary>
        protected Edit Class;

        /// <summary>
        /// The text.
        /// </summary>
        protected Edit Text;

        /// <summary>
        /// The url.
        /// </summary>
        protected Edit Url;

        /* ReSharper restore UnassignedField.Global */

        /// <summary>
        /// Gets or sets the mode.
        /// </summary>
        /// <value>
        /// The mode.
        /// </value>
        [NotNull]
        protected string Mode
        {
            get
            {
                var mode = StringUtil.GetString(this.ServerProperties["Mode"]);
                if (!string.IsNullOrEmpty(mode))
                {
                    return mode;
                }

                return "shell";
            }

            set
            {
                Assert.ArgumentNotNull(value, "value");
                this.ServerProperties["Mode"] = value;
            }
        }

        /// <summary>
        /// Called when [cancel].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected override void OnCancel([NotNull] object sender, [NotNull] EventArgs args)
        {
            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull(args, "args");

            if (this.Mode == "webedit")
            {
                base.OnCancel(sender, args);
            }
            else
            {
                SheerResponse.Eval("scCancel()");
            }
        }

        /// <summary>
        /// The on ok.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The args.</param>
        protected override void OnOK([NotNull] object sender, [NotNull] EventArgs args)
        {
            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull(args, "args");
            var url = this.GetTelephone();
            var text = this.Text.Value;
            var cssClass = this.Class.Value;

            if (this.Mode == "webedit")
            {
                SheerResponse.SetDialogValue(StringUtil.EscapeJavascriptString(url));
                base.OnOK(sender, args);
            }
            else
            {
                SheerResponse.Eval(
                    "scClose(" + StringUtil.EscapeJavascriptString(url) + "," + StringUtil.EscapeJavascriptString(text)
                    + "," + StringUtil.EscapeJavascriptString(cssClass) + ")");
            }
        }

        /// <summary>
        /// The get telephone.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [NotNull]
        private string GetTelephone()
        {
            var value = this.Url.Value;

            if (value.Length > 0 && value.IndexOf(":", StringComparison.InvariantCulture) < 0)
            {
                value = string.Concat("tel:", value);
            }

            return value;
        }
    }
}