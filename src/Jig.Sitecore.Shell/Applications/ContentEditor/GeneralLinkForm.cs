﻿namespace Jig.Sitecore.Shell.Applications.ContentEditor
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Globalization;
    using global::Sitecore.Resources.Media;
    using global::Sitecore.Shell.Framework;
    using global::Sitecore.StringExtensions;
    using global::Sitecore.Utils;
    using global::Sitecore.Web;
    using global::Sitecore.Web.UI.HtmlControls;
    using global::Sitecore.Web.UI.Sheer;
    using global::Sitecore.Web.UI.WebControls;
    using global::Sitecore.Xml;

    /// <summary>
    /// The general link form.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate",
    Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1306:FieldNamesMustBeginWithLowerCaseLetter",
        Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1126:PrefixCallsCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public class GeneralLinkForm : global::Sitecore.Shell.Applications.Dialogs.LinkForm
    {
        /* ReSharper disable UnassignedField.Global */

        /// <summary>
        /// The class.
        /// </summary>
        protected Edit Class;

        /// <summary>
        /// The custom.
        /// </summary>
        protected Literal Custom;

        /// <summary>
        /// The custom target.
        /// </summary>
        protected Edit CustomTarget;

        /// <summary>
        /// The internal link data context.
        /// </summary>
        protected DataContext InternalLinkDataContext;

        /// <summary>
        /// The internal link treeview.
        /// </summary>
        protected TreeviewEx InternalLinkTreeview;

        /// <summary>
        /// The internal link treeview container.
        /// </summary>
        protected Border InternalLinkTreeviewContainer;

        /// <summary>
        /// The link anchor.
        /// </summary>
        protected Edit LinkAnchor;

        /// <summary>
        /// The mail to container.
        /// </summary>
        protected Border MailToContainer;

        /// <summary>
        /// The mail to link.
        /// </summary>
        protected Edit MailToLink;

        /// <summary>
        /// The media link data context.
        /// </summary>
        protected DataContext MediaLinkDataContext;

        /// <summary>
        /// The media link treeview.
        /// </summary>
        protected TreeviewEx MediaLinkTreeview;

        /// <summary>
        /// The media link treeview container.
        /// </summary>
        protected Border MediaLinkTreeviewContainer;

        /// <summary>
        /// The media preview.
        /// </summary>
        protected Border MediaPreview;

        /// <summary>
        /// The modes.
        /// </summary>
        protected Border Modes;

        /// <summary>
        /// The querystring.
        /// </summary>
        protected Edit Querystring;

        /// <summary>
        /// The section header.
        /// </summary>
        protected Literal SectionHeader;

        /// <summary>
        /// The target.
        /// </summary>
        protected Combobox Target;

        /// <summary>
        /// The text.
        /// </summary>
        protected Edit Text;

        /// <summary>
        /// The title.
        /// </summary>
        protected Edit Title;

        /// <summary>
        /// The treeview container.
        /// </summary>
        protected Scrollbox TreeviewContainer;

        /// <summary>
        /// The upload media.
        /// </summary>
        protected Button UploadMedia;

        /// <summary>
        /// The url.
        /// </summary>
        protected Edit Url;

        /// <summary>
        /// The telephone link
        /// </summary>
        protected Edit TelephoneLink;

        /// <summary>
        /// The url container.
        /// </summary>
        protected Border UrlContainer;

        /// <summary>
        /// The tel container
        /// </summary>
        protected Border TelContainer;

        /* ReSharper restore UnassignedField.Global */

        /// <summary>
        /// Gets or sets the current mode.
        /// </summary>
        /// <value>
        /// The current mode.
        /// </value>
        [NotNull]
        private string CurrentMode
        {
            get
            {
                var str = this.ServerProperties["current_mode"] as string;
                if (!string.IsNullOrEmpty(str))
                {
                    return global::Jig.Web.StringExtensions.GetValueOrDefault(str);
                }

                return "internal";
            }

            set
            {
                Assert.ArgumentNotNull(value, "value");
                this.ServerProperties["current_mode"] = value;
            }
        }

        /// <summary>
        /// The handle message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public override void HandleMessage([NotNull] Message message)
        {
            Assert.ArgumentNotNull(message, "message");
            if (this.CurrentMode != "media")
            {
                base.HandleMessage(message);
            }
            else
            {
                Item obj = null;
                if (message.Arguments.Count > 0 && ID.IsID(message.Arguments["id"]))
                {
                    var dataView = this.MediaLinkTreeview.GetDataView();
                    if (dataView != null)
                    {
                        obj = dataView.GetItem(message.Arguments["id"]);
                    }
                }

                if (obj == null)
                {
                    obj = this.MediaLinkTreeview.GetSelectionItem();
                }

                Dispatcher.Dispatch(message, obj);
            }
        }

        /// <summary>
        /// The on listbox changed.
        /// </summary>
        protected void OnListboxChanged()
        {
            if (this.Target.Value == "Custom")
            {
                this.CustomTarget.Disabled = false;
                this.Custom.Class = string.Empty;
            }
            else
            {
                this.CustomTarget.Value = string.Empty;
                this.CustomTarget.Disabled = true;
                this.Custom.Class = "disabled";
            }
        }

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnLoad([NotNull] EventArgs e)
        {
            Assert.ArgumentNotNull(e, "e");
            base.OnLoad(e);
            if (Context.ClientPage.IsEvent)
            {
                return;
            }

            this.CurrentMode = this.LinkType ?? string.Empty;
            this.InitControls();
            this.SetModeSpecificControls();
            RegisterScripts();
        }

        /// <summary>
        /// The on media open.
        /// </summary>
        protected void OnMediaOpen()
        {
            var selectionItem = this.MediaLinkTreeview.GetSelectionItem();
            if (selectionItem == null || !selectionItem.HasChildren)
            {
                return;
            }

            this.MediaLinkDataContext.SetFolder(selectionItem.Uri);
        }

        /// <summary>
        /// The on mode change.
        /// </summary>
        /// <param name="mode">
        /// The mode.
        /// </param>
        protected void OnModeChange([NotNull] string mode)
        {
            Assert.ArgumentNotNull(mode, "mode");
            this.CurrentMode = mode;
            this.SetModeSpecificControls();
            if (UIUtil.IsIE())
            {
                return;
            }

            SheerResponse.Eval("scForm.browser.initializeFixsizeElements();");
        }

        /// <summary>
        /// The on ok.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The args.</param>
        /// <exception cref="ArgumentException">Throws when mode is not valid</exception>
        protected override void OnOK([NotNull] object sender, [NotNull] EventArgs args)
        {
            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull(args, "args");
            var packet = new Packet("link");
            this.SetCommonAttributes(packet);
            bool flag;
            switch (this.CurrentMode)
            {
                case "internal":
                    flag = this.SetInternalLinkAttributes(packet);
                    break;
                case "media":
                    flag = this.SetMediaLinkAttributes(packet);
                    break;
                case "external":
                    flag = this.SetExternalLinkAttributes(packet);
                    break;
                case "telephone":
                    flag = this.SetTelephoneLinkAttributes(packet);
                    break;
                case "mailto":
                    flag = this.SetMailToLinkAttributes(packet);
                    break;
                case "anchor":
                    flag = this.SetAnchorLinkAttributes(packet);
                    break;
                default:
                    throw new ArgumentException("Unsupported mode: " + this.CurrentMode);
            }

            if (!flag)
            {
                return;
            }

            SheerResponse.SetDialogValue(packet.OuterXml);
            base.OnOK(sender, args);
        }

        /// <summary>
        /// The select media tree node.
        /// </summary>
        protected void SelectMediaTreeNode()
        {
            var selectionItem = this.MediaLinkTreeview.GetSelectionItem();
            if (selectionItem == null)
            {
                return;
            }

            this.UpdateMediaPreview(selectionItem);
        }

        /// <summary>
        /// The upload image.
        /// </summary>
        protected void UploadImage()
        {
            var selectionItem = this.MediaLinkTreeview.GetSelectionItem();
            if (selectionItem == null)
            {
                return;
            }

            if (!selectionItem.Access.CanCreate())
            {
                SheerResponse.Alert("You do not have permission to create a new item here.");
            }
            else
            {
                Context.ClientPage.SendMessage(this, "media:upload(edit=1,load=1)");
            }
        }

        /// <summary>
        /// The hide containing row.
        /// </summary>
        /// <param name="control">
        /// The control.
        /// </param>
        private static void HideContainingRow([NotNull] global::System.Web.UI.Control control)
        {
            Assert.ArgumentNotNull(control, "control");
            if (!Context.ClientPage.IsEvent)
            {
                var gridPanel = control.Parent as GridPanel;
                if (gridPanel == null)
                {
                    return;
                }

                gridPanel.SetExtensibleProperty(control, "row.style", "display:none");
            }
            else
            {
                SheerResponse.SetStyle(control.ID + "Row", "display", "none");
            }
        }

        /// <summary>
        /// The show containing row.
        /// </summary>
        /// <param name="control">
        /// The control.
        /// </param>
        private static void ShowContainingRow([NotNull] Control control)
        {
            Assert.ArgumentNotNull(control, "control");
            if (!Context.ClientPage.IsEvent)
            {
                var gridPanel = control.Parent as GridPanel;
                if (gridPanel == null)
                {
                    return;
                }

                gridPanel.SetExtensibleProperty(control, "row.style", string.Empty);
            }
            else
            {
                SheerResponse.SetStyle(control.ID + "Row", "display", string.Empty);
            }
        }

        /// <summary>
        /// The register scripts.
        /// </summary>
        private static void RegisterScripts()
        {
            Context.ClientPage.ClientScript.RegisterClientScriptBlock(
                Context.ClientPage.GetType(), 
                "translationsScript", 
                "window.Texts = {{ ErrorOcurred: \"{0}\"}};".FormatWith(Translate.Text("An error occured:")), 
                true);
        }

        /// <summary>
        /// The init controls.
        /// </summary>
        private void InitControls()
        {
            var str = string.Empty;
            var target = this.LinkAttributes["target"];
            var linkTargetValue = GetLinkTargetValue(target);
            if (linkTargetValue == "Custom")
            {
                str = target;
                this.CustomTarget.Disabled = false;
                this.Custom.Class = string.Empty;
            }
            else
            {
                this.CustomTarget.Disabled = true;
                this.Custom.Class = "disabled";
            }

            this.Text.Value = this.LinkAttributes["text"];
            this.Target.Value = linkTargetValue;
            this.CustomTarget.Value = str;
            this.Class.Value = this.LinkAttributes["class"];
            this.Querystring.Value = this.LinkAttributes["querystring"];
            this.Title.Value = this.LinkAttributes["title"];
            this.InitMediaLinkDataContext();
            this.InitInternalLinkDataContext();
        }

        /// <summary>
        /// The init internal link data context.
        /// </summary>
        private void InitInternalLinkDataContext()
        {
            this.InternalLinkDataContext.GetFromQueryString();
            var queryString = WebUtil.GetQueryString("ro");
            var id = this.LinkAttributes["id"];
            if (!string.IsNullOrEmpty(id) && ID.IsID(id))
            {
                this.InternalLinkDataContext.SetFolder(new ItemUri(new ID(id), Client.ContentDatabase));
            }

            if (queryString.Length <= 0)
            {
                return;
            }

            this.InternalLinkDataContext.Root = queryString;
        }

        /// <summary>
        /// The init media link data context.
        /// </summary>
        private void InitMediaLinkDataContext()
        {
            this.MediaLinkDataContext.GetFromQueryString();
            var str = this.LinkAttributes["url"];
            if (this.CurrentMode != "media")
            {
                str = string.Empty;
            }

            if (str.Length == 0)
            {
                str = "/sitecore/media library";
            }
            else
            {
                if (!str.StartsWith("/sitecore", StringComparison.InvariantCulture)
                    && !str.StartsWith("/{11111111-1111-1111-1111-111111111111}", StringComparison.InvariantCulture))
                {
                    str = "/sitecore/media library" + str;
                }

                var dataView = this.MediaLinkTreeview.GetDataView();
                if (dataView == null)
                {
                    return;
                }

                var obj = dataView.GetItem(str);
                if (obj != null && obj.Parent != null)
                {
                    this.MediaLinkDataContext.SetFolder(obj.Uri);
                }
            }

            this.MediaLinkDataContext.AddSelected(new DataUri(str));
            this.MediaLinkDataContext.Root = "/sitecore/media library";
        }

        /// <summary>
        /// The set anchor link attributes.
        /// </summary>
        /// <param name="packet">
        /// The packet.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool SetAnchorLinkAttributes([NotNull] Packet packet)
        {
            Assert.ArgumentNotNull(packet, "packet");
            var str = this.LinkAnchor.Value;
            if (str.Length > 0 && str.StartsWith("#", StringComparison.InvariantCulture))
            {
                str = str.Substring(1);
            }

            SetAttribute(packet, "url", str);
            SetAttribute(packet, "anchor", str);
            return true;
        }

        /// <summary>
        /// The set anchor link controls.
        /// </summary>
        private void SetAnchorLinkControls()
        {
            ShowContainingRow(this.LinkAnchor);
            var str = this.LinkAttributes["anchor"];
            if (this.LinkType != "anchor" && string.IsNullOrEmpty(this.LinkAnchor.Value))
            {
                str = string.Empty;
            }

            if (!string.IsNullOrEmpty(str) && !str.StartsWith("#", StringComparison.InvariantCulture))
            {
                str = "#" + str;
            }

            this.LinkAnchor.Value = str ?? string.Empty;
            this.SectionHeader.Text =
                Translate.Text("Specify the name of the anchor, e.g. #header1, and any additional properties");
        }

        /// <summary>
        /// The set common attributes.
        /// </summary>
        /// <param name="packet">
        /// The packet.
        /// </param>
        private void SetCommonAttributes([NotNull] Packet packet)
        {
            Assert.ArgumentNotNull(packet, "packet");
            SetAttribute(packet, "linktype", this.CurrentMode);
            SetAttribute(packet, "text", this.Text);
            SetAttribute(packet, "title", this.Title);
            SetAttribute(packet, "class", this.Class);
        }

        /// <summary>
        /// The set external link attributes.
        /// </summary>
        /// <param name="packet">
        /// The packet.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool SetExternalLinkAttributes([NotNull] Packet packet)
        {
            Assert.ArgumentNotNull(packet, "packet");
            var str = this.Url.Value;
            if (str.Length > 0 && str.IndexOf("://", StringComparison.InvariantCulture) < 0
                && !str.StartsWith("/", StringComparison.InvariantCulture))
            {
                str = "http://" + str;
            }

            var attributeFromValue = GetLinkTargetAttributeFromValue(this.Target.Value, this.CustomTarget.Value);
            SetAttribute(packet, "url", str);
            SetAttribute(packet, "anchor", string.Empty);
            SetAttribute(packet, "target", attributeFromValue);
            return true;
        }

        /// <summary>
        /// The set external link controls.
        /// </summary>
        private void SetExternalLinkControls()
        {
            if (this.LinkType == "external" && string.IsNullOrEmpty(this.Url.Value))
            {
                this.Url.Value = this.LinkAttributes["url"];
            }

            ShowContainingRow(this.UrlContainer);
            ShowContainingRow(this.Target);
            ShowContainingRow(this.CustomTarget);
            this.SectionHeader.Text =
                Translate.Text("Specify the URL, e.g. http://www.sitecore.net and any additional properties.");
        }

        /// <summary>
        /// The set internal link attributes.
        /// </summary>
        /// <param name="packet">
        /// The packet.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool SetInternalLinkAttributes([NotNull] Packet packet)
        {
            Assert.ArgumentNotNull(packet, "packet");
            var selectionItem = this.InternalLinkTreeview.GetSelectionItem();
            if (selectionItem == null)
            {
                Context.ClientPage.ClientResponse.Alert("Select an item.");
                return false;
            }

            var attributeFromValue = GetLinkTargetAttributeFromValue(this.Target.Value, this.CustomTarget.Value);
            var str = this.Querystring.Value;
            if (str.StartsWith("?", StringComparison.InvariantCulture))
            {
                str = str.Substring(1);
            }

            SetAttribute(packet, "anchor", this.LinkAnchor);
            SetAttribute(packet, "querystring", str);
            SetAttribute(packet, "target", attributeFromValue);
            SetAttribute(packet, "id", selectionItem.ID.ToString());
            return true;
        }

        /// <summary>
        /// The set internal link controls.
        /// </summary>
        private void SetInternalLinkControls()
        {
            this.LinkAnchor.Value = this.LinkAttributes["anchor"];
            this.InternalLinkTreeviewContainer.Visible = true;
            this.MediaLinkTreeviewContainer.Visible = false;
            ShowContainingRow(this.TreeviewContainer);
            ShowContainingRow(this.Querystring);
            ShowContainingRow(this.LinkAnchor);
            ShowContainingRow(this.Target);
            ShowContainingRow(this.CustomTarget);
            this.SectionHeader.Text =
                Translate.Text(
                    "Select the item that you want to create a link to and specify the appropriate properties.");
        }

        /// <summary>
        /// The set mail link controls.
        /// </summary>
        private void SetMailLinkControls()
        {
            if (this.LinkType == "mailto" && string.IsNullOrEmpty(this.MailToLink.Value))
            {
                this.MailToLink.Value = this.LinkAttributes["url"].Replace("mailto:", string.Empty);
            }

            ShowContainingRow(this.MailToContainer);
            this.SectionHeader.Text =
                Translate.Text(
                    "Specify the email address and any additional properties. To send a test mail use the 'Send a test mail' button.");
        }

        /// <summary>
        /// The set mail to link attributes.
        /// </summary>
        /// <param name="packet">
        /// The packet.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool SetMailToLinkAttributes([NotNull] Packet packet)
        {
            Assert.ArgumentNotNull(packet, "packet");
            var value = this.MailToLink.Value;

            if (!EmailUtility.IsValidEmailAddress(value))
            {
                SheerResponse.Alert("The e-mail address is invalid.");
                return false;
            }

            if (value.Length > 0 && value.IndexOf(":", StringComparison.InvariantCulture) < 0)
            {
                value = string.Concat("mailto:", value);
            }

            SetAttribute(packet, "url", value);
            SetAttribute(packet, "anchor", string.Empty);
            return true;
        }

        /// <summary>
        /// The set media link attributes.
        /// </summary>
        /// <param name="packet">
        /// The packet.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool SetMediaLinkAttributes([NotNull] Packet packet)
        {
            Assert.ArgumentNotNull(packet, "packet");
            var selectionItem = this.MediaLinkTreeview.GetSelectionItem();
            if (selectionItem == null)
            {
                Context.ClientPage.ClientResponse.Alert("Select a media item.");
                return false;
            }

            var attributeFromValue = GetLinkTargetAttributeFromValue(this.Target.Value, this.CustomTarget.Value);
            SetAttribute(packet, "target", attributeFromValue);
            SetAttribute(packet, "id", selectionItem.ID.ToString());
            return true;
        }

        /// <summary>
        /// The set media link controls.
        /// </summary>
        private void SetMediaLinkControls()
        {
            this.InternalLinkTreeviewContainer.Visible = false;
            this.MediaLinkTreeviewContainer.Visible = true;
            this.MediaPreview.Visible = true;
            this.UploadMedia.Visible = true;
            var folder = this.MediaLinkDataContext.GetFolder();
            if (folder != null)
            {
                this.UpdateMediaPreview(folder);
            }

            ShowContainingRow(this.TreeviewContainer);
            ShowContainingRow(this.Target);
            ShowContainingRow(this.CustomTarget);
            this.SectionHeader.Text =
                Translate.Text("Select an item from the media library and specify any additional properties.");
        }

        /// <summary>
        /// Sets the telephone link attributes.
        /// </summary>
        /// <param name="packet">The packet.</param>
        /// <returns>Always true</returns>
        private bool SetTelephoneLinkAttributes([NotNull] Packet packet)
        {
            Assert.ArgumentNotNull(packet, "packet");
            var value = this.TelephoneLink.Value;

            if (value.Length > 0 && value.IndexOf(":", StringComparison.InvariantCulture) < 0)
            {
                value = string.Concat("tel:", value);
            }

            SetAttribute(packet, "url", value);
            SetAttribute(packet, "anchor", string.Empty);
            return true;
        }

        /// <summary>
        /// Sets the telephone link controls.
        /// </summary>
        private void SetTelephoneLinkControls()
        {
            if (this.LinkType == "telephone" && string.IsNullOrEmpty(this.TelephoneLink.Value))
            {
                this.TelephoneLink.Value = this.LinkAttributes["url"].Replace("telephone:", string.Empty);
            }

            ShowContainingRow(this.TelContainer);
            this.SectionHeader.Text =
                Translate.Text("Specify the telephone number, e.g. 1-888-555-8585 and any additional properties.");
        }

        /// <summary>
        /// The set mode specific controls.
        /// </summary>
        /// <exception cref="ArgumentException">
        /// Throws when mode is invalid
        /// </exception>
        private void SetModeSpecificControls()
        {
            HideContainingRow(this.TreeviewContainer);
            this.MediaPreview.Visible = false;
            this.UploadMedia.Visible = false;
            HideContainingRow(this.UrlContainer);
            HideContainingRow(this.Querystring);
            HideContainingRow(this.TelContainer);
            HideContainingRow(this.MailToContainer);
            HideContainingRow(this.LinkAnchor);
            HideContainingRow(this.Target);
            HideContainingRow(this.CustomTarget);
            switch (this.CurrentMode)
            {
                case "internal":
                    this.SetInternalLinkControls();
                    break;
                case "media":
                    this.SetMediaLinkControls();
                    break;
                case "external":
                    this.SetExternalLinkControls();
                    break;
                case "telephone":
                    this.SetTelephoneLinkControls();
                    break;
                case "mailto":
                    this.SetMailLinkControls();
                    break;
                case "anchor":
                    this.SetAnchorLinkControls();
                    break;
                default:
                    throw new ArgumentException("Unsupported mode: " + this.CurrentMode);
            }

            foreach (Border border in this.Modes.Controls)
            {
                if (border != null)
                {
                    border.Class = border.ID.ToLowerInvariant() == this.CurrentMode ? "selected" : string.Empty;
                }
            }
        }

        /// <summary>
        /// The update media preview.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        private void UpdateMediaPreview([NotNull] Item item)
        {
            Assert.ArgumentNotNull(item, "item");
            var thumbnailOptions = MediaUrlOptions.GetThumbnailOptions(item);
            thumbnailOptions.UseDefaultIcon = true;
            thumbnailOptions.Width = 96;
            thumbnailOptions.Height = 96;
            thumbnailOptions.Language = item.Language;
            thumbnailOptions.AllowStretch = false;
            this.MediaPreview.InnerHtml = "<img src=\"" + MediaManager.GetMediaUrl(item, thumbnailOptions)
                                          + "\" width=\"96px\" height=\"96px\" border=\"0\" alt=\"\" />";
        }
    }
}