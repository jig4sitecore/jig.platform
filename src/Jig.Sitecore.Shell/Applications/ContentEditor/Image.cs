﻿namespace Jig.Sitecore.Shell.Applications.ContentEditor
{
    using System;
    using System.Collections.Specialized;
    using System.Web;
    using System.Web.UI;
    using System.Xml.Linq;

    using Jig.Web.Html;

    using global::Sitecore;

    using global::Sitecore.Data;

    using global::Sitecore.Data.Items;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Globalization;

    /// <summary>
    /// The image.
    /// </summary>
    public sealed partial class Image : global::Sitecore.Shell.Applications.ContentEditor.Image
    {
        #region Methods

        /// <summary>
        /// The do render.
        /// </summary>
        /// <param name="output">
        /// The output.
        /// </param>
        protected override void DoRender([NotNull] HtmlTextWriter output)
        {
            MediaItem mediaItem = this.GetMediaItem();
            if (mediaItem != null)
            {
                base.DoRender(output);
                return;
            }

            TemplateFieldItem templateItem = this.TryResolveImageTemplateFieldItem();

            if (templateItem != null && !string.IsNullOrWhiteSpace(templateItem.HelpLink.Url))
            {
                string clientEvent = global::Sitecore.Context.ClientPage.GetClientEvent(this.ID + ".Browse");

                var model = new ImageViewModel
                                {
                                    Id = this.ID,
                                    ServiceUrl = templateItem.HelpLink.Url,
                                    ClientEvent = clientEvent
                                };

                this.Render(output, model);
                return;
            }

            base.DoRender(output);
        }

        /// <summary>
        /// Renders the specified output.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="model">The model.</param>
        private void Render([NotNull] HtmlTextWriter output, [NotNull] ImageViewModel model)
        {
            /*
                <div id="@panel" class="scContentControlImagePane">
                    <div class="scContentControlImageImage" onclick="@Model.ClientEvent">
                        <iframe id="@image" src="@Model.ServiceUrl" alt="Image Placeholder" marginwidth="0" marginheight="0" style="width: 100%; height: 128px"></iframe>
                    </div>
                    <div id="@details" class="scContentControlImageDetails">
                    </div>
                </div>
             */
            var panel = model.Id + "_pane";
            var image = model.Id + "_image";
            var details = model.Id + "_details";
            using (output.RenderDiv(cssClass: "scContentControlImagePane", attributes: new IdAttribute(panel)))
            {
                using (output.RenderDiv(cssClass: "scContentControlImageImage", attributes: new XAttribute("onclick", model.ClientEvent)))
                {
                    XAttribute[] attr =
                        {
                            new IdAttribute(image), 
                            new AltAttribute("Image Placeholder"), 
                            new XAttribute("marginwidth", "0"),
                            new XAttribute("marginheight", "0"),
                            new StyleAttribute("width: 100%; height: 128px")
                        };

                    output.IFrame(src: model.ServiceUrl, attributes: attr);
                }

                using (output.RenderDiv(cssClass: "scContentControlImageDetails", attributes: new IdAttribute(details)))
                {
                }
            }
        }

        /// <summary>
        /// Tries the resolve image template field item.
        /// </summary>
        /// <returns>The template field item</returns>
        [CanBeNull]
        private TemplateFieldItem TryResolveImageTemplateFieldItem()
        {
            try
            {
                /*
                 * There is no direct access to field template id, but its possible to extract from "onfocus" attribute
                    this.Attributes["onfocus"]
                    "javascript:return scContent.activate(this,event,'sitecore://master/{110D559F-DEA5-42EA-9C1C-8A5DF7E70EF9}?lang=en&ver=1&fld={A1BDC093-A086-481F-B04F-E155864F54DB}&ctl=FIELD780305112')"                
                 */
                string attr = this.Attributes["onfocus"];
                if (!string.IsNullOrWhiteSpace(attr))
                {
                    var position01 = attr.IndexOf('?');
                    var position02 = attr.LastIndexOf('\'');
                    if (position01 > 0 && position02 > 0 && position01 < position02)
                    {
                        var query = attr.Substring(position01 + 1, position02 - position01);
                        NameValueCollection collection = HttpUtility.ParseQueryString(query);
                        var language = Language.Parse(collection["lang"]);
                        var fieldItemId = new ID(collection["fld"]);
                        var version = global::Sitecore.Data.Version.Parse(collection["ver"]);

                        var database = global::Sitecore.Configuration.Factory.GetDatabase("master");
                        var item = database.GetItem(fieldItemId, language, version);
                        return item;
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error(exception.Message, exception, this);
            }

            return null;
        }

        /// <summary>
        /// Gets the media item.
        /// </summary>
        /// <returns>
        /// The media item.
        /// </returns>
        [CanBeNull]
        private Item GetMediaItem()
        {
            string attribute = this.XmlValue.GetAttribute("mediaid");
            if (attribute.Length > 0)
            {
                Language language = Language.Parse(this.ItemLanguage);
                MediaItem item = global::Sitecore.Client.ContentDatabase.GetItem(attribute, language);
                return item;
            }

            return null;
        }

        #endregion

        /// <summary>
        /// The image view model.
        /// </summary>
        public sealed class ImageViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ImageViewModel"/> class.
            /// </summary>
            public ImageViewModel()
            {
                this.ClientEvent = string.Empty;
                this.Id = string.Empty;
                this.ServiceUrl = string.Empty;
            }

            #region Public Properties

            /// <summary>
            /// Gets or sets the client event.
            /// </summary>
            [NotNull]
            public string ClientEvent { get; set; }

            /// <summary>
            /// Gets or sets the control id.
            /// </summary>
            [NotNull]
            public string Id { get; set; }

            /// <summary>
            /// Gets or sets the service url.
            /// </summary>
            [NotNull]
            public string ServiceUrl { get; set; }

            #endregion
        }
    }
}