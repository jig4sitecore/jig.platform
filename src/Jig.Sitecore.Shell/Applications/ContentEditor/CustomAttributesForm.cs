﻿namespace Jig.Sitecore.Shell.Applications.ContentEditor
{
    using System;
    using System.Collections.Specialized;
    using System.Diagnostics.CodeAnalysis;
    using System.Text;
    using System.Web;
    using System.Xml;

    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Shell.Applications.ContentEditor;
    using global::Sitecore.Text;
    using global::Sitecore.Web.UI.Sheer;
    using global::Sitecore.Xml;

    /// <summary>
    /// The custom attributes form.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate",
        Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1306:FieldNamesMustBeginWithLowerCaseLetter",
        Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1126:PrefixCallsCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public class CustomAttributesForm : global::Sitecore.Shell.Applications.Dialogs.LinkForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomAttributesForm"/> class.
        /// </summary>
        public CustomAttributesForm()
        {
            this.Values = new NameValue();
            this.CustomAttributes = new NameValueCollection();
        }

        /// <summary>
        /// Gets or sets the values.
        /// </summary>
        [NotNull]
        protected NameValue Values { get; set; }

        /// <summary>
        /// Gets or sets the custom attributes.
        /// </summary>
        [NotNull]
        protected NameValueCollection CustomAttributes { get; set; }

        /// <summary>
        /// Initializes the custom attributes.
        /// </summary>
        protected void InitCustomAttributes()
        {
            var db = global::Sitecore.Configuration.Factory.GetDatabase("master");
            if (db != null)
            {
                /* /sitecore/system/Sites/Shared/Custom Attributes */
                var rootItem = db.GetItem(new ID("B295C358-3995-46EC-99C4-7E287554931C"));
                if (rootItem != null && rootItem.HasChildren)
                {
                    foreach (global::Sitecore.Data.Items.Item item in rootItem.Children)
                    {
                        Jig.Sitecore.CustomItems.TagItem tagItem = item;
                        if (tagItem != null)
                        {
                            var key = tagItem.TagName.Value;
                            if (!string.IsNullOrWhiteSpace(key))
                            {
                                this.CustomAttributes[EncodeAttributeName(key.Replace("data-", string.Empty))] = " ";
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnLoad([NotNull] EventArgs e)
        {
            Assert.ArgumentNotNull(e, "e");
            base.OnLoad(e);
            if (Context.ClientPage.IsEvent)
            {
                return;
            }

            this.Values.Value = new UrlString(this.CustomAttributes).ToString();
        }

        /// <summary>
        /// The parse link.
        /// </summary>
        /// <param name="link">
        /// The link.
        /// </param>
        protected override void ParseLink([NotNull] string link)
        {
            base.ParseLink(link);
            var xmlDocument = XmlUtil.LoadXml(link);
            if (xmlDocument == null)
            {
                return;
            }

            var xmlNode = xmlDocument.SelectSingleNode("/link");
            if (xmlNode == null || xmlNode.Attributes == null)
            {
                return;
            }

            this.InitCustomAttributes();

            foreach (XmlAttribute attr in xmlNode.Attributes)
            {
                if (attr.LocalName.StartsWith("data-", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.CustomAttributes[EncodeAttributeName(attr.LocalName.Replace("data-", string.Empty))] = XmlUtil.GetAttribute(
                        attr.LocalName,
                        xmlNode);
                }
            }
        }

        /// <summary>
        /// Handles the <see cref="E:OK" /> event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected override void OnOK([NotNull] object sender, [NotNull] EventArgs args)
        {
            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull(args, "args");

            var packet = new Packet("link");
            this.SetAllAttributes(packet);

            var xml = packet.OuterXml;
            SheerResponse.SetDialogValue(xml);
            base.OnOK(sender, args);
        }

        /// <summary>
        /// Encodes the attribute name.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The attribute name</returns>
        [NotNull]
        private static string EncodeAttributeName([NotNull] string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return string.Empty;
            }

            if (input.StartsWith("data_", StringComparison.InvariantCultureIgnoreCase))
            {
                input = input.Substring(5);
            }

            var builder = new StringBuilder(input.Length + 5);

            if (!input.StartsWith("data-"))
            {
                builder.Append("data-");
            }

            for (var i = 0; i < input.Length; i++)
            {
                var current = input[i];

                if ('_'.Equals(current) && i < input.Length - 1)
                {
                    // if underscore and not last character, create escaped underscore
                    builder.Append(current);
                    builder.Append('_');
                }
                else if ('-'.Equals(current))
                {
                    // convert underscore to dash
                    builder.Append('-');
                }
                else
                {
                    builder.Append(current);
                }
            }

            return builder.ToString();
        }

        /// <summary>
        /// Decodes attribute name.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The attribute name</returns>
        [NotNull]
        private static string DecodeAttributeName([NotNull] string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return string.Empty;
            }

            if (input.StartsWith("data_", StringComparison.InvariantCultureIgnoreCase))
            {
                input = input.Substring(5);
            }

            var builder = new StringBuilder(input.Length + 5);
            if (!input.StartsWith("data-"))
            {
                builder.Append("data-");
            }

            for (var i = 0; i < input.Length; i++)
            {
                var current = input[i];

                if ('_'.Equals(current))
                {
                    // if last character, append and continue
                    if (i == input.Length - 1)
                    {
                        builder.Append(current);
                        continue;
                    }

                    // check for escape character and skip if it was an escaped underscore
                    if ('_'.Equals(input[i + 1]))
                    {
                        builder.Append(current);
                        i++;
                    }
                    else
                    {
                        // convert underscore to dash
                        builder.Append('-');
                    }
                }
                else
                {
                    builder.Append(current);
                }
            }

            return builder.ToString();
        }

        /// <summary>
        /// The set all attributes.
        /// </summary>
        /// <param name="packet">
        /// The packet.
        /// </param>
        private void SetAllAttributes([NotNull] Packet packet)
        {
            foreach (string linkAttribute in this.LinkAttributes.Keys)
            {
                var attrValue = HttpUtility.UrlDecode(this.LinkAttributes[linkAttribute]);
                if (!linkAttribute.StartsWith("data-", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrWhiteSpace(attrValue))
                {
                    SetAttribute(packet, linkAttribute, this.LinkAttributes[linkAttribute]);
                }
            }

            var customAttributes = StringUtil.ParseNameValueCollection(this.Values.Value, '&', '=');

            foreach (string customAttribute in customAttributes.Keys)
            {
                var value = customAttributes[customAttribute];
                if (!string.IsNullOrWhiteSpace(value))
                {
                    var attrKey = DecodeAttributeName(customAttribute);
                    var attrValue = HttpUtility.UrlDecode(value);
                    if (!string.IsNullOrWhiteSpace(attrValue))
                    {
                        SetAttribute(packet, attrKey, attrValue.Trim());
                    }
                }
            }
        }
    }
}