﻿namespace Jig.Sitecore.Shell.Applications.ContentEditor
{
    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Text;
    using global::Sitecore.Web.UI.Sheer;

    /// <summary>
    /// The link.
    /// </summary>
    public class Link : global::Sitecore.Shell.Applications.ContentEditor.Link
    {
        /// <summary>
        /// The handle message.
        /// </summary>
        /// <param name="message">The message.</param>
        public override void HandleMessage([NotNull] Message message)
        {
            Assert.ArgumentNotNull(message, "message");
            base.HandleMessage(message);
            if (message["id"] != this.ID)
            {
                return;
            }

            if (!string.IsNullOrWhiteSpace(message.Name))
            {
                switch (message.Name)
                {
                    case "platform:contentlink:telephonelink":
                        {
                            var url = new UrlString(UIUtil.GetUri("control:TelephoneLink"));
                            this.Insert(url.ToString());
                            break;
                        }

                    case "platform:contentlink:customattributes":
                        {
                            var url = new UrlString(UIUtil.GetUri("control:CustomAttributes"));
                            this.Insert(url.ToString());
                            break;
                        }
                }
            }
        }
    }
}