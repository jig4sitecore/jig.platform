﻿namespace Jig.Sitecore.Shell.Applications.ContentEditor.Gutters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Jig.Sitecore.Sites.Shared.Data.Interfaces;
    using Jig.Sitecore.Sites.Shared.Data.Pages;
    using global::Sitecore;
    using global::Sitecore.Collections;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Data.Managers;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Globalization;
    using global::Sitecore.Shell.Applications.ContentEditor.Gutters;

    /// <summary>
    /// Modified version of sitecore MissingVersions gutter.
    /// </summary>
    public sealed class MissingVersions : GutterRenderer
    {
        /// <summary>
        /// The folder template identifier
        /// </summary>
        private static readonly ID FolderTemplateId = new ID("A87A00B1-E6DB-45AB-8B54-636FEC3B5523");

        /// <summary>
        /// Gets the icon descriptor.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <returns>Gutter Icon Descriptor.</returns>
        [CanBeNull] 
        protected override GutterIconDescriptor GetIconDescriptor([NotNull]Item item)
        {
            Assert.ArgumentNotNull(item, "item");

            /* Filter for home */
            if (!item.Paths.FullPath.StartsWith(
                Jig.Sitecore.Constants.Paths.Sites.Content,
                StringComparison.InvariantCultureIgnoreCase))
            {
                return null;
            }

            if (!item.Database.Name.Equals("master", StringComparison.InvariantCultureIgnoreCase))
            {
                return null;
            }

            if (!string.IsNullOrWhiteSpace(item.Paths.ContentPath)
                && item.Key.StartsWith("_", StringComparison.InvariantCultureIgnoreCase))
            {
                return null;
            }

            if (!item.Paths.FullPath.ToLowerInvariant().Contains("/home"))
            {
                return null;
            }

            if (item.Template == null
                || (item.Template.ID == FolderTemplateId
                || item.Template.BaseTemplates.Any(x => x.ID == FolderTemplateId)))
            {
                return null;
            }

            var widgetId = new ID(typeof(IWidget).GUID);
            var contentId = new ID(typeof(IContentPage).GUID);
            if (item.Template.ID == widgetId
                || item.Template.BaseTemplates.Any(x => x.ID == widgetId)
                || item.Template.ID == contentId
                || item.Template.BaseTemplates.Any(x => x.ID == contentId))
            {
                return null;
            }

            LanguageCollection languages = LanguageManager.GetLanguages(item.Database);
            var list = new List<Language>();

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (Language language in languages)
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                var versionNumbers = item.Database.GetItem(item.ID, language).Versions.GetVersionNumbers(false);
                if (versionNumbers == null || versionNumbers.Length == 0)
                {
                    list.Add(language);
                }
            }

            if (list.Count == 0)
            {
                return null;
            }

            var gutterIconDescriptor = new GutterIconDescriptor
                                                            {
                                                                Icon = "Applications/16x16/bullet_ball_red.png"
                                                            };

            var builder = new StringBuilder(128);
            builder.Append(Translate.Text("Missing versions in:"));
            for (int i = 0; i < list.Count; i++)
            {
                Language language = list[i];
                builder.Append(language.CultureInfo.EnglishName);

                if (i + 1 != list.Count)
                {
                    builder.Append(", ");
                }
            }

            gutterIconDescriptor.Tooltip = builder.ToString();
            return gutterIconDescriptor;
        }
    }
}