﻿namespace Jig.Sitecore.Shell.Applications.ContentEditor
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Shell.Applications.Dialogs;
    using global::Sitecore.Web.UI.HtmlControls;

    using global::Sitecore.Web.UI.Sheer;

    using global::Sitecore.Xml;

    /// <summary>
    /// The telephone link form.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate",
        Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1306:FieldNamesMustBeginWithLowerCaseLetter",
        Justification = "Reviewed. Suppression is OK here.")]
    public class TelephoneLinkForm : global::Sitecore.Shell.Applications.Dialogs.LinkForm
    {
        #region Fields
        /* ReSharper disable UnassignedField.Global */

        /// <summary>
        /// The class.
        /// </summary>
        protected Edit Class;

        /// <summary>
        /// The text.
        /// </summary>
        protected Edit Text;

        /// <summary>
        /// The title.
        /// </summary>
        protected Edit Title;

        /// <summary>
        /// The url.
        /// </summary>
        protected Edit Url;

        /* ReSharper restore UnassignedField.Global */

        #endregion

        #region Methods

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnLoad([NotNull] EventArgs e)
        {
            Assert.ArgumentNotNull(e, "e");
            base.OnLoad(e);
            if (Context.ClientPage.IsEvent)
            {
                return;
            }

            var item = this.LinkAttributes["url"];
            if (this.LinkType != "telephone")
            {
                item = string.Empty;
            }

            this.Text.Value = this.LinkAttributes["text"];
            this.Url.Value = item.Replace("tel:", string.Empty);
            this.Class.Value = this.LinkAttributes["class"];
            this.Title.Value = this.LinkAttributes["title"];
        }

        /// <summary>
        /// The on ok.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        protected override void OnOK([NotNull] object sender, [NotNull] EventArgs args)
        {
            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull(args, "args");
            var tel = this.GetTelephone();
            if (tel == "__Canceled")
            {
                SheerResponse.Alert("The telephone number is invalid.");
                return;
            }

            var packet = new Packet("link");
            LinkForm.SetAttribute(packet, "text", this.Text);
            LinkForm.SetAttribute(packet, "linktype", "telephone");
            LinkForm.SetAttribute(packet, "url", tel);
            LinkForm.SetAttribute(packet, "anchor", string.Empty);
            LinkForm.SetAttribute(packet, "title", this.Title);
            LinkForm.SetAttribute(packet, "class", this.Class);
            SheerResponse.SetDialogValue(packet.OuterXml);
            base.OnOK(sender, args);
        }

        /// <summary>
        /// The get telephone.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [NotNull]
        private string GetTelephone()
        {
            var value = this.Url.Value;

            if (value.Length > 0 && value.IndexOf(":", StringComparison.InvariantCulture) < 0)
            {
                value = string.Concat("tel:", value);
            }

            return value;
        }

        #endregion
    }
}