﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: Guid("dd6e1cac-6d27-4c90-8f48-6d4c388d6b4f")]
[assembly: AssemblyTitle("Jig.Sitecore.Shell.SitePublishing")]
[assembly: AssemblyDescription("The library extending the Sitecore CMS.")]

[assembly: AssemblyCompany("Jig4Sitecore Team")]
[assembly: AssemblyProduct("Jig4Sitecore Framework")]
[assembly: AssemblyCopyright("Copyright © Jig4Sitecore Team 2014")]
[assembly: AssemblyTrademark("Jig4Sitecore Team")]

[assembly: ComVisible(false)]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyVersion("2.0.0")]
[assembly: AssemblyFileVersion("2.0.0")]