﻿namespace Jig.Sitecore.Shell.SitePublishing
{
    using System;
    using System.Linq;

    using global::Sitecore;
    using global::Sitecore.Caching;
    using global::Sitecore.Configuration;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Events;
    using global::Sitecore.Publishing;
    using global::Sitecore.Sites;

    /// <summary>
    /// Class HtmlCacheClearer.
    /// </summary>
    public class HtmlCacheClearer
    {
        /// <summary>
        /// Clears the cache.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void ClearCache([NotNull] object sender, [NotNull] EventArgs args)
        {
            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull(args, "args");

            // 'publish:end' event provides one argument : instance of
            // Sitecore.Publishing.Publisher class performing the actual publish.
            var publisher = Event.ExtractParameter(args, 0) as Publisher;
            if (publisher == null)
            {
                var type = this.GetType();
                System.Diagnostics.Trace.TraceInformation("HtmlCacheClearer| unable to extract the publisher from event in " + type.FullName);
                return;
            }

            // Write publish information into the log.
            var msg = string.Format("HtmlCacheClearer| Publish completed. Target database: {0}. Begin html cache clearing.", publisher.Options.TargetDatabase.Name);

            System.Diagnostics.Trace.TraceInformation(msg);

            var databaseName = publisher.Options.TargetDatabase.Name;

            var sites =
                Factory.GetSiteInfoList()
                    .Where(x => !string.IsNullOrWhiteSpace(x.HostName) 
                        && !string.Equals(x.Name, "website", StringComparison.OrdinalIgnoreCase)
                        && string.Equals(x.Database, databaseName, StringComparison.InvariantCultureIgnoreCase))
                    .Select(x => x.Name)
                    .ToArray();

            if (sites.Length == 0)
            {
                System.Diagnostics.Trace.TraceInformation("HtmlCacheClearer| No sites found with site HostName property and Database name: " + databaseName);
                return;
            }

            foreach (var siteName in sites)
            {
                SiteContext site = Factory.GetSite(siteName);
                if (site == null)
                {
                    System.Diagnostics.Trace.TraceInformation("HtmlCacheClearer| Couldn't resolve site: " + siteName);
                    continue;
                }

                HtmlCache htmlCache = CacheManager.GetHtmlCache(site);
                if (htmlCache != null)
                {
                    System.Diagnostics.Trace.TraceInformation("HtmlCacheClearer| Clearing HTML caches for site: " + site.Name);
                    htmlCache.Clear();
                }
                else
                {
                    System.Diagnostics.Trace.TraceInformation("HtmlCacheClearer| Site has no html cache set: " + site.Name);
                }
            }

            System.Diagnostics.Trace.TraceInformation(typeof(HtmlCacheClearer).FullName + "  done.");
        }
    }
}