namespace Jig.Sitecore.Shell.SitePublishing
{
    using System;
    using System.Collections.Generic;

    using global::Sitecore;


    /// <summary>
    /// Interface Cdn Storage Client
    /// </summary>
    public interface ICdnBlobClient
    {
        /// <summary>
        /// Gets or sets the name of the account.
        /// </summary>
        /// <value>The name of the account.</value>
        [NotNull]
        string AccountName { get; set; }

        /// <summary>
        /// Gets or sets the account key.
        /// </summary>
        /// <value>The account key.</value>
        [NotNull]
        string AccountKey { get; set; }

        /// <summary>
        /// Gets or sets the BLOB container.
        /// </summary>
        /// <value>The BLOB container.</value>
        [NotNull]
        string BlobContainerName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [use item paths].
        /// </summary>
        /// <value><c>true</c> if [use item paths]; otherwise, <c>false</c>.</value>
        bool UseItemPaths { get; set; }

        /// <summary>
        /// Ensures the BLOB container exists.
        /// </summary>
        /// <returns>The Cdn Blob Client.</returns>
        ICdnBlobClient EnsureBlobContainerExists();

        bool UploadBlob(ICdnBlob blob);
    }

    /// <summary>
    /// Class CdnBlobClient.
    /// </summary>
    public class CdnBlobClient : ICdnBlobClient
    {
        /// <summary>
        /// Gets or sets the name of the account.
        /// </summary>
        /// <value>The name of the account.</value>
        public string AccountName { get; set; }

        /// <summary>
        /// Gets or sets the account key.
        /// </summary>
        /// <value>The account key.</value>
        public string AccountKey { get; set; }

        /// <summary>
        /// Gets or sets the BLOB container.
        /// </summary>
        /// <value>The BLOB container.</value>
        public string BlobContainerName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [use item paths].
        /// </summary>
        /// <value><c>true</c> if [use item paths]; otherwise, <c>false</c>.</value>
        public bool UseItemPaths { get; set; }

        /// <summary>
        /// Ensures the BLOB container exists.
        /// </summary>
        /// <returns>The Cdn Blob Client.</returns>
        public ICdnBlobClient EnsureBlobContainerExists()
        {
            return this;
        }

        public bool UploadBlob(ICdnBlob blob)
        {
            throw new NotImplementedException();
        }
    }

    public interface ICdnBlob
    {
        byte[] Bytes { get; set; }
        string ContentType { get; set; }
        string CacheControl { get; set; }
        string FilePrefix { get; set; }

        /// <summary>
        /// Gets the meta data.
        /// </summary>
        /// <value>The meta data.</value>
        [NotNull]
        IDictionary<string, string> MetaData { get; }
    }
}