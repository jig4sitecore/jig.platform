﻿namespace Jig.Sitecore.Shell.SitePublishing
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Web;

    using Ionic.Zip;

    using log4net;

    using Microsoft.WindowsAzure.Storage;
    using Microsoft.WindowsAzure.Storage.Auth;
    using Microsoft.WindowsAzure.Storage.Blob;
    using Microsoft.WindowsAzure.Storage.Shared.Protocol;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    /// <summary>
    /// The continuous integration task.
    /// </summary>
    public sealed class FrontEndContinuousIntegrationTask
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger("ContinuousIntegrations");

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FrontEndContinuousIntegrationTask" /> class.
        /// </summary>
        /// <param name="accountName">Name of the account.</param>
        /// <param name="accountKey">The account key.</param>
        /// <param name="blobContainerName">Name of the BLOB container.</param>
        public FrontEndContinuousIntegrationTask([NotNull]string accountName, [NotNull] string accountKey, [NotNull] string blobContainerName)
        {
            Assert.IsNotNullOrEmpty(accountName, "accountName");
            Assert.IsNotNullOrEmpty(accountKey, "accountKey");
            Assert.IsNotNullOrEmpty(blobContainerName, "blobContainerName");

            this.AccountName = accountName;
            this.AccountKey = accountKey;
            this.BlobContainerName = blobContainerName;
            this.CacheControl = "public, max-age=2592000, s-max-age=2592000";
            this.FilePrefix = DateTime.Now.ToString("yyyy-MM-dd--HHmmss");
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the account key.
        /// </summary>
        [NotNull]
        public string AccountKey { get; private set; }

        /// <summary>
        /// Gets the account name.
        /// </summary>
        [NotNull]
        public string AccountName { get; private set; }

        /// <summary>
        /// Gets the blob container.
        /// </summary>
        [NotNull]
        public string BlobContainerName { get; private set; }

        /// <summary>
        /// Gets or sets the cache control.
        /// </summary>
        [NotNull]
        public string CacheControl { get; set; }

        /// <summary>
        /// Gets or sets the file prefix.
        /// </summary>
        [NotNull]
        public string FilePrefix { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The try sync.
        /// </summary>
        /// <param name="siteZipUrl">The site zip url.</param>
        /// <returns>The <see cref="bool" />.</returns>
        [NotNull]
        public string Run([NotNull] Uri siteZipUrl)
        {
            bool isEnabled =
                Jig.Sitecore.Configuration.ConfigurationSettingManager.RetrieveSettingAsBoolean(
                    typeof(FrontEndContinuousIntegrationTask).FullName + ".IsEnabled",
                    false);

            if (!isEnabled)
            {
                Log.Warn("The Jig.Sitecore.Shell.SitePublishing.FrontEndContinuousIntegrationTask.IsEnabled is not enabled!");
                return string.Empty;
            }

            try
            {
                var credentials = new StorageCredentials(this.AccountName, this.AccountKey);
                var account = new CloudStorageAccount(credentials, useHttps: true);

                CloudBlobClient client = account.CreateCloudBlobClient();

                CloudBlobContainer container = client.GetContainerReference(this.BlobContainerName);

                if (!container.Exists())
                {
                    container.Create(BlobContainerPublicAccessType.Container);

                    // CORS should be enabled once at service startup
                    // Nullifying un-needed properties so that we don't
                    // override the existing ones
                    var serviceProperties = new ServiceProperties
                    {
                        HourMetrics = null,
                        MinuteMetrics = null,
                        Logging = null,
                        Cors = new CorsProperties()
                    };

                    serviceProperties.Cors.CorsRules.Add(new CorsRule
                    {
                        AllowedHeaders = new List<string> { "*" },
                        AllowedMethods = CorsHttpMethods.Get | CorsHttpMethods.Head,
                        AllowedOrigins = new List<string> { "*" },
                        ExposedHeaders = new List<string> { "*" },
                        MaxAgeInSeconds = 3600 * 24 * 30 // 30 Days
                    });

                    client.SetServiceProperties(serviceProperties);
                }

                using (var webClient = new WebClient())
                {
                    var data = webClient.DownloadData(siteZipUrl);

                    using (var zipReader = ZipFile.Read(new MemoryStream(data)))
                    {
                        var entries = zipReader.Entries;
                        foreach (var entry in entries)
                        {
                            if (entry.IsDirectory)
                            {
                                continue;
                            }

                            string fileName = entry.FileName;

                            byte[] bytes;
                            using (var memoryStream = new MemoryStream())
                            {
                                entry.Extract(memoryStream);
                                bytes = memoryStream.ToArray();
                            }

                            var name = this.FilePrefix + "/" + fileName.Replace('\\', '/');
                            CloudBlockBlob fileBlob = container.GetBlockBlobReference(name.ToLowerInvariant());

                            fileBlob.Properties.ContentType = MimeMapping.GetMimeMapping(fileName);
                            fileBlob.Properties.CacheControl = this.CacheControl;

                            fileBlob.UploadFromByteArrayAsync(bytes, 0, bytes.Length);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error(exception);
                throw;
            }

            return this.FilePrefix;
        }

        #endregion
    }
}