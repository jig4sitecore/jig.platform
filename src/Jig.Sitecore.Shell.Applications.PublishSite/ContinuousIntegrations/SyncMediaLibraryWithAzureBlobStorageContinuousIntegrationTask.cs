﻿namespace Jig.Sitecore.Shell.SitePublishing
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    using Jig.Sitecore.Configuration;
    using Microsoft.WindowsAzure.Storage;
    using Microsoft.WindowsAzure.Storage.Auth;
    using Microsoft.WindowsAzure.Storage.Blob;
    using Microsoft.WindowsAzure.Storage.Shared.Protocol;
    using global::Sitecore;
    using global::Sitecore.Configuration;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Events;
    using global::Sitecore.Publishing;
    using global::Sitecore.SecurityModel;

    /// <summary>
    /// Sync Media Library With Azure Blob Storage Continuous Integration Task.
    /// </summary>
    public class SyncMediaLibraryWithAzureBlobStorageContinuousIntegrationTask
    {
        /// <summary>
        /// The setting prefix
        /// </summary>
        private static readonly string SettingPrefix = typeof(SyncMediaLibraryWithAzureBlobStorageContinuousIntegrationTask).FullName;

        /// <summary>
        /// The is enabled
        /// </summary>
        private static readonly bool IsEnabled = Jig.Sitecore.Configuration.ConfigurationSettingManager.RetrieveSettingAsBoolean(SettingPrefix + ".IsEnabled", false);

        /// <summary>
        /// The log
        /// </summary>
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(SyncMediaLibraryWithAzureBlobStorageContinuousIntegrationTask));

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncMediaLibraryWithAzureBlobStorageContinuousIntegrationTask"/> class.
        /// </summary>
        public SyncMediaLibraryWithAzureBlobStorageContinuousIntegrationTask()
        {
            /* /sitecore/media library/sites */
            this.MediaPath = global::Jig.Sitecore.Constants.Paths.Sites.Media;
            this.CacheControl = "public, max-age=2592000, s-max-age=2592000";

            this.AccountName = ConfigurationSettingManager.RetrieveSetting(SettingPrefix + ".AccountName");
            this.AccountKey = ConfigurationSettingManager.RetrieveSetting(SettingPrefix + ".AccountKey");
            this.BlobContainer = ConfigurationSettingManager.RetrieveSetting(SettingPrefix + ".BlobContainer");
            this.UseItemPaths = global::Sitecore.Configuration.Settings.Media.UseItemPaths;
        }

        /// <summary>
        /// Gets or sets the media path.
        /// </summary>
        /// <value>The media path.</value>
        [CanBeNull]
        public string MediaPath { get; [UsedImplicitly] set; }

        /// <summary>
        /// Gets or sets the name of the account.
        /// </summary>
        /// <value>The name of the account.</value>
        [CanBeNull]
        public string AccountName { get; [UsedImplicitly] set; }

        /// <summary>
        /// Gets or sets the account key.
        /// </summary>
        /// <value>The account key.</value>
        [CanBeNull]
        public string AccountKey { get; [UsedImplicitly] set; }

        /// <summary>
        /// Gets or sets the BLOB container.
        /// </summary>
        /// <value>The BLOB container.</value>
        [CanBeNull]
        public string BlobContainer { get; [UsedImplicitly] set; }

        /// <summary>
        /// Gets or sets the cache control.
        /// </summary>
        /// <value>The cache control.</value>
        [CanBeNull]
        public string CacheControl { get; [UsedImplicitly] set; }

        /// <summary>
        /// Gets or sets a value indicating whether [use item paths].
        /// </summary>
        /// <value><c>true</c> if [use item paths]; otherwise, <c>false</c>.</value>
        public bool UseItemPaths { get; [UsedImplicitly] set; }

        /// <summary>
        /// Gets or sets the name of the active site.
        /// </summary>
        /// <value>The name of the active site.</value>
        [CanBeNull]
        public string ActiveSiteName { get; [UsedImplicitly] set; }

        /// <summary>
        /// Handles the <see cref="E:PublishEnd" /> event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        // ReSharper disable UnusedParameter.Global
        public void OnPublishEnd([NotNull] object sender, [NotNull] EventArgs args)
        // ReSharper restore UnusedParameter.Global
        {
            if (!IsEnabled)
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(this.AccountName))
            {
                var type = this.GetType();
                System.Diagnostics.Trace.TraceInformation("No AccountName is set to sync with Azure " + type.FullName);

                return;
            }

            if (string.IsNullOrWhiteSpace(this.AccountKey))
            {
                var type = this.GetType();
                System.Diagnostics.Trace.TraceInformation("No AccountKey is set to sync with Azure " + type.FullName);

                return;
            }

            if (string.IsNullOrWhiteSpace(this.BlobContainer))
            {
                var type = this.GetType();
                System.Diagnostics.Trace.TraceInformation("No BlobContainer is set to sync with Azure " + type.FullName);

                return;
            }

            if (string.IsNullOrWhiteSpace(this.MediaPath))
            {
                var type = this.GetType();
                System.Diagnostics.Trace.TraceInformation("No MediaPath is set to sync with Azure " + type.FullName);
                return;
            }

            // 'publish:end' event provides one argument : instance of
            // Sitecore.Publishing.Publisher class performing the actual publish.
            var publisher = Event.ExtractParameter(args, 0) as Publisher;
            if (publisher == null)
            {
                var type = this.GetType();
                System.Diagnostics.Trace.TraceInformation("unable to extract the publisher from event in " + type.FullName);
                return;
            }

            // Write publish information into the log.
            var msg = string.Format("Publish completed. Target database: {0}. Begin media asset sync", publisher.Options.TargetDatabase.Name);

            System.Diagnostics.Trace.TraceInformation(msg);

            Database database = Factory.GetDatabase(publisher.Options.TargetDatabase.Name);
            if (database == null)
            {
                return;
            }

            var root = database.GetItem(this.MediaPath);
            if (root == null)
            {
                var type = this.GetType();
                System.Diagnostics.Trace.TraceInformation("Could not find Media root '" + this.MediaPath + "' event in " + type.FullName);
                return;
            }

            if (!string.IsNullOrWhiteSpace(this.ActiveSiteName))
            {
                global::Sitecore.Context.SetActiveSite(this.ActiveSiteName);
            }

            var credentials = new StorageCredentials(this.AccountName, this.AccountKey);
            var account = new CloudStorageAccount(credentials, useHttps: true);

            CloudBlobClient client = account.CreateCloudBlobClient();

            CloudBlobContainer container = client.GetContainerReference(this.BlobContainer);

            if (!container.Exists())
            {
                container.Create(BlobContainerPublicAccessType.Container);

                // CORS should be enabled once at service startup
                // Nullifying un-needed properties so that we don't
                // override the existing ones
                var serviceProperties = new ServiceProperties
                {
                    HourMetrics = null,
                    MinuteMetrics = null,
                    Logging = null,
                    Cors = new CorsProperties()
                };

                serviceProperties.Cors.CorsRules.Add(new CorsRule
                {
                    AllowedHeaders = new List<string> { "*" },
                    AllowedMethods = CorsHttpMethods.Get | CorsHttpMethods.Head,
                    AllowedOrigins = new List<string> { "*" },
                    ExposedHeaders = new List<string> { "*" },
                    MaxAgeInSeconds = 3600 * 24 * 30 // 30 Days
                });

                client.SetServiceProperties(serviceProperties);
            }

            System.Diagnostics.Trace.TraceInformation(string.Concat("Starting parallel uploading media to CDN storage container ", this.BlobContainer, " with url ", container.Uri));

            var mediaLinkPrefix = Settings.Media.MediaLinkPrefix;

            var items = this.GetDescendants(root).ToArray();

            foreach (MediaItem mediaItem in items)
            {
                this.UploadMediaItemToBlobContainer(mediaItem, mediaLinkPrefix, container);
            }
        }

        /// <summary>
        /// Uploads the media item to BLOB container.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="mediaLinkPrefix">The media link prefix.</param>
        /// <param name="container">The container.</param>
        protected void UploadMediaItemToBlobContainer([NotNull] MediaItem mediaItem, [NotNull] string mediaLinkPrefix, [NotNull] CloudBlobContainer container)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (mediaItem != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var stream = mediaItem.GetMediaStream();
                if (stream != null)
                {
                    try
                    {
                        /*
                         * Note:
                         *  This code is design to transfer up to 30MB files
                         */
                        byte[] bytes;
                        using (var binaryReader = new BinaryReader(stream))
                        {
                            bytes = binaryReader.ReadBytes((int)stream.Length);
                        }

                        /*
                         * Issue:
                         *      The Sitecore RTE field will ignore <setting name="Media.UseItemPaths">false</setting> in the links. So we need upload all the tine.
                         * However, if media library doesn't have a SEO engine rules implementation and content authors upload "junk" it will create problems with CDN, then better to use GUID.
                         */
                        var name = mediaItem.MediaPath.ToLowerInvariant();

                        if (this.UseItemPaths)
                        {
                            name = string.Concat(
                                mediaLinkPrefix,
                                '/',
                                mediaItem.ID.Guid.ToString("N"),
                                '.',
                                mediaItem.Extension);

                            this.UploadMediaItemToCdn(mediaItem, container, name, bytes);
                        }
                        else
                        {
                            this.UploadMediaItemToCdn(mediaItem, container, name, bytes);
                        }
                    }
                    catch (Exception exception)
                    {
                        Log.Error(exception);
                    }
                }
            }
        }

        /// <summary>
        /// Uploads the media item to CDN.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="container">The container.</param>
        /// <param name="name">The name.</param>
        /// <param name="bytes">The bytes.</param>
        protected void UploadMediaItemToCdn([NotNull] MediaItem mediaItem, [NotNull] CloudBlobContainer container, [NotNull] string name, [NotNull] byte[] bytes)
        {
            Task.Run(
                () =>
                    {
                        CloudBlockBlob fileBlob = container.GetBlockBlobReference(name);

                        if (!fileBlob.Exists())
                        {
                            /* For some reason the SVG type mime type isn't correct */
                            fileBlob.Properties.ContentType =
                                Jig.Sitecore.Shell.SitePublishing.MimeMapping.GetMimeMapping(mediaItem.Extension);
                            fileBlob.Properties.CacheControl = this.CacheControl;

                            /* This Data never send to the client but could be accessed in Azure API */
                            fileBlob.Metadata.Add("MediaItemName", mediaItem.Name);
                            fileBlob.Metadata.Add("MediaItemPath", mediaItem.MediaPath);
                            fileBlob.Metadata.Add("MediaItemID", mediaItem.ID.Guid.ToString("D"));
                            fileBlob.Metadata.Add(
                                "MediaItemModified",
                                mediaItem.InnerItem.Statistics.Updated.ToString(CultureInfo.InvariantCulture));

                            var infoMsg = string.Concat(
                                "Uploading file: ",
                                name,
                                " with ID ",
                                mediaItem.DisplayName,
                                " ",
                                mediaItem.ID);

                            fileBlob.UploadFromByteArray(bytes, 0, bytes.Length);
                            Trace.TraceInformation(infoMsg);
                        }
                    });
        }

        /// <summary>
        /// Gets the media items.
        /// </summary>
        /// <param name="root">The root.</param>
        /// <returns>The collection of children items</returns>
        [NotNull]
        protected IEnumerable<MediaItem> GetDescendants([NotNull] Item root)
        {
            using (new SecurityDisabler())
            {
                var items = root.Axes.GetDescendants().ToArray();
                foreach (MediaItem mediaItem in items)
                {
                    if (mediaItem != null)
                    {
                        yield return mediaItem;
                    }
                }
            }
        }
    }
}