﻿namespace Jig.Sitecore.Shell.Publishing.PublishStatus
{
    using System.Collections.Generic;
    using System.Linq;

    using global::Sitecore;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Globalization;

    /// <summary>
    /// The publish status manager.
    /// </summary>
    public static class PublishStatusManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets the publish status.
        /// </summary>
        /// <param name="contextItem">The context item.</param>
        /// <returns>The collection of publish statuses.</returns>
        public static IEnumerable<PublishStatusInfo> GetPublishStatus(Item contextItem)
        {
            return GetPublishStatus(contextItem, new List<Language>());
        }

        /// <summary>
        /// The get publish status.
        /// </summary>
        /// <param name="contextItem">The context item.</param>
        /// <param name="languageFilter">The language filter.</param>
        /// <returns>The collection of publish infos.</returns>
        public static IEnumerable<PublishStatusInfo> GetPublishStatus(Item contextItem, List<Language> languageFilter)
        {
            const string PublishingTargetsPath = "/sitecore/system/publishing targets";

            var publishStatus = new List<PublishStatusInfo>();

            // Retrieve the publishing targets database names
            var publishingTargetsDatabases =
                Client.GetItemNotNull(PublishingTargetsPath)
                    .Children.ToDictionary(x => x[FieldIDs.PublishingTargetDatabase], x => x.Name);

            // Check publishing targets available
            if (publishingTargetsDatabases.Count == 0)
            {
                publishStatus.Add(PublishStatusInfo.NoPublishingTargets());
            }
            else
            {
                foreach (var databaseName in publishingTargetsDatabases.Keys)
                {
                    if (DatabaseExists(databaseName))
                    {
                        var database = global::Sitecore.Configuration.Factory.GetDatabase(databaseName);
                        var targetItem = database.GetItem(contextItem.ID);

                        var databaseDisplayName = publishingTargetsDatabases[databaseName];

                        PopulatePublishStatus(
                            publishStatus,
                            databaseDisplayName,
                            contextItem,
                            targetItem,
                            languageFilter);
                    }
                }
            }

            return publishStatus;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The database exists.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The <see cref="bool" />.</returns>
        private static bool DatabaseExists(string key)
        {
            return global::Sitecore.Configuration.Factory.GetDatabaseNames().Any(d => d.Equals(key));
        }

        /// <summary>
        /// The populate publish status.
        /// </summary>
        /// <param name="publishStatus">The publish status.</param>
        /// <param name="database">The database.</param>
        /// <param name="contextItem">The context item.</param>
        /// <param name="targetItem">The target item.</param>
        /// <param name="languageFilter">The language filter.</param>
        private static void PopulatePublishStatus(
            List<PublishStatusInfo> publishStatus,
            string database,
            Item contextItem,
            Item targetItem,
            List<Language> languageFilter)
        {
            // Check item published to database
            if (targetItem == null)
            {
                publishStatus.Add(PublishStatusInfo.NotPublishedToDatabase(database));
            }
            else
            {
                IEnumerable<Language> languages = contextItem.Languages;

                if (languageFilter.Any())
                {
                    languages = languages.Intersect(languageFilter);
                }

                foreach (var language in languages)
                {
                    PopulatePublishStatus(publishStatus, database, language, contextItem, targetItem);
                }
            }
        }

        /// <summary>
        /// The populate publish status.
        /// </summary>
        /// <param name="publishStatus">The publish status.</param>
        /// <param name="database">The database.</param>
        /// <param name="language">The language.</param>
        /// <param name="contextItem">The context item.</param>
        /// <param name="targetItem">The target item.</param>
        private static void PopulatePublishStatus(
            List<PublishStatusInfo> publishStatus,
            string database,
            Language language,
            Item contextItem,
            Item targetItem)
        {
            var contextLanguageVersion = contextItem.Versions.GetLatestVersion(language);

            if (contextLanguageVersion == null || contextLanguageVersion.Versions.Count == 0)
            {
                // No version for this language in the context items so the language isn't used at all
                return;
            }

            var languageIcon = LanguageService.GetIcon(language, contextItem.Database);
            var languageName = language.CultureInfo.DisplayName;

            // Check item published for language
            var targetLanguageVersion = targetItem.Versions.GetLatestVersion(language);
            if (targetLanguageVersion == null || targetLanguageVersion.Versions.Count == 0)
            {
                publishStatus.Add(PublishStatusInfo.NotPublishedForLanguage(database, languageIcon, languageName));
                return;
            }

            // Check latest version is published
            var contextLanguageVersionNumber = contextLanguageVersion.Version.Number;
            var targetLanguageVersionNumber = targetLanguageVersion.Version.Number;

            if (targetLanguageVersionNumber != contextLanguageVersionNumber)
            {
                publishStatus.Add(
                    PublishStatusInfo.NotLatestVersion(
                        database,
                        languageIcon,
                        languageName,
                        contextLanguageVersionNumber,
                        targetLanguageVersionNumber));

                return;
            }

            // Check latest revision is published
            var contextRevision = contextLanguageVersion.Statistics.Revision;
            var targetRevision = targetLanguageVersion.Statistics.Revision;
            if (contextRevision != targetRevision)
            {
                publishStatus.Add(
                    PublishStatusInfo.NotLatestRevision(
                        database,
                        languageIcon,
                        languageName,
                        contextLanguageVersionNumber));
                return;
            }

            // It seems latest and greatest is published to this database
            publishStatus.Add(
                PublishStatusInfo.Latest(database, languageIcon, languageName, contextLanguageVersionNumber));
        }

        #endregion
    }
}