﻿namespace Jig.Sitecore.Shell.Publishing.PublishStatus
{
    /// <summary>
    /// The publish status info.
    /// </summary>
    public class PublishStatusInfo
    {
        #region Constructors and Destructors

        /// <summary>
        /// Prevents a default instance of the <see cref="PublishStatusInfo"/> class from being created.
        /// </summary>
        private PublishStatusInfo()
        {
            // Only utility static methods are used to obtain an instance of this class
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the database.
        /// </summary>
        public string Database { get; private set; }

        /// <summary>
        /// Gets the language icon.
        /// </summary>
        public string LanguageIcon { get; private set; }

        /// <summary>
        /// Gets the language name.
        /// </summary>
        public string LanguageName { get; private set; }

        /// <summary>
        /// Gets the type.
        /// </summary>
        public PublishStatusType Type { get; private set; }

        /// <summary>
        /// Gets the version latest.
        /// </summary>
        public int VersionLatest { get; private set; }

        /// <summary>
        /// Gets the version published.
        /// </summary>
        public int VersionPublished { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The latest.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="languageIcon">The language icon.</param>
        /// <param name="languageName">The language name.</param>
        /// <param name="versionLatest">The version latest.</param>
        /// <returns>The <see cref="PublishStatusInfo" />.</returns>
        public static PublishStatusInfo Latest(
            string database,
            string languageIcon,
            string languageName,
            int versionLatest)
        {
            return new PublishStatusInfo
                       {
                           Type = PublishStatusType.Latest,
                           Database = database,
                           LanguageIcon = languageIcon,
                           LanguageName = languageName,
                           VersionLatest = versionLatest
                       };
        }

        /// <summary>
        /// The no publishing targets.
        /// </summary>
        /// <returns>
        /// The <see cref="PublishStatusInfo"/>.
        /// </returns>
        public static PublishStatusInfo NoPublishingTargets()
        {
            return new PublishStatusInfo { Type = PublishStatusType.NoPublishingTargets };
        }

        /// <summary>
        /// The not latest revision.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="languageIcon">The language icon.</param>
        /// <param name="languageName">The language name.</param>
        /// <param name="versionLatest">The version latest.</param>
        /// <returns>The <see cref="PublishStatusInfo" />.</returns>
        public static PublishStatusInfo NotLatestRevision(
            string database,
            string languageIcon,
            string languageName,
            int versionLatest)
        {
            return new PublishStatusInfo
                       {
                           Type = PublishStatusType.NotLatestRevision,
                           Database = database,
                           LanguageIcon = languageIcon,
                           LanguageName = languageName,
                           VersionLatest = versionLatest
                       };
        }

        /// <summary>
        /// The not latest version.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="languageIcon">The language icon.</param>
        /// <param name="languageName">The language name.</param>
        /// <param name="versionLatest">The version latest.</param>
        /// <param name="versionPublished">The version published.</param>
        /// <returns>The <see cref="PublishStatusInfo" />.</returns>
        public static PublishStatusInfo NotLatestVersion(
            string database,
            string languageIcon,
            string languageName,
            int versionLatest,
            int versionPublished)
        {
            return new PublishStatusInfo
                       {
                           Type = PublishStatusType.NotLatestVersion,
                           Database = database,
                           LanguageIcon = languageIcon,
                           LanguageName = languageName,
                           VersionLatest = versionLatest,
                           VersionPublished = versionPublished
                       };
        }

        /// <summary>
        /// The not published for language.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="languageIcon">The language icon.</param>
        /// <param name="languageName">The language name.</param>
        /// <returns>The <see cref="PublishStatusInfo" />.</returns>
        public static PublishStatusInfo NotPublishedForLanguage(
            string database,
            string languageIcon,
            string languageName)
        {
            return new PublishStatusInfo
                       {
                           Type = PublishStatusType.NotPublishedForLanguage,
                           Database = database,
                           LanguageIcon = languageIcon,
                           LanguageName = languageName
                       };
        }

        /// <summary>
        /// The not published to database.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>The <see cref="PublishStatusInfo" />.</returns>
        public static PublishStatusInfo NotPublishedToDatabase(string database)
        {
            return new PublishStatusInfo
                       {
                           Type = PublishStatusType.NotPublishedToDatabase,
                           Database = database
                       };
        }

        #endregion
    }
}