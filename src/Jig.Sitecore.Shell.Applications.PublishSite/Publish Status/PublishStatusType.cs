﻿namespace Jig.Sitecore.Shell.Publishing.PublishStatus
{
    /// <summary>
    /// The publish status type.
    /// </summary>
    public enum PublishStatusType
    {
        /// <summary>
        /// The no publishing targets.
        /// </summary>
        NoPublishingTargets, 

        /// <summary>
        /// The not published to database.
        /// </summary>
        NotPublishedToDatabase, 

        /// <summary>
        /// The not published for language.
        /// </summary>
        NotPublishedForLanguage, 

        /// <summary>
        /// The not latest version.
        /// </summary>
        NotLatestVersion, 

        /// <summary>
        /// The not latest revision.
        /// </summary>
        NotLatestRevision, 

        /// <summary>
        /// The latest.
        /// </summary>
        Latest
    }
}