﻿namespace Jig.Sitecore.Shell.Applications.ContentManager.Galleries.GalleryForm
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Web.UI;
    using Jig.Sitecore.Shell.Publishing.PublishStatus;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Resources;
    using global::Sitecore.Web.UI.HtmlControls;

    /// <summary>
    /// The publish status form.
    /// </summary>
    public class PublishStatusForm : global::Sitecore.Shell.Applications.ContentManager.Galleries.GalleryForm
    {
        #region Fields

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>The result.</value>
        public Border Result { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">The e.</param>
        protected override void OnLoad(EventArgs e)
        {
            Assert.ArgumentNotNull(e, "e");

            base.OnLoad(e);

            if (global::Sitecore.Context.ClientPage.IsEvent)
            {
                return;
            }

            var contextItem = UIUtil.GetItemFromQueryString(global::Sitecore.Context.ContentDatabase);
            Assert.IsNotNull(contextItem, "item");

            var result = new StringBuilder();

            var statuses = PublishStatusManager.GetPublishStatus(contextItem);
            foreach (var statusForDatabase in statuses.GroupBy(x => x.Database))
            {
                result.Append("<div style='font-weight: bold; padding: 2px 0 0 5px'>");
                result.Append(statusForDatabase.Key);
                result.Append("</div>");

                foreach (var status in statusForDatabase)
                {
                    result.Append("<div style='padding: 2px 0 0 10px'>");
                    result.Append(RenderSingleStatus(status));
                    result.Append("</div>");
                }
            }

            this.Result.Controls.Add(new LiteralControl(result.ToString()));
        }

        /// <summary>
        /// The render single status.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>The <see cref="string" />.</returns>
        private static string RenderSingleStatus(PublishStatusInfo status)
        {
            var languageString = string.Empty;
            if (!string.IsNullOrEmpty(status.LanguageIcon) && !string.IsNullOrEmpty(status.LanguageName))
            {
                languageString = Images.GetImage(status.LanguageIcon, 16, 16, "absmiddle", "0 4px 0 0")
                                 + status.LanguageName;
            }

            switch (status.Type)
            {
                case PublishStatusType.NoPublishingTargets:
                    return "No publishing targets.";
                case PublishStatusType.NotPublishedToDatabase:
                    return "<strong>Not published to database.</strong>";
                case PublishStatusType.NotPublishedForLanguage:
                    return string.Format("{0}: <strong>Not published for language.</strong>", languageString);
                case PublishStatusType.NotLatestVersion:
                    return string.Format(
                        "{0}, v{1}: <strong>Not latest version.</strong> Latest is v{2}.",
                        languageString,
                        status.VersionPublished,
                        status.VersionLatest);
                case PublishStatusType.NotLatestRevision:
                    return string.Format(
                        "{0}, v{1}: <strong>Not latest revision.</strong>",
                        languageString,
                        status.VersionLatest);
                case PublishStatusType.Latest:
                    return string.Format("{0}, v{1}: Latest is published.", languageString, status.VersionLatest);
            }

            return string.Empty;
        }

        #endregion
    }
}