﻿namespace Jig.Sitecore.Shell.SitePublishing.Dialogs
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;

    using global::Sitecore;

    using global::Sitecore.Configuration;

    using global::Sitecore.Data;

    using global::Sitecore.Data.Items;

    using global::Sitecore.Data.Managers;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Extensions;

    using global::Sitecore.Globalization;

    using global::Sitecore.Jobs;

    using global::Sitecore.Publishing;

    using global::Sitecore.Security.AccessControl;

    using global::Sitecore.SecurityModel;

    using global::Sitecore.Text;

    using global::Sitecore.Web;

    using global::Sitecore.Web.UI.HtmlControls;

    using global::Sitecore.Web.UI.Pages;

    using global::Sitecore.Web.UI.Sheer;

    /// <summary>
    /// The publish form.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate",
        Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1306:FieldNamesMustBeginWithLowerCaseLetter",
        Justification = "Reviewed. Suppression is OK here.")]
    public class PublishSiteForm : WizardForm
    {
        #region Fields

        // ReSharper disable UnassignedField.Global

        /// <summary>
        /// The error text.
        /// </summary>
        protected Memo ErrorText;

        /// <summary>
        /// The languages.
        /// </summary>
        protected Border Languages;

        /// <summary>
        /// The languages panel.
        /// </summary>
        protected Groupbox LanguagesPanel;

        /// <summary>
        /// The no publishing target.
        /// </summary>
        protected Border NoPublishingTarget;

        /// <summary>
        /// The publishing panel.
        /// </summary>
        protected Groupbox PublishingPanel;

        /// <summary>
        /// The publishing targets.
        /// </summary>
        protected Border PublishingTargets;

        /// <summary>
        /// The publishing targets panel.
        /// </summary>
        protected Groupbox PublishingTargetsPanel;

        /// <summary>
        /// The republish.
        /// </summary>
        protected Radiobutton Republish;

        /// <summary>
        /// The republish pane.
        /// </summary>
        protected Border RepublishPane;

        /// <summary>
        /// The result label.
        /// </summary>
        protected Border ResultLabel;

        /// <summary>
        /// The result text.
        /// </summary>
        protected Memo ResultText;

        /// <summary>
        /// The select all languages.
        /// </summary>
        protected Checkbox SelectAllLanguages;

        /// <summary>
        /// The select site pane.
        /// </summary>
        protected Scrollbox SelectSitePane;

        /// <summary>
        /// The settings pane.
        /// </summary>
        protected Scrollbox SettingsPane;

        /// <summary>
        /// The show result pane.
        /// </summary>
        protected Border ShowResultPane;

        /// <summary>
        /// The sites.
        /// </summary>
        protected Border Sites;

        /// <summary>
        /// The sites panel.
        /// </summary>
        protected Groupbox SitesPanel;

        /// <summary>
        /// The smart publish.
        /// </summary>
        protected Radiobutton SmartPublish;

        /// <summary>
        /// The smart publish pane.
        /// </summary>
        protected Border SmartPublishPane;

        /// <summary>
        /// The status.
        /// </summary>
        protected Literal Status;

        // ReSharper restore UnassignedField.Global
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether confirmed options.
        /// </summary>
        protected bool ConfirmedOptions
        {
            get
            {
                return (bool)Context.ClientPage.ServerProperties["ConfirmedOptions"];
            }

            set
            {
                Context.ClientPage.ServerProperties["ConfirmedOptions"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the item id.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        [NotNull]
        // ReSharper disable InconsistentNaming
        protected string ItemID
        // ReSharper restore InconsistentNaming
        {
            get
            {
                return StringUtil.GetString(this.ServerProperties["ItemID"]);
            }

            set
            {
                Assert.ArgumentNotNull(value, "value");
                this.ServerProperties["ItemID"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the item publishing.
        /// </summary>
        [NotNull]
        protected string ItemPublishing
        {
            get
            {
                return StringUtil.GetString(this.ServerProperties["ItemPublishing"]);
            }

            set
            {
                Assert.ArgumentNotNullOrEmpty(value, "value");
                this.ServerProperties["ItemPublishing"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the items to publish.
        /// </summary>
        [NotNull]
        protected ListString ItemsToPublish
        {
            get
            {
                return new ListString { StringUtil.GetString(this.ServerProperties["ItemsToPublish"]) };
            }

            set
            {
                Assert.IsNotNull(value, "value!= null");
                this.ServerProperties["ItemsToPublish"] = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the job handle.
        /// </summary>
        [NotNull]
        protected string JobHandle
        {
            get
            {
                return StringUtil.GetString(this.ServerProperties["JobHandle"]);
            }

            set
            {
                Assert.ArgumentNotNullOrEmpty(value, "value");
                this.ServerProperties["JobHandle"] = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether rebuild.
        /// </summary>
        protected bool Rebuild
        {
            get
            {
                return WebUtil.GetQueryString("mo") == "rebuild";
            }
        }

        /// <summary>
        /// Gets or sets the total processed items.
        /// </summary>
        protected long TotalProcessedItems
        {
            get
            {
                return MainUtil.GetLong(this.ServerProperties["TotalProcessedItems"], 0);
            }

            set
            {
                this.ServerProperties["TotalProcessedItems"] = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The check status.
        /// </summary>
        /// <exception cref="Exception">
        /// Throws when publishing process interrupted
        /// </exception>
        public void CheckStatus()
        {
            var handle = Handle.Parse(this.JobHandle);
            if (!handle.IsLocal)
            {
                SheerResponse.Timer("CheckStatus", Settings.Publishing.PublishDialogPollingInterval);
            }
            else
            {
                var status = PublishManager.GetStatus(handle);
                if (status == null)
                {
                    throw new Exception("The publishing process was unexpectedly interrupted.");
                }

                if (status.Failed)
                {
                    this.Active = "Retry";
                    this.NextButton.Disabled = true;
                    this.BackButton.Disabled = false;
                    this.CancelButton.Disabled = false;
                    this.ErrorText.Value = StringUtil.StringCollectionToString(status.Messages);
                }
                else
                {
                    string str1;
                    if (status.State == JobState.Running)
                    {
                        str1 = string.Format(
                            "{0} {1}<br/><br/>{2} {3}<br/><br/>{4} {5}",
                            (object)Translate.Text("Database:"),
                            (object)StringUtil.Capitalize(status.CurrentTarget.NullOr(db => db.Name)),
                            (object)Translate.Text("Language:"),
                            (object)(status.CurrentLanguageMessage ?? string.Empty),
                            (object)Translate.Text("Processed:"),
                            (object)status.Processed);
                    }
                    else
                    {
                        str1 = status.State != JobState.Initializing
                                   ? Translate.Text("Queued.")
                                   : Translate.Text("Initializing.");

                        str1 += "<br/>" + this.ItemPublishing;
                    }

                    if (status.IsDone)
                    {
                        var totalProcessed = this.TotalProcessedItems;
                        totalProcessed += status.Processed;
                        this.TotalProcessedItems = totalProcessed;

                        this.Status.Text = Translate.Text("Items processed: {0}.", (object)totalProcessed.ToString(CultureInfo.InvariantCulture));

                        var str2 = "Item: " + this.ItemPublishing + "\n";
                        str2 += StringUtil.StringCollectionToString(status.Messages, "\n");
                        if (!string.IsNullOrEmpty(str2))
                        {
                            this.ResultText.Value += str2 + "\n\n";
                        }

                        var itemsToPublish = this.ItemsToPublish;

                        if (itemsToPublish.Count == 0)
                        {
                            this.Active = "LastPage";
                            this.BackButton.Disabled = true;

                            Log.Debug("PublishSite-End, id: " + this.ItemID, this.GetType());

                            return;
                        }

                        SheerResponse.Timer("StartPublisher", 10);
                    }
                    else
                    {
                        SheerResponse.SetInnerHtml("PublishingTarget", str1);
                        SheerResponse.Timer("CheckStatus", Settings.Publishing.PublishDialogPollingInterval);
                    }
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The active page changed.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="oldPage">The old page.</param>
        protected override void ActivePageChanged([NotNull] string page, [NotNull] string oldPage)
        {
            Assert.ArgumentNotNull(page, "page");
            Assert.ArgumentNotNull(oldPage, "oldPage");
            this.NextButton.Header = Translate.Text("Next");
            if (page == "Settings")
            {
                var id = Context.ClientPage.ClientRequest.Form["SiteToPublish"];
                if (!string.IsNullOrEmpty(id))
                {
                    this.ItemID = id;
                }

                this.NextButton.Header = Translate.Text("Publish");
            }

            base.ActivePageChanged(page, oldPage);
            if (page != "Publishing")
            {
                return;
            }

            this.NextButton.Disabled = true;
            this.BackButton.Disabled = true;
            this.CancelButton.Disabled = true;

            this.BuildItemsToPublish();

            SheerResponse.SetInnerHtml("PublishingTarget", "&nbsp;");

            Log.Debug("PublishSite, id: " + this.ItemID, this.GetType());

            SheerResponse.Timer("StartPublisher", 10);
        }

        /// <summary>
        /// The active page changing.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="newPage">The new page.</param>
        /// <returns>
        /// The <see cref="bool" />.
        /// </returns>
        protected override bool ActivePageChanging([NotNull] string page, [NotNull] ref string newPage)
        {
            Assert.ArgumentNotNull(page, "page");
            Assert.ArgumentNotNull(newPage, "newPage");
            if (page == "Retry")
            {
                newPage = "Settings";
            }

            if (newPage == "Publishing")
            {
                var languages = GetLanguages();
                if (languages != null && languages.Length == 0)
                {
                    SheerResponse.Alert(Translate.Text("You must pick at least one language to publish."));
                    return false;
                }

                var publishingTargetDatabases = GetPublishingTargetDatabases();
                if (publishingTargetDatabases != null && publishingTargetDatabases.Length == 0)
                {
                    SheerResponse.Alert(Translate.Text("You must pick at least one publishing target."));
                    return false;
                }

                if (this.Republish.Checked && !this.ConfirmedOptions)
                {
                    Context.ClientPage.Start(this, "ConfirmPublishingOptions");
                    return false;
                }
            }

            return base.ActivePageChanging(page, ref newPage);
        }

        /// <summary>
        /// The confirm publishing options.
        /// </summary>
        /// <param name="args">The args.</param>
        protected void ConfirmPublishingOptions([NotNull] ClientPipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            if (!args.HasResult)
            {
                var checked3 = this.Republish.Checked;
                var str = string.Empty;
                if (!string.IsNullOrEmpty(this.ItemID))
                {
                    str = Translate.Text("You are about to publish the current item and its subitems.");
                }
                else
                {
                    str += Translate.Text("You are about to republish the whole database.");
                }

                if (checked3)
                {
                    if (!string.IsNullOrEmpty(str))
                    {
                        str += "\n\n";
                    }

                    str +=
                        Translate.Text(
                            "Republishing is an expensive operation that overwrites every item in the selected languages, even if that data has not changed.\nYou should only republish if the databases appear to be inconsistent and only after you have tried to fix the problem by performing a Smart Publishing operation.");
                }

                if (!string.IsNullOrEmpty(str))
                {
                    SheerResponse.Confirm(str + "\n\n" + Translate.Text("Do you want to proceed?"));
                    args.WaitForPostBack(true);
                }
                else
                {
                    this.ConfirmedOptions = true;
                    this.Next();
                }
            }
            else
            {
                if (args.Result != "yes")
                {
                    return;
                }

                this.ConfirmedOptions = true;
                this.Next();
            }
        }

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">The e.</param>
        protected override void OnLoad([NotNull] EventArgs e)
        {
            Assert.ArgumentNotNull(e, "e");
            base.OnLoad(e);
            if (Context.ClientPage.IsEvent)
            {
                return;
            }

            this.ItemID = WebUtil.GetQueryString("id", global::Jig.Sitecore.Constants.Id.Sites.Content.Guid.ToString("D"));
            this.SmartPublish.Checked = Registry.GetBool("/Current_User/Publish/SmartPublish", false);
            this.Republish.Checked = Registry.GetBool("/Current_User/Publish/Republish", false);

            this.BuildSites();

            this.BuildPublishingTargets();
            this.BuildLanguages();
            this.BuildPublishTypes();
        }

        /// <summary>
        /// The show result.
        /// </summary>
        protected void ShowResult()
        {
            this.ShowResultPane.Visible = false;
            this.ResultText.Visible = true;
            this.ResultLabel.Visible = true;
        }

        /// <summary>
        /// The start publisher.
        /// </summary>
        protected void StartPublisher()
        {
            var languages = GetLanguages();
            var publishingTargets = GetPublishingTargets();
            var publishingTargetDatabases = GetPublishingTargetDatabases();
            var flag2 = Context.ClientPage.ClientRequest.Form["PublishMode"] == "SmartPublish";
            var flag3 = Context.ClientPage.ClientRequest.Form["PublishMode"] == "Republish";
            var rebuild = this.Rebuild;

            var itemsToPublish = this.ItemsToPublish;
            var str = itemsToPublish[0];
            itemsToPublish.Remove(str);
            this.ItemsToPublish = itemsToPublish;

            this.ItemPublishing = Context.ContentDatabase.Items[str].Paths.FullPath;

            if (string.IsNullOrEmpty(str))
            {
                str = "null";
            }

            string message;
            if (rebuild)
            {
                message = string.Format(
                    "Rebuild database, databases: {0}",
                    StringUtil.Join(publishingTargetDatabases, ", "));
            }
            else
            {
                message =
                    string.Format(
                        "Publish, root: {0}, languages:{1}, targets:{2}, databases:{3}, smart:{4}, republish:{5}",
                        (object)str,
                        (object)StringUtil.Join(languages, ", "),
                        (object)StringUtil.Join(publishingTargets, ", ", "Name"),
                        (object)StringUtil.Join(publishingTargetDatabases, ", "),
                        (object)MainUtil.BoolToString(flag2),
                        (object)MainUtil.BoolToString(flag3));
            }

            Log.Debug(message, this.GetType());
            var listString1 = new ListString();
            if (languages != null)
            {
                foreach (var language in languages)
                {
                    listString1.Add(language.ToString());
                }

                Registry.SetString("/Current_User/Publish/Languages", listString1.ToString());
                var listString2 = new ListString();
                foreach (var obj in publishingTargets)
                {
                    listString2.Add(obj.ID.ToString());
                }

                Registry.SetString("/Current_User/Publish/Targets", listString2.ToString());
                Registry.SetBool("/Current_User/Publish/SmartPublish", flag2);
                Registry.SetBool("/Current_User/Publish/Republish", flag3);
                this.JobHandle =
                    (string.IsNullOrEmpty(this.ItemID)
                         ? (!flag2
                                ? (!rebuild
                                       ? PublishManager.Republish(
                                           global::Sitecore.Client.ContentDatabase,
                                           publishingTargetDatabases,
                                           languages,
                                           Context.Language)
                                       : PublishManager.RebuildDatabase(
                                           global::Sitecore.Client.ContentDatabase,
                                           publishingTargetDatabases))
                                : PublishManager.PublishSmart(
                                    global::Sitecore.Client.ContentDatabase,
                                    publishingTargetDatabases,
                                    languages,
                                    Context.Language))
                         : PublishManager.PublishItem(
                             global::Sitecore.Client.GetItemNotNull(str),
                             publishingTargetDatabases,
                             languages,
                             true,
                             flag2,
                             false)).ToString();
            }

            SheerResponse.Timer("CheckStatus", Settings.Publishing.PublishDialogPollingInterval);
        }

        /// <summary>
        /// The add item to publish.
        /// </summary>
        /// <param name="groupItemId">The group item id.</param>
        /// <param name="siteName">The site name.</param>
        /// <param name="itemsToPublish">The items to publish.</param>
        private static void AddItemToPublish([NotNull] ID groupItemId, [NotNull] string siteName, [NotNull] ListString itemsToPublish)
        {
            var groupItem = Context.ContentDatabase.Items[groupItemId];

            var siteFolder = groupItem.GetChildren().FirstOrDefault(c => c.Name == siteName);

            if (siteFolder != null)
            {
                itemsToPublish.Add(siteFolder.ID.ToString());
            }
        }

        /// <summary>
        /// The get languages.
        /// </summary>
        /// <returns>
        /// The languages"/>.
        /// </returns>
        [CanBeNull]
        private static Language[] GetLanguages()
        {
            var arrayList = new ArrayList();
            foreach (string index in Context.ClientPage.ClientRequest.Form.Keys)
            {
                if (index != null && index.StartsWith("la_", StringComparison.InvariantCulture))
                {
                    arrayList.Add(Language.Parse(Context.ClientPage.ClientRequest.Form[index]));
                }
            }

            return arrayList.ToArray(typeof(Language)) as Language[];
        }

        /// <summary>
        /// The get publishing target databases.
        /// </summary>
        /// <returns>
        /// The Databases.
        /// </returns>
        [CanBeNull]
        private static Database[] GetPublishingTargetDatabases()
        {
            var arrayList = new ArrayList();
            foreach (var baseItem in GetPublishingTargets())
            {
                var name = baseItem["Target database"];
                var database = Factory.GetDatabase(name);
                Assert.IsNotNull(database, typeof(Database), Translate.Text("Database \"{0}\" not found."), name);
                arrayList.Add(database);
            }

            return arrayList.ToArray(typeof(Database)) as Database[];
        }

        /// <summary>
        /// The get publishing targets.
        /// </summary>
        /// <returns>
        /// The List of publishing targets.
        /// </returns>
        [NotNull]
        private static List<Item> GetPublishingTargets()
        {
            var list = new List<Item>();
            foreach (string str in Context.ClientPage.ClientRequest.Form.Keys)
            {
                if (str != null && str.StartsWith("pb_", StringComparison.InvariantCulture))
                {
                    var obj = Context.ContentDatabase.Items[ShortID.Decode(str.Substring(3))];
                    Assert.IsNotNull(obj, typeof(Item), "Publishing target not found.");
                    list.Add(obj);
                }
            }

            return list;
        }

        /// <summary>
        /// The build items to publish.
        /// </summary>
        private void BuildItemsToPublish()
        {
            var itemsToPublish = new ListString();
            if (!string.IsNullOrWhiteSpace(this.ItemID))
            {
                var siteRootItem = Context.ContentDatabase.Items[this.ItemID];
                if (siteRootItem != null)
                {
                    var siteName = siteRootItem.Name;

                    AddItemToPublish(global::Jig.Sitecore.Constants.Id.Sites.Templates, siteName, itemsToPublish);
                    AddItemToPublish(
                        global::Jig.Sitecore.Constants.Id.Sites.PlaceholderSettings,
                        siteName,
                        itemsToPublish);

                    AddItemToPublish(global::Jig.Sitecore.Constants.Id.Sites.Sublayouts, siteName, itemsToPublish);
                    AddItemToPublish(global::Jig.Sitecore.Constants.Id.Sites.Renderings, siteName, itemsToPublish);
                    AddItemToPublish(global::Jig.Sitecore.Constants.Id.Sites.Models, siteName, itemsToPublish);
                    AddItemToPublish(global::Jig.Sitecore.Constants.Id.Sites.Controllers, siteName, itemsToPublish);
                    AddItemToPublish(global::Jig.Sitecore.Constants.Id.Sites.Layouts, siteName, itemsToPublish);
                    AddItemToPublish(global::Jig.Sitecore.Constants.Id.Sites.Branches, siteName, itemsToPublish);
                    AddItemToPublish(global::Jig.Sitecore.Constants.Id.Sites.System, siteName, itemsToPublish);

                    AddItemToPublish(global::Jig.Sitecore.Constants.Id.Sites.Media, siteName, itemsToPublish);

                    itemsToPublish.Add(this.ItemID);
                }
                else
                {
                    Log.Warn("Could not resolve the site ID: " + this.ItemID, this);
                }
            }
            else
            {
                Log.Warn("The Site ID is null!", this);
            }

            this.ItemsToPublish = itemsToPublish;
        }

        /// <summary>
        /// The build languages.
        /// </summary>
        private void BuildLanguages()
        {
            this.Languages.Controls.Clear();
            if (!Settings.Publishing.Enabled)
            {
                return;
            }

            HtmlGenericControl htmlGenericControl1 = null;
            var listString = new ListString(Registry.GetString("/Current_User/Publish/Languages"));
            var languages = LanguageManager.GetLanguages(Context.ContentDatabase);
            this.SelectAllLanguages.Checked = listString.Count == languages.Count;
            foreach (var language in languages.OrderBy(l => l.GetDisplayName()))
            {
                if (Settings.CheckSecurityOnLanguages)
                {
                    var languageItemId = LanguageManager.GetLanguageItemId(language, Context.ContentDatabase);
                    if (!ItemUtil.IsNull(languageItemId))
                    {
                        var obj = Context.ContentDatabase.GetItem(languageItemId);
                        if (obj == null || !this.CanWriteLanguage(obj))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }

                var uniqueId = global::Sitecore.Web.UI.HtmlControls.Control.GetUniqueID("la_");
                htmlGenericControl1 = new HtmlGenericControl("input");
                this.Languages.Controls.Add(htmlGenericControl1);
                htmlGenericControl1.Attributes["type"] = "checkbox";
                htmlGenericControl1.ID = uniqueId;
                htmlGenericControl1.Attributes["value"] = language.Name;
                if (listString.IndexOf(language.ToString()) >= 0)
                {
                    htmlGenericControl1.Attributes["checked"] = "checked";
                }

                var htmlGenericControl2 = new HtmlGenericControl("label");
                this.Languages.Controls.Add(htmlGenericControl2);
                htmlGenericControl2.Attributes["for"] = uniqueId;
                htmlGenericControl2.InnerText = language.CultureInfo.DisplayName;
                this.Languages.Controls.Add(new LiteralControl("<br>"));
            }

            if (languages.Count != 1 || htmlGenericControl1 == null)
            {
                return;
            }

            this.LanguagesPanel.Disabled = true;
            htmlGenericControl1.Attributes["checked"] = "checked";
            htmlGenericControl1.Attributes["disabled"] = "disabled";
        }

        /// <summary>
        /// The build publish types.
        /// </summary>
        private void BuildPublishTypes()
        {
            if (!Settings.Publishing.Enabled)
            {
                return;
            }

            this.SmartPublishPane.Visible = Policy.IsAllowed("Publish/Can Perform Smart Publish");
            this.RepublishPane.Visible = Policy.IsAllowed("Publish/Can Perform Republish");
            this.ConfirmedOptions = false;
            if (!string.IsNullOrEmpty(this.ItemID))
            {
                this.SmartPublish.Checked = true;
            }
            else if (this.Rebuild)
            {
                this.PublishingPanel.Style["display"] = "none";
                this.LanguagesPanel.Style["display"] = "none";
                this.SmartPublish.Checked = false;
            }
            else
            {
                Assert.CanRunApplication("Publish");
            }
        }

        /// <summary>
        /// The build publishing targets.
        /// </summary>
        private void BuildPublishingTargets()
        {
            this.PublishingTargets.Controls.Clear();
            var itemNotNull = global::Sitecore.Client.GetItemNotNull("/sitecore/system/publishing targets");
            var listString = new ListString(Registry.GetString("/Current_User/Publish/Targets"));
            var str1 = Settings.DefaultPublishingTargets.ToLowerInvariant();
            var children = itemNotNull.Children;
            if (children.Count <= 0 || !Settings.Publishing.Enabled)
            {
                this.SettingsPane.Visible = false;
                this.NoPublishingTarget.Visible = true;
                this.NextButton.Disabled = true;
            }
            else
            {
                var htmlGenericControl1 = (HtmlGenericControl)null;
                foreach (var obj in children.OrderBy(c => c.DisplayName))
                {
                    var str2 = "pb_" + ShortID.Encode(obj.ID);
                    htmlGenericControl1 = new HtmlGenericControl("input");
                    this.PublishingTargets.Controls.Add(htmlGenericControl1);
                    htmlGenericControl1.Attributes["type"] = "checkbox";
                    htmlGenericControl1.ID = str2;
                    var flag = str1.IndexOf('|' + obj.Key + '|', StringComparison.InvariantCulture) >= 0 || listString.Contains(obj.ID.ToString());

                    if (flag)
                    {
                        htmlGenericControl1.Attributes["checked"] = "checked";
                    }

                    htmlGenericControl1.Disabled = children.Count == 1 || !obj.Access.CanWrite();
                    var htmlGenericControl2 = new HtmlGenericControl("label");
                    this.PublishingTargets.Controls.Add(htmlGenericControl2);
                    htmlGenericControl2.Attributes["for"] = str2;
                    htmlGenericControl2.InnerText =
                        string.Concat(new[] { obj.DisplayName, " (", obj[FieldIDs.PublishingTargetDatabase], ")" });
                    this.PublishingTargets.Controls.Add(new LiteralControl("<br>"));
                }

                if (children.Count != 1)
                {
                    return;
                }

                this.PublishingTargetsPanel.Disabled = true;
                if (htmlGenericControl1 == null)
                {
                    return;
                }

                htmlGenericControl1.Attributes["checked"] = "checked";
            }
        }

        /// <summary>
        /// The build sites.
        /// </summary>
        /// <exception cref="Exception">
        /// Throws if no sites exist to publish
        /// </exception>
        private void BuildSites()
        {
            this.Sites.Controls.Clear();
            var item = Context.ContentDatabase.Items[this.ItemID];

            if (item == null)
            {
                return;
            }

            var template = item.Database.GetTemplate(typeof(global::Jig.Sitecore.Sites.Shared.Data.Folders.Site));
            if (template == null)
            {
                return;
            }

            if (item.IsDerived(template))
            {
                // If a site root, then continue
                this.Next();
                return;
            }

            // Build site list
            HtmlGenericControl htmlGenericControl1 = null;
            var sites =
                Context.ContentDatabase.SelectItems(
                    global::Jig.Sitecore.Constants.Paths.Sites.Content
                    + "/*[@@templateid='{652DDB99-0DA0-4B13-9CDC-E3E7AF7189CC}']");

            if (sites.Length == 0)
            {
                throw new Exception("No sites exist to publish.");
            }

            var first = true;
            foreach (var site in sites)
            {
                var uniqueId = global::Sitecore.Web.UI.HtmlControls.Control.GetUniqueID("site_");
                htmlGenericControl1 = new HtmlGenericControl("input");
                this.Sites.Controls.Add(htmlGenericControl1);
                htmlGenericControl1.Attributes["type"] = "radio";
                htmlGenericControl1.ID = uniqueId;
                htmlGenericControl1.Attributes["value"] = site.ID.ToString();
                htmlGenericControl1.Attributes["name"] = "SiteToPublish";
                if (first)
                {
                    first = false;
                    htmlGenericControl1.Attributes["checked"] = "checked";
                }

                var htmlGenericControl2 = new HtmlGenericControl("label");
                this.Sites.Controls.Add(htmlGenericControl2);
                htmlGenericControl2.Attributes["for"] = uniqueId;
                htmlGenericControl2.InnerText = site.DisplayName;
                this.Sites.Controls.Add(new LiteralControl("<br>"));
            }

            if (sites.Length != 1 || htmlGenericControl1 == null)
            {
                return;
            }

            this.SitesPanel.Disabled = true;
            htmlGenericControl1.Attributes["checked"] = "checked";
            htmlGenericControl1.Attributes["disabled"] = "disabled";
        }

        /// <summary>
        /// The can write language.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The true if Can write Language.
        /// </returns>
        private bool CanWriteLanguage([NotNull] Item item)
        {
            return AuthorizationManager.IsAllowed(item, AccessRight.LanguageWrite, Context.User);
        }

        #endregion
    }
}