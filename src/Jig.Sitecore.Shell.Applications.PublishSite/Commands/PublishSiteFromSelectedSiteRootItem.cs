﻿namespace Jig.Sitecore.Shell.SitePublishing.Commands
{
    using global::Sitecore;
    using global::Sitecore.Configuration;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Shell.Framework.Commands;
    using global::Sitecore.Web.UI.Sheer;

    /// <summary>
    /// The publish site.
    /// </summary>
    public class PublishSiteFromSelectedSiteRootItem : Command
    {
        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public override void Execute([NotNull] CommandContext context)
        {
            Assert.ArgumentNotNull(context, "context");

            SheerResponse.CheckModified(false);
            SheerResponse.Broadcast(
                SheerResponse.ShowModalDialog("/sitecore/shell/Applications/PublishSite.aspx"),
                "Shell");
        }

        /// <summary>
        /// The query state.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>
        /// The <see cref="CommandState" />.
        /// </returns>
        public override CommandState QueryState([NotNull] CommandContext context)
        {
            Assert.ArgumentNotNull(context, "context");
            return !Settings.Publishing.Enabled ? CommandState.Hidden : CommandState.Enabled;
        }
    }
}