﻿namespace Jig.Sitecore.Shell.SitePublishing.Commands
{
    using System;
    using System.Diagnostics;
    using Jig.Sitecore.Shell.SitePublishing;
    using Jig.Sitecore.Sites.Shared.Data.System;
    using global::Sitecore;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Shell.Framework.Commands;
    using global::Sitecore.Web.UI.Sheer;

    /// <summary>
    /// Class FrontEndSiteSyncCommand. This class cannot be inherited.
    /// </summary>
    public sealed class FrontEndSiteSyncCommand : Command
    {
        /// <summary>
        /// Executes the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        public override void Execute([NotNull]CommandContext context)
        {
            Item item = context.Items[0];

            var continuesIntegrationItem = item.As<StaticFileHostNameWithContinuesIntegration>();
            if (continuesIntegrationItem != null)
            {
                string accountName = continuesIntegrationItem.AccountName.Value;
                if (string.IsNullOrWhiteSpace(accountName))
                {
                    SheerResponse.Alert("The Account Name is missing!");
                    return;
                }

                string accountKey = continuesIntegrationItem.AccountKey.Value;

                if (string.IsNullOrWhiteSpace(accountKey))
                {
                    SheerResponse.Alert("The Account Key is missing!");
                    return;
                }

                string blobContainerName = continuesIntegrationItem.BlobContainerName.Value;
                if (string.IsNullOrWhiteSpace(blobContainerName))
                {
                    SheerResponse.Alert("The Blob Container Name is missing!");
                    return;
                }

                var urlValue = continuesIntegrationItem.FrontEndSiteZipUrl;
                if (string.IsNullOrWhiteSpace(urlValue))
                {
                    SheerResponse.Alert("The Front End Site Zip Url is missing!");
                    return;
                }

                Uri uri;
                if (!Uri.TryCreate(urlValue, UriKind.Absolute, out uri))
                {
                    SheerResponse.Alert("The Front End Site Zip Url could not be parsed!");
                    return;
                }

                try
                {
                    var folder = this.SyncSite(accountName, accountKey, blobContainerName, uri);
                    this.SyncItem(continuesIntegrationItem, folder);

                    SheerResponse.Alert("Site Sync finished!");
                }
                catch (System.Exception ex)
                {
                    Trace.TraceError(ex.Message);
                    Log.Error(ex.Message, ex, this.GetType());
                    SheerResponse.ShowError(ex);
                    return;
                }

                global::Sitecore.Context.ClientPage.SendMessage(this, "item:load(id=" + item.ID + ")");
            }
        }

        /// <summary>
        /// Queries the state.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>Command State</returns>
        public override CommandState QueryState([NotNull]CommandContext context)
        {
            Assert.ArgumentNotNull(context, "context");

            if (context.Items.Length != 1)
            {
                return CommandState.Hidden;
            }

            Item item = context.Items[0];
            if (item.Appearance.ReadOnly)
            {
                return CommandState.Disabled;
            }

            if (item.Versions.Count == 0)
            {
                return CommandState.Disabled;
            }

            if (item.TemplateID.Guid == typeof(StaticFileHostNameWithContinuesIntegration).GUID)
            {
                return CommandState.Disabled;
            }

            bool isEnabled =
                            Jig.Sitecore.Configuration.ConfigurationSettingManager.RetrieveSettingAsBoolean(
                                typeof(FrontEndContinuousIntegrationTask).FullName + ".IsEnabled",
                                false);

            if (!isEnabled)
            {
                return CommandState.Disabled;
            }

            return base.QueryState(context);
        }

        /// <summary>
        /// Synchronizes the site.
        /// </summary>
        /// <param name="accountName">Name of the account.</param>
        /// <param name="accountKey">The account key.</param>
        /// <param name="blobContainerName">Name of the BLOB container.</param>
        /// <param name="siteZipUri">The site zip URI.</param>
        /// <returns>folder name</returns>
        /// <exception cref="System.ArgumentException">Could not parse FrontEnd Site Zip Url!</exception>
        [NotNull]
        private string SyncSite([NotNull]string accountName, [NotNull]string accountKey, [NotNull]string blobContainerName, [NotNull] Uri siteZipUri)
        {
            var task = new FrontEndContinuousIntegrationTask(accountName, accountKey, blobContainerName);
            var folder = task.Run(siteZipUri);
            return folder;
        }

        /// <summary>
        /// Synchronizes the item.
        /// </summary>
        /// <param name="continuesIntegrationItem">The continues integration item.</param>
        /// <param name="folder">The folder.</param>
        private void SyncItem([NotNull]StaticFileHostNameWithContinuesIntegration continuesIntegrationItem, [NotNull] string folder)
        {
            try
            {
                string path = continuesIntegrationItem.StaticFileHostNameWithOptionalPath.Value;
                var item = continuesIntegrationItem.InnerItem;
                item.Editing.BeginEdit();

                var field = item.Fields[StaticFileHostNameWithContinuesIntegration.FieldNames.StaticFileHostNameWithOptionalPath];
                if (string.IsNullOrWhiteSpace(path))
                {
                    field.Value = path;
                }
                else
                {
                    var index = path.LastIndexOf('/');
                    if (index < 1)
                    {
                        /* Case:
                         * '/' + {folderName} 
                         */
                        field.Value = string.Concat(path, folder);
                    }
                    else
                    {
                        /* Case:
                         * '{hostname}/' + {folderName} 
                         */
                        string newValue = path.Substring(0, index) + "/" + folder;
                        field.Value = newValue;
                    }
                }

                item.Editing.AcceptChanges(true, false);
            }
            catch
            {
                continuesIntegrationItem.InnerItem.Editing.CancelEdit();
                throw;
            }
        }
    }
}
