﻿namespace Jig.Sitecore.Shell.SitePublishing.Commands
{
    using System.Collections.Specialized;
    using System.Linq;

    using Jig.Sitecore.Sites.Shared.Data.Folders;

    using global::Sitecore;

    using global::Sitecore.Configuration;

    using global::Sitecore.Data;

    using global::Sitecore.Data.Items;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Globalization;

    using global::Sitecore.Shell.Framework.Commands;

    using global::Sitecore.Text;

    using global::Sitecore.Web.UI.Sheer;

    /// <summary>
    /// The publish site.
    /// </summary>
    public class PublishSiteFromAnySelectedSiteItem : Command
    {
        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public override void Execute([NotNull] CommandContext context)
        {
            if (context.Items.Length != 1)
            {
                return;
            }

            var obj = context.Items[0];
            var parameters = new NameValueCollection();
            parameters["id"] = obj.ID.ToString();
            parameters["language"] = obj.Language.ToString();
            parameters["version"] = obj.Version.ToString();
            parameters["workflow"] = "0";
            Context.ClientPage.Start(this, "Run", parameters);
        }

        /// <summary>
        /// The query state.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>
        /// The <see cref="CommandState" />.
        /// </returns>
        public override CommandState QueryState([NotNull] CommandContext context)
        {
            if (context.Items.Length != 1 || !Settings.Publishing.Enabled)
            {
                return CommandState.Hidden;
            }

            var item = context.Items[0];
            if (item == null)
            {
                return CommandState.Hidden;
            }

            var template = item.Database.GetTemplate(typeof(Site));
            if (template == null)
            {
                return CommandState.Hidden;
            }

            if (!item.IsDerived(template))
            {
                return CommandState.Hidden;
            }

            return base.QueryState(context);
        }

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected void Run([NotNull] ClientPipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            var index = args.Parameters["id"];
            var name = args.Parameters["language"];
            var str = args.Parameters["version"];
            if (!SheerResponse.CheckModified())
            {
                return;
            }

            var obj = Context.ContentDatabase.Items[index, Language.Parse(name), Version.Parse(str)];
            if (obj == null)
            {
                SheerResponse.Alert("Item not found.");
            }
            else
            {
                if (!CheckWorkflow(args, obj))
                {
                    return;
                }

                Log.Debug(string.Format("Publish item: {0}", AuditFormatter.FormatItem(obj)), this);

                SheerResponse.CheckModified(false);
                var urlString = new UrlString("/sitecore/shell/Applications/PublishSite.aspx");
                urlString.Append("id", obj.ID.ToString());
                SheerResponse.Broadcast(SheerResponse.ShowModalDialog(urlString.ToString()), "Shell");
            }
        }

        /// <summary>
        /// The check workflow.
        /// </summary>
        /// <param name="args">The args.</param>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The <see cref="bool" />.
        /// </returns>
        private static bool CheckWorkflow([NotNull] ClientPipelineArgs args, [NotNull] Item item)
        {
            Assert.ArgumentNotNull(args, "args");
            Assert.ArgumentNotNull(item, "item");
            if (args.Parameters["workflow"] == "1")
            {
                return true;
            }

            args.Parameters["workflow"] = "1";
            if (args.IsPostBack)
            {
                if (args.Result == "yes")
                {
                    args.IsPostBack = false;
                    return true;
                }

                args.AbortPipeline();
                return false;
            }

            var site = Factory.GetSite("publisher");
            if (site != null && !site.EnableWorkflow)
            {
                return true;
            }

            var workflowProvider = Context.ContentDatabase.WorkflowProvider;
            if (workflowProvider == null || workflowProvider.GetWorkflows().Length <= 0)
            {
                return true;
            }

            var workflow = workflowProvider.GetWorkflow(item);
            if (workflow == null)
            {
                return true;
            }

            var state = workflow.GetState(item);
            if (state == null || state.FinalState)
            {
                return true;
            }

            args.Parameters["workflow"] = "0";
            if (state.PreviewPublishingTargets.Any())
            {
                return true;
            }

            SheerResponse.Confirm(
                Translate.Text(
                    "The current item \"{0}\" is in the workflow state \"{1}\"\n and will not be published.\n\nAre you sure you want to publish?",
                    (object)item.DisplayName,
                    (object)state.DisplayName));
            args.WaitForPostBack();
            return false;
        }
    }
}