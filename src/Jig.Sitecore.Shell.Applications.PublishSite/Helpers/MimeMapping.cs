namespace Jig.Sitecore.Shell.SitePublishing
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Type Mime Mapping.
    /// </summary>
    public static class MimeMapping
    {
        /// <summary>
        /// The MIME mappings
        /// </summary>
        private static readonly IDictionary<string, string> MimeMappings = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase) 
                                                                               {
                                                                                   { ".svg", "image/svg+xml" },
                                                                                   { ".svgz", "image/svg+xml" },
                                                                                   { ".ico", "image/x-icon" },
                                                                                   { ".js", "application/x-javascript" },
                                                                                   { ".css", "text/css" },
                                                                                   { ".jpeg", "image/jpeg" },
                                                                                   { ".jpg", "image/jpeg" },
                                                                                   { ".gif", "image/gif" },
                                                                                   { ".png", "image/png" },
                                                                                   { ".htm", "text/html" },
                                                                                   { ".html", "text/html" },
                                                                                   { ".pdf", "application/octet-stream" },
                                                                                   { ".eot", "application/pdf" },
                                                                                   { ".ttf", "application/vnd.ms-fontobject" },
                                                                                   { ".woff", "application/font-woff" },
                                                                                   { ".woff2", "application/font-woff2" },
                                                                                   { ".map", "application/json" },
                                                                                   { ".json", "application/json" }
                                                                               };

        /// <summary>
        /// Gets the type of the MIME.
        /// </summary>
        /// <param name="extension">The extension.</param>
        /// <returns>The mime</returns>
        public static string GetMimeMapping(string extension)
        {
            if (extension == null)
            {
                throw new ArgumentNullException("extension");
            }

            if (!extension.StartsWith("."))
            {
                extension = "." + extension;
            }

            string mime;
            if (MimeMappings.TryGetValue(extension, out mime))
            {
                return mime;
            }

            return System.Web.MimeMapping.GetMimeMapping(extension);
        }
    }
}