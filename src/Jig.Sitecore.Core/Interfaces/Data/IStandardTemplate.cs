﻿namespace Jig.Sitecore.Data
{
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using global::Newtonsoft.Json;
    using global::Sitecore;
    using global::Sitecore.Mvc.Presentation;

    /// <summary>
    /// Represents a Sitecore Item that descends from the Standard Template.
    /// </summary>
    [Guid("1930BBEB-7805-471A-A3BE-4858AC7CF696")]
    [JsonObject(MemberSerialization.OptIn)]
    public interface IStandardTemplate : ICustomItem, IRenderingModel
    {
        /// <summary>
        /// Gets the database that originated the Item.
        /// </summary>
        [NotNull]
        global::Sitecore.Data.Database Database { get; }

        /// <summary>
        /// Gets the Display Name for the current version and language
        /// of the Item, otherwise returns the Item.Name value.
        /// </summary>
        [NotNull]
        string DisplayName { get; }

        /// <summary>
        /// Gets the help object associated with this Item.
        /// </summary>
        [NotNull]
        global::Sitecore.Data.Items.ItemHelp Help { get; }

        /// <summary>
        /// Gets the ID of the Item.
        /// </summary>
        // ReSharper disable InconsistentNaming
        [NotNull]
        global::Sitecore.Data.ID ID { get; }

        // ReSharper restore InconsistentNaming

        /// <summary>
        /// Gets the internal Item version.
        /// </summary>
        [NotNull]
        global::Sitecore.Data.Items.Item InnerItem { get; }

        /// <summary>
        /// Verifies that the Item, in its current Language, is not effectively empty.
        /// </summary>
        /// <returns>True if the Item's Language has one or more Versions.</returns>
        bool LanguageVersionIsEmpty();

        /// <summary>
        /// Given an Item in a specific language dialect (ex: en-GB), determines if the
        /// dialect has a version, and if not, attempts to find a more generalized language (ex: en).
        /// If there is a more generalized language, returns an item instance in that more generalized
        /// language. The instance may be "empty" and should be checked using the LanguageVersionIsEmpty() extension.
        /// </summary>
        /// <param name="targetLanguage">The expected language.</param>
        /// <returns>The current language version, or a language version in a more generalized language if available.</returns>
        [CanBeNull]
        IStandardTemplate GetBestFitLanguageVersion([NotNull] global::Sitecore.Globalization.Language targetLanguage);

        /// <summary>
        /// Given an Item in a specific language dialect (ex: en-GB), determines if the
        /// dialect has a version, and if not, attempts to find a more generalized language (ex: en).
        /// If there is a more generalized language, returns an item instance in that more generalized
        /// language. The instance may be "empty" and should be checked using the LanguageVersionIsEmpty() extension.
        /// </summary>
        /// <typeparam name="TItem">The Constellation.Sitecore.Items Item type.</typeparam>
        /// <param name="targetLanguage">
        /// The expected language.
        /// </param>
        /// <returns>
        /// The current language version, or a language version in a more generalized language if available.
        /// </returns>
        [CanBeNull]
        TItem GetBestFitLanguageVersion<TItem>([NotNull] global::Sitecore.Globalization.Language targetLanguage) where TItem : class, IStandardTemplate;

        /// <summary>
        /// Given an Item in a specific language dialect (ex: en-GB), determines if the
        /// dialect has a version, and if not, attempts to find a more generalized language (ex: en).
        /// If there is a more generalized language, returns an item instance in that more generalized
        /// language. The instance may be "empty" and should be checked using the LanguageVersionIsEmpty() extension.
        /// </summary>
        /// <returns>The current language version, or a language version in a more generalized language if available.</returns>
        [CanBeNull]
        IStandardTemplate GetBestFitLanguageVersion();

        /// <summary>
        /// Given an Item in a specific language dialect (ex: en-GB), determines if the
        /// dialect has a version, and if not, attempts to find a more generalized language (ex: en).
        /// If there is a more generalized language, returns an item instance in that more generalized
        /// language. The instance may be "empty" and should be checked using the LanguageVersionIsEmpty() extension.
        /// </summary>
        /// <typeparam name="TItem">The Constellation.Sitecore.Items Item type.</typeparam>
        /// <returns>
        /// The current language version, or a language version in a more generalized language if available.
        /// </returns>
        [CanBeNull]
        TItem GetBestFitLanguageVersion<TItem>() where TItem : class, IStandardTemplate;

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <typeparam name="TItem">The type of the item.</typeparam>
        /// <returns>The Children collection</returns>
        [NotNull]
        IList<TItem> GetChildren<TItem>() where TItem : class, IStandardTemplate;

        /// <summary>
        /// Gets the descendants.
        /// </summary>
        /// <typeparam name="TItem">The type of the t item.</typeparam>
        /// <returns>The descendant list</returns>
        [NotNull]
        IList<TItem> GetDescendants<TItem>() where TItem : class, IStandardTemplate;

        /// <summary>
        /// Gets the ancestors.
        /// </summary>
        /// <typeparam name="TItem">The type of the t item.</typeparam>
        /// <returns>The ancestor list</returns>
        [NotNull]
        IList<TItem> GetAncestors<TItem>() where TItem : class, IStandardTemplate;

        /// <summary>
        /// Gets the first or default child as.
        /// </summary>
        /// <typeparam name="TItem">The type of the t item.</typeparam>
        /// <returns>TItem instance or default value.</returns>
        [CanBeNull]
        TItem GetFirstOrDefaultChildAs<TItem>() where TItem : class, IStandardTemplate;

        /// <summary>
        /// Gets the first or default descendant as.
        /// </summary>
        /// <typeparam name="TItem">The type of the t item.</typeparam>
        /// <returns>TItem instance or default value.</returns>
        [CanBeNull]
        TItem GetFirstOrDefaultDescendantAs<TItem>() where TItem : class, IStandardTemplate;

        /// <summary>
        /// Gets the first or default ancestor as.
        /// </summary>
        /// <typeparam name="TItem">The type of the t item.</typeparam>
        /// <returns>TItem instance or default value.</returns>
        [CanBeNull]
        TItem GetFirstOrDefaultAncestorAs<TItem>() where TItem : class, IStandardTemplate;

        // ReSharper restore ReturnTypeCanBeEnumerable.Global
    }
}
