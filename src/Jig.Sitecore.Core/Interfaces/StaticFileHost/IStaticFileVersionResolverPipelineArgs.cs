﻿namespace Jig.Sitecore.Pipelines
{
    using Jig.Sitecore.Web.UI;

    using global::Sitecore;

    /// <summary>
    /// The Static File Version Resolver Pipeline Args interface.
    /// </summary>
    public interface IStaticFileVersionResolverPipelineArgs : IStaticFileHostNameResolverPipelineArgs, IStaticFileVersion
    {
        #region Public Properties

        /// <summary>
        /// Gets the static file source.
        /// </summary>
        [NotNull] 
        string StaticFileSource { get; }

        #endregion
    }
}
