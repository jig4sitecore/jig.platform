namespace Jig.Sitecore.Web.UI
{
    using global::Sitecore;

    /// <summary>
    /// Interface IStaticFileVersion
    /// </summary>
    public interface IStaticFileVersion
    {
        /// <summary>
        /// Gets or sets the static file version.
        /// </summary>
        [CanBeNull]
        string StaticFileVersion { get; set; }
    }
}