namespace Jig.Sitecore.Pipelines
{
    using global::Sitecore;

    /// <summary>
    /// Interface IStaticFileUriResolverPipelineArgs
    /// </summary>
    public interface IStaticFileUriResolverPipelineArgs : IStaticFileVersionResolverPipelineArgs
    {
        /// <summary>
        /// Gets or sets the URL scheme.
        /// </summary>
        /// <value>The URL scheme.</value>
        [CanBeNull]
        string UrlScheme { get; set; }

        /// <summary>
        /// Gets or sets the URI.
        /// </summary>
        /// <value>The URI.</value>
        [CanBeNull] 
        string Uri { get; set; }
    }
}