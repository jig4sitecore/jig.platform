﻿namespace Jig.Sitecore.Pipelines
{
    using Jig.Sitecore.Web.UI;

    using global::Sitecore;

    using global::Sitecore.Data;

    /// <summary>
    /// The Host Name Resolver Pipeline Args interface.
    /// </summary>
    public interface IStaticFileHostNameResolverPipelineArgs : IStaticFileHostName, IHttpContext, ISiteInfo
    {
        /// <summary>
        /// Gets or sets the database.
        /// </summary>
        /// <value>The database.</value>
        [CanBeNull]
        [UsedImplicitly]
        Database Database { get; set; }
    }
}
