﻿namespace Jig.Sitecore.Pipelines
{
    using global::Sitecore;

    /// <summary>
    /// The Static File Uri Builder interface.
    /// </summary>
    public interface IStaticFileUriResolver
    {
        /// <summary>
        /// Resolves the static file uri.
        /// </summary>
        /// <param name="args">The <see cref="StaticFileUriResolverPipelineArgs"/> instance containing the event data.</param>
        void Process([NotNull] StaticFileUriResolverPipelineArgs args);
    }
}
