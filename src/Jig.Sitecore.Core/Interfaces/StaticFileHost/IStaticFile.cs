﻿namespace Jig.Sitecore.Web.UI
{
    using System.ComponentModel;
    using System.Xml.Linq;

    using global::Sitecore;

    using global::Sitecore.Sites;

    /// <summary>
    /// The StaticFile interface.
    /// </summary>
    public interface IStaticFile : IRenderView, IStaticFileHostName, ISiteInfo, IStaticFileVersion
    {
        #region Public Properties

        /// <summary>
        ///     Gets the inner HTM.
        /// </summary>
        /// <value>
        ///     The inner HTM.
        /// </value>
        [NotNull]
        [Bindable(false)]
        [Browsable(false)]
        [Localizable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        XElement InnerHtml { get; }

        /// <summary>
        /// Gets or sets the site information.
        /// </summary>
        /// <value>The site information.</value>
        [CanBeNull]
        new global::Sitecore.Sites.SiteContext SiteInfo { get; set; }

        /// <summary>
        /// Gets or sets the rendering behavior.
        /// </summary>
        /// <value>
        /// The rendering behavior.
        /// </value>
        DisplayMode DisplayMode { get; set; }

        #endregion
    }
}
