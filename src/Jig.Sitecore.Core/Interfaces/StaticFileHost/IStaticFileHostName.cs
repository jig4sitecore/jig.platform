﻿namespace Jig.Sitecore.Web.UI
{
    using global::Sitecore;

    /// <summary>
    /// The StaticFileHostname interface.
    /// </summary>
    public interface IStaticFileHostName
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the static file hostname.
        /// </summary>
        /// <value>
        /// The static file hostname.
        /// </value>
        [CanBeNull]
        string StaticFileHostname { get; set; }

        #endregion
    }
}
