﻿namespace Jig.Sitecore.Pipelines
{
    using global::Sitecore;

    /// <summary>
    /// The HostName Resolver interface.
    /// </summary>
    public interface IHostNameResolver
    {
        /// <summary>
        /// Resolves the host name mapping.
        /// </summary>
        /// <param name="args">The IStaticFileHostnameResolverPipelineArgs instance containing the event data.</param>
        void Process([NotNull] StaticFileHostNameResolverPipelineArgs args);
    }
}
