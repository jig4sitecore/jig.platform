﻿namespace Jig.Sitecore.Pipelines
{
    using global::Sitecore;

    /// <summary>
    /// The StaticFileVersionProvider interface.
    /// </summary>
    public interface IStaticFileVersionResolver
    {
        /// <summary>
        /// Resolves the static file version.
        /// </summary>
        /// <param name="args">The <see cref="StaticFileVersionResolverPipelineArgs"/> instance containing the event data.</param>
        void Process([NotNull] StaticFileVersionResolverPipelineArgs args);
    }
}