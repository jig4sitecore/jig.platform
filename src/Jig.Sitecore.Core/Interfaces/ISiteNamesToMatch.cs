﻿namespace Jig.Sitecore.Context
{
    using global::Sitecore;

    /// <summary>
    /// The SiteInfo NamesTo Match interface.
    /// </summary>
    public interface ISiteNamesToMatch
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets a comma-delimited list of Site names that should to match.
        /// </summary>
        /// <value>
        /// The match site information names.
        /// </value>
        /// <remarks>
        /// Comma delimited list
        /// </remarks>
        [CanBeNull]
        string MatchSiteInfoNames { get; set; }

        #endregion
    }
}