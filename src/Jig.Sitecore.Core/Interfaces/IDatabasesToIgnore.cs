﻿namespace Jig.Sitecore.Context
{
    using global::Sitecore;

    /// <summary>
    /// The DatabasesToIgnore interface.
    /// </summary>
    public interface IDatabasesToIgnore
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets a comma-delimited list of Database names that should cause the Processor to ignore the request.
        /// </summary>
        [CanBeNull]
        string DatabasesToIgnore { get; set; }

        #endregion
    }
}