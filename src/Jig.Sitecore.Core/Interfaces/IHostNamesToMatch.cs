﻿namespace Jig.Sitecore.Context
{
    using global::Sitecore;

    /// <summary>
    /// The HostName To Match interface.
    /// </summary>
    public interface IHostNamesToMatch
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the match host name.
        /// </summary>
        /// <remarks>Comma delimited list</remarks>
        [CanBeNull]
        string MatchHostNames { get; set; }

        #endregion
    }
}