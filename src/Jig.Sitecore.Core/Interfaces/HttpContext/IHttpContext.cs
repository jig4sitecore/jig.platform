﻿namespace Jig.Sitecore.Web.UI
{
    using System.Web;

    using global::Sitecore;

    /// <summary>
    /// The HttpContext interface.
    /// </summary>
    public interface IHttpContext
    {
        /// <summary>
        /// Gets the HTTP context.
        /// </summary>
        /// <value>
        /// The HTTP context.
        /// </value>
        [NotNull]
        HttpContextBase HttpContext { get; }
    }
}
