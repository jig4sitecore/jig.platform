﻿namespace Jig.Sitecore.Web.UI
{
    using global::Sitecore;

    /// <summary>
    /// Interface ISiteInfo
    /// </summary>
    public interface ISiteInfo
    {
        /// <summary>
        /// Gets the site information.
        /// </summary>
        /// <value>The site information.</value>
        [CanBeNull]
        global::Sitecore.Sites.SiteContext SiteInfo { get; }
    }
}