﻿namespace Jig.Sitecore.Pipelines.RenderLayout
{
    using global::Sitecore;

    using global::Sitecore.Pipelines.RenderLayout;

    /// <summary>
    /// The enderLayoutProcessor interface.
    /// </summary>
    public interface IRenderLayoutProcessor
    {
        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        // ReSharper disable UnusedParameter.Global
        void Process([NotNull] RenderLayoutArgs args);
        // ReSharper restore UnusedParameter.Global
        #endregion
    }
}