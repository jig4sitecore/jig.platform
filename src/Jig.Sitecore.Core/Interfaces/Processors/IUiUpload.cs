﻿namespace Jig.Sitecore.Rules.Processors
{
    using global::Sitecore;

    using global::Sitecore.Pipelines.Upload;

    /// <summary>
    /// The UiUpload interface.
    /// </summary>
    public interface IUiUpload
    {
        #region Public Methods and Operators

        /// <summary>
        /// Processes the specified upload arguments.
        /// </summary>
        /// <param name="args">
        /// The arguments.
        /// </param>
        void Process([NotNull] UploadArgs args);

        #endregion
    }
}