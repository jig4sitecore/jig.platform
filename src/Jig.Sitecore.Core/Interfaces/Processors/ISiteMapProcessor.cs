﻿namespace Jig.Sitecore.Pipelines
{
    using global::Sitecore;

    /// <summary>
    /// The SiteMapProcessor interface.
    /// </summary>
    public interface ISiteMapProcessor
    {
        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">The args.</param>
        void Process([NotNull] SiteMapPipelineArgs args);

        #endregion
    }
}