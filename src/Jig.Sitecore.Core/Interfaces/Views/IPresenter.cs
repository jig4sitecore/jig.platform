﻿namespace Jig.Sitecore.Views.WebForms.Presenters
{
    using System.Web;

    using Jig.Sitecore.Views;
    using global::Sitecore;

    /// <summary>
    /// The contract for Presenters.
    /// </summary>
    /// <typeparam name="TModel">The Model required by the View.</typeparam>
    public interface IPresenter<TModel> where TModel : class
    {
        /// <summary>
        /// Creates a TModel based upon the Datasource information supplied by the View.
        /// </summary>
        /// <param name="view">The view instance requiring a model.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        /// An instance of TModel or null.
        /// </returns>
        // ReSharper disable UnusedParameter.Global
        [CanBeNull]
        TModel GetModel([NotNull] IView<TModel> view, [NotNull] HttpContextBase context);
        // ReSharper restore UnusedParameter.Global
    }
}
