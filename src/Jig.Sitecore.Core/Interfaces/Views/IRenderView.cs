﻿namespace Jig.Sitecore.Web.UI
{
    using System.Web;
    using System.Web.UI;

    using global::Sitecore;

    /// <summary>
    /// The Render Control interface.
    /// </summary>
    public interface IRenderView
    {
        #region Public Methods and Operators

        /// <summary>
        /// Outputs server control content to a provided System.Web.UI.HtmlTextWriter object and stores
        /// tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
        /// <param name="context">The context.</param>
        // ReSharper disable UnusedParameter.Global
        void RenderView([NotNull] HtmlTextWriter writer, [NotNull] HttpContextBase context);
        // ReSharper restore UnusedParameter.Global
        #endregion
    }
}
