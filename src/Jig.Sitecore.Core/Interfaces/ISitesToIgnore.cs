﻿namespace Jig.Sitecore.Context
{
    using global::Sitecore;

    /// <summary>
    /// The SitesToIgnore interface.
    /// </summary>
    public interface ISitesToIgnore
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets a comma-delimited list of Site names that should cause the Processor to ignore the request.
        /// </summary>
        [CanBeNull]
        string SitesToIgnore { get; set; }

        #endregion
    }
}