﻿namespace Jig.Sitecore.Data
{
    using global::Sitecore;
    using global::Sitecore.Collections;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Fields;

    /// <summary>
    /// The CustomItem interface.
    /// </summary>
    public interface ICustomItem
    {
        /// <summary>
        /// Gets a value indicating whether the Item (version) is empty.
        /// </summary>
        bool Empty { get; }

        /// <summary>
        /// Gets a value indicating whether the Item has child Items.
        /// </summary>
        bool HasChildren { get; }

        /// <summary>
        /// Gets the Item's Key (lowercased Item.Name).
        /// </summary>
        [NotNull]
        string Key { get; }

        /// <summary>
        /// Gets the current Item Version's Language.
        /// </summary>
        [NotNull]
        global::Sitecore.Globalization.Language Language { get; }

        /// <summary>
        /// Gets a value indicating whether the Item has been modified.
        /// </summary>
        bool Modified { get; }

        /// <summary>
        /// Gets the Item's Name.
        /// </summary>
        [NotNull]
        string Name { get; }

        /// <summary>
        /// Gets the ID of the Item's Parent.
        /// </summary>
        // ReSharper disable InconsistentNaming
        [NotNull]
        global::Sitecore.Data.ID ParentID { get; }

        // ReSharper restore InconsistentNaming

        /// <summary>
        /// Gets the ID of the Item's Data Template.
        /// </summary>
        // ReSharper disable InconsistentNaming
        [NotNull]
        global::Sitecore.Data.ID TemplateID { get; }

        // ReSharper restore InconsistentNaming

        /// <summary>
        /// Gets the name of the Item's Data Template.
        /// </summary>
        [NotNull]
        string TemplateName { get; }

        /// <summary>
        /// Gets the Item's Version.
        /// </summary>
        [NotNull]
        global::Sitecore.Data.Version Version { get; }

        /// <summary>
        /// Gets the full path.
        /// </summary>
        /// <value>The full path.</value>
        [NotNull]
        string FullPath { get; }

        /// <summary>
        /// Gets the help text.
        /// </summary>
        /// <value>The help text.</value>
        [NotNull]
        string HelpText { get; }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        [NotNull]
        global::Sitecore.Collections.ChildList Children { get; }

        /// <summary>
        /// Gets the versions.
        /// </summary>
        /// <value>The versions.</value>
        [NotNull]
        global::Sitecore.Data.Items.ItemVersions Versions { get; }

        #region Public Indexers

        /// <summary>
        /// The this.
        /// </summary>
        /// <param name="fieldId">
        /// The field id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string this[ID fieldId] { get; set; }

        /// <summary>
        /// The this.
        /// </summary>
        /// <param name="fieldIndex">
        /// The field index.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string this[int fieldIndex] { get; }

        /// <summary>
        /// Gets or sets the <see cref="System.String"/> with the specified field name.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns>The field value</returns>
        string this[string fieldName] { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The fields.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Field"/>.
        /// </returns>
        Field Fields(ID id);

        /// <summary>
        /// The fields.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="Field"/>.
        /// </returns>
        Field Fields(string name);

        /// <summary>
        /// The get children.
        /// </summary>
        /// <returns>The <see cref="ChildList" />.</returns>
        ChildList GetChildren();

        #endregion
    }
}