namespace Jig.Sitecore.Context
{
    using global::Sitecore;

    /// <summary>
    /// The UrlPathsToIgnore interface.
    /// </summary>
    public interface IUrlRequestPathsToIgnore
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets a comma-delimited list of file paths that should cause the Processor to ignore 
        /// </summary>
        /// <remarks>The code use starts with implementation</remarks>
        [CanBeNull]
        string UrlRequestPathsToIgnore { get; set; }

        #endregion
    }
}