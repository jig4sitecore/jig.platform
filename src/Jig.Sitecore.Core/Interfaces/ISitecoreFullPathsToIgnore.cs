﻿namespace Jig.Sitecore.Context
{
    using global::Sitecore;

    /// <summary>
    /// The SitecoreFullPathsToIgnore interface.
    /// </summary>
    public interface ISitecoreFullPathsToIgnore
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the sitecore full paths to ignore.
        /// </summary>
        /// <remarks>The code use starts with implementation</remarks>
        [CanBeNull]
        string SitecoreFullPathsToIgnore { get; set; }

        #endregion
    }
}