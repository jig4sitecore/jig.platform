﻿namespace Jig.Sitecore.Pipelines
{
    using global::Sitecore;

    /// <summary>
    /// The Robots.txt Processor interface.
    /// </summary>
    public interface IRobotTextFileProcessor
    {
        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">The args.</param>
        void Process([NotNull] RobotTextFilePipelineArgs args);

        #endregion
    }
}