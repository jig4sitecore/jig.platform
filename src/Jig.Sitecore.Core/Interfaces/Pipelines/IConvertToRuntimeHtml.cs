﻿namespace Jig.Sitecore.Pipelines.ConvertToRuntimeHtml
{
    using global::Sitecore;

    using global::Sitecore.Pipelines.ConvertToRuntimeHtml;

    /// <summary>
    /// The ConvertToRuntimeHtml interface.
    /// </summary>
    public interface IConvertToRuntimeHtml
    {
        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">The args.</param>
        void Process([NotNull] ConvertToRuntimeHtmlArgs args);

        #endregion
    }
}