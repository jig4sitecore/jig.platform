﻿namespace Jig.Sitecore.Pipelines.GetLookupSourceItems
{
    using global::Sitecore;

    using global::Sitecore.Pipelines.GetLookupSourceItems;

    /// <summary>
    /// The GetLookupSourceItems interface.
    /// </summary>
    public interface IGetLookupSourceItems
    {
        #region Public Methods and Operators

        /// <summary>
        /// Processes the specified arguments.
        /// </summary>
        /// <param name="args">
        /// The arguments.
        /// </param>
        void Process([NotNull] GetLookupSourceItemsArgs args);

        #endregion
    }
}