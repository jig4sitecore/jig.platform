namespace Jig.Sitecore.Pipelines.HttpRequest
{
    using global::Sitecore;

    using global::Sitecore.Pipelines.HttpRequest;

    /// <summary>
    /// The HttpRequestProcessor interface.
    /// </summary>
    public interface IHttpRequest
    {
        #region Public Methods and Operators

        /// <summary>
        /// Processes the specified arguments.
        /// </summary>
        /// <param name="args">
        /// The arguments.
        /// </param>
        void Process([NotNull] HttpRequestArgs args);

        #endregion
    }
}