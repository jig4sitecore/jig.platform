﻿namespace Jig.Sitecore.Pipelines.ConvertToDesignTimeHtml
{
    using global::Sitecore;

    using global::Sitecore.Pipelines.ConvertToDesignTimeHtml;

    /// <summary>
    /// The ConvertToDesignTimeHtml interface.
    /// </summary>
    public interface IConvertToDesignTimeHtml
    {
        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        void Process([NotNull] ConvertToDesignTimeHtmlArgs args);

        #endregion
    }
}