﻿namespace Jig.Sitecore.Pipelines.GetRenderingDatasource
{
    using global::Sitecore;

    using global::Sitecore.Pipelines.GetRenderingDatasource;

    /// <summary>
    /// The GetRenderingDatasource interface.
    /// </summary>
    public interface IGetRenderingDatasource
    {
        #region Public Methods and Operators

        /// <summary>
        /// Processes the specified arguments.
        /// </summary>
        /// <param name="args">
        /// The arguments.
        /// </param>
        void Process([NotNull] GetRenderingDatasourceArgs args);

        #endregion
    }
}