﻿namespace Jig.Sitecore.Pipelines.FixXHtml
{
    using global::Sitecore;

    using global::Sitecore.Pipelines.FixXHtml;

    /// <summary>
    /// The FixXHtml interface.
    /// </summary>
    public interface IFixXHtml
    {
        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        void Process([NotNull] FixXHtmlArgs args);

        #endregion
    }
}