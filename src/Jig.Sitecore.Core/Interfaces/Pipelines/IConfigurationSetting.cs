﻿namespace Jig.Sitecore.Pipelines.Configuration
{
    using global::Sitecore;

    /// <summary>
    /// The ConfigurationSetting interface.
    /// </summary>
    public interface IConfigurationSetting
    {
        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        void Process([NotNull] ConfigurationSettingPipelineArgs args);

        #endregion
    }
}