﻿namespace Jig.Sitecore.Pipelines
{
    using global::Sitecore;

    /// <summary>
    /// The HtmlAgilityParserProcessor interface.
    /// </summary>
    public interface IHtmlAgilityParserProcessor
    {
        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">The args.</param>
        void Process([NotNull] HtmlAgilityParserPipelineArgs args);

        #endregion
    }
}