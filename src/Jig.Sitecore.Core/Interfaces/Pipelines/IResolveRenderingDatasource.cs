﻿namespace Jig.Sitecore.Pipelines
{
    using global::Sitecore;

    using global::Sitecore.Pipelines.ResolveRenderingDatasource;

    /// <summary>
    /// The ResolveRenderingDatasource interface.
    /// </summary>
    public interface IResolveRenderingDatasource
    {
        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        void Process([NotNull] ResolveRenderingDatasourceArgs args);

        #endregion
    }
}