﻿namespace Jig.Sitecore.Data
{
    using global::Sitecore;
    using global::Sitecore.Data;

    using global::Sitecore.Data.Fields;

    using global::Sitecore.Globalization;

    /// <summary>
    /// The label helper.
    /// </summary>
    /// <remarks>This class not intent use anywhere else except code generation support</remarks>
    public static partial class LabelHelper
    {
        /// <summary>
        /// The default language
        /// </summary>
        private static readonly Language DefaultLanguage = Language.Parse("en");

        /// <summary>
        /// The dictionary entry phrase field identifier
        /// </summary>
        private static readonly ID DictionaryEntryPhraseFieldId = new ID("53709c6b-fa39-4264-84d5-4251d47c58ea");

        #region Public Methods and Operators

        /// <summary>
        /// The get language.
        /// </summary>
        /// <returns>
        /// The <see cref="Language" />.
        /// </returns>
        [NotNull]
        public static Language GetLanguage()
        {
            var item = global::Sitecore.Context.Item;
            if (item != null)
            {
                return item.Language;
            }

            var lang = global::Sitecore.Context.Language;
            if (lang != null)
            {
                return lang;
            }

            return DefaultLanguage;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get field.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="fieldId">The field id.</param>
        /// <param name="language">The language.</param>
        /// <returns>
        /// The <see cref="Field" />.
        /// </returns>
        [CanBeNull]
        public static Field GetField(
            [NotNull] ID id,
            [NotNull] ID fieldId,
            [NotNull] Language language)
        {
            var db = global::Sitecore.Context.Database;
            if (db == null)
            {
                return null;
            }

            var item = db.GetItem(id, language);
            if (item == null)
            {
                return null;
            }

            var field = item.Fields[fieldId];
            return field;
        }

        /// <summary>
        /// The get field.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="language">The language.</param>
        /// <returns>
        /// The Sitecore item.
        /// </returns>
        [CanBeNull]
        public static global::Sitecore.Data.Items.Item GetItemByLanguage(
            [NotNull] ID id,
            [NotNull] Language language)
        {
            var db = global::Sitecore.Context.Database;
            if (db == null)
            {
                return null;
            }

            var item = db.GetItem(id, language);

            return item;
        }

        /// <summary>
        /// Gets the field value.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="fieldId">The field Id.</param>
        /// <returns>The field Value</returns>
        [NotNull]
        public static string GetFieldValue(
            [NotNull] ID id,
            [NotNull] ID fieldId)
        {
            var language = LabelHelper.GetLanguage();
            var field = LabelHelper.GetField(id, fieldId, language);
            if (field != null)
            {
                return field.Value;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the dictionary entry value.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// The Dictionary entry
        /// </returns>
        [NotNull]
        public static string GetDictionaryEntryValue(
            [NotNull] ID id)
        {
            var language = LabelHelper.GetLanguage();
            var field = LabelHelper.GetField(id, DictionaryEntryPhraseFieldId, language);
            if (field != null)
            {
                return field.Value;
            }

            return string.Empty;
        }

        #endregion
    }
}