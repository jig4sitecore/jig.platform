﻿namespace Jig.Sitecore
{
    using System;
    using global::Sitecore;
    using global::Sitecore.Text;

    /// <summary>
    /// Class UriHelpers.
    /// </summary>
    public static class UriHelpers
    {
        /// <summary>
        /// Matches the URI host.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="hostNameWithWildcard">The host name with wildcard.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool MatchUriHost([NotNull] this Uri uri, [NotNull] string hostNameWithWildcard)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (uri == null || string.IsNullOrWhiteSpace(hostNameWithWildcard))
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                return false;
            }

            var hostNameWithWildcardParts = WildCardParser.GetParts(hostNameWithWildcard);
            return WildCardParser.Matches(uri.Host, hostNameWithWildcardParts);
        }

        /// <summary>
        /// Matches the URI host.
        /// </summary>
        /// <param name="hostname">The hostname.</param>
        /// <param name="hostNamesWithWildcard">The host name with wildcard.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool MatchUriHost([NotNull] string hostname, [NotNull] string hostNamesWithWildcard)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (string.IsNullOrWhiteSpace(hostname) || string.IsNullOrWhiteSpace(hostNamesWithWildcard))
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                return false;
            }

            var hostnames = hostNamesWithWildcard.Split(new[] { '|', ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var hostNameWithWildcard in hostnames) // ReSharper restore LoopCanBeConvertedToQuery
            {
                var hostNameWithWildcardParts = WildCardParser.GetParts(hostNameWithWildcard);
                var match = WildCardParser.Matches(hostname, hostNameWithWildcardParts);
                if (match)
                {
                    return true;
                }
            }

            return false;
        }
    }
}