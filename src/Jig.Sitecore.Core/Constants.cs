﻿namespace Jig.Sitecore
{
    using global::Sitecore.Data;

    /// <summary>
    /// The constants.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// The HttpContext constants.
        /// </summary>
        public static class HttpContext
        {
            /// <summary>
            /// The HttpContext Item constants.
            /// </summary>
            public static class Items
            {
            }
        }

        /// <summary>
        /// The site info.
        /// </summary>
        public static class SiteInfo
        {
            /// <summary>
            /// Class PropertyNames.
            /// </summary>
            public static class PropertyNames
            {
                #region Constants

                /// <summary>
                /// The static files host name folder path
                /// </summary>
                public const string StaticFilesHostNameFolderPath = "staticFilesHostNameFolderPath";

                /// <summary>
                /// The not found page item path.
                /// </summary>
                public const string NotFoundPageItemPath = "notFoundPageItemPath";

                /// <summary>
                /// The language embedding
                /// </summary>
                public const string LanguageEmbedding = "languageEmbedding";

                /// <summary>
                /// The link provider
                /// </summary>
                public const string LinkProvider = "linkProvider";

                #endregion
            }

            /// <summary>
            /// Class PropertyStandardValues.
            /// </summary>
            /// <remarks>These are default values for site info node. However, it can be overridden in config file per site</remarks>
            public static class PropertyStandardValues
            {              
                /// <summary>
                /// The not found page item path.
                /// </summary>
                public const string NotFoundPageItemPath = "/404";

                /// <summary>
                /// The alias folder
                /// </summary>
                public const string AliasFolder = "/aliases";

                /// <summary>
                /// The static files host name folder path
                /// </summary>
                public const string StaticFilesHostNameFolder = "/static-files-hostnames";
            }
        }

        /// <summary>
        /// Sitecore Full Paths.
        /// </summary>
        public static class Paths
        {
            /// <summary>
            /// The Site Paths
            /// </summary>
            public static class Sites
            {
                /// <summary>
                /// The content
                /// </summary>
                public const string Content = "/sitecore/content/sites";

                /// <summary>
                /// The media
                /// </summary>
                public const string Media = "/sitecore/media library/sites";

                /// <summary>
                /// The layouts
                /// </summary>
                public const string Layouts = "/sitecore/layout/layouts/sites";

                /// <summary>
                /// The renderings
                /// </summary>
                public const string Renderings = "/sitecore/layout/renderings/sites";

                /// <summary>
                /// The sublayouts
                /// </summary>
                public const string Sublayouts = "/sitecore/layout/sublayouts/sites";

                /// <summary>
                /// The placeholder settings
                /// </summary>
                public const string PlaceholderSettings = "/sitecore/layout/placeholder settings/sites";

                /// <summary>
                /// The system
                /// </summary>
                public const string System = "/sitecore/system/sites";

                /// <summary>
                /// The templates
                /// </summary>
                public const string Templates = "/sitecore/templates/sites";

                /// <summary>
                /// The branches
                /// </summary>
                public const string Branches = "/sitecore/templates/branches/sites";

                /// <summary>
                /// The controllers
                /// </summary>
                public const string Controllers = "/sitecore/layout/controllers";

                /// <summary>
                /// The models
                /// </summary>
                public const string Models = "/sitecore/layout/models";
            }
        }

        /// <summary>
        /// Class Id.
        /// </summary>
        public static class Id
        {
            /// <summary>
            /// Site ID
            /// </summary>
            public class Sites
            {
                /// <summary>
                /// The custom experience buttons
                /// </summary>
                public static readonly ID CustomExperienceButtons = new ID("3ED27FFC-1CCC-492D-97F2-C0E3BEF28B61");

                /// <summary>
                /// The content
                /// </summary>
                public static readonly ID Content = new ID("D184982C-2C30-4AA0-AA3A-B9091B2B1269");

                /// <summary>
                /// The media
                /// </summary>
                public static readonly ID Media = new ID("729D1040-8257-4859-86E9-A3B43B91DBE2");

                /// <summary>
                /// The layouts
                /// </summary>
                public static readonly ID Layouts = new ID("E12F002A-C8CD-4FDB-917F-E98F0FDDD350");

                /// <summary>
                /// The renderings
                /// </summary>
                public static readonly ID Renderings = new ID("CFCACFC6-39CA-4BE1-B961-94037203B2FC");

                /// <summary>
                /// The sublayouts
                /// </summary>
                public static readonly ID Sublayouts = new ID("498FEBDA-45BD-4893-A77C-362FA62A3D5C");

                /// <summary>
                /// The placeholder settings
                /// </summary>
                public static readonly ID PlaceholderSettings = new ID("AB4495F1-629C-44E4-91BC-3EA14186B6BB");

                /// <summary>
                /// The system
                /// </summary>
                public static readonly ID System = new ID("D2057480-CBAC-43B2-B503-3443A2EF23EA");

                /// <summary>
                /// The templates
                /// </summary>
                public static readonly ID Templates = new ID("DA956380-38C6-48FB-8E79-2D9BE12476D3");

                /// <summary>
                /// The branches
                /// </summary>
                public static readonly ID Branches = new ID("0D542763-5FC4-400F-84DC-3D66AC8A9F67");

                /// <summary>
                /// The controllers
                /// </summary>
                public static readonly ID Controllers = new ID("E2E88698-4BEB-4B00-8708-612526B04D05");

                /// <summary>
                /// The models
                /// </summary>
                public static readonly ID Models = new ID("F1D9ACF9-D64B-45A9-A435-87201C261B99");
            }
        }

        /// <summary>
        /// Field Name and ID
        /// </summary>
        public static partial class FieldNames
        {
            /// <summary> The Field Info:
            ///     <para>Field Type: Multilist</para>
            ///     <para>Field Name: Languages</para>
            ///     <para>Field Path: /sitecore/templates/Sites/Shared/System/Interfaces/Supported Languages/Content/Languages</para>
            /// </summary>
            public static readonly ID Languages = new ID("ab7adf16-d450-49e7-9f5e-6279c24ea7c0");
        }

        /// <summary>
        /// Placeholder Keys
        /// </summary>
        public static class PlaceholderKeys
        {
            /// <summary>
            /// The placeholder head
            /// </summary>
            public const string Head = "head";

            /// <summary>
            /// The placeholder body top
            /// </summary>
            public const string BodyTop = "body-top";

            /// <summary>
            /// The placeholder body bottom
            /// </summary>
            public const string BodyBottom = "body-bottom";
        }
    }
}