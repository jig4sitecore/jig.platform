﻿namespace Jig.Sitecore
{
    using System.Web;

    /// <summary>
    /// It implements IHtmlString, so when calling HttpUtility.HtmlEncode(MvcHtmlString), the string won't be encoded.
    /// </summary>
    public class MvcHtmlString : IHtmlString
    {
        #region Static Fields

        /// <summary>
        /// The empty.
        /// </summary>
        public static readonly MvcHtmlString Empty = MvcHtmlString.Create(string.Empty);

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MvcHtmlString"/> class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public MvcHtmlString(string value)
        {
            this.Html = string.IsNullOrWhiteSpace(value) ? string.Empty : value;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the html.
        /// </summary>
        public string Html { get; protected set; }

        /// <summary>
        /// Gets a value indicating whether this instance has value.
        /// </summary>
        /// <value><c>true</c> if this instance has value; otherwise, <c>false</c>.</value>
        public bool HasValue
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.Html);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The <see cref="MvcHtmlString" />.</returns>
        public static MvcHtmlString Create(string value)
        {
            return new MvcHtmlString(value);
        }

        /// <summary>
        /// The op_ implicit.
        /// </summary>
        /// <param name="mvcHtmlString">The html string.</param>
        /// <returns>The result of the conversion.</returns>
        public static implicit operator string(MvcHtmlString mvcHtmlString)
        {
            if (mvcHtmlString == null)
            {
                return string.Empty;
            }

            return mvcHtmlString.Html;
        }

        /// <summary>
        /// The op_ implicit.
        /// </summary>
        /// <param name="htmlString">The html string.</param>
        /// <returns>The result of the conversion.</returns>
        public static implicit operator MvcHtmlString(string htmlString)
        {
            return new MvcHtmlString(htmlString);
        }

        /// <summary>
        /// The to html string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string ToHtmlString()
        {
            return this.Html;
        }

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.Html;
        }

        #endregion
    }
}