﻿namespace Jig.Sitecore.Pipelines.Configuration
{
    using System;

    using global::Sitecore;

    /// <summary>
    /// The configuration setting pipeline args.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2240:ImplementISerializableCorrectly", Justification = "The sitecore implementation is enought")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2229:ImplementSerializationConstructors", Justification = "No unmanaged resources were allocated")]
    [Serializable]
    public sealed class ConfigurationSettingPipelineArgs : global::Sitecore.Pipelines.PipelineArgs
    {
        #region Fields

        /// <summary>
        /// The setting value.
        /// </summary>
        private string settingValue;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationSettingPipelineArgs"/> class.
        /// </summary>
        /// <param name="settingName">
        /// The setting Name.
        /// </param>
        public ConfigurationSettingPipelineArgs([NotNull] string settingName)
        {
            this.SettingName = settingName;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether this instance is setting value resolved.
        /// </summary>
        /// <value><c>true</c> if this instance is setting value resolved; otherwise, <c>false</c>.</value>
        public bool IsSettingValueResolved { get; private set; }

        /// <summary>
        /// Gets the setting name.
        /// </summary>
        [NotNull]
        public string SettingName { get; private set; }

        /// <summary>
        /// Gets or sets the setting value.
        /// </summary>
        [CanBeNull]
        [UsedImplicitly]
        public string SettingValue
        {
            get
            {
                return this.settingValue;
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.settingValue = value;
                    this.IsSettingValueResolved = true;
                    this.AbortPipeline();
                }
            }
        }

        #endregion
    }
}