﻿namespace Jig.Sitecore.Pipelines
{
    using System.Web;

    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Diagnostics;

    using global::Sitecore.Pipelines;

    using global::Sitecore.Sites;

    /// <summary>
    /// Static File Hostname Resolver PipelineArgs.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable", Justification = "The serialization never used any more. Legacy")]
    public class StaticFileVersionResolverPipelineArgs : PipelineArgs, IStaticFileVersionResolverPipelineArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StaticFileVersionResolverPipelineArgs"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="staticFileSource">The static file path.</param>
        /// <param name="staticFileHostname">The static file hostname.</param>
        public StaticFileVersionResolverPipelineArgs([NotNull] HttpContextBase context, [NotNull] string staticFileSource, [CanBeNull] string staticFileHostname)
        {
            Assert.IsNotNull(context, "context");
            this.HttpContext = context;

            Assert.IsNotNullOrEmpty(staticFileSource, "staticFileSource");
            this.StaticFileSource = staticFileSource;

            this.StaticFileHostname = staticFileHostname ?? string.Empty;
        }

        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>The context.</value>
        [NotNull]
        [UsedImplicitly]
        public HttpContextBase HttpContext { get; private set; }

        /// <summary>
        /// Gets or sets the site info.
        /// </summary>
        /// <value>The site info.</value>
        [CanBeNull]
        [UsedImplicitly]
        public SiteContext SiteInfo { get; set; }

        /// <summary>
        /// Gets or sets the static file hostname.
        /// </summary>
        /// <value>The static file hostname.</value>
        [CanBeNull]
        [UsedImplicitly]
        public string StaticFileHostname { get; set; }

        /// <summary>
        /// Gets the static file source.
        /// </summary>
        /// <value>The static file source.</value>
        [NotNull]
        public string StaticFileSource { get; private set; }

        /// <summary>
        /// Gets or sets the static file version.
        /// </summary>
        /// <value>The static file version.</value>
        [CanBeNull]
        [UsedImplicitly]
        public string StaticFileVersion { get; set; }

        /// <summary>
        /// Gets or sets the database.
        /// </summary>
        /// <value>The database.</value>
        [CanBeNull]
        [UsedImplicitly]
        public Database Database { get; set; }
    }
}