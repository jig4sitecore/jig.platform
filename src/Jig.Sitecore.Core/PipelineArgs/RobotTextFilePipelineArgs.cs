﻿namespace Jig.Sitecore.Pipelines
{
    using System;
    using System.IO;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using Newtonsoft.Json;
    using global::Sitecore;
    using global::Sitecore.Pipelines;

    using global::Sitecore.Sites;

    /// <summary>
    /// The site map pipeline args.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Justification = "No Native resources"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable", Justification = "The serialization never used any more"), JsonObject(MemberSerialization.OptIn)]
    public partial class RobotTextFilePipelineArgs : PipelineArgs, IDisposable
    {
        /// <summary>
        /// The site context
        /// </summary>
        private SiteContext siteContext;

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RobotTextFilePipelineArgs" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public RobotTextFilePipelineArgs([NotNull] HttpContextBase context)
        {
            this.Context = context;
            this.Output = new HtmlTextWriter(new StringWriter(new StringBuilder(8096)));

            /* Example:
                    User-agent: * 
                    Disallow: /folder/
             
                    OR
             
                    User-agent: * 
                    Disallow: /file.html
                    Complex wildcards can also be used.
             */
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>The context.</value>
        [NotNull]
        public HttpContextBase Context { get; private set; }

        /// <summary>
        /// Gets the output.
        /// </summary>
        [NotNull]
        public HtmlTextWriter Output { get; private set; }

        /// <summary>
        /// Gets or sets the site.
        /// </summary>
        /// <value>The site.</value>
        [CanBeNull]
        [UsedImplicitly]
        public SiteContext SiteContext
        {
            get
            {
                return this.siteContext;
            }

            set
            {
                if (value != null)
                {
                    this.siteContext = value;
                    global::Sitecore.Context.SetActiveSite(value.Name);
                }
            }
        }

        /// <summary>
        /// Gets or sets the database.
        /// </summary>
        /// <value>The database.</value>
        [CanBeNull]
        [UsedImplicitly]
        public global::Sitecore.Data.Database Database { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The dispose.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Justification = "No Native resources")]
        public new void Dispose()
        {
            base.Dispose();
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (this.Output != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                this.Output.Dispose();
            }
        }

        #endregion
    }
}