﻿namespace Jig.Sitecore.Pipelines
{
    using System.Web;
    using global::Sitecore;

    using global::Sitecore.Data;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Pipelines;

    using global::Sitecore.Sites;

    /// <summary>
    /// Static File Hostname Resolver PipelineArgs.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable", Justification = "The serialization never used any more")]
    public class StaticFileHostNameResolverPipelineArgs : PipelineArgs, IStaticFileHostNameResolverPipelineArgs
    {
        /// <summary>
        /// The static file hostname
        /// </summary>
        private string staticFileHostname;

        /// <summary>
        /// Initializes a new instance of the <see cref="StaticFileHostNameResolverPipelineArgs" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public StaticFileHostNameResolverPipelineArgs([NotNull] HttpContextBase context)
        {
            Assert.IsNotNull(context, "context");
            this.HttpContext = context;
        }

        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        [NotNull]
        [UsedImplicitly]
        public HttpContextBase HttpContext { get; private set; }

        /// <summary>
        /// Gets or sets the site info.
        /// </summary>
        /// <value>
        /// The site info.
        /// </value>
        [UsedImplicitly]
        public SiteContext SiteInfo { get; set; }

        /// <summary>
        /// Gets or sets the database.
        /// </summary>
        /// <value>The database.</value>
        [CanBeNull]
        [UsedImplicitly]
        public Database Database { get; set; }

        /// <summary>
        /// Gets or sets the static file hostname.
        /// </summary>
        /// <value>The static file hostname.</value>
        [CanBeNull]
        [UsedImplicitly]
        public string StaticFileHostname 
        {
            get
            {
                return this.staticFileHostname;
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    // ReSharper disable PossibleNullReferenceException
                    this.staticFileHostname = value.Trim(' ', '/').ToLowerInvariant();
                    // ReSharper restore PossibleNullReferenceException
                    this.AbortPipeline();
                }
            }
        }
    }
}