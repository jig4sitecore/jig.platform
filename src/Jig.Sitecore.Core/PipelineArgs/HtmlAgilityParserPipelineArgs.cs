﻿namespace Jig.Sitecore.Pipelines
{
    using System.Web;

    using global::HtmlAgilityPack;

    using Newtonsoft.Json;

    using global::Sitecore;

    using global::Sitecore.Pipelines;

    using global::Sitecore.Sites;

    /// <summary>
    /// The html agility parser pipeline args.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable", Justification = "The serialization never used any more"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2229:ImplementSerializationConstructors", Justification = "The serialization never used any more")]
    [JsonObject(MemberSerialization.OptIn)]
    public partial class HtmlAgilityParserPipelineArgs : PipelineArgs
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlAgilityParserPipelineArgs" /> class.
        /// </summary>
        /// <param name="html">The HTML.</param>
        /// <exception cref="HttpException">The html cannot be null!</exception>
        /// <exception cref="System.Web.HttpException">The link property field cannot be null!</exception>
        public HtmlAgilityParserPipelineArgs([NotNull] string html)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (string.IsNullOrWhiteSpace(html))
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                throw new HttpException("The html cannot be null!");
            }

            this.OriginalHtml = html.Trim();

            this.HtmlDocument = new HtmlDocument
                                    {
                                        /* http://stackoverflow.com/questions/5556089/html-agility-pack-removes-break-tag-close */
                                        OptionWriteEmptyNodes = true,
                                        OptionCheckSyntax = true,
                                        OptionFixNestedTags = true
                                    };

            // Add container div to parse html as XHTML
            // ReSharper disable PossibleNullReferenceException
            string htmlFragment = this.OriginalHtml.Length > 0 && this.OriginalHtml[0].Equals('<') ? this.OriginalHtml : string.Concat("<div>", this.OriginalHtml, "</div>");
            // ReSharper restore PossibleNullReferenceException
            this.HtmlDocument.LoadHtml(htmlFragment);

            this.HtmlNodeCollection = this.HtmlDocument.DocumentNode.SelectNodes("//*");
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        [CanBeNull]
        public HttpContextBase Context { get; set; }

        /// <summary>
        /// Gets the HTML.
        /// </summary>
        /// <value>
        /// The HTML.
        /// </value>
        [NotNull]
        public string Html
        {
            get
            {
                return this.HtmlDocument.DocumentNode.InnerHtml;
            }
        }

        /// <summary>
        /// Gets the html document.
        /// </summary>
        [NotNull]
        public HtmlDocument HtmlDocument { get; private set; }

        /// <summary>
        /// Gets the html node collection.
        /// </summary>
        [NotNull]
        public HtmlNodeCollection HtmlNodeCollection { get; private set; }

        /// <summary>
        /// Gets the original html.
        /// </summary>
        [NotNull]
        public string OriginalHtml { [UsedImplicitly] get; private set; }

        /// <summary>
        /// Gets or sets the site info.
        /// </summary>
        /// <value>
        /// The site info.
        /// </value>
        [CanBeNull]
        public SiteContext SiteInfo { [UsedImplicitly] get; set; }

        #endregion
    }
}