﻿namespace Jig.Sitecore.Pipelines
{
    using System.Web;

    using global::Sitecore;

    /// <summary>
    /// Static File Uri Builder PipelineArgs.
    /// </summary>
    public class StaticFileUriResolverPipelineArgs : StaticFileVersionResolverPipelineArgs, IStaticFileUriResolverPipelineArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StaticFileUriResolverPipelineArgs" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="staticFileSource">The static file path.</param>
        /// <param name="staticFileHostname">The static file hostname.</param>
        // ReSharper disable NotNullMemberIsNotInitialized
        public StaticFileUriResolverPipelineArgs([NotNull] HttpContextBase context, [NotNull] string staticFileSource, [CanBeNull] string staticFileHostname)
        // ReSharper restore NotNullMemberIsNotInitialized
            : base(context, staticFileSource, staticFileHostname)
        {
        }

        /// <summary>
        /// Gets or sets the URL scheme.
        /// </summary>
        /// <value>The URL scheme.</value>
        [CanBeNull]
        [UsedImplicitly]
        public string UrlScheme { get; set; }

        /// <summary>
        /// Gets or sets the URI.
        /// </summary>
        /// <value>The URI.</value>
        [CanBeNull]
        [UsedImplicitly]
        public string Uri { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [convert to lower case].
        /// </summary>
        /// <value><c>true</c> if [convert to lower case]; otherwise, <c>false</c>.</value>
        [UsedImplicitly]
        public bool ConvertToLowerCase { get; set; }
    }
}