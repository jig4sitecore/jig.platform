﻿namespace Jig.Sitecore.Pipelines
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Web;
    using System.Web.UI;

    using Newtonsoft.Json;

    using global::Sitecore;
    using global::Sitecore.Globalization;
    using global::Sitecore.Pipelines;

    using global::Sitecore.Sites;

    /// <summary>
    /// The site map pipeline args.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Justification = "No Native resources"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable", Justification = "The serialization never used any more"), JsonObject(MemberSerialization.OptIn)]
    public partial class SiteMapPipelineArgs : PipelineArgs, IDisposable
    {
        /// <summary>
        /// The site context
        /// </summary>
        private SiteContext siteContext;

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SiteMapPipelineArgs" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public SiteMapPipelineArgs([NotNull] HttpContextBase context)
        {
            this.Context = context;
            this.Output = new HtmlTextWriter(new StringWriter(new StringBuilder(8096)));

            /* NOTE:
             * The new sitemaps can contain images, video, news namespace declarations like
                <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" 
                  xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" 
                  xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">
                  <url> 
             *  Read more https://support.google.com/webmasters/answer/183668?hl=en
             *  The new namespace could be added in the pipeline in the future
             */
            this.SitemapNamespaces = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase)
                                         {
                                             @"xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"""
                                         };

            this.SupportedLanguages = new HashSet<Language>();

            /* Could be reset in pipelines , but by default secure url must be used for production IIS */
            this.ForceToHttps = true;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        [NotNull]
        public HttpContextBase Context { get; private set; }

        /// <summary>
        /// Gets the output.
        /// </summary>
        [NotNull]
        public HtmlTextWriter Output { get; private set; }

        /// <summary>
        /// Gets the supported languages.
        /// </summary>
        /// <value>The supported languages.</value>
        [NotNull]
        [UsedImplicitly]
        public ICollection<global::Sitecore.Globalization.Language> SupportedLanguages { get; private set; }

        /// <summary>
        /// Gets or sets the site.
        /// </summary>
        /// <value>
        /// The site.
        /// </value>
        [CanBeNull]
        [UsedImplicitly]
        public SiteContext SiteContext
        {
            get
            {
                return this.siteContext;
            }

            set
            {
                if (value != null)
                {
                    this.siteContext = value;
                    global::Sitecore.Context.SetActiveSite(value.Name);
                }
            }
        }

        /// <summary>
        /// Gets or sets the database.
        /// </summary>
        /// <value>The database.</value>
        [CanBeNull]
        [UsedImplicitly]
        public global::Sitecore.Data.Database Database { get; set; }

        /// <summary>
        /// Gets the sitemap namespaces.
        /// </summary>
        /// <value>The sitemap namespaces.</value>
        [NotNull]
        public HashSet<string> SitemapNamespaces { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [force to HTTPS].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [force to HTTPS]; otherwise, <c>false</c>.
        /// </value>
        /// <remarks>The flag to force all url location use Https instead of http</remarks>
        [UsedImplicitly] 
        public bool ForceToHttps { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The dispose.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Justification = "No Native resources")]
        public new void Dispose()
        {
            base.Dispose();
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (this.Output != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                this.Output.Dispose();
            }
        }

        #endregion
    }
}