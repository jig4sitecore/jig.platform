﻿namespace Jig.Sitecore
{
    using System.Collections.Generic;
    using System.Linq;
    using global::Sitecore;
    using global::Sitecore.Data.Items;

    /// <summary>
    ///     The Sitecore Item extensions
    /// </summary>
    public static partial class LinkDatabaseExtensions
    {
        /// <summary>
        /// Gets the referrers as items.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>The list of referred items</returns>
        [NotNull]
        public static IEnumerable<Item> GetReferrersAsItems([NotNull] this Item item)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var links = global::Sitecore.Globals.LinkDatabase.GetReferrers(item);

                return links.Select(referreditem => referreditem.GetTargetItem()).Where(i => i != null);
            }

            return new Item[] { };
        }

        /// <summary>
        /// Gets the source as items.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>The list of the source items</returns>
        [NotNull]
        public static IEnumerable<Item> GetSourceAsItems([NotNull] this Item item)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var links = global::Sitecore.Globals.LinkDatabase.GetReferrers(item);

                return links.Select(referreditem => referreditem.GetSourceItem()).Where(i => i != null);
            }

            return new Item[] { };
        }
    }
}
