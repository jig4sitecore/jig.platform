﻿namespace Jig.Sitecore.Context
{
    using System;
    using System.Linq;
    using System.Web;
    using global::Sitecore;

    /// <summary>
    /// Context validation extensions.
    /// </summary>
    public static class HostNamesToMatchExtensions
    {
        /// <summary>
        /// Determines whether the specified request is match.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="request">The request.</param>
        /// <returns><c>true</c> if the specified request is match; otherwise, <c>false</c>.</returns>
        /// <exception cref="System.Web.HttpException">
        /// The target cannot be null!
        /// or
        /// The request cannot be null!
        /// </exception>
        public static bool IsMatchHostName(
                                   [NotNull] this IHostNamesToMatch target,
                                   [NotNull] HttpRequestBase request)
        {
            if (target == null)
            {
                throw new HttpException("The target cannot be null!");
            }

            if (request == null || request.Url == null)
            {
                throw new HttpException("The request cannot be null!");
            }

            var hostname = request.Url.Host;
            var list = target.MatchHostNames.ConvertToList();

            var result = list.Any(x => string.Equals(x, hostname, StringComparison.InvariantCultureIgnoreCase));

            return result;
        }

        /// <summary>
        /// Determines whether the specified request is match.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="request">The request.</param>
        /// <returns><c>true</c> if the specified request is match; otherwise, <c>false</c>.</returns>
        /// <exception cref="System.Web.HttpException">
        /// The target cannot be null!
        /// or
        /// The request cannot be null!
        /// </exception>
        public static bool IsMatchHostName(
                                   [NotNull] this IHostNamesToMatch target,
                                   [NotNull] HttpRequest request)
        {
            if (target == null)
            {
                throw new HttpException("The target cannot be null!");
            }

            if (request == null)
            {
                throw new HttpException("The request cannot be null!");
            }

            var hostname = request.Url.Host;
            var list = target.MatchHostNames.ConvertToList();

            var result = list.Any(x => string.Equals(x, hostname, StringComparison.InvariantCultureIgnoreCase));

            return result;
        }
    }
}