﻿namespace Jig.Sitecore
{
    using global::Sitecore;
    using global::Sitecore.Resources.Media;

    /// <summary>
    /// The media item extensions.
    /// </summary>
    public static partial class MediaItemExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get default media url options.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <returns>
        /// The default MediaUrlOptions.
        /// </returns>
        [NotNull]
        public static global::Sitecore.Resources.Media.MediaUrlOptions GetDefaultMediaUrlOptions(
            [NotNull] this global::Sitecore.Data.Items.MediaItem mediaItem)
        {
            var options = new MediaUrlOptions
            {
                LowercaseUrls = true,
                AbsolutePath = false,
                IncludeExtension = true,
                UseItemPath = false
            };

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (mediaItem != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                options.RequestExtension = mediaItem.Extension;
            }

            return options;
        }

        /// <summary>
        /// Gets the media URL.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="maxWidth">The maximum width.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <returns>
        /// The Media Url
        /// </returns>
        [NotNull]
        public static string GetMediaItemUrl(
            [NotNull] this global::Sitecore.Data.Items.MediaItem mediaItem, 
            int? width = null, 
            int? height = null, 
            int? maxWidth = null, 
            int? maxHeight = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (mediaItem != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var options = new MediaUrlOptions
                                  {
                                      LowercaseUrls = true,
                                      AbsolutePath = false,
                                      IncludeExtension = true,
                                      UseItemPath = false,
                                      RequestExtension = mediaItem.Extension
                                  };

                if (width.HasValue)
                {
                    options.Width = width.Value;
                }

                if (height.HasValue)
                {
                    options.Height = height.Value;
                }

                if (maxWidth.HasValue)
                {
                    options.MaxWidth = maxWidth.Value;
                }

                if (maxHeight.HasValue)
                {
                    options.MaxHeight = maxHeight.Value;
                }

                var mediaUrl = MediaManager.GetMediaUrl(mediaItem, options);

                if (string.IsNullOrWhiteSpace(mediaUrl))
                {
                    return string.Empty;
                }

                if (mediaUrl.StartsWith("http:"))
                {
                    return mediaUrl.Remove(0, "http:".Length);
                }

                if (mediaUrl.StartsWith("https:"))
                {
                    return mediaUrl.Remove(0, "https:".Length);
                }

                return mediaUrl;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the media item URL.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <param name="mediaUrlOptions">The media URL options.</param>
        /// <returns>The Media Url</returns>
        [NotNull]
        public static string GetMediaItemUrl(
            [NotNull] this global::Sitecore.Data.Items.MediaItem mediaItem, 
            [NotNull] global::Sitecore.Resources.Media.MediaUrlOptions mediaUrlOptions)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (mediaItem != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var mediaUrl = MediaManager.GetMediaUrl(mediaItem, mediaUrlOptions);

                if (string.IsNullOrWhiteSpace(mediaUrl))
                {
                    return string.Empty;
                }

                if (mediaUrl.StartsWith("http:"))
                {
                    return mediaUrl.Remove(0, "http:".Length);
                }

                if (mediaUrl.StartsWith("https:"))
                {
                    return mediaUrl.Remove(0, "https:".Length);
                }

                return mediaUrl;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the media item as base64.
        /// </summary>
        /// <param name="mediaItem">The media item.</param>
        /// <returns>The media item as base 64 string</returns>
        [NotNull]
        public static string GetMediaItemAsBase64(
            [NotNull] this global::Sitecore.Data.Items.MediaItem mediaItem)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (mediaItem != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                using (var stream = mediaItem.GetMediaStream())
                {
                    /* The image might be not published but content item could be. Verify */
                    if (stream != null)
                    {
                        var bytes = new byte[stream.Length];
                        stream.Read(bytes, 0, bytes.Length);
                        var src = string.Concat("data:", mediaItem.MimeType, ";base64,", System.Convert.ToBase64String(bytes));
                        
                        return src;
                    }
                }               
            }

            return string.Empty;
        }

        #endregion
    }
}