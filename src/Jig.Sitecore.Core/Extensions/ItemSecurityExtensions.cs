﻿namespace Jig.Sitecore
{
    using System.Linq;
    using global::Sitecore;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Security.AccessControl;

    /// <summary>
    ///     The Sitecore Item extensions
    /// </summary>
    public static partial class SitecoreExtensions
    {
        /// <summary>
        /// Checks if an id has explicit security deny rules on it.
        /// </summary>
        /// <param name="item">Sitecore ID.</param>
        /// <returns>
        ///     <c>true</c> if [has explicit denies] [the specified item]; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasExplicitDenies([NotNull] this Item item)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return
                    item.Security.GetAccessRules().Any(rule => rule.SecurityPermission == SecurityPermission.DenyAccess);
            }

            return false;
        }

        /// <summary>
        /// Determines whether the specified item is hidden.
        /// </summary>
        /// <param name="item">The Sitecore item.</param>
        /// <param name="recursiveCall">if set to <c>true</c> [recursive call].</param>
        /// <returns>
        ///   <c>true</c> if the specified item is hidden; otherwise, <c>false</c>.
        /// </returns>
        // ReSharper disable CodeAnnotationAnalyzer
        public static bool IsHidden(this Item item, bool recursiveCall = false)
        // ReSharper restore CodeAnnotationAnalyzer
        {
            if (item != null)
            {
                if (recursiveCall)
                {
                    return item.Appearance.Hidden || ((item.Parent != null) && IsHidden(item.Parent, true));
                }

                return item.Appearance.Hidden;
            }

            return false;
        }
    }
}
