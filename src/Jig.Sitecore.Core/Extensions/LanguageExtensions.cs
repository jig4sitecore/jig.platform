﻿namespace Jig.Sitecore
{
    using global::Sitecore;
    using global::Sitecore.Globalization;

    /// <summary>
    /// Language Extensions
    /// </summary>
    public static class LanguageExtensions
    {
        /// <summary>
        /// Given an Item in a specific language dialect (ex: en-GB), determines if the
        /// dialect has a version, and if not, attempts to find a more generalized language (ex: en).
        /// If there is a more generalized language, returns an item instance in that more generalized
        /// language. The instance may be "empty" and should be checked using the LanguageVersionIsEmpty() extension.
        /// </summary>
        /// <param name="item">The Item to test.</param>
        /// <param name="targetLanguage">The expected language.</param>
        /// <returns>The current language version, or a language version in a more generalized language if available.</returns>
        [NotNull]
        public static global::Sitecore.Data.Items.Item GetBestFitLanguageVersion([NotNull] this global::Sitecore.Data.Items.Item item, [NotNull] global::Sitecore.Globalization.Language targetLanguage)
        {
            var languageSpecificItem = item.Database.GetItem(item.ID, targetLanguage);

            return languageSpecificItem.GetBestFitLanguageVersion();
        }

        /// <summary>
        /// Given an Item in a specific language dialect (ex: en-GB), determines if the
        /// dialect has a version, and if not, attempts to find a more generalized language (ex: en).
        /// If there is a more generalized language, returns an item instance in that more generalized
        /// language. The instance may be "empty" and should be checked using the LanguageVersionIsEmpty() extension.
        /// </summary>
        /// <param name="item">The Item to test.</param>
        /// <returns>The current language version, or a language version in a more generalized language if available.</returns>
        [NotNull]
        public static global::Sitecore.Data.Items.Item GetBestFitLanguageVersion([NotNull] this global::Sitecore.Data.Items.Item item)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return null;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            if (item.LanguageVersionIsEmpty())
            {
                Language generalizedLanguage;
                if (Language.TryParse(item.Language.Name.Substring(0, 2), out generalizedLanguage))
                {
                    return item.Database.GetItem(item.ID, generalizedLanguage);
                }
            }

            return item;
        }
    }
}
