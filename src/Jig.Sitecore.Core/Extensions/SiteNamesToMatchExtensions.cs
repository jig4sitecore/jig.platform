﻿namespace Jig.Sitecore.Context
{
    using System;
    using System.Linq;
    using System.Web;
    using global::Sitecore;
    using global::Sitecore.Sites;

    /// <summary>
    /// Context validation extensions.
    /// </summary>
    public static class SiteNamesToMatchExtensions
    {
        /// <summary>
        /// Determines whether the specified site context is match.
        /// </summary>
        /// <param name="target">The object that requires evaluation.</param>
        /// <param name="siteContext">The site context.</param>
        /// <returns><c>true</c> if any site name match, <c>false</c> otherwise.</returns>
        public static bool IsMatchSiteName(
            [NotNull] this ISiteNamesToMatch target,
            [NotNull] SiteContext siteContext)
        {
            if (target == null)
            {
                throw new HttpException("The target cannot be null!");
            }

            if (siteContext == null)
            {
                throw new HttpException("The siteContext cannot be null!");
            }

            var list = target.MatchSiteInfoNames.ConvertToList();

            var result = list.Any(x => string.Equals(x, siteContext.Name, StringComparison.InvariantCultureIgnoreCase));

            return result;
        }

        /// <summary>
        /// Determines whether the specified site context is match.
        /// </summary>
        /// <param name="target">The object that requires evaluation.</param>
        /// <param name="siteContext">The site context.</param>
        /// <returns><c>true</c> if any site name match, <c>false</c> otherwise.</returns>
        public static bool IsMatchSiteName(
            [NotNull] this ISiteNamesToMatch target,
            [NotNull] Site siteContext)
        {
            if (target == null)
            {
                throw new HttpException("The target cannot be null!");
            }

            if (siteContext == null)
            {
                throw new HttpException("The siteContext cannot be null!");
            }

            var list = target.MatchSiteInfoNames.ConvertToList();

            var result = list.Any(x => string.Equals(x, siteContext.Name, StringComparison.InvariantCultureIgnoreCase));

            return result;
        }
    }
}
