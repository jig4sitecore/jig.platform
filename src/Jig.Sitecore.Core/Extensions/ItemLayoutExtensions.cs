﻿namespace Jig.Sitecore
{
    using System;
    using System.Linq;

    using global::Sitecore;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Data.Managers;
    using global::Sitecore.Globalization;

    using Version = global::Sitecore.Data.Version;

    /// <summary>
    ///     The Sitecore Item extensions
    /// </summary>
    public static partial class ItemLayoutExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Determines whether the specified id has layout.
        /// </summary>
        /// <param name="item">
        /// The Sitecore item.
        /// </param>
        /// <returns>
        /// <c>true</c> if the specified id has layout; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasLayout([NotNull] this Item item)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null)
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                LayoutField layoutField = item.Fields[global::Sitecore.FieldIDs.LayoutField];
                if (!string.IsNullOrEmpty(layoutField.Value) && layoutField.Value.IndexOf("l=\"{", StringComparison.InvariantCultureIgnoreCase) > 0)
                {
                    /* Quick and cheap dirty */
                    return true;
                }
 
                var finalLayoutField = item.Fields[FieldIDs.FinalLayoutField];
                if (finalLayoutField != null)
                {
                    var fieldValue = item[FieldIDs.FinalLayoutField];
                    if (!string.IsNullOrWhiteSpace(fieldValue) && fieldValue.IndexOf("l=\"{", StringComparison.InvariantCultureIgnoreCase) > 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// The has versioned renderings.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public static bool HasVersionedRenderings(this Item item)
        {
            if (item.Fields[FieldIDs.FinalLayoutField] == null)
            {
                return false;
            }

            var field = item.Fields[FieldIDs.FinalLayoutField];
            return !string.IsNullOrWhiteSpace(field.GetValue(false, false));
        }

        /// <summary>
        /// The has versioned renderings on any language.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public static bool HasVersionedRenderingsOnAnyLanguage(this Item item)
        {
            return ItemManager.GetContentLanguages(item).Any(item.HasVersionedRenderingsOnLanguage);
        }

        /// <summary>
        /// The has versioned renderings on context language.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public static bool HasVersionedRenderingsOnContextLanguage(this Item item)
        {
            return item.HasVersionedRenderingsOnLanguage(global::Sitecore.Context.Language);
        }

        /// <summary>
        /// The has versioned renderings on language.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="language">The language.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public static bool HasVersionedRenderingsOnLanguage(this Item item, Language language)
        {
            return item != null && item.Database.GetItem(item.ID, language).HasVersionedRenderings();
        }

        /// <summary>
        /// The has versioned renderings on version.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="language">The language.</param>
        /// <param name="version">The version.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public static bool HasVersionedRenderingsOnVersion(
            this Item item,
            Language language,
            Version version)
        {
            var versionItem = item.Database.GetItem(item.ID, language, version);
            return versionItem != null && versionItem.HasVersionedRenderings();
        }

        #endregion
    }
}