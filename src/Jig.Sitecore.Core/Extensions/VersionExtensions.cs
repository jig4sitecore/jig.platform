﻿namespace Jig.Sitecore
{
    using global::Sitecore;

    /// <summary>
    /// Version extensions
    /// </summary>
    public static class VersionExtensions
    {
        /// <summary>
        /// Verifies that the Item, in its current Language, is not effectively empty.
        /// </summary>
        /// <param name="item">The Item to test.</param>
        /// <returns>True if the Item's Language has one or more Versions.</returns>
        public static bool LanguageVersionIsEmpty([NotNull] this global::Sitecore.Data.Items.Item item)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return true;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            var langItem = item.Database.GetItem(item.ID, item.Language);

            return langItem == null || langItem.Versions.Count == 0;
        }
    }
}
