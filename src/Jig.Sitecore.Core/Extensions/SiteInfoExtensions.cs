﻿namespace Jig.Sitecore
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Jig.Sitecore.CustomItems;

    using global::Sitecore;

    using global::Sitecore.Data;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Sites;
    using global::Sitecore.Web;

    /// <summary>
    /// Extension Methods for the Sitecore.Web.SiteInfo class.
    /// </summary>
    public static class SiteInfoExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets the default URL options.
        /// </summary>
        /// <param name="site">The site.</param>
        /// <returns>
        /// The default Link manager options
        /// </returns>
        [NotNull]
        public static global::Sitecore.Links.UrlOptions GetDefaultUrlOptions(
            [NotNull] this global::Sitecore.Sites.Site site)
        {
            var urlOptions = new global::Sitecore.Links.UrlOptions
            {
                /* These three are common sense/mandatory options */
                AddAspxExtension = false,
                ShortenUrls = true,
                LowercaseUrls = true
            };

            /* Sitecore is design for multi-language sites. Old systems are not has no concept LinkManager by site */
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (site != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var languageEmbedding = site.GetLanguageEmbedding();
                if (languageEmbedding != null)
                {
                    urlOptions.LanguageEmbedding = languageEmbedding.Value;
                }
            }

            return urlOptions;
        }

        /// <summary>
        /// Gets the default URL options.
        /// </summary>
        /// <param name="site">The site.</param>
        /// <returns>
        /// The default Link manager options
        /// </returns>
        [NotNull]
        public static global::Sitecore.Links.UrlOptions GetDefaultUrlOptions([NotNull]this global::Sitecore.Sites.SiteContext site)
        {
            var urlOptions = new global::Sitecore.Links.UrlOptions
            {
                /* These three are common sense/mandatory options */
                AddAspxExtension = false,
                ShortenUrls = true,
                LowercaseUrls = true
            };

            /* Sitecore is design for multi-language sites. Old systems are not has no concept LinkManager by site */
            var languageEmbedding = site.GetLanguageEmbedding();
            if (languageEmbedding != null)
            {
                urlOptions.LanguageEmbedding = languageEmbedding.Value;
            }

            return urlOptions;
        }

        /// <summary>
        /// Gets the language embedding.
        /// </summary>
        /// <param name="site">The site.</param>
        /// <param name="siteInfoProperty">The site information property.</param>
        /// <returns>The Language Embedding option</returns>
        public static global::Sitecore.Links.LanguageEmbedding? GetLanguageEmbedding(
            [NotNull] this global::Sitecore.Sites.Site site,
            [NotNull] string siteInfoProperty = Jig.Sitecore.Constants.SiteInfo.PropertyNames.LanguageEmbedding)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (site != null && !string.IsNullOrWhiteSpace(siteInfoProperty))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var value = site.Properties[siteInfoProperty];
                if (!string.IsNullOrWhiteSpace(value))
                {
                    global::Sitecore.Links.LanguageEmbedding languageEmbedding;
                    if (Enum.TryParse(value, ignoreCase: true, result: out languageEmbedding))
                    {
                        return languageEmbedding;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Languages the embedding.
        /// </summary>
        /// <param name="siteContext">The site context.</param>
        /// <param name="siteInfoProperty">The site information property.</param>
        /// <returns>
        /// The Language Embedding option
        /// </returns>
        public static global::Sitecore.Links.LanguageEmbedding? GetLanguageEmbedding(
            [NotNull] this global::Sitecore.Sites.SiteContext siteContext,
            [NotNull] string siteInfoProperty = Jig.Sitecore.Constants.SiteInfo.PropertyNames.LanguageEmbedding)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (siteContext != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var options = siteContext.SiteInfo.GetLanguageEmbedding(siteInfoProperty);
                return options;
            }

            return null;
        }

        /// <summary>
        /// The language embedding.
        /// </summary>
        /// <param name="siteInfo">The site info.</param>
        /// <param name="siteInfoProperty">The site info property.</param>
        /// <returns>
        /// The Language Embedding option
        /// </returns>
        public static global::Sitecore.Links.LanguageEmbedding? GetLanguageEmbedding(
            [NotNull] this global::Sitecore.Web.SiteInfo siteInfo,
            [NotNull] string siteInfoProperty = Jig.Sitecore.Constants.SiteInfo.PropertyNames.LanguageEmbedding)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (siteInfo != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var value = siteInfo.Properties[siteInfoProperty];
                if (!string.IsNullOrWhiteSpace(value))
                {
                    global::Sitecore.Links.LanguageEmbedding languageEmbedding;
                    if (Enum.TryParse(value, ignoreCase: true, result: out languageEmbedding))
                    {
                        return languageEmbedding;
                    }
                }

                var contextLanguage = global::Sitecore.Context.Language;
                if (contextLanguage != null
                    && string.Equals(
                        contextLanguage.Name,
                        siteInfo.Language,
                        StringComparison.InvariantCultureIgnoreCase))
                {
                    return global::Sitecore.Links.LanguageEmbedding.Never;
                }
            }

            return null;
        }

        /// <summary>
        /// Retrieves a collection of supported languages for the SiteInfo instance. The
        /// collection can be populated by adding a custom "supportedLanguages" parameter to
        /// a Sitecore "site" configuration node. Language codes should be separated by commas.
        /// The returned collection includes the official "language" value for the SiteInfo instance,
        /// but not the "contentLanguage" value.
        /// </summary>
        /// <param name="siteInfo">The SiteInfo to interrogate.</param>
        /// <returns>
        /// The collection includes the official "language" value for the SiteInfo instance,
        /// but not the "contentLanguage" value.
        /// </returns>
        [NotNull]
        public static IList<string> GetSupportedLanguages(
            [NotNull] this global::Sitecore.Web.SiteInfo siteInfo)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (siteInfo == null || siteInfo.Database == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return new string[] { };
            }

            var database = global::Sitecore.Context.ContentDatabase;
            if (database == null || string.Equals(database.Name, "core"))
            {
                database = global::Sitecore.Configuration.Factory.GetDatabase(siteInfo.Database);
            }

            if (database == null)
            {
                return new string[] { };
            }

            /* ReSharper restore HeuristicUnreachableCode */

            return GetSupportedLanguages(siteInfo, database);
        }

        /// <summary>
        /// Supported the languages.
        /// </summary>
        /// <param name="siteContext">The site information.</param>
        /// <returns>
        /// The collection includes the official "language" value for the SiteInfo instance,
        /// but not the "contentLanguage" value.
        /// </returns>
        // ReSharper disable ReturnTypeCanBeEnumerable.Global
        [NotNull]
        public static IList<string> GetSupportedLanguages(
            // ReSharper restore ReturnTypeCanBeEnumerable.Global
            [NotNull] this global::Sitecore.Sites.SiteContext siteContext)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (siteContext == null || siteContext.Database == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return new string[] { };
            }

            /* ReSharper restore HeuristicUnreachableCode */

            var collection = siteContext.SiteInfo.GetSupportedLanguages(siteContext.Database);
            return collection;
        }

        /// <summary>
        /// Gets the not found page item path.
        /// </summary>
        /// <param name="site">The site.</param>
        /// <param name="siteProperty">The site property.</param>
        /// <returns>Not found page path</returns>
        [NotNull]
        public static string GetNotFoundPageItemPath(
            [NotNull]this global::Sitecore.Sites.Site site,
            [NotNull]string siteProperty = Jig.Sitecore.Constants.SiteInfo.PropertyNames.NotFoundPageItemPath)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (site != null) // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                var property = site.Properties[siteProperty];
                if (!string.IsNullOrWhiteSpace(property))
                {
                    return property.Trim();
                }
            }

            return Jig.Sitecore.Constants.SiteInfo.PropertyStandardValues.NotFoundPageItemPath;
        }

        /// <summary>
        /// Gets the not found page item path.
        /// </summary>
        /// <param name="site">The site.</param>
        /// <param name="siteProperty">The site property.</param>
        /// <returns>Not found page path</returns>
        [NotNull]
        public static string GetNotFoundPageItemPath(
            [NotNull]this global::Sitecore.Sites.SiteContext site,
            [NotNull]string siteProperty = Jig.Sitecore.Constants.SiteInfo.PropertyNames.NotFoundPageItemPath)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (site != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                var property = site.Properties[siteProperty];
                if (!string.IsNullOrWhiteSpace(property))
                {
                    return property.Trim();
                }
            }

            return Jig.Sitecore.Constants.SiteInfo.PropertyStandardValues.NotFoundPageItemPath;
        }

        /// <summary>
        /// Gets the supported languages.
        /// </summary>
        /// <param name="siteInfo">The site information.</param>
        /// <param name="database">The database.</param>
        /// <returns>The collection of supported item</returns>
        [NotNull]
        public static IList<string> GetSupportedLanguages([NotNull]this SiteInfo siteInfo, [NotNull] Database database)
        {
            Assert.IsNotNull(siteInfo, "siteInfo");
            Assert.IsNotNull(database, "database");

            var languageCodes = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);

            global::Sitecore.Data.Items.Item rootItem = database.GetItem(siteInfo.RootPath);
            if (rootItem != null)
            {
                var field = rootItem.Fields[Jig.Sitecore.Constants.FieldNames.Languages];
                if (field != null && !string.IsNullOrWhiteSpace(field.Value))
                {
                    global::Sitecore.Data.Fields.MultilistField languages = field;
                    var languagesItems = languages.GetItems();
                    foreach (LanguageItem languageItem in languagesItems)
                    {
                        if (languageItem != null)
                        {
                            var value = languageItem.Iso.Value;
                            if (!string.IsNullOrWhiteSpace(value))
                            {
                                languageCodes.Add(value);
                            }

                            value = languageItem.RegionalIsoCode.Value;
                            if (!string.IsNullOrWhiteSpace(value))
                            {
                                languageCodes.Add(value);
                            }
                        }
                    }
                }
            }

            return languageCodes.ToArray();
        }

        /// <summary>
        /// Gets the supported languages.
        /// </summary>
        /// <param name="siteContext">The site context.</param>
        /// <param name="database">The database.</param>
        /// <returns>The collection of supported item</returns>
        [NotNull]
        public static IList<string> GetSupportedLanguages(
            [NotNull] this SiteContext siteContext,
            [NotNull] Database database)
        {
            Assert.IsNotNull(siteContext, "siteContext");
            return siteContext.SiteInfo.GetSupportedLanguages(database);
        }

        /// <summary>
        /// Gets the static files host name folder path.
        /// </summary>
        /// <param name="site">The site.</param>
        /// <param name="siteProperty">The site property.</param>
        /// <returns>The Static file Host Name Path</returns>
        [NotNull]
        public static string GetStaticFilesHostNameFolderPath(
            [NotNull]this global::Sitecore.Sites.SiteContext site,
            [NotNull]string siteProperty = Jig.Sitecore.Constants.SiteInfo.PropertyNames.StaticFilesHostNameFolderPath)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (site != null) // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                var property = site.Properties[siteProperty];
                if (!string.IsNullOrWhiteSpace(property))
                {
                    return site.RootPath + property.Trim();
                }

                return site.RootPath + Jig.Sitecore.Constants.SiteInfo.PropertyStandardValues.StaticFilesHostNameFolder;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the static files host name folder path.
        /// </summary>
        /// <param name="site">The site.</param>
        /// <param name="siteProperty">The site property.</param>
        /// <returns>The Static file Host Name Path</returns>
        [NotNull]
        public static string GetStaticFilesHostNameFolderPath(
            [NotNull]global::Sitecore.Web.SiteInfo site,
            [NotNull]string siteProperty = Jig.Sitecore.Constants.SiteInfo.PropertyNames.StaticFilesHostNameFolderPath)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (site != null) // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                var property = site.Properties[siteProperty];
                if (!string.IsNullOrWhiteSpace(property))
                {
                    return site.RootPath + property.Trim();
                }

                return site.RootPath + Jig.Sitecore.Constants.SiteInfo.PropertyStandardValues.StaticFilesHostNameFolder;
            }

            return string.Empty;
        }

        #endregion
    }
}