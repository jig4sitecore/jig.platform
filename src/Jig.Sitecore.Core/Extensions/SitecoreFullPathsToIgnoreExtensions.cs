﻿namespace Jig.Sitecore.Context
{
    using System;
    using System.Linq;
    using System.Web;
    using global::Sitecore;
    using global::Sitecore.Sites;

    /// <summary>
    /// Context validation extensions.
    /// </summary>
    public static class SitecoreFullPathsToIgnoreExtensions
    {
        /// <summary>
        /// Determines whether [is match databases to ignore] [the specified database].
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="siteRequest">The site request.</param>
        /// <returns><c>true</c> if [is match sitecore full paths to ignore] [the specified site request]; otherwise, <c>false</c>.</returns>
        /// <exception cref="System.Web.HttpException">The target cannot be null!
        /// or
        /// The database cannot be null!</exception>
        public static bool IsMatchSitecoreFullPathsToIgnore(
                            [NotNull] this ISitecoreFullPathsToIgnore target,
                            [NotNull] SiteRequest siteRequest)
        {
            if (target == null)
            {
                throw new HttpException("The target cannot be null!");
            }

            if (siteRequest == null)
            {
                throw new HttpException("The siteRequest cannot be null!");
            }

            var list = target.SitecoreFullPathsToIgnore.ConvertToList();

            var result = list.Any(x => siteRequest.FilePath.StartsWith(x, StringComparison.InvariantCultureIgnoreCase));

            return result;
        }

        /// <summary>
        /// Determines whether [is match sitecore full paths to ignore] [the specified request path].
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="requestPath">The request path.</param>
        /// <returns><c>true</c> if [is match sitecore full paths to ignore] [the specified request path]; otherwise, <c>false</c>.</returns>
        /// <exception cref="System.Web.HttpException">The target cannot be null!
        /// or
        /// The requestPath cannot be null!</exception>
        public static bool IsMatchSitecoreFullPathsToIgnore(
                    [NotNull] this ISitecoreFullPathsToIgnore target,
                    [NotNull] string requestPath)
        {
            if (target == null)
            {
                throw new HttpException("The target cannot be null!");
            }

            if (string.IsNullOrWhiteSpace(requestPath))
            {
                throw new HttpException("The requestPath cannot be null!");
            }

            var list = target.SitecoreFullPathsToIgnore.ConvertToList();

            var result = list.Any(x => requestPath.StartsWith(x, StringComparison.InvariantCultureIgnoreCase));

            return result;
        }
    }
}