﻿namespace Jig.Sitecore
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using global::Sitecore;
    using global::Sitecore.Data;

    using global::Sitecore.Data.Items;

    using global::Sitecore.Diagnostics;

    /// <summary>
    /// Set of extensions for interacting with Sitecore items.
    /// </summary>
    public static class ItemExtensions
    {
        /// <summary>
        /// Determines whether is item not null.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>True if item is not null or empty</returns>
        public static bool IsNotNull([CanBeNull] this global::Sitecore.Data.Items.Item item)
        {
            return item != null && item.Versions.Count > 0;
        }

        /// <summary>
        /// Determines whether this item is null.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>True if item is null or empty</returns>
        public static bool IsNull([CanBeNull] this global::Sitecore.Data.Items.Item item)
        {
            return item == null || item.Versions.Count == 0;
        }

        /// <summary>
        /// Work up the tree until an item is found that uses the specified template.
        /// </summary>
        /// <param name="current">The current item.</param>
        /// <param name="templateId">Template Id of the ancestor to look for.</param>
        /// <returns>The matching ancestor or null.</returns>
        [CanBeNull]
        public static global::Sitecore.Data.Items.Item FindAncestorByTemplateId([NotNull] this global::Sitecore.Data.Items.Item current, [NotNull] string templateId)
        {
            return current.FindAncestorByTemplateId(new ID(templateId));
        }

        /// <summary>
        /// Work up the tree until an item is found that uses the specified template.
        /// </summary>
        /// <param name="current">The current item.</param>
        /// <param name="templateId">Template Id of the ancestor to look for.</param>
        /// <returns>The matching ancestor or null.</returns>
        [CanBeNull]
        public static global::Sitecore.Data.Items.Item FindAncestorByTemplateId([NotNull] this global::Sitecore.Data.Items.Item current, [NotNull] global::Sitecore.Data.ID templateId)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (current == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return null;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            if (current.IsDerived(templateId))
            {
                return current;
            }

            return FindAncestorByTemplateId(current.Parent, templateId);
        }

        /// <summary>
        /// Work up the tree until an item is found that uses the specified template.
        /// </summary>
        /// <param name="current">The current item.</param>
        /// <param name="baseTemplateId">Base template Id of the ancestor to look for.</param>
        /// <returns>The matching ancestor or null.</returns>
        [CanBeNull]
        public static global::Sitecore.Data.Items.Item FindAncestorByBaseTemplateId([NotNull] this global::Sitecore.Data.Items.Item current, [NotNull] string baseTemplateId)
        {
            return current.FindAncestorByTemplateId(baseTemplateId);
        }

        /// <summary>
        /// Work up the tree until an item is found that uses the specified template.
        /// </summary>
        /// <param name="current">The current item.</param>
        /// <param name="baseTemplateId">Base template Id of the ancestor to look for.</param>
        /// <returns>The matching ancestor or null.</returns>
        [CanBeNull]
        public static global::Sitecore.Data.Items.Item FindAncestorByBaseTemplateId([NotNull] this global::Sitecore.Data.Items.Item current, [NotNull] global::Sitecore.Data.ID baseTemplateId)
        {
            return current.FindAncestorByTemplateId(baseTemplateId);
        }

        /// <summary>
        /// Work up the tree to see if there is an ancestor that matches the provided template id.
        /// If so get its Sitecore ID.
        /// </summary>
        /// <param name="current">The current item.</param>
        /// <param name="templateId">Template Id of the ancestor to look for.</param>
        /// <returns>ID of the matching ancestor or an empty string.</returns>
        [NotNull]
        public static string GetAncestorId([NotNull] this global::Sitecore.Data.Items.Item current, [NotNull] string templateId)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (current == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return string.Empty;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            Item ancestor = current.FindAncestorByTemplateId(templateId);

            if (ancestor == null)
            {
                return string.Empty;
            }

            return ancestor.ID.ToString();
        }

        /// <summary>
        /// Determine if a particular item is derived from a specific template.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <param name="templateName">Template to check.</param>
        /// <returns>True if template matches, false if no match or either parameter is null.</returns>
        public static bool IsDerived([NotNull] this global::Sitecore.Data.Items.Item item, [NotNull] string templateName)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null || string.IsNullOrEmpty(templateName))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return false;
            }

            var templateItem = item.Database.Templates[templateName];
            return item.IsDerived(templateItem);
        }

        /// <summary>
        /// Determine if a particular item is derived from a specific template.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <param name="templateId">Template to check.</param>
        /// <returns>True if template matches, false if no match or either parameter is null.</returns>
        public static bool IsDerived([NotNull] this global::Sitecore.Data.Items.Item item, [NotNull] global::Sitecore.Data.ID templateId)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null || object.ReferenceEquals(templateId, null) || templateId.IsNull)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return false;
            }

            var templateItem = item.Database.Templates[templateId];
            return item.IsDerived(templateItem);
        }

        /// <summary>
        /// Determine if a particular item is derived from a specific template.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <param name="templateId">Template to check.</param>
        /// <returns>True if template matches, false if no match or either parameter is null.</returns>
        public static bool IsDerived([NotNull] this global::Sitecore.Data.Items.Item item, Guid templateId)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return false;
            }

            // ReSharper restore HeuristicUnreachableCode
            var templateItem = item.Database.Templates[new ID(templateId)];
            return item.IsDerived(templateItem);
        }

        /// <summary>
        /// Determine if a particular item is derived from a specific template.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <param name="template">Template to check.</param>
        /// <returns>True if template matches, false if no match or either parameter is null.</returns>
        public static bool IsDerived([NotNull] this global::Sitecore.Data.Items.Item item, [NotNull] Type template)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null || template == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return false;
            }

            // ReSharper restore HeuristicUnreachableCode
            var templateItem = item.Database.Templates[new ID(template.GUID)];
            return item.IsDerived(templateItem);
        }

        /// <summary>
        /// Determine if a particular item is derived from a specific template.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <param name="templateToCompare">Template to check.</param>
        /// <returns>True if template matches, false if no match or either parameter is null.</returns>
        public static bool IsDerived([NotNull] this global::Sitecore.Data.Items.Item item, [NotNull] global::Sitecore.Data.Items.TemplateItem templateToCompare)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null || templateToCompare == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return false;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            if (item.TemplateID == templateToCompare.ID)
            {
                return true;
            }

            var itemTemplate = item.Database.Templates[item.TemplateID];

            if (itemTemplate == null)
            {
                return false;
            }

            return itemTemplate.ID == templateToCompare.ID || TemplateIsDerived(itemTemplate, templateToCompare);
        }

        /// <summary>
        /// Check to see if the current item is a template.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <returns>True if the item is a template, false if not a template or item is null.</returns>
        public static bool IsTemplate([NotNull] this global::Sitecore.Data.Items.Item item)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            return item != null && item.Database.Engines.TemplateEngine.IsTemplatePart(item);
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
        }

        /// <summary>
        /// Get all child items that are derived from a particular template.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <param name="templateId">Template Id of desired items.</param>
        /// <returns>All children that have the supplied template.</returns>
        [NotNull]
        public static IEnumerable<global::Sitecore.Data.Items.Item> ChildrenDerivedFrom([NotNull] this global::Sitecore.Data.Items.Item item, [NotNull] global::Sitecore.Data.ID templateId)
        {
            var childrenDerivedFrom = new List<Item>();
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var children = item.GetChildren();

                foreach (Item child in children)
                {
                    if (child.IsDerived(templateId))
                    {
                        childrenDerivedFrom.Add(child);
                    }
                }
            }

            return childrenDerivedFrom;
        }

        /// <summary>
        /// Determines if the current item is in the context site.
        /// </summary>
        /// <param name="context">The context item.</param>
        /// <returns>True if item is in the current site, false if not or item is null.</returns>
        public static bool IsInContextSite([NotNull] this global::Sitecore.Data.Items.Item context)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (context == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return false;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            Assert.IsNotNull(
                global::Sitecore.Context.Site,
                "Sitecore.Context.Site required by the Item.IsInContextSite extension is null");

            Assert.IsNotNullOrEmpty(
                global::Sitecore.Context.Site.RootPath,
                "Sitecore.Context.Site.RootPath required by the Item.IsInSite extension is null or empty");

            Assert.IsNotNull(
                global::Sitecore.Context.Database,
                "Sitecore.Context.Database required by the Item.IsInSite extension is null");

            var rootItem = global::Sitecore.Context.Site.Database.Items[global::Sitecore.Context.Site.RootPath];

            Assert.IsNotNull(
                rootItem,
                string.Format(CultureInfo.CurrentCulture, "Unable to retrieve the item at path {0} using the database {1}", global::Sitecore.Context.Site.RootPath, global::Sitecore.Context.Database.Name));

            return rootItem.Axes.IsAncestorOf(context);
        }

        /// <summary>
        /// If a child item does not exist, create it.
        /// </summary>
        /// <param name="parent">The parent item.</param>
        /// <param name="name">The name of the item to find or create.</param>
        /// <param name="templateId">The template to use if creating a new item.</param>
        /// <returns>An item matching the name specified under the parent item.</returns>
        [NotNull]
        public static global::Sitecore.Data.Items.Item FindOrCreateChildItem([NotNull] this global::Sitecore.Data.Items.Item parent, [NotNull] string name, [NotNull] global::Sitecore.Data.ID templateId)
        {
            Assert.IsNotNull(parent, "parent cannot be null.");

            Assert.IsNotNullOrEmpty(name, "name cannot be null or empty.");

            Assert.IsNotNull(templateId, "templateId cannot be null.");

            var child = parent.Database.GetItem(parent.Paths.FullPath + "/" + name);

            if (child == null)
            {
                name = ItemUtil.ProposeValidItemName(name);

                child = parent.Add(name, new TemplateID(templateId));
            }

            return child;
        }

        /// <summary>
        /// Determine if a particular template is derived from a specific template.
        /// </summary>
        /// <param name="template">The template being examined.</param>
        /// <param name="templateToCompare">The proposed ancestor.</param>
        /// <returns>True if template matches.</returns>
        private static bool TemplateIsDerived([NotNull] global::Sitecore.Data.Items.TemplateItem template, [NotNull] global::Sitecore.Data.Items.TemplateItem templateToCompare)
        {
            var result = false;

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (template != null && templateToCompare != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var baseTemplate in template.BaseTemplates)
                {
                    if (baseTemplate.ID == templateToCompare.ID)
                    {
                        result = true;
                        break;
                    }

                    result = TemplateIsDerived(baseTemplate, templateToCompare);
                }
            }

            return result;
        }
    }
}