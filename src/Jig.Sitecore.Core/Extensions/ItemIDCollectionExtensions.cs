﻿namespace Jig.Sitecore
{
    using System.Collections.Generic;

    using Jig.Sitecore.Data;

    using global::Sitecore;

    /// <summary>
    /// Item Id extensions
    /// </summary>
    public static class ItemIdCollectionExtensions
    {
        /// <summary>
        /// Gets a collection of <see cref="IStandardTemplate" /> items.
        /// </summary>
        /// <param name="targetIDs">The target i ds.</param>
        /// <param name="database">The database.</param>
        /// <param name="language">The language.</param>
        /// <returns>A collection of IStandardTemplateItem, the collection may be empty.</returns>
        [NotNull]
        // ReSharper disable ParameterTypeCanBeEnumerable.Global
        public static IList<IStandardTemplate> GetTargetItems([NotNull] this IList<global::Sitecore.Data.ID> targetIDs, [NotNull] global::Sitecore.Data.Database database, [NotNull] global::Sitecore.Globalization.Language language)
        // ReSharper restore ParameterTypeCanBeEnumerable.Global
        {
            var list = new List<IStandardTemplate>();

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (targetIDs != null && database != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                // ReSharper disable LoopCanBeConvertedToQuery
                foreach (var id in targetIDs)
                // ReSharper restore LoopCanBeConvertedToQuery
                {
                    var item = database.GetItem(id, language);

                    if (item != null && item.Versions.Count != 0)
                    {
                        list.Add(item.AsStronglyTyped(language));
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Gets a collection TItem based on the field's target items. Only Items that are of the
        /// supplied Type are returned.
        /// </summary>
        /// <typeparam name="TItem">The Item type to filter for.</typeparam>
        /// <param name="targetIDs">The target i ds.</param>
        /// <param name="database">The database.</param>
        /// <param name="language">The language.</param>
        /// <returns>A collection of TItem. The collection  may be empty.</returns>
        [NotNull]
        // ReSharper disable once ParameterTypeCanBeEnumerable.Global
        public static IList<TItem> GetTargetItems<TItem>([NotNull]this IEnumerable<global::Sitecore.Data.ID> targetIDs, [NotNull] global::Sitecore.Data.Database database, [NotNull] global::Sitecore.Globalization.Language language) where TItem : class, IStandardTemplate
        // ReSharper restore ParameterTypeCanBeEnumerable.Global
        {
            var list = new List<TItem>();

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (targetIDs != null && database != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                // ReSharper disable LoopCanBeConvertedToQuery
                foreach (var id in targetIDs)
                // ReSharper restore LoopCanBeConvertedToQuery
                {
                    global::Sitecore.Data.Items.Item item = database.GetItem(id, language);

                    if (item != null && item.Versions.Count != 0)
                    {
                        var targetItem = item.As<TItem>(language);
                        if (targetItem != null)
                        {
                            list.Add(targetItem);
                        }
                    }
                }
            }

            return list;
        }
    }
}
