﻿namespace Jig.Sitecore
{
    using System.Collections.Generic;
    using System.Linq;
    using Jig.Sitecore.Data;
    using global::Sitecore;

    /// <summary>
    /// Allows for the conversion of native Sitecore Item arrays into
    /// strongly typed versions for easier programming.
    /// </summary>
    public static class ItemCollectionExtensions
    {
        /// <summary>
        /// Given an array of Items, returns an enumerable list of
        /// strongly typed items.
        /// </summary>
        /// <typeparam name="TItem">
        /// The strongly-typed class or interface to convert the supplied Items to.
        /// </typeparam>
        /// <param name="items">
        /// The original array.
        /// </param>
        /// <returns>
        /// An enumerable list of strongly typed items in the supplied type parameter.
        /// </returns>
        [NotNull]
        public static IEnumerable<TItem> As<TItem>(
            [NotNull] this IEnumerable<global::Sitecore.Data.Items.Item> items)
            where TItem : class, IStandardTemplate
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (items == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return Enumerable.Empty<TItem>();
            }

            /* ReSharper restore HeuristicUnreachableCode */

            var results = items.Select(i => i.As<TItem>()).Where(x => x != null && x.Versions.Count != 0).ToArray();
            return results;
        }

        /// <summary>
        /// Given an array of Items, returns an enumerable list of
        /// strongly typed items.
        /// </summary>
        /// <typeparam name="TItem">
        /// The strongly-typed class or interface to convert the supplied Items to.
        /// </typeparam>
        /// <param name="items">
        /// The original array.
        /// </param>
        /// <param name="language">
        /// The language of the strongly typed items in the returned list.
        /// </param>
        /// <returns>
        /// An enumerable list of strongly typed items in the supplied type parameter. Some Items may be null or empty.
        /// </returns>
        [NotNull]
        public static IEnumerable<TItem> As<TItem>(
            [NotNull] this IEnumerable<global::Sitecore.Data.Items.Item> items,
            [NotNull] global::Sitecore.Globalization.Language language)
            where TItem : class, IStandardTemplate
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (items == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return Enumerable.Empty<TItem>();
            }

            /* ReSharper restore HeuristicUnreachableCode */

            var results = items.Select(i => i.As<TItem>(language)).Where(x => x != null && x.Versions.Count != 0).ToArray();
            return results;
        }

        /// <summary>
        /// Given an array of Items, returns an enumerable list of
        /// strongly typed items.
        /// </summary>
        /// <typeparam name="TItem">
        /// The strongly-typed class or interface to convert the supplied Items to.
        /// </typeparam>
        /// <param name="items">
        /// The original array.
        /// </param>
        /// <returns>
        /// An enumerable list of strongly typed items in the supplied type parameter.
        /// </returns>
        [NotNull]
        public static IEnumerable<TItem> As<TItem>([NotNull] this global::Sitecore.Collections.ChildList items)
            where TItem : class, IStandardTemplate
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (items == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return Enumerable.Empty<TItem>();
            }

            /* ReSharper restore HeuristicUnreachableCode */

            var results = items.Select(i => i.As<TItem>()).Where(x => x != null && x.Versions.Count != 0).ToArray();
            return results;
        }

        /// <summary>
        /// Given an array of Items, returns an enumerable list of
        /// strongly typed items.
        /// </summary>
        /// <typeparam name="TItem">
        /// The strongly-typed class or interface to convert the supplied Items to.
        /// </typeparam>
        /// <param name="items">
        /// The original array.
        /// </param>
        /// <param name="language">
        /// The language of the strongly typed items in the returned list.
        /// </param>
        /// <returns>
        /// An enumerable list of strongly typed items in the supplied type parameter. Some Items may be null or empty.
        /// </returns>
        [NotNull]
        public static IEnumerable<TItem> As<TItem>(
            [NotNull] this global::Sitecore.Collections.ChildList items,
            [NotNull] global::Sitecore.Globalization.Language language)
            where TItem : class, IStandardTemplate
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (items == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return Enumerable.Empty<TItem>();
            }

            /* ReSharper restore HeuristicUnreachableCode */

            var results = items.Select(i => i.As<TItem>(language)).Where(x => x != null && x.Versions.Count != 0).ToArray();
            return results;
        }
    }
}
