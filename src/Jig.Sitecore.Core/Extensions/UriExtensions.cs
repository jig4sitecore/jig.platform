﻿namespace Jig.Sitecore
{
    using System;
    using global::Sitecore;
    using global::Sitecore.Text;

    /// <summary>
    /// Class UriExtensions.
    /// </summary>
    public static class UriExtensions
    {
        /// <summary>
        /// Matches the URI host.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="hostNamesWithWildcard">The host name with wildcard.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool MatchUriHost([NotNull] this Uri uri, [NotNull] string hostNamesWithWildcard)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (uri == null || string.IsNullOrWhiteSpace(hostNamesWithWildcard))
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                return false;
            }

            var hostnames = hostNamesWithWildcard.Split(new[] { '|', ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var hostNameWithWildcard in hostnames)
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                var hostNameWithWildcardParts = WildCardParser.GetParts(hostNameWithWildcard);
                var match = WildCardParser.Matches(uri.Host, hostNameWithWildcardParts);
                if (match)
                {
                    return true;
                }
            }

            return false;
        }
    }
}