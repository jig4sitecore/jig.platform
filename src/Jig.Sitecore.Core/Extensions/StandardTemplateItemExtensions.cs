﻿namespace Jig.Sitecore
{
    using Jig.Sitecore.Data;
    using global::Sitecore;

    /// <summary>
    /// The item extensions.
    /// </summary>
    public static partial class StandardTemplateItemExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Converts an Item into a strongly typed Item descending from
        /// IStandardTemplateItem. Will return null if there is no class
        /// defined for the Item's TemplateID.
        /// </summary>
        /// <typeparam name="TItem">The Type of object to create, must descend from IStandardTemplateItem.</typeparam>
        /// <param name="item">The Item to convert.</param>
        /// <returns>
        /// An instance of TItem or null if conversion fails.
        /// </returns>
        [CanBeNull]
        public static TItem As<TItem>([NotNull] this global::Sitecore.Data.Items.Item item) where TItem : class, IStandardTemplate
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null || item.Versions.Count == 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return null;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            var instance = item.AsStronglyTyped() as TItem;

            return instance;
        }

        /// <summary>
        /// Converts an Item into a strongly typed Item descending from
        /// IStandardTemplateItem. Will return null if there is no class
        /// defined for the Item's TemplateID.
        /// </summary>
        /// <typeparam name="TItem">The Type of object to create, must descend from IStandardTemplateItem.</typeparam>
        /// <param name="item">The Item to convert.</param>
        /// <param name="language">The language.</param>
        /// <returns>
        /// An instance of TItem or null if conversion fails.
        /// </returns>
        [CanBeNull]
        public static TItem As<TItem>([NotNull] this global::Sitecore.Data.Items.Item item, [NotNull] global::Sitecore.Globalization.Language language) where TItem : class, IStandardTemplate
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null || item.Versions.Count == 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return null;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            var instance = item.AsStronglyTyped(language) as TItem;

            return instance;
        }

        /// <summary>
        /// Converts an Item into a strongly typed Item descending from
        /// IStandardTemplateItem. Will return null if there is no class
        /// defined for the Item's TemplateID.
        /// </summary>
        /// <param name="item">The Item to convert.</param>
        /// <returns>
        /// An instance of IStandardTemplateItem or null.
        /// </returns>
        [CanBeNull]
        public static IStandardTemplate AsStronglyTyped([NotNull] this global::Sitecore.Data.Items.Item item)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null || item.Versions.Count == 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return null;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            var instance = ItemFactory.GetStronglyTypedItem(item);
            return instance;
        }

        /// <summary>
        /// Converts an Item into a strongly typed Item descending from
        /// IStandardTemplateItem. Will return null if there is no class
        /// defined for the Item's TemplateID.
        /// </summary>
        /// <param name="item">
        /// The Item to convert.
        /// </param>
        /// <param name="language">
        /// The language.
        /// </param>
        /// <returns>
        /// An instance of IStandardTemplateItem or null.
        /// </returns>
        [CanBeNull]
        public static IStandardTemplate AsStronglyTyped([NotNull] this global::Sitecore.Data.Items.Item item, [NotNull] global::Sitecore.Globalization.Language language)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null || item.Versions.Count == 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return null;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            var instance = ItemFactory.GetStronglyTypedItem(item.GetBestFitLanguageVersion(language));
            return instance;
        }

        /// <summary>
        /// Gets the friendly URL of the item.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <returns>The url of the current item.</returns>
        [NotNull]
        public static string GetUrl([NotNull] this IStandardTemplate item)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return item.InnerItem.GetUrl();
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the friendly URL of the item.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <param name="language">The language.</param>
        /// <returns>The url of the current item.</returns>
        [NotNull]
        public static string GetUrl([NotNull] this IStandardTemplate item, global::Sitecore.Globalization.Language language)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return item.InnerItem.GetUrl(language);
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the friendly URL of the item.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <param name="urlOptions">The URL options.</param>
        /// <returns>The url of the current item.</returns>
        [NotNull]
        public static string GetUrl([NotNull] this IStandardTemplate item, global::Sitecore.Links.UrlOptions urlOptions)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return item.InnerItem.GetUrl(urlOptions);
            }

            return string.Empty;
        }

        #endregion
    }
}