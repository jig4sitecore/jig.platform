﻿namespace Jig.Sitecore.Context
{
    using System;
    using System.Linq;
    using System.Web;
    using global::Sitecore;

    /// <summary>
    /// Context validation extensions.
    /// </summary>
    public static class DatabasesToIgnoreExtensions
    {
        /// <summary>
        /// Determines whether [is match databases to ignore] [the specified request].
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="database">The database.</param>
        /// <returns><c>true</c> if [is match databases to ignore] [the specified database]; otherwise, <c>false</c>.</returns>
        /// <exception cref="System.Web.HttpException">The target cannot be null!
        /// or
        /// The request cannot be null!</exception>
        public static bool IsMatchDatabasesToIgnore(
                                   [NotNull] this IDatabasesToIgnore target,
                                   [NotNull] global::Sitecore.Data.Database database)
        {
            if (target == null)
            {
                throw new HttpException("The target cannot be null!");
            }

            if (database == null)
            {
                throw new HttpException("The database cannot be null!");
            }

            var list = target.DatabasesToIgnore.ConvertToList();

            var result = list.Any(x => string.Equals(x, database.Name, StringComparison.InvariantCultureIgnoreCase));

            return result;
        }
    }
}