﻿namespace Jig.Sitecore
{
    using global::Sitecore;
    using global::Sitecore.Configuration;
    using global::Sitecore.Links;

    /// <summary>
    /// Item Url Extensions
    /// </summary>
    public static class ItemUrlExtensions
    {
        /// <summary>
        /// Gets the friendly URL of the item.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <returns>The url of the current item.</returns>
        [NotNull]
        public static string GetUrl([NotNull] this global::Sitecore.Data.Items.Item item)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var urlOptions = global::Sitecore.Context.Site.GetDefaultUrlOptions();

                urlOptions.SiteResolving = Settings.Rendering.SiteResolving;

                var url = LinkManager.GetItemUrl(item, urlOptions);
                return url ?? string.Empty;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the friendly URL of the item.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <param name="language">The language.</param>
        /// <returns>The url of the current item.</returns>
        [NotNull]
        public static string GetUrl([NotNull] this global::Sitecore.Data.Items.Item item, global::Sitecore.Globalization.Language language)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var urlOptions = global::Sitecore.Context.Site.GetDefaultUrlOptions();

                urlOptions.SiteResolving = Settings.Rendering.SiteResolving;
                urlOptions.Language = language;

                var url = LinkManager.GetItemUrl(item, urlOptions);
                return url ?? string.Empty;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the friendly URL of the item.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <param name="urlOptions">The URL options.</param>
        /// <returns>The url of the current item.</returns>
        [NotNull]
        public static string GetUrl([NotNull] this global::Sitecore.Data.Items.Item item, global::Sitecore.Links.UrlOptions urlOptions)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var url = LinkManager.GetItemUrl(item, urlOptions);
                return url ?? string.Empty;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the friendly URL of the item.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <returns>The url of the current item.</returns>
        [NotNull]
        public static string GetUrl([NotNull] this global::Sitecore.Data.Items.CustomItemBase item)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null && item.InnerItem != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return GetUrl(item.InnerItem);
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the friendly URL of the item.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <param name="urlOptions">The URL options.</param>
        /// <returns>The url of the current item.</returns>
        [NotNull]
        public static string GetUrl([NotNull] this global::Sitecore.Data.Items.CustomItemBase item, global::Sitecore.Links.UrlOptions urlOptions)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null && item.InnerItem != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return GetUrl(item.InnerItem, urlOptions);
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the friendly URL of the item.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <param name="language">The language.</param>
        /// <returns>The url of the current item.</returns>
        [NotNull]
        public static string GetUrl([NotNull] this global::Sitecore.Data.Items.CustomItemBase item, global::Sitecore.Globalization.Language language)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null && item.InnerItem != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return GetUrl(item.InnerItem, language);
            }

            return string.Empty;
        }
    }
}
