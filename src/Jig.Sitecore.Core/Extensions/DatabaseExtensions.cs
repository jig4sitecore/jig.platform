﻿namespace Jig.Sitecore
{
    using System;
    using System.Diagnostics.Contracts;

    using global::Sitecore;

    using global::Sitecore.Data;

    using global::Sitecore.Data.Items;

    /// <summary>
    /// Class DatabaseExtensions.
    /// </summary>
    public static class DatabaseExtensions
    {
        /// <summary>
        /// Gets the template.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="type">The type.</param>
        /// <returns>Template Item.</returns>
        [CanBeNull]
        public static TemplateItem GetTemplate([NotNull] this Database database, [NotNull] Type type)
        {
            Contract.Assume(null != database, "database != null");
            
            var template = database.GetTemplate(new ID(type.GUID));

            return template;
        }

        /// <summary>
        /// Gets the template.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>Template Item.</returns>
        [CanBeNull]
        public static TemplateItem GetTemplate([NotNull] this Database database, Guid id)
        {
            Contract.Assume(null != database, "database != null");

            var template = database.GetTemplate(new ID(id));

            return template;
        }
    }
}
