﻿namespace Jig.Sitecore.Context
{
    using System;
    using System.Linq;
    using System.Web;
    using global::Sitecore;
    using global::Sitecore.Sites;

    /// <summary>
    /// Class UrlRequestPathsToIgnoreExtensions.
    /// </summary>
    public static class UrlRequestPathsToIgnoreExtensions
    {
        /// <summary>
        /// Context validation extensions.
        /// </summary>
        /// <param name="target">The object that requires evaluation.</param>
        /// <param name="siteRequest">The site request.</param>
        /// <returns><c>true</c> if any site name match, <c>false</c> otherwise.</returns>
        public static bool IsMatchUrlRequestPathsToIgnore(
            [NotNull] this IUrlRequestPathsToIgnore target,
            [NotNull] SiteRequest siteRequest)
        {
            if (target == null)
            {
                throw new HttpException("The target cannot be null!");
            }

            if (siteRequest == null)
            {
                throw new HttpException("The siteRequest cannot be null!");
            }

            var list = target.UrlRequestPathsToIgnore.ConvertToList();

            var result = list.Any(x => siteRequest.FilePath.StartsWith(x, StringComparison.InvariantCultureIgnoreCase));

            return result;
        }
    }
}