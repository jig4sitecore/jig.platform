﻿namespace Jig.Sitecore.Pipelines.Configuration
{
    using System;

    /// <summary>
    /// Class EnvironmentVariableSetting. This class cannot be inherited.
    /// </summary>
    public sealed class EnvironmentVariableSetting : IConfigurationSetting
    {
        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process(ConfigurationSettingPipelineArgs args)
        {
            if (!args.IsSettingValueResolved)
            {
                var value = Environment.GetEnvironmentVariable(args.SettingName);
                args.SettingValue = value;
            }
        }
    }
}
