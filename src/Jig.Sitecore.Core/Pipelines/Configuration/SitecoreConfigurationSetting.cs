﻿namespace Jig.Sitecore.Pipelines.Configuration
{
    /// <summary>
    /// The sitecore setting.
    /// </summary>
    public sealed class SitecoreConfigurationSetting : IConfigurationSetting
    {
        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process(ConfigurationSettingPipelineArgs args)
        {
            if (!args.IsSettingValueResolved)
            {
                var value = global::Sitecore.Configuration.Settings.GetSetting(args.SettingName);
                args.SettingValue = value;
            }
        }
    }
}
