﻿namespace Jig.Sitecore.Pipelines.Configuration
{
    using System.Web.Configuration;

    /// <summary>
    /// The app setting.
    /// </summary>
    public sealed class WebConfigurationManagerAppSetting : IConfigurationSetting
    {
        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process(ConfigurationSettingPipelineArgs args)
        {
            if (!args.IsSettingValueResolved)
            {
                var value = WebConfigurationManager.AppSettings[args.SettingName];
                args.SettingValue = value;
            }
        }
    }
}
