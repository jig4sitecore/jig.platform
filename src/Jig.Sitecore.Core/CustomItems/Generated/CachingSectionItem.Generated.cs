#pragma warning disable 1591
#pragma warning disable 0108
namespace Jig.Sitecore.CustomItems
{
    using System.Linq;
    using global::Sitecore;
    [System.Runtime.InteropServices.Guid("e8d2dd19-1347-4562-ae3f-310dc0b21a6c")]
    [System.Diagnostics.DebuggerStepThrough]
    public partial class CachingSectionItem : global::Jig.Sitecore.CustomItems.CustomItem
    {
        protected CachingSectionItem ([NotNull] global::Sitecore.Data.Items.Item innerItem)
            : base(innerItem)
        {
            /* 
             * All Sitecore or Custom fields must have implicit operator.
             * 'static implicit operator {Type}(Field field)' 
             */
        }
        public global::Sitecore.Data.Fields.CheckboxField Cacheable
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Cacheable];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField ClearOnIndexUpdate
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.ClearOnIndexUpdate];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField VaryByData
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.VaryByData];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField VaryByDevice
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.VaryByDevice];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField VaryByLogin
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.VaryByLogin];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField VaryByParm
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.VaryByParm];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField VaryByQueryString
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.VaryByQueryString];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField VaryByUser
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.VaryByUser];
                return field;
            }
        }
        [CanBeNull]
        public static implicit operator CachingSectionItem ([NotNull] global::Sitecore.Data.Items.Item item)
        {
            /* DESIGN: Should not loop in the base templates too deep */
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            var typeId = typeof(CachingSectionItem).GUID;
            if (item != null && (item.TemplateID.Guid == typeId || item.Template.BaseTemplates.Any(x => x.ID.Guid == typeId)))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return new CachingSectionItem (item);
            }
            return null;
        }
        public static partial class FieldNames 
        {
          public static readonly global::Sitecore.Data.ID Cacheable = new global::Sitecore.Data.ID("3d08db46-2267-41b0-bc52-be69fd618633");
          public static readonly global::Sitecore.Data.ID ClearOnIndexUpdate = new global::Sitecore.Data.ID("f3e7e552-d7c8-469b-a150-69e4e14ab35c");
          public static readonly global::Sitecore.Data.ID VaryByData = new global::Sitecore.Data.ID("8b6d532b-6128-4486-a044-ca06d90948ba");
          public static readonly global::Sitecore.Data.ID VaryByDevice = new global::Sitecore.Data.ID("c98cf969-ba71-42da-833d-b3fc1368ba27");
          public static readonly global::Sitecore.Data.ID VaryByLogin = new global::Sitecore.Data.ID("8d9232b0-613f-440b-a2fa-dcdd80fbd33e");
          public static readonly global::Sitecore.Data.ID VaryByParm = new global::Sitecore.Data.ID("3ad2506a-dc39-4b1e-959f-9d524addbf50");
          public static readonly global::Sitecore.Data.ID VaryByQueryString = new global::Sitecore.Data.ID("1084d3d2-0457-456a-abbc-eb4cc0966072");
          public static readonly global::Sitecore.Data.ID VaryByUser = new global::Sitecore.Data.ID("0e54a8dc-72ad-4372-a7c7-bb4773fad44d");
        }
    }
}
