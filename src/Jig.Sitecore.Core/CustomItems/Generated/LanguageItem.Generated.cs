#pragma warning disable 1591
#pragma warning disable 0108
namespace Jig.Sitecore.CustomItems
{
    using System.Linq;
    using global::Sitecore;
    [System.Runtime.InteropServices.Guid("f68f13a6-3395-426a-b9a1-fa2dc60d94eb")]
    [System.Diagnostics.DebuggerStepThrough]
    public partial class LanguageItem : global::Jig.Sitecore.CustomItems.CustomItem
    {
        protected LanguageItem ([NotNull] global::Sitecore.Data.Items.Item innerItem)
            : base(innerItem)
        {
            /* 
             * All Sitecore or Custom fields must have implicit operator.
             * 'static implicit operator {Type}(Field field)' 
             */
        }
        public global::Sitecore.Data.Fields.TextField Charset
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Charset];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField CodePage
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.CodePage];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Dictionary
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Dictionary];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Encoding
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Encoding];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.ValueLookupField FallbackLanguage
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.FallbackLanguage];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Iso
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Iso];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField RegionalIsoCode
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.RegionalIsoCode];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField WorldLingoLanguageIdentifier
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.WorldLingoLanguageIdentifier];
                return field;
            }
        }
        [CanBeNull]
        public static implicit operator LanguageItem ([NotNull] global::Sitecore.Data.Items.Item item)
        {
            /* DESIGN: Should not loop in the base templates too deep */
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            var typeId = typeof(LanguageItem).GUID;
            if (item != null && (item.TemplateID.Guid == typeId || item.Template.BaseTemplates.Any(x => x.ID.Guid == typeId)))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return new LanguageItem (item);
            }
            return null;
        }
        public static partial class FieldNames 
        {
          public static readonly global::Sitecore.Data.ID Charset = new global::Sitecore.Data.ID("49e5e8f3-ed4a-4a06-abe1-b9408951dee9");
          public static readonly global::Sitecore.Data.ID CodePage = new global::Sitecore.Data.ID("990596ce-0024-43f3-bf4c-a991bfa69b45");
          public static readonly global::Sitecore.Data.ID Dictionary = new global::Sitecore.Data.ID("60669e54-7b9c-4b55-a0c4-8f25059d8b94");
          public static readonly global::Sitecore.Data.ID Encoding = new global::Sitecore.Data.ID("8728d8ff-66d9-40c2-8b34-c4fc1466942e");
          public static readonly global::Sitecore.Data.ID FallbackLanguage = new global::Sitecore.Data.ID("892975ac-496f-4ac9-8826-087095c68e1d");
          public static readonly global::Sitecore.Data.ID Iso = new global::Sitecore.Data.ID("c437e416-8948-427d-a982-8ed37ae3f553");
          public static readonly global::Sitecore.Data.ID RegionalIsoCode = new global::Sitecore.Data.ID("0620f810-9294-4f14-af9f-f5772efca0b2");
          public static readonly global::Sitecore.Data.ID WorldLingoLanguageIdentifier = new global::Sitecore.Data.ID("4cb05a97-3bf9-47fe-a835-6088e592fde6");
        }
    }
}
