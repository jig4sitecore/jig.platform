#pragma warning disable 1591
#pragma warning disable 0108
namespace Jig.Sitecore.CustomItems
{
    using System.Linq;
    using global::Sitecore;
    [System.Runtime.InteropServices.Guid("455a3e98-a627-4b40-8035-e683a0331ac7")]
    [System.Diagnostics.DebuggerStepThrough]
    public partial class TemplateFieldItem : global::Jig.Sitecore.CustomItems.CustomItem
    {
        protected TemplateFieldItem ([NotNull] global::Sitecore.Data.Items.Item innerItem)
            : base(innerItem)
        {
            /* 
             * All Sitecore or Custom fields must have implicit operator.
             * 'static implicit operator {Type}(Field field)' 
             */
        }
        public global::Sitecore.Data.Fields.CheckboxField Blob
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Blob];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField DefaultValue
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.DefaultValue];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField EnableLanguageFallback
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.EnableLanguageFallback];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField EnableSharedLanguageFallback
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.EnableSharedLanguageFallback];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField ExcludeFromTextSearch
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.ExcludeFromTextSearch];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField IgnoreDictionaryTranslations
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.IgnoreDictionaryTranslations];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField IsDisplayedInSearchResults
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.IsDisplayedInSearchResults];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.MultilistField PageEditorButtons
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.PageEditorButtons];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.MultilistField QuickActionBar
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.QuickActionBar];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField ResetBlank
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.ResetBlank];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField Shared
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Shared];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TemplateFieldSourceField Source
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Source];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Title
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Title];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.GroupedDroplistField Type
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Type];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField Unversioned
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Unversioned];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.MultilistField ValidateButton
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.ValidateButton];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Validation
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Validation];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField ValidationText
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.ValidationText];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.MultilistField ValidatorBar
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.ValidatorBar];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.MultilistField Workflow
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Workflow];
                return field;
            }
        }
        [CanBeNull]
        public static implicit operator TemplateFieldItem ([NotNull] global::Sitecore.Data.Items.Item item)
        {
            /* DESIGN: Should not loop in the base templates too deep */
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            var typeId = typeof(TemplateFieldItem).GUID;
            if (item != null && (item.TemplateID.Guid == typeId || item.Template.BaseTemplates.Any(x => x.ID.Guid == typeId)))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return new TemplateFieldItem (item);
            }
            return null;
        }
        public static partial class FieldNames 
        {
          public static readonly global::Sitecore.Data.ID Blob = new global::Sitecore.Data.ID("ff8a2d01-8a77-4f1b-a966-65806993cd31");
          public static readonly global::Sitecore.Data.ID DefaultValue = new global::Sitecore.Data.ID("b118496a-0f78-4a27-b55f-0a6c4b0b0fe1");
          public static readonly global::Sitecore.Data.ID EnableLanguageFallback = new global::Sitecore.Data.ID("fa622538-0c13-4130-a001-45984241aa00");
          public static readonly global::Sitecore.Data.ID EnableSharedLanguageFallback = new global::Sitecore.Data.ID("24cb32f0-e364-4f37-b400-0f2899097b5b");
          public static readonly global::Sitecore.Data.ID ExcludeFromTextSearch = new global::Sitecore.Data.ID("9cd5874f-0eed-481c-8e75-ae162e613b59");
          public static readonly global::Sitecore.Data.ID IgnoreDictionaryTranslations = new global::Sitecore.Data.ID("285b7992-78df-4d0e-8f4f-53b782cf6f81");
          public static readonly global::Sitecore.Data.ID IsDisplayedInSearchResults = new global::Sitecore.Data.ID("dca13350-3ec5-408a-a190-d8fc9f63dcd1");
          public static readonly global::Sitecore.Data.ID PageEditorButtons = new global::Sitecore.Data.ID("8730c4c0-f843-425b-844b-f016ca132255");
          public static readonly global::Sitecore.Data.ID QuickActionBar = new global::Sitecore.Data.ID("337e20e1-999a-4eea-85ad-b58a03ae75cc");
          public static readonly global::Sitecore.Data.ID ResetBlank = new global::Sitecore.Data.ID("bf110aa2-f46c-4b8c-8fa5-fa4ceaccf99d");
          public static readonly global::Sitecore.Data.ID Shared = new global::Sitecore.Data.ID("be351a73-fcb0-4213-93fa-c302d8ab4f51");
          public static readonly global::Sitecore.Data.ID Source = new global::Sitecore.Data.ID("1eb8ae32-e190-44a6-968d-ed904c794ebf");
          public static readonly global::Sitecore.Data.ID Title = new global::Sitecore.Data.ID("19a69332-a23e-4e70-8d16-b2640cb24cc8");
          public static readonly global::Sitecore.Data.ID Type = new global::Sitecore.Data.ID("ab162cc0-dc80-4abf-8871-998ee5d7ba32");
          public static readonly global::Sitecore.Data.ID Unversioned = new global::Sitecore.Data.ID("39847666-389d-409b-95bd-f2016f11eed5");
          public static readonly global::Sitecore.Data.ID ValidateButton = new global::Sitecore.Data.ID("21828437-ea4b-40a1-8c61-4ce60ea41db6");
          public static readonly global::Sitecore.Data.ID Validation = new global::Sitecore.Data.ID("074f44ca-359a-4c13-b3aa-4a6be2a675b1");
          public static readonly global::Sitecore.Data.ID ValidationText = new global::Sitecore.Data.ID("b12e4906-b96b-495e-b343-cd2e92dc6347");
          public static readonly global::Sitecore.Data.ID ValidatorBar = new global::Sitecore.Data.ID("9c903e29-650d-4af2-b9bd-526d5c14a1a5");
          public static readonly global::Sitecore.Data.ID Workflow = new global::Sitecore.Data.ID("53c432c4-7122-4e2d-8296-db4184fd1735");
        }
    }
}
