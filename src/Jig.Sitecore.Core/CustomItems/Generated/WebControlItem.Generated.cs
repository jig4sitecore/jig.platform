#pragma warning disable 1591
#pragma warning disable 0108
namespace Jig.Sitecore.CustomItems
{
    using System.Linq;
    using global::Sitecore;
    [System.Runtime.InteropServices.Guid("1dde3f02-0bd7-4779-867a-dc578adf91ea")]
    [System.Diagnostics.DebuggerStepThrough]
    public partial class WebControlItem : global::Jig.Sitecore.CustomItems.CustomItem
    {
        protected WebControlItem ([NotNull] global::Sitecore.Data.Items.Item innerItem)
            : base(innerItem)
        {
            /* 
             * All Sitecore or Custom fields must have implicit operator.
             * 'static implicit operator {Type}(Field field)' 
             */
        }
        public global::Sitecore.Data.Fields.TextField Assembly
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Assembly];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField Cacheable
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Cacheable];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField ClearOnIndexUpdate
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.ClearOnIndexUpdate];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.MultilistField CompatibleRenderings
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.CompatibleRenderings];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.LinkField CustomizePage
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.CustomizePage];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.DatasourceField DatasourceLocation
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.DatasourceLocation];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.InternalLinkField DatasourceTemplate
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.DatasourceTemplate];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.HtmlField Description
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Description];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField Editable
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Editable];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField EnableDatasourceQuery
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.EnableDatasourceQuery];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.ReferenceField LoginRendering
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.LoginRendering];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Namespace
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Namespace];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.MultilistField PageEditorButtons
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.PageEditorButtons];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Parameters
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Parameters];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.ReferenceField ParametersTemplate
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.ParametersTemplate];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Placeholder
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Placeholder];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Tag
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Tag];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField TagPrefix
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.TagPrefix];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField VaryByData
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.VaryByData];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField VaryByDevice
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.VaryByDevice];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField VaryByLogin
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.VaryByLogin];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField VaryByParm
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.VaryByParm];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField VaryByQueryString
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.VaryByQueryString];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.CheckboxField VaryByUser
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.VaryByUser];
                return field;
            }
        }
        [CanBeNull]
        public static implicit operator WebControlItem ([NotNull] global::Sitecore.Data.Items.Item item)
        {
            /* DESIGN: Should not loop in the base templates too deep */
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            var typeId = typeof(WebControlItem).GUID;
            if (item != null && (item.TemplateID.Guid == typeId || item.Template.BaseTemplates.Any(x => x.ID.Guid == typeId)))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return new WebControlItem (item);
            }
            return null;
        }
        public static partial class FieldNames 
        {
          public static readonly global::Sitecore.Data.ID Assembly = new global::Sitecore.Data.ID("c50b1f5b-26da-476a-aac6-9b734975971c");
          public static readonly global::Sitecore.Data.ID Cacheable = new global::Sitecore.Data.ID("3d08db46-2267-41b0-bc52-be69fd618633");
          public static readonly global::Sitecore.Data.ID ClearOnIndexUpdate = new global::Sitecore.Data.ID("f3e7e552-d7c8-469b-a150-69e4e14ab35c");
          public static readonly global::Sitecore.Data.ID CompatibleRenderings = new global::Sitecore.Data.ID("e441abe7-2ca3-4640-ae26-3789967925d7");
          public static readonly global::Sitecore.Data.ID CustomizePage = new global::Sitecore.Data.ID("ca07d29a-01f7-44e7-a590-6933bcb6b919");
          public static readonly global::Sitecore.Data.ID DatasourceLocation = new global::Sitecore.Data.ID("b5b27af1-25ef-405c-87ce-369b3a004016");
          public static readonly global::Sitecore.Data.ID DatasourceTemplate = new global::Sitecore.Data.ID("1a7c85e5-dc0b-490d-9187-bb1dbcb4c72f");
          public static readonly global::Sitecore.Data.ID Description = new global::Sitecore.Data.ID("4a7f5f08-125d-47a4-b849-9ec366d51928");
          public static readonly global::Sitecore.Data.ID Editable = new global::Sitecore.Data.ID("308e88f5-cd6e-4f8f-bafe-95a47dedefdc");
          public static readonly global::Sitecore.Data.ID EnableDatasourceQuery = new global::Sitecore.Data.ID("f172b787-7b88-4bd5-ae4d-6308e102ef11");
          public static readonly global::Sitecore.Data.ID LoginRendering = new global::Sitecore.Data.ID("b6adcf38-9bc1-48dd-8c0c-4c239940f888");
          public static readonly global::Sitecore.Data.ID Namespace = new global::Sitecore.Data.ID("1f424eb0-6138-4bc4-93b0-bd45ec6b1cb2");
          public static readonly global::Sitecore.Data.ID OpenPropertiesAfterAdd = new global::Sitecore.Data.ID("c2fd8eb3-41b2-4cd9-816b-c8f515980c7a");
          public static readonly global::Sitecore.Data.ID PageEditorButtons = new global::Sitecore.Data.ID("a2f5d9df-8cba-4a1d-99eb-51acb94cb057");
          public static readonly global::Sitecore.Data.ID Parameters = new global::Sitecore.Data.ID("a116f3a4-0d3a-4dd9-bd7c-0f37470be5dc");
          public static readonly global::Sitecore.Data.ID ParametersTemplate = new global::Sitecore.Data.ID("9003574a-fb95-4f3c-b17c-33893dad35a9");
          public static readonly global::Sitecore.Data.ID Placeholder = new global::Sitecore.Data.ID("6d6a0da0-f3ea-4799-9cae-ce748e0b6a24");
          public static readonly global::Sitecore.Data.ID Tag = new global::Sitecore.Data.ID("b8979e80-7623-4b3a-bfd2-3f3f05883d58");
          public static readonly global::Sitecore.Data.ID TagPrefix = new global::Sitecore.Data.ID("e2abdec6-83a6-4b68-97d1-b3cd79f10a8e");
          public static readonly global::Sitecore.Data.ID VaryByData = new global::Sitecore.Data.ID("8b6d532b-6128-4486-a044-ca06d90948ba");
          public static readonly global::Sitecore.Data.ID VaryByDevice = new global::Sitecore.Data.ID("c98cf969-ba71-42da-833d-b3fc1368ba27");
          public static readonly global::Sitecore.Data.ID VaryByLogin = new global::Sitecore.Data.ID("8d9232b0-613f-440b-a2fa-dcdd80fbd33e");
          public static readonly global::Sitecore.Data.ID VaryByParm = new global::Sitecore.Data.ID("3ad2506a-dc39-4b1e-959f-9d524addbf50");
          public static readonly global::Sitecore.Data.ID VaryByQueryString = new global::Sitecore.Data.ID("1084d3d2-0457-456a-abbc-eb4cc0966072");
          public static readonly global::Sitecore.Data.ID VaryByUser = new global::Sitecore.Data.ID("0e54a8dc-72ad-4372-a7c7-bb4773fad44d");
        }
    }
}
