#pragma warning disable 1591
#pragma warning disable 0108
namespace Jig.Sitecore.CustomItems
{
    using System.Linq;
    using global::Sitecore;
    [System.Runtime.InteropServices.Guid("4df55861-4ff6-4b20-9289-50710ec13458")]
    [System.Diagnostics.DebuggerStepThrough]
    public partial class TagItem : global::Jig.Sitecore.CustomItems.CustomItem
    {
        protected TagItem ([NotNull] global::Sitecore.Data.Items.Item innerItem)
            : base(innerItem)
        {
            /* 
             * All Sitecore or Custom fields must have implicit operator.
             * 'static implicit operator {Type}(Field field)' 
             */
        }
        public global::Sitecore.Data.Fields.TextField TagName
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.TagName];
                return field;
            }
        }
        [CanBeNull]
        public static implicit operator TagItem ([NotNull] global::Sitecore.Data.Items.Item item)
        {
            /* DESIGN: Should not loop in the base templates too deep */
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            var typeId = typeof(TagItem).GUID;
            if (item != null && (item.TemplateID.Guid == typeId || item.Template.BaseTemplates.Any(x => x.ID.Guid == typeId)))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return new TagItem (item);
            }
            return null;
        }
        public static partial class FieldNames 
        {
          public static readonly global::Sitecore.Data.ID TagName = new global::Sitecore.Data.ID("1908d171-0f14-4f7d-becd-3fef45abba11");
        }
    }
}
