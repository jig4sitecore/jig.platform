#pragma warning disable 1591
#pragma warning disable 0108
namespace Jig.Sitecore.CustomItems
{
    using System.Linq;
    using global::Sitecore;
    [System.Runtime.InteropServices.Guid("962b53c4-f93b-4df9-9821-415c867b8903")]
    [System.Diagnostics.DebuggerStepThrough]
    public partial class MediaFileItem : global::Jig.Sitecore.CustomItems.CustomItem
    {
        protected MediaFileItem ([NotNull] global::Sitecore.Data.Items.Item innerItem)
            : base(innerItem)
        {
            /* 
             * All Sitecore or Custom fields must have implicit operator.
             * 'static implicit operator {Type}(Field field)' 
             */
        }
        public global::Sitecore.Data.Fields.ReferenceField Asset1
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Asset1];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.ReferenceField Asset2
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Asset2];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.ReferenceField Asset3
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Asset3];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField CountryCode
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.CountryCode];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Description
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Description];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Extension
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Extension];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField FilePath
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.FilePath];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Format
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Format];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Keywords
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Keywords];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField LocationDescription
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.LocationDescription];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.ReferenceField MarketingAsset
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.MarketingAsset];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField MimeType
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.MimeType];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Size
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Size];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField Title
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.Title];
                return field;
            }
        }
        public global::Sitecore.Data.Fields.TextField ZipCode
        {
            get
            {
                var field = this.InnerItem.Fields[FieldNames.ZipCode];
                return field;
            }
        }
        [CanBeNull]
        public static implicit operator MediaFileItem ([NotNull] global::Sitecore.Data.Items.Item item)
        {
            /* DESIGN: Should not loop in the base templates too deep */
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            var typeId = typeof(MediaFileItem).GUID;
            if (item != null && (item.TemplateID.Guid == typeId || item.Template.BaseTemplates.Any(x => x.ID.Guid == typeId)))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return new MediaFileItem (item);
            }
            return null;
        }
        public static partial class FieldNames 
        {
          public static readonly global::Sitecore.Data.ID Asset1 = new global::Sitecore.Data.ID("c390ba94-0393-45d5-a299-0a5a8b52660f");
          public static readonly global::Sitecore.Data.ID Asset2 = new global::Sitecore.Data.ID("5f70e8f4-b06e-4256-ac4f-091cf9341f95");
          public static readonly global::Sitecore.Data.ID Asset3 = new global::Sitecore.Data.ID("d294697a-b019-452a-8a73-ad520679318f");
          public static readonly global::Sitecore.Data.ID Blob = new global::Sitecore.Data.ID("40e50ed9-ba07-4702-992e-a912738d32dc");
          public static readonly global::Sitecore.Data.ID CountryCode = new global::Sitecore.Data.ID("ff01bc1a-ef22-407c-9115-7dbee11451a5");
          public static readonly global::Sitecore.Data.ID Description = new global::Sitecore.Data.ID("ba8341a1-ff30-47b8-ae6a-f4947e4113f0");
          public static readonly global::Sitecore.Data.ID Extension = new global::Sitecore.Data.ID("c06867fe-9a43-4c7d-b739-48780492d06f");
          public static readonly global::Sitecore.Data.ID FilePath = new global::Sitecore.Data.ID("2134867a-ac67-4dac-836c-a9264fd9d6d6");
          public static readonly global::Sitecore.Data.ID Format = new global::Sitecore.Data.ID("fbd90442-db0c-48f1-a0f4-0427cd9afcce");
          public static readonly global::Sitecore.Data.ID Keywords = new global::Sitecore.Data.ID("2fafe7cb-2691-4800-8848-255efa1d31aa");
          public static readonly global::Sitecore.Data.ID Latitude = new global::Sitecore.Data.ID("5d589ec4-4842-4630-a860-540357a196f3");
          public static readonly global::Sitecore.Data.ID LocationDescription = new global::Sitecore.Data.ID("e8fa62d2-13fc-4c5c-91e5-71d615591420");
          public static readonly global::Sitecore.Data.ID Longitude = new global::Sitecore.Data.ID("11cb6358-8f71-4f80-ab30-04d745d99d56");
          public static readonly global::Sitecore.Data.ID MarketingAsset = new global::Sitecore.Data.ID("70829c03-7349-47ff-8734-295f33f15aa3");
          public static readonly global::Sitecore.Data.ID MimeType = new global::Sitecore.Data.ID("6f47a0a5-9c94-4b48-abeb-42d38def6054");
          public static readonly global::Sitecore.Data.ID Size = new global::Sitecore.Data.ID("6954b7c7-2487-423f-8600-436cb3b6dc0e");
          public static readonly global::Sitecore.Data.ID Title = new global::Sitecore.Data.ID("3f4b20e9-36e6-4d45-a423-c86567373f82");
          public static readonly global::Sitecore.Data.ID ZipCode = new global::Sitecore.Data.ID("fb540bf1-b2eb-47c9-a55f-f01b2fca5851");
        }
    }
}
