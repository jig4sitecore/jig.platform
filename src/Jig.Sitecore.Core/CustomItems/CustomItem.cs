﻿namespace Jig.Sitecore.CustomItems
{
    using System.Diagnostics;

    using Newtonsoft.Json;

    using global::Sitecore;

    using global::Sitecore.Collections;

    using global::Sitecore.Data;

    using global::Sitecore.Data.Fields;

    using global::Sitecore.Data.Items;

    /// <summary>
    /// The base class for Sitecore Shell operation. This class should not be used for website.
    /// </summary>
    [DebuggerStepThrough]
    [DebuggerDisplay("Name={Name}, ID={ID}, Language={Language}")]
    [JsonObject(MemberSerialization.OptIn)]
    public abstract class CustomItem : global::Sitecore.Data.Items.CustomItemBase, Jig.Sitecore.Data.ICustomItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomItem"/> class.
        /// </summary>
        /// <param name="innerItem">The inner item.</param>
        protected CustomItem([NotNull]global::Sitecore.Data.Items.Item innerItem)
            : base(innerItem)
        {
        }

        /// <summary>
        /// Gets the language.
        /// </summary>
        /// <value>The language.</value>
        [NotNull]
        public global::Sitecore.Globalization.Language Language
        {
            get
            {
                return this.InnerItem.Language;
            }
        }

        #region Properties

        /// <summary>
        /// Gets a value indicating whether the Item (version) is empty.
        /// </summary>
        [JsonIgnore]
        public bool Empty
        {
            get { return this.InnerItem.Empty; }
        }

        /// <summary>
        /// Gets a value indicating whether the Item has child Items.
        /// </summary>
        [JsonIgnore]
        public bool HasChildren
        {
            get { return this.InnerItem.HasChildren; }
        }

        /// <summary>
        /// Gets the help object associated with this Item.
        /// </summary>
        [NotNull]
        [JsonIgnore]
        public ItemHelp Help
        {
            get { return this.InnerItem.Help; }
        }

        /// <summary>
        /// Gets the appearance.
        /// </summary>
        /// <value>The appearance.</value>
        [NotNull]
        [JsonIgnore]
        public ItemAppearance Appearance
        {
            get
            {
                return this.InnerItem.Appearance; 
            }
        }

        /// <summary>
        /// Gets the Item's Key (lowercased Item.Name).
        /// </summary>
        [NotNull]
        [JsonIgnore]
        public string Key
        {
            get { return this.InnerItem.Key; }
        }

        /// <summary>
        /// Gets a value indicating whether the Item has been modified.
        /// </summary>
        [JsonIgnore]
        public bool Modified
        {
            get { return this.InnerItem.Modified; }
        }

        /// <summary>
        /// Gets the ID of the Item's Parent.
        /// </summary>
        [NotNull]
        [JsonIgnore]
        // ReSharper disable InconsistentNaming
        public ID ParentID
        // ReSharper restore InconsistentNaming
        {
            get { return this.InnerItem.ParentID; }
        }

        /// <summary>
        /// Gets the ID of the Item's Data Template.
        /// </summary>
        [NotNull]
        [JsonIgnore]
        // ReSharper disable InconsistentNaming
        public ID TemplateID
        // ReSharper restore InconsistentNaming
        {
            get { return this.InnerItem.TemplateID; }
        }

        /// <summary>
        /// Gets the name of the Item's Data Template.
        /// </summary>
        [NotNull]
        [JsonIgnore]
        public string TemplateName
        {
            get { return this.InnerItem.TemplateName; }
        }

        /// <summary>
        /// Gets the full path.
        /// </summary>
        /// <value>The full path.</value>
        [NotNull]
        [JsonIgnore]
        public string FullPath
        {
            get { return this.InnerItem.Paths.FullPath; }
        }

        /// <summary>
        /// Gets the help text.
        /// </summary>
        /// <value>The help text.</value>
        [NotNull]
        [JsonIgnore]
        public string HelpText
        {
            get { return this.InnerItem.Help.Text; }
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        [NotNull]
        [JsonIgnore]
        public ChildList Children
        {
            get { return this.InnerItem.Children; }
        }

        /// <summary>
        /// Gets the versions.
        /// </summary>
        /// <value>The versions.</value>
        [NotNull]
        [JsonIgnore]
        public ItemVersions Versions
        {
            get
            {
                return this.InnerItem.Versions;
            }
        }

        #endregion

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <value>The version.</value>
        [NotNull]
        [JsonIgnore]
        public global::Sitecore.Data.Version Version
        {
            get
            {
                return this.InnerItem.Version;
            }
        }

        /// <summary>
        /// The this.
        /// </summary>
        /// <param name="fieldId">The field identifier.</param>
        /// <returns>The <see cref="string" />.</returns>
        public string this[ID fieldId]
        {
            get
            {
                return this.InnerItem[fieldId];
            }

            set
            {
                this.InnerItem[fieldId] = value;
            }
        }

        /// <summary>
        /// The this.
        /// </summary>
        /// <param name="fieldIndex">Index of the field.</param>
        /// <returns>The <see cref="string" />.</returns>
        public string this[int fieldIndex]
        {
            get
            {
                return this.InnerItem[fieldIndex];
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="System.String" /> with the specified field name.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns>The field value</returns>
        public string this[string fieldName]
        {
            get
            {
                return this.InnerItem[fieldName];
            }

            set
            {
                this.InnerItem[fieldName] = value;
            }
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <returns>The inner item children</returns>
        [NotNull]
        public ChildList GetChildren()
        {
            return this.InnerItem.GetChildren();
        }

        /// <summary>
        /// Shorthand for accessing a Field from the inner item.
        /// </summary>
        /// <param name="id">The ID of the field.</param>
        /// <returns>The field with the matching ID or null.</returns>
        [CanBeNull]
        public Field Fields([NotNull] ID id)
        {
            return this.InnerItem.Fields[id];
        }

        /// <summary>
        /// Shorthand for accessing a Field from the inner item.
        /// </summary>
        /// <param name="name">The Name of the field.</param>
        /// <returns>The field with the matching name or null.</returns>
        /// <remarks>Always to use by field ID. However, sometimes are cases then using access by name is more meanfull</remarks>
        [CanBeNull]
        public Field Fields([NotNull] string name)
        {
            return this.InnerItem.Fields[name];
        }
    }
}