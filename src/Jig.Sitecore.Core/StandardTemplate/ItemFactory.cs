﻿namespace Jig.Sitecore.Data
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Reflection;
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;

    /// <summary>
    /// Translates an Item from a Sitecore object to a strongly typed object
    /// using discovered wrapper classes that map 1:1 with Sitecore data templates.
    /// </summary>
    public static class ItemFactory
    {
        #region Fields
        /// <summary>
        /// The internal list of candidate types. Do not reference this list directly.
        /// </summary>
        private static IDictionary<Guid, Type> candidateClasses;

        /// <summary>
        /// The internal list of candidate types. Do not reference this list directly.
        /// </summary>
        private static IDictionary<Guid, Type> candidateInterfaces;
        #endregion

        #region Properties
        /// <summary>
        /// Gets an initialized list of types.
        /// </summary>
        [NotNull]
        public static IDictionary<Guid, Type> CandidateClasses
        {
            get
            {
                return candidateClasses ?? (candidateClasses = CreateCandidateClassesList());
            }
        }

        /// <summary>
        /// Gets an initialized list of types.
        /// </summary>
        [NotNull]
        public static IDictionary<Guid, Type> CandidateInterfaces
        {
            get
            {
                return candidateInterfaces ?? (candidateInterfaces = CreateCandidateInterfacesList());
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the appropriate Type for a given Sitecore Template ID.
        /// </summary>
        /// <param name="id">The ID to look for</param>
        /// <returns>The Type represented by that ID</returns>
        [CanBeNull]
        public static Type GetTemplateInterfaceType(Guid id)
        {
            if (CandidateInterfaces.ContainsKey(id))
            {
                return CandidateInterfaces[id];
            }

            return null;
        }

        /// <summary>
        /// Wraps the supplied Item with a class that represents the Item's data template.
        /// </summary>
        /// <param name="item">The Item to wrap.</param>
        /// <returns>An instance of IStandardTemplateItem or null if the Item cannot be cast successfully.</returns>
        [CanBeNull]
        internal static StandardTemplate GetStronglyTypedItem([NotNull] Item item)
        {
            Assert.IsNotNull(item, "item cannot be null.");
            if (item.Versions.Count == 0)
            {
                return null;
            }

            Type type;
            if (CandidateClasses.TryGetValue(item.TemplateID.Guid, out type))
            {
                var instance = Activator.CreateInstance(type, item) as StandardTemplate;
                if (instance != null)
                {
                    return instance;
                }
            }

            return new StandardTemplate(item);
        }

        #endregion

        #region Main Logic
        /// <summary>
        /// Crawls all available assemblies looking for Classes that represent Sitecore
        /// Data Templates.
        /// </summary>
        /// <returns>A dictionary of possible classes, keyed by the Sitecore Template IDs that they match to.</returns>
        [NotNull]
        private static IDictionary<Guid, Type> CreateCandidateClassesList()
        {
            var list = new ConcurrentDictionary<Guid, Type>();

            var assemblies = GetNonSitecoreCmsAssemblies();

            foreach (Assembly assembly in assemblies)
            {
                var types = GetLoadableTypes(assembly);

                foreach (Type type in types)
                {
                    if (type.IsClass)
                    {
                        // ReSharper disable InconsistentNaming
                        ID templateID = GetTemplateIdFromAttribute(type);

                        if (!ID.IsNullOrEmpty(templateID))
                        {
                            System.Diagnostics.Trace.TraceInformation(
                                "CreateCandidateClassesList Mapping TemplateID: {0} ==> Type: {1}",
                                templateID,
                                type);

                            // ReSharper disable PossibleNullReferenceException
                            list[templateID.Guid] = type;
                            // ReSharper restore PossibleNullReferenceException
                        }
                        // ReSharper restore InconsistentNaming
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Crawls all available assemblies looking for Interfaces that represent Sitecore
        /// Data Templates.
        /// </summary>
        /// <returns>A dictionary of possible interfaces, keyed by the Sitecore Template IDs that they match to.</returns>
        [NotNull]
        private static IDictionary<Guid, Type> CreateCandidateInterfacesList()
        {
            var list = new ConcurrentDictionary<Guid, Type>();

            var assemblies = GetNonSitecoreCmsAssemblies();

            foreach (Assembly assembly in assemblies)
            {
                var types = new HashSet<Type>();

                foreach (var type in GetLoadableTypes(assembly))
                {
                    try
                    {
                        if (type.IsInterface)
                        {
                            types.Add(type);
                        }
                    }
#pragma warning disable 168
                    // ReSharper disable UnusedVariable
                    catch (ReflectionTypeLoadException e) // ReSharper restore UnusedVariable
#pragma warning restore 168
                    {
                        // We can't use that particular type.
                    }
                }

                foreach (Type type in types)
                {
                    // ReSharper disable InconsistentNaming
                    ID templateID = GetTemplateIdFromAttribute(type);

                    if (!ID.IsNullOrEmpty(templateID))
                    {
                        System.Diagnostics.Trace.TraceInformation(
                            "CreateCandidateInterfacesList Mapping TemplateID: {0} ==> Type: {1}",
                            templateID,
                            type);

                        // ReSharper disable PossibleNullReferenceException
                        list[templateID.Guid] = type;
                        // ReSharper restore PossibleNullReferenceException
                    }
                    // ReSharper restore InconsistentNaming
                }
            }

            return list;
        }

        /// <summary>
        /// The get assemblies.
        /// </summary>
        /// <returns>
        /// The collection of non sitecore assemblies
        /// </returns>
        [NotNull]
        private static IEnumerable<Assembly> GetNonSitecoreCmsAssemblies()
        {
            var assemblies =
                AppDomain.CurrentDomain.GetAssemblies()
                    .Where(
                        x =>
                        x.FullName.Contains("Data") && !x.FullName.StartsWith("System", StringComparison.OrdinalIgnoreCase))
                    .ToArray();

            return assemblies;
        }

        #endregion

        #region Helpers
        /// <summary>
        /// Inspects the attribute of the provided type to determine its 
        /// association with a Sitecore template type.
        /// </summary>
        /// <param name="type">The type to inspect.</param>
        /// <returns>The ID of the related Sitecore Template.</returns>
        [CanBeNull]
        private static ID GetTemplateIdFromAttribute([NotNull] Type type)
        {
            try
            {
                // Get the ID property
                var attributes = type.GetCustomAttributes(typeof(TemplateAttribute), false);

                if (attributes.Length > 0)
                {
                    var attribute = attributes[0] as TemplateAttribute;

                    if (attribute != null && !attribute.ID.IsNull)
                    {
                        return attribute.ID;
                    }
                }
            }
            catch
            {
                return null;
            }

            return null;
        }

        /// <summary>
        /// Inspects the provided assembly and returns only a list of types that are capable
        /// of being loaded by the current application.
        /// </summary>
        /// <remarks>
        /// See http://haacked.com/archive/2012/07/23/get-all-types-in-an-assembly.aspx for 
        /// details on why this is required.
        /// </remarks>
        /// <param name="assembly">The assembly to parse.</param>
        /// <returns>A list of loadable types.</returns>
        [NotNull]
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Stylecop hates URLs.")]
        private static IEnumerable<Type> GetLoadableTypes([NotNull] Assembly assembly)
        {
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                Log.Warn("Could not load assembly: " + assembly.FullName + Environment.NewLine, e);
                Trace.TraceError(e.ToString());
                return e.Types.Where(t => t != null);
            }
        }

        #endregion
    }
}
