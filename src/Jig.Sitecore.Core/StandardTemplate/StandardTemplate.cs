﻿namespace Jig.Sitecore.Data
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Web;
    using Newtonsoft.Json;
    using global::Sitecore;
    using global::Sitecore.Collections;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Globalization;
    using global::Sitecore.Mvc.Presentation;

    /// <summary>
    /// Represents a Sitecore Item that descends from the Standard Template.
    /// </summary>
    /// <remarks>The Standard Template doesn't implement SSitecore Custom Item to allow support to web form as well as MVC projects</remarks>
    [Guid("1930BBEB-7805-471A-A3BE-4858AC7CF696")]
    [JsonObject(MemberSerialization.OptIn)]
    [DebuggerDisplay("Name={Name}, ID={ID},  TemplateName={TemplateName}, Language={Language}")]
    public class StandardTemplate : global::Jig.Sitecore.Data.IStandardTemplate
    {
        /// <summary>
        /// The rendering
        /// </summary>
        [CanBeNull]
        private Rendering rendering;

        /// <summary>
        /// Initializes a new instance of the <see cref="StandardTemplate"/> class.
        /// </summary>
        /// <param name="item">
        /// The Item to wrap.
        /// </param>
        public StandardTemplate([NotNull] Item item)
        {
            this.InnerItem = item;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StandardTemplate"/> class.
        /// </summary>
        // ReSharper disable NotNullMemberIsNotInitialized
        protected StandardTemplate()
        // ReSharper restore NotNullMemberIsNotInitialized
        {
            // This for the Framework initialization and Sitecore MVC. Not intended to be used in code
            // The web forms would use constructor with item and MVC will initialize instance and call 
            // public void Initialize(Rendering rendering)
        }

        #region Public Properties

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        [NotNull]
        [JsonIgnore]
        public ChildList Children
        {
            get
            {
                return this.InnerItem.Children;
            }
        }

        /// <summary>
        /// Gets the database that originated the Item.
        /// </summary>
        /// <value>The database.</value>
        [NotNull]
        [JsonIgnore]
        public Database Database
        {
            get
            {
                return this.InnerItem.Database;
            }
        }

        /// <summary>
        /// Gets the Display Name for the current version and language
        /// of the Item, otherwise returns the Item.Name value.
        /// </summary>
        /// <value>The display name.</value>
        [NotNull]
        [JsonIgnore]
        public string DisplayName
        {
            get
            {
                return this.InnerItem.DisplayName;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the Item (version) is empty.
        /// </summary>
        [JsonIgnore]
        public bool Empty
        {
            get
            {
                return this.InnerItem.Empty;
            }
        }

        /// <summary>
        /// Gets the full path.
        /// </summary>
        /// <value>The full path.</value>
        [NotNull]
        [JsonIgnore]
        public string FullPath
        {
            get
            {
                return this.InnerItem.Paths.FullPath;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the Item has child Items.
        /// </summary>
        [JsonIgnore]
        public bool HasChildren
        {
            get
            {
                return this.InnerItem.HasChildren;
            }
        }

        /// <summary>
        /// Gets the help object associated with this Item.
        /// </summary>
        [NotNull]
        [JsonIgnore]
        public ItemHelp Help
        {
            get
            {
                return this.InnerItem.Help;
            }
        }

        /// <summary>
        /// Gets the help text.
        /// </summary>
        /// <value>The help text.</value>
        [NotNull]
        [JsonIgnore]
        public string HelpText
        {
            get
            {
                return this.InnerItem.Help.Text;
            }
        }

        /// <summary>
        /// Gets the id.
        /// </summary>
        [NotNull]
        [JsonIgnore]
        public ID ID
        {
            get
            {
                return this.InnerItem.ID;
            }
        }

        /// <summary>
        /// Gets or sets the internal Item.
        /// </summary>
        /// <value>The inner item.</value>
        public Item InnerItem { get; protected set; }

        /// <summary>
        /// Gets the Item's Key (lowercased Item.Name).
        /// </summary>
        [NotNull]
        [JsonIgnore]
        public string Key
        {
            get
            {
                return this.InnerItem.Key;
            }
        }

        /// <summary>
        /// Gets the language.
        /// </summary>
        /// <value>The language.</value>
        [JsonIgnore]
        [NotNull]
        public Language Language
        {
            get
            {
                return this.InnerItem.Language;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the Item has been modified.
        /// </summary>
        [JsonIgnore]
        public bool Modified
        {
            get
            {
                return this.InnerItem.Modified;
            }
        }

        /// <summary>
        /// Gets the Item's Name.
        /// </summary>
        /// <value>The name.</value>
        [NotNull]
        [JsonIgnore]
        public string Name
        {
            get
            {
                return this.InnerItem.Name;
            }
        }

        /// <summary>
        /// Gets the ID of the Item's Parent.
        /// </summary>
        [NotNull]
        [JsonIgnore]

        // ReSharper disable InconsistentNaming
        public ID ParentID
        {
            // ReSharper restore InconsistentNaming
            get
            {
                return this.InnerItem.ParentID;
            }
        }

        /// <summary>
        /// Gets the ID of the Item's Data Template.
        /// </summary>
        [NotNull]
        [JsonIgnore]

        // ReSharper disable InconsistentNaming
        public ID TemplateID
        {
            // ReSharper restore InconsistentNaming
            get
            {
                return this.InnerItem.TemplateID;
            }
        }

        /// <summary>
        /// Gets the name of the Item's Data Template.
        /// </summary>
        [NotNull]
        [JsonIgnore]
        public string TemplateName
        {
            get
            {
                return this.InnerItem.TemplateName;
            }
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <value>The version.</value>
        [NotNull]
        [JsonIgnore]
        public global::Sitecore.Data.Version Version
        {
            get
            {
                return this.InnerItem.Version;
            }
        }

        /// <summary>
        /// Gets the versions.
        /// </summary>
        /// <value>The versions.</value>
        [NotNull]
        [JsonIgnore]
        public ItemVersions Versions
        {
            get
            {
                return this.InnerItem.Versions;
            }
        }

        /// <summary>
        /// Gets the rendering.
        /// </summary>
        /// <value>The rendering.</value>
        /// <exception cref="System.InvalidOperationException">The Rendering not initialized</exception>
        [NotNull]
        public virtual Rendering Rendering
        {
            get
            {
                if (this.rendering == null)
                {
                    throw new InvalidOperationException(string.Format("{0} has not been initialized.", this.GetType()));
                }

                return this.rendering;
            }
        }

        #endregion

        #region Public Indexers

        /// <summary>
        /// The this.
        /// </summary>
        /// <param name="fieldId">
        /// The field identifier.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [NotNull]
        public virtual string this[[NotNull] ID fieldId]
        {
            get
            {
                return this.InnerItem[fieldId];
            }

            set
            {
                this.InnerItem[fieldId] = value;
            }
        }

        /// <summary>
        /// The this.
        /// </summary>
        /// <param name="fieldIndex">
        /// Index of the field.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [NotNull]
        public virtual string this[int fieldIndex]
        {
            get
            {
                return this.InnerItem[fieldIndex];
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="System.String"/> with the specified field name.
        /// </summary>
        /// <param name="fieldName">
        /// Name of the field.
        /// </param>
        /// <returns>
        /// The field value
        /// </returns>
        public virtual string this[string fieldName]
        {
            get
            {
                return this.InnerItem[fieldName];
            }

            set
            {
                this.InnerItem[fieldName] = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Provides compatibility with Sitecore Item.
        /// </summary>
        /// <param name="item">The Item to convert.</param>
        /// <returns>A new instance of IStandardTemplateItem.</returns>
        public static implicit operator StandardTemplate(Item item)
        {
            return ItemFactory.GetStronglyTypedItem(item);
        }

        /// <summary>
        /// Provides compatibility with Sitecore Item.
        /// </summary>
        /// <param name="item">The StandardTemplateItem to convert.</param>
        /// <returns>The InnerItem.</returns>
        public static implicit operator Item(StandardTemplate item)
        {
            return item.InnerItem;
        }

        /// <summary>
        /// Shorthand for accessing a Field from the inner item.
        /// </summary>
        /// <param name="id">
        /// The ID of the field.
        /// </param>
        /// <returns>
        /// The field with the matching ID or null.
        /// </returns>
        [CanBeNull]
        public virtual Field Fields([NotNull] ID id)
        {
            return this.InnerItem.Fields[id];
        }

        /// <summary>
        /// Shorthand for accessing a Field from the inner item.
        /// </summary>
        /// <param name="name">
        /// The Name of the field.
        /// </param>
        /// <returns>
        /// The field with the matching name or null.
        /// </returns>
        /// <remarks>
        /// Always to use by field ID. However, sometimes are cases then using access by name is more mean full
        /// </remarks>
        [CanBeNull]
        public virtual Field Fields([NotNull] string name)
        {
            return this.InnerItem.Fields[name];
        }

        /// <summary>
        /// Gets the ancestors.
        /// </summary>
        /// <typeparam name="TItem">The type of the t item.</typeparam>
        /// <returns>The ancestor list</returns>
        [NotNull]
        public virtual IList<TItem> GetAncestors<TItem>() where TItem : class, IStandardTemplate
        {
            var collection = this.InnerItem.Axes.GetAncestors().As<TItem>();
            return collection.ToArray();
        }

        /// <summary>
        /// Given an Item in a specific language dialect (ex: en-GB), determines if the
        /// dialect has a version, and if not, attempts to find a more generalized language (ex: en).
        /// If there is a more generalized language, returns an item instance in that more generalized
        /// language. The instance may be "empty" and should be checked using the LanguageVersionIsEmpty() extension.
        /// </summary>
        /// <param name="targetLanguage">
        /// The expected language.
        /// </param>
        /// <returns>
        /// The current language version, or a language version in a more generalized language if available.
        /// </returns>
        public virtual IStandardTemplate GetBestFitLanguageVersion([NotNull] Language targetLanguage)
        {
            return this.InnerItem.GetBestFitLanguageVersion(targetLanguage).AsStronglyTyped();
        }

        /// <summary>
        /// Given an Item in a specific language dialect (ex: en-GB), determines if the
        /// dialect has a version, and if not, attempts to find a more generalized language (ex: en).
        /// If there is a more generalized language, returns an item instance in that more generalized
        /// language. The instance may be "empty" and should be checked using the LanguageVersionIsEmpty() extension.
        /// </summary>
        /// <typeparam name="TItem">
        /// The Constellation.Sitecore.Items Item Type
        /// </typeparam>
        /// <param name="targetLanguage">
        /// The expected language.
        /// </param>
        /// <returns>
        /// The current language version, or a language version in a more generalized language if available.
        /// </returns>
        public virtual TItem GetBestFitLanguageVersion<TItem>([NotNull] Language targetLanguage)
            where TItem : class, IStandardTemplate
        {
            return this.InnerItem.GetBestFitLanguageVersion(targetLanguage).As<TItem>();
        }

        /// <summary>
        /// Given an Item in a specific language dialect (ex: en-GB), determines if the
        /// dialect has a version, and if not, attempts to find a more generalized language (ex: en).
        /// If there is a more generalized language, returns an item instance in that more generalized
        /// language. The instance may be "empty" and should be checked using the LanguageVersionIsEmpty() extension.
        /// </summary>
        /// <returns>The current language version, or a language version in a more generalized language if available.</returns>
        public virtual IStandardTemplate GetBestFitLanguageVersion()
        {
            return this.InnerItem.GetBestFitLanguageVersion().AsStronglyTyped();
        }

        /// <summary>
        /// Given an Item in a specific language dialect (ex: en-GB), determines if the
        /// dialect has a version, and if not, attempts to find a more generalized language (ex: en).
        /// If there is a more generalized language, returns an item instance in that more generalized
        /// language. The instance may be "empty" and should be checked using the LanguageVersionIsEmpty() extension.
        /// </summary>
        /// <typeparam name="TItem">The Constellation.Sitecore.Items Item Type</typeparam>
        /// <returns>The current language version, or a language version in a more generalized language if available.</returns>
        public virtual TItem GetBestFitLanguageVersion<TItem>() where TItem : class, IStandardTemplate
        {
            return this.InnerItem.GetBestFitLanguageVersion().As<TItem>();
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <typeparam name="TItem">The type of the item.</typeparam>
        /// <returns>The Children collection</returns>
        [NotNull]
        public virtual IList<TItem> GetChildren<TItem>() where TItem : class, IStandardTemplate
        {
            var collection = this.InnerItem.GetChildren().As<TItem>();
            return collection.ToArray();
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <returns>The inner item children</returns>
        [NotNull]
        public virtual ChildList GetChildren()
        {
            return this.InnerItem.GetChildren();
        }

        /// <summary>
        /// Gets the descendants.
        /// </summary>
        /// <typeparam name="TItem">The type of the t item.</typeparam>
        /// <returns>The descendant list</returns>
        [NotNull]
        public virtual IList<TItem> GetDescendants<TItem>() where TItem : class, IStandardTemplate
        {
            var collection = this.InnerItem.Axes.GetDescendants().As<TItem>();
            return collection.ToArray();
        }

        /// <summary>
        /// Gets the first or default ancestor as.
        /// </summary>
        /// <typeparam name="TItem">The type of the t item.</typeparam>
        /// <returns>TItem instance or default value.</returns>
        [CanBeNull]
        public virtual TItem GetFirstOrDefaultAncestorAs<TItem>() where TItem : class, IStandardTemplate
        {
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var item in this.InnerItem.Axes.GetAncestors())
            {
                // ReSharper restore LoopCanBeConvertedToQuery
                var instance = item.As<TItem>();
                if (instance != null)
                {
                    return instance;
                }
            }

            return default(TItem);
        }

        /// <summary>
        /// Gets the first or default child as.
        /// </summary>
        /// <typeparam name="TItem">The type of the t item.</typeparam>
        /// <returns>TItem instance or default value.</returns>
        [CanBeNull]
        public virtual TItem GetFirstOrDefaultChildAs<TItem>() where TItem : class, IStandardTemplate
        {
            foreach (Item item in this.InnerItem.Children)
            {
                var instance = item.As<TItem>();
                if (instance != null)
                {
                    return instance;
                }
            }

            return default(TItem);
        }

        /// <summary>
        /// Gets the first or default descendant as.
        /// </summary>
        /// <typeparam name="TItem">The type of the t item.</typeparam>
        /// <returns>TItem instance or default value.</returns>
        [CanBeNull]
        public virtual TItem GetFirstOrDefaultDescendantAs<TItem>() where TItem : class, IStandardTemplate
        {
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var item in this.InnerItem.Axes.GetDescendants())
            {
                // ReSharper restore LoopCanBeConvertedToQuery
                var instance = item.As<TItem>();
                if (instance != null)
                {
                    return instance;
                }
            }

            return default(TItem);
        }

        /// <summary>
        /// Initializes the specified rendering.
        /// </summary>
        /// <param name="renderingInstance">
        /// The rendering.
        /// </param>
        /// <exception cref="System.Web.HttpException">
        /// The MVC rendering is null!
        /// </exception>
        public virtual void Initialize(Rendering renderingInstance)
        {
            if (renderingInstance == null)
            {
                throw new HttpException("The MVC rendering is null!");
            }

            this.InnerItem = renderingInstance.Item;
            this.rendering = renderingInstance;
        }

        /// <summary>
        /// Verifies that the Item, in its current Language, is not effectively empty.
        /// </summary>
        /// <returns>True if the Item's Language has one or more Versions.</returns>
        public virtual bool LanguageVersionIsEmpty()
        {
            return this.InnerItem.LanguageVersionIsEmpty();
        }

        #endregion
    }
}