﻿namespace Jig.Sitecore
{
    using System;
    using System.Collections.Generic;
    using global::Sitecore;

    /// <summary>
    /// The internal extensions.
    /// </summary>
    internal static partial class InternalExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Given a comma-delimited string, separates it into its component parts.
        /// </summary>
        /// <param name="values">The string to parse.</param>
        /// <param name="toLowerCase">if set to <c>true</c> [to lower case].</param>
        /// <returns>A list of values.</returns>
        [NotNull]
        public static IEnumerable<string> ConvertToList([CanBeNull] this string values, bool toLowerCase = false)
        {
            var output = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);

            if (!string.IsNullOrWhiteSpace(values))
            {
                // ReSharper disable PossibleNullReferenceException
                var separated = values.Split(new[] { ',', '|', ';' }, StringSplitOptions.RemoveEmptyEntries);
                // ReSharper restore PossibleNullReferenceException
                foreach (var value in separated)
                {
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        var listvalue = value.Trim();
                        output.Add(toLowerCase ? listvalue.ToLowerInvariant() : listvalue);
                    }
                }
            }

            return output;
        }

        #endregion
    }
}