namespace Jig.Sitecore.Configuration
{
    using System;
    using System.Collections.Concurrent;
    using System.Text;
    using System.Web;
    using Jig.Sitecore.Pipelines.Configuration;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Pipelines;

    /// <summary>
    /// Log Helper.
    /// </summary>
    public static class ConfigurationSettingManager
    {
        /// <summary>
        /// The cache
        /// </summary>
        private static readonly ConcurrentDictionary<string, string> Cache = new ConcurrentDictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

        /// <summary>
        /// The retrieve setting.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The <see cref="string" />.</returns>
        [NotNull]
        public static string RetrieveSetting([NotNull] string name, [CanBeNull] string defaultValue = "")
        {
            global::Sitecore.Diagnostics.Assert.IsNotNullOrEmpty(name, "Setting Name");

            string value;
            if (Cache.TryGetValue(name, out value))
            {
                return value;
            }

            var pipelineArgs = new ConfigurationSettingPipelineArgs(name);

            var pipelineName = typeof(ConfigurationSettingManager).FullName;

            CorePipeline pipeline = CorePipelineFactory.GetPipeline(pipelineName, string.Empty);
            if (pipeline == null)
            {
                var msg = new StringBuilder(128);
                msg.Append("Could not get pipeline: ").AppendLine(pipelineName);
                msg.AppendLine("Missing config file: /App_Config/Include/Jig4Sitecore/cms/Jig.Sitecore.Configuration.config");

                throw new HttpException(msg.ToString());
            }

            pipeline.Run(pipelineArgs);

            var errors = pipelineArgs.GetMessages(PipelineMessageFilter.Errors);
            foreach (var error in errors)
            {
                Log.Error(error.Text, typeof(ConfigurationSettingPipelineArgs));
            }

            /* Performance: The pipeline should execute only once per application life time */
            value = (pipelineArgs.IsSettingValueResolved ? pipelineArgs.SettingValue : defaultValue) ?? string.Empty;

            Cache[name] = value;

            return value;
        }

        /// <summary>
        /// Retrieves the setting as boolean.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">if set to <c>true</c> [default value].</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool RetrieveSettingAsBoolean([NotNull] string name, bool defaultValue = false)
        {
            string setting = RetrieveSetting(name);
            bool value;
            if (!string.IsNullOrWhiteSpace(setting) && bool.TryParse(setting, out value))
            {
                return value;
            }

            return defaultValue;
        }

        /// <summary>
        /// Retrieves the setting as boolean nullable.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">if set to <c>true</c> [default value].</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        [CanBeNull]
        public static bool? RetrieveSettingAsBooleanNullable([NotNull] string name, bool? defaultValue = null)
        {
            string setting = RetrieveSetting(name);
            bool value;
            if (!string.IsNullOrWhiteSpace(setting) && bool.TryParse(setting, out value))
            {
                return value;
            }

            return defaultValue;
        }

        /// <summary>
        /// Retrieves the setting as int.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The int or default value</returns>
        public static int RetrieveSettingAsInt([NotNull] string name, int defaultValue = 0)
        {
            string setting = RetrieveSetting(name);
            int value;
            if (!string.IsNullOrWhiteSpace(setting) && int.TryParse(setting, out value))
            {
                return value;
            }

            return defaultValue;
        }

        /// <summary>
        /// Retrieves the setting as int nullable.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The nullable int or default value</returns>
        [CanBeNull]
        public static int? RetrieveSettingAsIntNullable([NotNull] string name, int? defaultValue = null)
        {
            string setting = RetrieveSetting(name);
            int value;
            if (!string.IsNullOrWhiteSpace(setting) && int.TryParse(setting, out value))
            {
                return value;
            }

            return defaultValue;
        }

        /// <summary>
        /// Retrieves the setting as float.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The float or default value</returns>
        public static float RetrieveSettingAsFloat([NotNull] string name, float defaultValue = 0)
        {
            string setting = RetrieveSetting(name);
            float value;
            if (!string.IsNullOrWhiteSpace(setting) && float.TryParse(setting, out value))
            {
                return value;
            }

            return defaultValue;
        }

        /// <summary>
        /// Retrieves the setting as float nullable.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The nullable float or default value</returns>
        [CanBeNull]
        public static float? RetrieveSettingAsFloatNullable([NotNull] string name, float? defaultValue = null)
        {
            string setting = RetrieveSetting(name);
            float value;
            if (!string.IsNullOrWhiteSpace(setting) && float.TryParse(setting, out value))
            {
                return value;
            }

            return defaultValue;
        }

        /// <summary>
        /// Retrieves the setting as double.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The double or default value.</returns>
        public static double RetrieveSettingAsDouble([NotNull] string name, double defaultValue = 0)
        {
            string setting = RetrieveSetting(name);
            double value;
            if (!string.IsNullOrWhiteSpace(setting) && double.TryParse(setting, out value))
            {
                return value;
            }

            return defaultValue;
        }

        /// <summary>
        /// Retrieves the setting as double nullable.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The nullable double or default value</returns>
        [CanBeNull]
        public static double? RetrieveSettingAsDoubleNullable([NotNull] string name, double? defaultValue = null)
        {
            string setting = RetrieveSetting(name);
            double value;
            if (!string.IsNullOrWhiteSpace(setting) && double.TryParse(setting, out value))
            {
                return value;
            }

            return defaultValue;
        }
    }
}