﻿namespace Jig.Sitecore
{
    using System;
    using System.Diagnostics;
    using System.Text;

    using global::Sitecore;

    using global::Sitecore.Data;

    using global::Sitecore.Diagnostics;

    /// <summary>
    /// Allows a class to be labeled as representing a specific Sitecore Data Template.
    /// </summary>
    [DebuggerDisplay("{ToString()}")]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    // ReSharper disable InconsistentNaming
    public class TemplateAttribute : Attribute
    // ReSharper restore InconsistentNaming
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateAttribute" /> class.
        /// </summary>
        /// <param name="id">The ID of the Sitecore Data Template represented by the class.</param>
        /// <param name="templateType">Type of the template.</param>
        public TemplateAttribute([NotNull] ID id, [NotNull] Type templateType)
        {
            Assert.IsNotNull(id, "id");
            Assert.IsNotNull(templateType, "templateType");

            this.ID = id;
            this.TemplateType = templateType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateAttribute" /> class.
        /// </summary>
        /// <param name="id">The ID of the Sitecore Data Template represented by the class.</param>
        /// <param name="templateType">Type of the template.</param>
        public TemplateAttribute([NotNull] string id, [NotNull] Type templateType)
        {
            Assert.IsNotNullOrEmpty(id, "id");
            Assert.IsNotNull(templateType, "templateType");

            this.ID = new ID(id);
            this.TemplateType = templateType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateAttribute" /> class.
        /// </summary>
        /// <param name="id">The ID of the Sitecore Data Template represented by the class.</param>
        /// <param name="templateType">Type of the template.</param>
        public TemplateAttribute(Guid id, [NotNull] Type templateType)
        {
            Assert.IsNotNull(id, "id");
            Assert.IsTrue(id != Guid.Empty, "id");
            Assert.IsNotNull(templateType, "templateType");

            this.ID = new ID(id);
            this.TemplateType = templateType;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the template.
        /// </summary>
        /// <value>
        /// The name of the template.
        /// </value>
        [CanBeNull]
        [UsedImplicitly]
        public string TemplateName { get; set; }

        /// <summary>
        /// Gets the type of the template.
        /// </summary>
        /// <value>
        /// The type of the template.
        /// </value>
        [NotNull]
        [UsedImplicitly]
        public Type TemplateType { get; private set; }

        /// <summary>
        /// Gets the ID of the Sitecore Data Template represented by the class.
        /// </summary>
        // ReSharper disable InconsistentNaming
        [NotNull]
        public ID ID { get; private set; }
        // ReSharper restore InconsistentNaming
        #endregion

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            var builder = new StringBuilder(128);
            builder.Append("ID=").Append(this.ID);
            builder.Append(", TemplateName=").Append(this.TemplateName);
            builder.Append(", TemplateType=").Append(this.TemplateType);

            return builder.ToString();
        }
    }
}
