﻿namespace Jig.Sitecore
{
    using System;
    using System.Diagnostics;
    using System.Text;

    using global::Sitecore;

    using global::Sitecore.Data;

    /// <summary>
    /// Specifies what Sitecore Field a given property represents.
    /// </summary>
    [DebuggerDisplay("{ToString()}")]
    [AttributeUsage(AttributeTargets.Property)]
    // ReSharper disable InconsistentNaming
    public class FieldAttribute : Attribute
    // ReSharper restore InconsistentNaming
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldAttribute"/> class.
        /// </summary>
        /// <param name="id">The ID of the Field.</param>
        public FieldAttribute([NotNull] ID id)
        {
            this.ID = id;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldAttribute"/> class.
        /// </summary>
        /// <param name="id">The ID of the Field as a Guid.</param>
        public FieldAttribute(Guid id)
        {
            this.ID = new ID(id);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldAttribute"/> class.
        /// </summary>
        /// <param name="id">The ID of the Field as a String.</param>
        public FieldAttribute([NotNull] string id)
        {
            this.ID = new ID(id);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the type of the field.
        /// </summary>
        /// <value>
        /// The type of the field.
        /// </value>
        [CanBeNull]
        [UsedImplicitly]
        public string FieldType { get; set; }

        /// <summary>
        /// Gets or sets the name of the field.
        /// </summary>
        /// <value>The name of the field.</value>
        [CanBeNull]
        [UsedImplicitly]
        public string FieldName { get; set; }

        /// <summary>
        /// Gets The ID of the Field.
        /// </summary>
        // ReSharper disable InconsistentNaming
        [NotNull]
        public ID ID { get; private set; }
        // ReSharper restore InconsistentNaming
        #endregion

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            var builder = new StringBuilder(128);
            builder.Append("ID=").Append(this.ID);
            builder.Append(", FieldName=").Append(this.FieldName);
            builder.Append(", FieldType=").Append(this.FieldType);
            return builder.ToString();
        }
    }
}
