﻿namespace Jig.Sitecore
{
    using System;
    using System.Diagnostics;
    using System.Text;
    using global::Sitecore.Data;
    using global::Sitecore.Diagnostics;

    /// <summary>
    /// The view attribute.
    /// </summary>
    [DebuggerDisplay("{ToString()}")]
    [AttributeUsage(AttributeTargets.Class)]
    public class ViewAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewAttribute"/> class.
        /// </summary>
        /// <param name="id">
        /// The ID of the Sitecore Data Template represented by the class.
        /// </param>
        public ViewAttribute([global::Sitecore.NotNull] ID id)
        {
            Assert.IsNotNull(id, "id");

            this.ID = id;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewAttribute"/> class.
        /// </summary>
        /// <param name="id">
        /// The ID of the Sitecore Data Template represented by the class.
        /// </param>
        public ViewAttribute([global::Sitecore.NotNull] string id)
            : this(new ID(id))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewAttribute"/> class.
        /// </summary>
        /// <param name="id">
        /// The ID of the Sitecore Data Template represented by the class.
        /// </param>
        public ViewAttribute(Guid id)
            : this(new ID(id))
        {
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        [global::Sitecore.NotNull]
        // ReSharper disable InconsistentNaming
        public ID ID { get; set; }
        // ReSharper restore InconsistentNaming

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        /// <value>The path.</value>
        [global::Sitecore.CanBeNull]
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        [global::Sitecore.CanBeNull]
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the template identifier.
        /// </summary>
        /// <value>The template identifier.</value>
        [global::Sitecore.CanBeNull]
        public string TemplateId { get; set; }

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            var builder = new StringBuilder(128);
            builder.Append("ID=").Append(this.ID);

            builder.Append(", DisplayName=").Append(this.DisplayName);
            builder.Append(", TemplateId=").Append(this.TemplateId);

            builder.Append(", Path=").Append(this.Path);

            return builder.ToString();
        }
    }
}