﻿namespace Jig.Sitecore.Links
{
    using global::Sitecore;
    using global::Sitecore.Links;

    /// <summary>
    /// Class SeoFriendlyLinkProvider.
    /// </summary>
    public class SeoFriendlyLinkProvider : LinkProvider
    {
        /// <summary>
        /// Gets the language embedding.
        /// </summary>
        public override LanguageEmbedding LanguageEmbedding
        {
            get
            {
                var site = global::Sitecore.Context.Site;
                if (site != null && !string.IsNullOrWhiteSpace(site.HostName))
                {
                    var option = site.GetLanguageEmbedding();
                    if (option != null)
                    {
                        return option.Value;
                    }
                }

                return base.LanguageEmbedding;
            }
        }

        /// <summary>
        /// Gets a value indicating whether lowercase urls.
        /// </summary>
        public override bool LowercaseUrls
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the language location.
        /// </summary>
        public override LanguageLocation LanguageLocation
        {
            get
            {
                return LanguageLocation.FilePath;
            }
        }

        /// <summary>
        /// Gets a value indicating whether add aspx extension.
        /// </summary>
        public override bool AddAspxExtension
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether shorten urls.
        /// </summary>
        public override bool ShortenUrls
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// The get default url options.
        /// </summary>
        /// <returns>
        /// The <see cref="UrlOptions"/>.
        /// </returns>
        [NotNull]
        public override UrlOptions GetDefaultUrlOptions()
        {
            return new global::Sitecore.Links.UrlOptions
            {
                /* These three are common sense/mandatory options */
                AddAspxExtension = this.AddAspxExtension,
                ShortenUrls = this.ShortenUrls,
                LowercaseUrls = this.LowercaseUrls,
                /* Sitecore is design for multi-language sites. Old systems are not has no concept LinkManager by site */
                LanguageEmbedding = this.LanguageEmbedding
            };
        }
    }
}