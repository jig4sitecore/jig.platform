﻿namespace Jig.Sitecore.Links
{
    using System.Collections.Specialized;
    using System.Web;
    using global::Sitecore;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Links;
    using global::Sitecore.Web;

    /// <summary>
    /// The switching link provider.
    /// </summary>
    public class SwitchingLinkProvider : LinkProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SwitchingLinkProvider"/> class.
        /// </summary>
        public SwitchingLinkProvider()
        {
            this.FallbackLinkProviderName = "sitecore";
        }

        /// <summary>
        /// Gets or sets the fallback provider name.
        /// </summary>
        [CanBeNull]
        public string FallbackLinkProviderName { get; set; }

        /// <summary>
        /// Gets or sets the enable site information property detection.
        /// </summary>
        /// <value>The enable site information property detection.</value>
        [CanBeNull]
        public string EnableSiteInfoPropertyDetection { get; [UsedImplicitly] set; }

        /// <summary>
        /// Gets the language embedding.
        /// </summary>
        public override LanguageEmbedding LanguageEmbedding
        {
            get
            {
                return this.ContextProvider.LanguageEmbedding;
            }
        }

        /// <summary>
        /// Gets a value indicating whether lowercase urls.
        /// </summary>
        public override bool LowercaseUrls
        {
            get
            {
                return this.ContextProvider.LowercaseUrls;
            }
        }

        /// <summary>
        /// Gets the language location.
        /// </summary>
        public override LanguageLocation LanguageLocation
        {
            get
            {
                return this.ContextProvider.LanguageLocation;
            }
        }

        /// <summary>
        /// Gets a value indicating whether add aspx extension.
        /// </summary>
        public override bool AddAspxExtension
        {
            get
            {
                return this.ContextProvider.AddAspxExtension;
            }
        }

        /// <summary>
        /// Gets a value indicating whether always include server url.
        /// </summary>
        public override bool AlwaysIncludeServerUrl
        {
            get
            {
                return this.ContextProvider.AlwaysIncludeServerUrl;
            }
        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        [NotNull]
        public override string Description
        {
            get
            {
                return this.ContextProvider.Description;
            }
        }

        /// <summary>
        /// Gets a value indicating whether encode names.
        /// </summary>
        public override bool EncodeNames
        {
            get
            {
                return this.ContextProvider.EncodeNames;
            }
        }

        /// <summary>
        /// Gets a value indicating whether shorten urls.
        /// </summary>
        public override bool ShortenUrls
        {
            get
            {
                return this.ContextProvider.ShortenUrls;
            }
        }

        /// <summary>
        /// Gets a value indicating whether use display name.
        /// </summary>
        public override bool UseDisplayName
        {
            get
            {
                return this.ContextProvider.UseDisplayName;
            }
        }

        /// <summary>
        /// Gets the context provider.
        /// </summary>
        [NotNull]
        private LinkProvider ContextProvider
        {
            get
            {
                var site = global::Sitecore.Context.Site;
                if (site != null && !string.IsNullOrWhiteSpace(site.HostName))
                {
                    LinkProvider provider = LinkManager.Providers[site.Name];
                    if (provider != null)
                    {
                        return provider;
                    }

                    var property = site.Properties[Jig.Sitecore.Constants.SiteInfo.PropertyNames.LinkProvider];
                    if (!string.IsNullOrWhiteSpace(property))
                    {
                        provider = LinkManager.Providers[property];
                        if (provider != null)
                        {
                            return provider;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(this.FallbackLinkProviderName))
                    {
                        provider = LinkManager.Providers[this.FallbackLinkProviderName];
                        if (provider != null)
                        {
                            return provider;
                        }
                    }
                }

                return LinkManager.Providers["sitecore"];
            }
        }

        /// <summary>
        /// The is dynamic link.
        /// </summary>
        /// <param name="linkText">
        /// The link text.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool IsDynamicLink([NotNull]string linkText)
        {
            return this.ContextProvider.IsDynamicLink(linkText);
        }

        /// <summary>
        /// The parse request url.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>
        /// The <see cref="RequestUrl" />.
        /// </returns>
        [NotNull]
        public override RequestUrl ParseRequestUrl([NotNull]HttpRequest request)
        {
            return this.ContextProvider.ParseRequestUrl(request);
        }

        /// <summary>
        /// The expand dynamic links.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="resolveSites">The resolve sites.</param>
        /// <returns>
        /// The <see cref="string" />.
        /// </returns>
        [NotNull]
        public override string ExpandDynamicLinks([NotNull]string text, bool resolveSites)
        {
            return this.ContextProvider.ExpandDynamicLinks(text, resolveSites);
        }

        /// <summary>
        /// The parse dynamic link.
        /// </summary>
        /// <param name="linkText">The link text.</param>
        /// <returns>
        /// The <see cref="DynamicLink" />.
        /// </returns>
        [NotNull]
        public override DynamicLink ParseDynamicLink([NotNull]string linkText)
        {
            return this.ContextProvider.ParseDynamicLink(linkText);
        }

        /// <summary>
        /// The get dynamic url.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="options">The options.</param>
        /// <returns>
        /// The <see cref="string" />.
        /// </returns>
        [NotNull]
        public override string GetDynamicUrl(
            [NotNull]Item item,
            [NotNull]LinkUrlOptions options)
        {
            return this.ContextProvider.GetDynamicUrl(item, options);
        }

        /// <summary>
        /// The get item url.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="options">The options.</param>
        /// <returns>
        /// The <see cref="string" />.
        /// </returns>
        [NotNull]
        public override string GetItemUrl(
            [NotNull]Item item,
            [NotNull] UrlOptions options)
        {
            return this.ContextProvider.GetItemUrl(item, options);
        }

        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="config">The config.</param>
        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);
            Assert.IsNotNullOrEmpty(this.FallbackLinkProviderName, "FallbackLinkProviderName");
        }

        /// <summary>
        /// The get default url options.
        /// </summary>
        /// <returns>
        /// The <see cref="UrlOptions"/>.
        /// </returns>
        [NotNull]
        public override UrlOptions GetDefaultUrlOptions()
        {
            return this.ContextProvider.GetDefaultUrlOptions();
        }
    }
}