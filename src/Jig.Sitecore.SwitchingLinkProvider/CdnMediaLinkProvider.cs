﻿namespace Jig.Sitecore.Links
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    using global::Sitecore;
    using global::Sitecore.Links;

    /// <summary>
    /// Class CdnMediaLinkProvider.
    /// </summary>
    /// <remarks>This Provider is required for Media Library site.
    /// The issue:
    /// Links in the Rich text like
    /// <![CDATA[
    /// <a target="blank" href="~/link.aspx?_id=C52562E5F3BB4156AF7090B5A7BC5FE1&amp;_z=z" title="8x10">8x10</a>
    /// ]]>
    /// will ignore Media.UseItemPaths=false flag and schemas
    /// </remarks>
    public class CdnMediaLinkProvider : SeoFriendlyLinkProvider
    {
        /// <summary>
        /// Gets a value indicating whether lowercase urls.
        /// </summary>
        public override bool LowercaseUrls
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the language location.
        /// </summary>
        public override LanguageLocation LanguageLocation
        {
            get
            {
                return LanguageLocation.FilePath;
            }
        }

        /// <summary>
        /// Gets a value indicating whether add aspx extension.
        /// </summary>
        public override bool AddAspxExtension
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether shorten urls.
        /// </summary>
        public override bool ShortenUrls
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Creates the link parser.
        /// </summary>
        /// <returns>LinkProvider LinkParser.</returns>
        [NotNull]
        protected override LinkProvider.LinkParser CreateLinkParser()
        {
            return new CdnLinkParser();
        }

        /// <summary>
        /// Class CdnLinkParser.
        /// </summary>
        public class CdnLinkParser : LinkParser
        {
            /// <summary>
            /// Gets the expanders.
            /// </summary>
            /// <returns>The Link Expanders.</returns>
            [NotNull]
            protected override IEnumerable<LinkExpander> GetExpanders()
            {
                return
                    new Collection<LinkExpander>()
                        {
                            new CdnMediaLinkExpander(),
                            new CdnItemLinkExpander()
                        };
            }
        }
    }
}