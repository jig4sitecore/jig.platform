﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: Guid("edd5d045-3ceb-4c01-b259-6d92defa1d9b")]
[assembly: AssemblyTitle("Jig.Sitecore.Links.SwitchingLinkProvider")]
[assembly: AssemblyDescription("The library extending the Sitecore CMS.")]

[assembly: AssemblyCompany("Jig4Sitecore Team")]
[assembly: AssemblyProduct("Jig4Sitecore Framework")]
[assembly: AssemblyCopyright("Copyright © Jig4Sitecore Team")]
[assembly: AssemblyTrademark("Jig4Sitecore Team")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyVersion("2.0.0")]
[assembly: AssemblyFileVersion("2.0.0")]
