﻿namespace Jig.Sitecore.Links
{
    using System;
    using System.Text;

    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Links;

    /// <summary>
    /// Class CdnItemLinkExpander.
    /// </summary>
    public class CdnItemLinkExpander : LinkExpander
    {
        #region Public Methods and Operators

        /// <summary>
        /// Expands the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="urlOptions">The URL options.</param>
        public override void Expand([NotNull] ref string text, [NotNull] UrlOptions urlOptions)
        {
            Assert.ArgumentNotNull(text, "text");
            Assert.ArgumentNotNull(urlOptions, "urlOptions");

            int startIndex1 = text.IndexOf("~/link.aspx?", StringComparison.InvariantCulture);
            if (startIndex1 == -1)
            {
                return;
            }

            var stringBuilder = new StringBuilder(text.Length);
            int startIndex2 = 0;
            for (;
                startIndex1 >= 0;
                startIndex1 = text.IndexOf("~/link.aspx?", startIndex2, StringComparison.InvariantCulture))
            {
                int num = text.IndexOf("_z=z", startIndex1, StringComparison.InvariantCulture);
                if (num < 0)
                {
                    text = stringBuilder.ToString();
                    return;
                }

                string url = DynamicLink.Parse(text.Substring(startIndex1, num - startIndex1)).GetUrl(urlOptions);
                string str = text.Substring(startIndex2, startIndex1 - startIndex2);
                stringBuilder.Append(str);
                stringBuilder.Append(url);
                startIndex2 = num + "_z=z".Length;
            }

            stringBuilder.Append(text.Substring(startIndex2));
            text = stringBuilder.ToString();
        }

        #endregion
    }
}