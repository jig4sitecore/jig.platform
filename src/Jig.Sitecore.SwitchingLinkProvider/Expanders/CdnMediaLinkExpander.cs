﻿namespace Jig.Sitecore.Links
{
    using System;
    using System.Text;

    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Links;
    using global::Sitecore.Resources.Media;
    using global::Sitecore.Text;

    /// <summary>
    /// Class CdnMediaLinkExpander.
    /// </summary>
    // ReSharper disable ClassWithVirtualMembersNeverInherited.Global
    public class CdnMediaLinkExpander : LinkExpander
    // ReSharper restore ClassWithVirtualMembersNeverInherited.Global
    {
        #region Public Methods and Operators

        /// <summary>
        /// Expands the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="urlOptions">The URL options.</param>
        public override void Expand([NotNull] ref string text, [NotNull] UrlOptions urlOptions)
        {
            Assert.ArgumentNotNull(text, "text");
            Assert.ArgumentNotNull(urlOptions, "urlOptions");
            var stringBuilder = new StringBuilder(256);

            int num = 0;
            string prefix;
            int mediaPrefix = this.FindMediaPrefix(text, 0, out prefix);
            while (mediaPrefix >= 0 && text.Length >= mediaPrefix + prefix.Length + 32)
            {
                string id = text.Substring(mediaPrefix + prefix.Length, 32);
                if (!ShortID.IsShortID(id))
                {
                    mediaPrefix = this.FindMediaPrefix(text, mediaPrefix + 1, out prefix);
                }
                else
                {
                    MediaItem mediaItem = null;
                    if (Context.Database != null)
                    {
                        mediaItem = Context.Database.GetItem(new ID(id));
                    }

                    if (mediaItem == null)
                    {
                        mediaPrefix = this.FindMediaPrefix(text, mediaPrefix + 1, out prefix);
                    }
                    else
                    {
                        /* No language specific media */
                        MediaUrlOptions options = new MediaUrlOptions
                                                      {
                                                          IncludeExtension = text.Substring(mediaPrefix + prefix.Length + 32 + 1, "ashx".Length) == "ashx"
                                                      };

                        string mediaUrl = MediaManager.GetMediaUrl(mediaItem, options);
                        stringBuilder.Append(text.Substring(num, mediaPrefix - num));
                        num = mediaPrefix + prefix.Length + 32;
                        if (options.IncludeExtension)
                        {
                            num += "ashx".Length + 1;
                        }

                        /* Remove schema to avoid any problems with protocols & caching */
                        if (mediaUrl.StartsWith("http:"))
                        {
                            mediaUrl = mediaUrl.Remove(0, "http:".Length);
                        }
                        else if (mediaUrl.StartsWith("https:"))
                        {
                            mediaUrl = mediaUrl.Remove(0, "https:".Length);
                        }

                        int closingQuotesIndex;
                        if (mediaPrefix > 0 && (this.IsSrc(text, mediaPrefix - 1) || this.IsHref(text, mediaPrefix - 1))
                            && (closingQuotesIndex = text.IndexOf('"', num)) > -1)
                        {
                            stringBuilder.Append(this.ComposeSource(text, num, closingQuotesIndex, mediaUrl));
                            num = closingQuotesIndex;
                        }
                        else
                        {
                            int length = mediaUrl.IndexOf("?", StringComparison.Ordinal);
                            stringBuilder.Append(length > -1 ? mediaUrl.Substring(0, length) : mediaUrl);
                        }

                        mediaPrefix = this.FindMediaPrefix(text, num, out prefix);
                    }
                }
            }

            stringBuilder.Append(text.Substring(num));
            text = stringBuilder.ToString();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds the media prefix.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="startIndex">The start index.</param>
        /// <param name="prefix">The prefix.</param>
        /// <returns>Media index</returns>
        protected virtual int FindMediaPrefix([NotNull] string text, int startIndex, out string prefix)
        {
            Assert.ArgumentNotNullOrEmpty(text, "text");
            int num1 = int.MaxValue;
            prefix = null;
            foreach (string str in MediaManager.Provider.Config.MediaPrefixes)
            {
                int num2 = text.IndexOf(str, startIndex, StringComparison.InvariantCulture);
                if (num2 >= 0 && num2 < num1)
                {
                    num1 = num2;
                    prefix = str;
                }
            }

            if (prefix == null)
            {
                return -1;
            }

            return num1;
        }

        /// <summary>
        /// Composes the source.
        /// </summary>
        /// <param name="originalText">The original text.</param>
        /// <param name="indexAfterOriginalUrl">The index after original URL.</param>
        /// <param name="closingQuotesIndex">Index of the closing quotes.</param>
        /// <param name="mediaPath">The media path.</param>
        /// <returns>The Link</returns>
        [NotNull]
        private string ComposeSource(
            [NotNull] string originalText,
            int indexAfterOriginalUrl,
            int closingQuotesIndex,
            [NotNull] string mediaPath)
        {
            StringBuilder stringBuilder = new StringBuilder();
            int num = mediaPath.IndexOf("?", StringComparison.Ordinal);
            if (num > -1)
            {
                stringBuilder.Append(mediaPath.Substring(0, num));
                stringBuilder.Append(
                    originalText.Substring(indexAfterOriginalUrl, closingQuotesIndex - indexAfterOriginalUrl).Trim());
                int startIndex = originalText.IndexOf('?', indexAfterOriginalUrl);
                if (startIndex > -1 && startIndex < closingQuotesIndex)
                {
                    UrlString urlString1 =
                        new UrlString(originalText.Substring(startIndex, closingQuotesIndex - startIndex));
                    UrlString urlString2 = new UrlString(mediaPath.Substring(num));
                    foreach (object obj in urlString1.Parameters)
                    {
                        if (urlString2[obj.ToString()] != null)
                        {
                            urlString2.Remove(obj.ToString());
                        }
                    }

                    if (urlString2.Parameters.Count > 0)
                    {
                        stringBuilder.Append("&").Append(urlString2.Query);
                    }
                }
                else
                {
                    stringBuilder.Append(mediaPath.Substring(num));
                }
            }
            else
            {
                stringBuilder.Append(mediaPath);
                stringBuilder.Append(
                    originalText.Substring(indexAfterOriginalUrl, closingQuotesIndex - indexAfterOriginalUrl));
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Determines whether the specified text is href.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="startIndex">The start index.</param>
        /// <returns><c>true</c> if the specified text is href; otherwise, <c>false</c>.</returns>
        private bool IsHref([NotNull] string text, int startIndex)
        {
            Assert.IsTrue(startIndex > 0, "startIndex");

            if (string.IsNullOrWhiteSpace(text))
            {
                return false;
            }

            if (startIndex >= text.Length || text[startIndex] != 34)
            {
                return false;
            }

            do
            {
                --startIndex;
            }
            while (startIndex > 0 && char.IsWhiteSpace(text[startIndex]));

            if (startIndex <= 0 || text[startIndex] != 61)
            {
                return false;
            }

            --startIndex;
            while (startIndex > 0 && char.IsWhiteSpace(text[startIndex]))
            {
                --startIndex;
            }

            return startIndex > 3 && text[startIndex - 3] == 104
                   && (text[startIndex - 2] == 114 && text[startIndex - 1] == 101) && text[startIndex] == 102;
        }

        /// <summary>
        /// Determines whether the specified text is source.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="startIndex">The start index.</param>
        /// <returns><c>true</c> if the specified text is source; otherwise, <c>false</c>.</returns>
        private bool IsSrc([NotNull] string text, int startIndex)
        {
            Assert.IsTrue(startIndex > 0, "startIndex");

            if (string.IsNullOrWhiteSpace(text))
            {
                return false;
            }

            if (startIndex >= text.Length || text[startIndex] != 34)
            {
                return false;
            }

            do
            {
                --startIndex;
            }
            while (startIndex > 0 && char.IsWhiteSpace(text[startIndex]));

            if (startIndex <= 0 || text[startIndex] != 61)
            {
                return false;
            }

            --startIndex;
            while (startIndex > 0 && char.IsWhiteSpace(text[startIndex]))
            {
                --startIndex;
            }

            return startIndex > 2 && text[startIndex - 2] == 115
                   && (text[startIndex - 1] == 114 && text[startIndex] == 99);
        }

        #endregion
    }
}