﻿namespace Jig.Sitecore.Shell.MediaLibrary.Processors
{
    using System;
    using System.Web;

    using global::Sitecore;

    using global::Sitecore.Data.Items;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Pipelines.Upload;

    /// <summary>
    /// The restriction checker.
    /// </summary>
    public class RestrictionChecker
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RestrictionChecker"/> class.
        /// </summary>
        public RestrictionChecker()
        {
            this.Database = "master";
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the database.
        /// </summary>
        [NotNull]
        public string Database { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process([NotNull] UploadArgs args)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (args == null || !string.IsNullOrEmpty(args.Folder))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return;
            }

            var db = Context.ContentDatabase;
            if (db == null || !string.Equals(db.Name, this.Database, StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }

            var mediaFolder = db.SelectSingleItem(args.Folder);

            if (mediaFolder != null)
            {
                foreach (string fileName in args.Files)
                {
                    var file = args.Files[fileName];
                    if (file != null && !string.IsNullOrEmpty(file.FileName))
                    {
                        string message = this.RunChecks(mediaFolder, file);

                        if (!string.IsNullOrEmpty(message))
                        {
                            args.ErrorText = message;
                            Log.Error(message, this);
                            args.AbortPipeline();
                            break;
                        }
                    }
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The run checks.
        /// </summary>
        /// <param name="mediaFolder">The media folder.</param>
        /// <param name="file">The file.</param>
        /// <returns>The <see cref="string"/>.</returns>
        [CanBeNull]
        private string RunChecks([NotNull] Item mediaFolder, [NotNull] HttpPostedFile file)
        {
            return null;
        }

        #endregion
    }
}