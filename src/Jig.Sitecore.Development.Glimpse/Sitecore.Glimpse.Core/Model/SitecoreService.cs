﻿namespace Jig.Sitecore.Development.Glimpse.Model
{
    /// <summary>
    /// The sitecore service.
    /// </summary>
    public class SitecoreService : ControllerBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SitecoreService"/> class.
        /// </summary>
        public SitecoreService()
        {
            this.CsrfProtection = Csrf.None;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether cors enabled.
        /// </summary>
        public bool CorsEnabled
        {
            get
            {
                return (!string.IsNullOrEmpty(this.Definition)) && this.Definition.Contains("EnableCors");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is entity service.
        /// </summary>
        public bool IsEntityService { get; set; }

        /// <summary>
        /// Gets or sets the metadata.
        /// </summary>
        public string Metadata { get; set; }

        /// <summary>
        /// Gets or sets the url.
        /// </summary>
        public string Url { get; set; }

        #endregion
    }
}