﻿namespace Jig.Sitecore.Development.Glimpse.Model.Analytics
{
    using System;

    /// <summary>
    /// The pattern card.
    /// </summary>
    public class PatternCard
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the dimension.
        /// </summary>
        public string Dimension { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        #endregion
    }
}