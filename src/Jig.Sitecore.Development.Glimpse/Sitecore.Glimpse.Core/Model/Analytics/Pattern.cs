﻿namespace Jig.Sitecore.Development.Glimpse.Model.Analytics
{
    /// <summary>
    /// The pattern.
    /// </summary>
    public class Pattern
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Pattern" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="image">The image.</param>
        /// <param name="description">The description.</param>
        public Pattern(string name, string image, string description)
        {
            this.Name = name;
            this.Image = image;
            this.Description = description;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the description.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the image.
        /// </summary>
        public string Image { get; private set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        #endregion
    }
}