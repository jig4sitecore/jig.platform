﻿namespace Jig.Sitecore.Development.Glimpse.Model.Analytics
{
    /// <summary>
    /// The profile.
    /// </summary>
    public class Profile
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the pattern card.
        /// </summary>
        public string PatternCard { get; set; }

        /// <summary>
        /// Gets or sets the values.
        /// </summary>
        public string Values { get; set; }

        #endregion
    }
}