﻿namespace Jig.Sitecore.Development.Glimpse.Model.Analytics
{
    using System;

    /// <summary>
    /// The page holder.
    /// </summary>
    public class PageHolder
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PageHolder" /> class.
        /// </summary>
        /// <param name="num">The num.</param>
        /// <param name="id">The id.</param>
        /// <param name="date">The date.</param>
        /// <param name="url">The url.</param>
        public PageHolder(int num, Guid id, DateTime date, string url)
        {
            this.Num = num;
            this.Id = id;
            this.Date = date;
            this.Url = url;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the num.
        /// </summary>
        public int Num { get; set; }

        /// <summary>
        /// Gets or sets the url.
        /// </summary>
        public string Url { get; set; }

        #endregion
    }
}