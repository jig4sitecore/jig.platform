﻿namespace Jig.Sitecore.Development.Glimpse.Model.Analytics
{
    using System;

    /// <summary>
    /// The goal.
    /// </summary>
    public class Goal
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        public DateTime Timestamp { get; set; }

        #endregion
    }
}