﻿namespace Jig.Sitecore.Development.Glimpse.Model
{
    using System;

    /// <summary>
    /// The controller.
    /// </summary>
    public class Controller : ControllerBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Controller" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="controllerType">The controller type.</param>
        /// <param name="definition">The definition.</param>
        /// <param name="csrfProtection">The csrf protection.</param>
        /// <param name="authorise">The authorise.</param>
        /// <exception cref="System.ArgumentNullException">
        /// name
        /// or
        /// definition
        /// </exception>
        /// <exception cref="ArgumentNullException"></exception>
        public Controller(
            string name, 
            ControllerType controllerType, 
            string definition, 
            Csrf csrfProtection, 
            bool authorise)
        {
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }

            if (definition == null)
            {
                throw new ArgumentNullException("definition");
            }

            this.Name = name;
            this.ControllerType = controllerType;
            this.Definition = definition;
            this.CsrfProtection = csrfProtection;
            this.Authorise = authorise;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the controller type.
        /// </summary>
        public ControllerType ControllerType { get; private set; }

        #endregion
    }
}