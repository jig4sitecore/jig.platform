﻿namespace Jig.Sitecore.Development.Glimpse.Model
{
    using System;

    /// <summary>
    /// The logged in user.
    /// </summary>
    public class LoggedInUser
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggedInUser" /> class.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        /// <param name="name">The name.</param>
        /// <param name="created">The created.</param>
        /// <param name="lastRequest">The last request.</param>
        /// <param name="isAdmin">The is admin.</param>
        public LoggedInUser(string sessionId, string name, DateTime created, DateTime lastRequest, bool isAdmin)
        {
            this.SessionId = sessionId;
            this.Name = name;
            this.Created = created;
            this.LastRequest = lastRequest;
            this.IsAdmin = isAdmin;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the created.
        /// </summary>
        public DateTime Created { get; private set; }

        /// <summary>
        /// Gets a value indicating whether is admin.
        /// </summary>
        public bool IsAdmin { get; private set; }

        /// <summary>
        /// Gets the last request.
        /// </summary>
        public DateTime LastRequest { get; private set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the session id.
        /// </summary>
        public string SessionId { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The is inactive.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsInactive()
        {
            return SystemTime.Now.Invoke().Subtract(this.LastRequest).TotalHours >= 2;
        }

        #endregion
    }
}