﻿namespace Jig.Sitecore.Development.Glimpse.Model
{
    using System;

    /// <summary>
    /// The controller type.
    /// </summary>
    [Flags]
    public enum ControllerType : byte
    {
        /// <summary>
        /// The mvc.
        /// </summary>
        Mvc, 

        /// <summary>
        /// The web api.
        /// </summary>
        WebApi
    }
}