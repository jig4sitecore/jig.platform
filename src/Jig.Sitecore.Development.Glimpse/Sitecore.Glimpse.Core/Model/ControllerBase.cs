﻿namespace Jig.Sitecore.Development.Glimpse.Model
{
    /// <summary>
    /// The controller base.
    /// </summary>
    public abstract class ControllerBase
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether authorise.
        /// </summary>
        public bool Authorise { get; set; }

        /// <summary>
        /// Gets or sets the csrf protection.
        /// </summary>
        public Csrf CsrfProtection { get; set; }

        /// <summary>
        /// Gets or sets the definition.
        /// </summary>
        public string Definition { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        #endregion
    }
}