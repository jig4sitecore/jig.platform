﻿namespace Jig.Sitecore.Development.Glimpse.Model
{
    using System;

    /// <summary>
    /// The csrf.
    /// </summary>
    [Flags]
    public enum Csrf : byte
    {
        /// <summary>
        /// The none.
        /// </summary>
        None = 0, 

        /// <summary>
        /// The class.
        /// </summary>
        Class, 

        /// <summary>
        /// The method.
        /// </summary>
        Method
    }
}