﻿namespace Jig.Sitecore.Development.Glimpse
{
    using System;
    using System.Linq;

    /// <summary>
    /// The TypeProvider interface.
    /// </summary>
    public interface ITypeProvider
    {
        #region Public Properties

        /// <summary>
        /// Gets the types.
        /// </summary>
        IQueryable<Type> Types { get; }

        #endregion
    }
}