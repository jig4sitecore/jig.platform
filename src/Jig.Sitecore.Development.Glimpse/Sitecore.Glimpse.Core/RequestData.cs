﻿namespace Jig.Sitecore.Development.Glimpse
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// The request data.
    /// </summary>
    public class RequestData
    {
        #region Fields

        /// <summary>
        /// The request data.
        /// </summary>
        private readonly Dictionary<DataKey, object> requestData = new Dictionary<DataKey, object>();

        #endregion

        #region Public Indexers

        /// <summary>
        /// The this.
        /// </summary>
        /// <param name="dataKey">The data key.</param>
        /// <returns>The <see cref="object" />.</returns>
        public object this[DataKey dataKey]
        {
            get
            {
                return this.GetKeyValue(dataKey);
            }

            set
            {
                this.requestData[dataKey] = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="dataKey">The data key.</param>
        /// <param name="value">The value.</param>
        public void Add(DataKey dataKey, object value)
        {
            this.requestData.Add(dataKey, value);
        }

        /// <summary>
        /// The get key value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The <see cref="object" />.</returns>
        public object GetKeyValue(DataKey key)
        {
            return this.requestData.ContainsKey(key) ? this.requestData[key] : null;
        }

        /// <summary>
        /// The has data.
        /// </summary>
        /// <returns>The <see cref="bool" />.</returns>
        public bool HasData()
        {
            if (this.requestData == null)
            {
                return false;
            }

            if (this.requestData.All(x => x.Value == null))
            {
                return false;
            }

            return true;
        }

        #endregion
    }
}