﻿namespace Jig.Sitecore.Development.Glimpse
{
    /// <summary>
    /// The Log interface.
    /// </summary>
    public interface ILog
    {
        #region Public Methods and Operators

        /// <summary>
        /// The write.
        /// </summary>
        /// <param name="message">The message.</param>
        void Write(string message);

        #endregion
    }
}