﻿namespace Jig.Sitecore.Development.Glimpse
{
    /// <summary>
    /// The constants.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// The sitecore.
        /// </summary>
        public static class Sitecore
        {
            /// <summary>
            /// The analytics.
            /// </summary>
            public static class Analytics
            {
                /// <summary>
                /// The templates.
                /// </summary>
                public static class Templates
                {
                    #region Constants

                    /// <summary>
                    /// The pattern card.
                    /// </summary>
                    public const string PatternCard = "{4A6A7E36-2481-438F-A9BA-0453ECC638FA}";

                    /// <summary>
                    /// The profile.
                    /// </summary>
                    public const string Profile = "{8E0C1738-3591-4C60-8151-54ABCC9807D1}";

                    /// <summary>
                    /// The traffic types.
                    /// </summary>
                    public const string TrafficTypes = "{7E265978-8C1B-419D-BC06-0B5D101F04DF}";

                    #endregion
                }
            }

            /// <summary>
            /// The marketing center.
            /// </summary>
            public static class MarketingCenter
            {
                #region Constants

                /// <summary>
                /// The profiles.
                /// </summary>
                public const string Profiles = "{12BD7E35-437B-449C-B931-23CFA12C03D8}";

                #endregion
            }
        }

        /// <summary>
        /// The wiki.
        /// </summary>
        public class Wiki
        {
            #region Constants

            /// <summary>
            /// The url.
            /// </summary>
            public const string Url = "http://kevinobee.github.io/Jig.Sitecore.Development.Glimpse/";

            #endregion
        }
    }
}