﻿namespace Jig.Sitecore.Development.Glimpse
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    /// <summary>
    /// The field list.
    /// </summary>
    public class FieldList
    {
        #region Fields

        /// <summary>
        /// The fields.
        /// </summary>
        private readonly List<KeyValuePair<string, object>> fields;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldList"/> class.
        /// </summary>
        public FieldList()
        {
            this.fields = new List<KeyValuePair<string, object>>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the fields.
        /// </summary>
        public KeyValuePair<string, object>[] Fields
        {
            get
            {
                return this.fields.ToArray();
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add field.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public void AddField(string key, object value)
        {
            this.fields.Add(new KeyValuePair<string, object>(key, value));
        }

        /// <summary>
        /// The get field.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetField(string key)
        {
            if (this.fields.Exists(x => x.Key.ToString(CultureInfo.InvariantCulture) == key))
            {
                return this.fields.First(x => x.Key.ToString(CultureInfo.InvariantCulture) == key).Value;
            }

            return null;
        }

        #endregion
    }
}