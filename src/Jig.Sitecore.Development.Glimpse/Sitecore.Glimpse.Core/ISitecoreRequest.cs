﻿namespace Jig.Sitecore.Development.Glimpse
{
    /// <summary>
    /// The SitecoreRequest interface.
    /// </summary>
    public interface ISitecoreRequest
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get data.
        /// </summary>
        /// <returns>The <see cref="RequestData" />.</returns>
        RequestData GetData();

        #endregion
    }
}