namespace Jig.Sitecore.Development.Glimpse
{
    /// <summary>
    /// The request data not loaded.
    /// </summary>
    public class RequestDataNotLoaded : RequestData
    {
    }
}