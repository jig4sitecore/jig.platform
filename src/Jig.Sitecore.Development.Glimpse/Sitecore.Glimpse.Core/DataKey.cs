﻿namespace Jig.Sitecore.Development.Glimpse
{
    using System;

    /// <summary>
    /// The data key.
    /// </summary>
    [Flags]
    public enum DataKey : byte
    {
        /// <summary>
        /// The item.
        /// </summary>
        Item = 0, 

        /// <summary>
        /// The goals.
        /// </summary>
        Goals, 

        /// <summary>
        /// The profiles.
        /// </summary>
        Profiles, 

        /// <summary>
        /// The last pages.
        /// </summary>
        LastPages, 

        /// <summary>
        /// The campaign.
        /// </summary>
        Campaign, 

        /// <summary>
        /// The traffic type.
        /// </summary>
        TrafficType, 

        /// <summary>
        /// The engagement value.
        /// </summary>
        EngagementValue, 

        /// <summary>
        /// The is new visitor.
        /// </summary>
        IsNewVisitor, 

        /// <summary>
        /// The site.
        /// </summary>
        Site, 

        /// <summary>
        /// The database.
        /// </summary>
        Database, 

        /// <summary>
        /// The user.
        /// </summary>
        User, 

        /// <summary>
        /// The domain.
        /// </summary>
        Domain, 

        /// <summary>
        /// The request.
        /// </summary>
        Request, 

        /// <summary>
        /// The item template.
        /// </summary>
        ItemTemplate, 

        /// <summary>
        /// The device.
        /// </summary>
        Device, 

        /// <summary>
        /// The diagnostics.
        /// </summary>
        Diagnostics, 

        /// <summary>
        /// The item visualization.
        /// </summary>
        ItemVisualization, 

        /// <summary>
        /// The language.
        /// </summary>
        Language, 

        /// <summary>
        /// The culture.
        /// </summary>
        Culture, 

        /// <summary>
        /// The services.
        /// </summary>
        Services, 

        /// <summary>
        /// The license.
        /// </summary>
        License, 

        /// <summary>
        /// The user list.
        /// </summary>
        UserList, 

        /// <summary>
        /// The version info.
        /// </summary>
        VersionInfo, 

        /// <summary>
        /// The page mode.
        /// </summary>
        PageMode, 

        /// <summary>
        /// The controllers.
        /// </summary>
        Controllers
    }
}