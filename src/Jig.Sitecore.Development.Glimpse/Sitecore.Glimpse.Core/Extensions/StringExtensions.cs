﻿namespace Jig.Sitecore.Development.Glimpse
{
    /// <summary>
    /// The string extensions.
    /// </summary>
    public static class StringExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The remove from end.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="suffix">The suffix.</param>
        /// <returns>The <see cref="string" />.</returns>
        public static string RemoveFromEnd(this string value, string suffix)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            return value.EndsWith(suffix) ? value.Substring(0, value.Length - suffix.Length) : value;
        }

        #endregion
    }
}