﻿namespace Jig.Sitecore.Development.Glimpse
{
    using System.Collections.Generic;

    /// <summary>
    /// The CollectionProvider interface.
    /// </summary>
    /// <typeparam name="T">The item</typeparam>
    public interface ICollectionProvider<T>
        where T : class
    {
        #region Public Properties

        /// <summary>
        /// Gets the collection.
        /// </summary>
        /// <value>The collection.</value>
        ICollection<T> Collection { get; }

        #endregion
    }
}