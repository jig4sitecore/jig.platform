﻿namespace Jig.Sitecore.Development.Glimpse.Caching
{
    /// <summary>
    /// The Cache interface.
    /// </summary>
    public interface ICache
    {
        #region Public Indexers

        /// <summary>
        /// The this.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns>The <see cref="object" />.</returns>
        object this[string fieldName] { get; set; }

        #endregion
    }
}