﻿namespace Jig.Sitecore.Development.Glimpse.Caching
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The caching collection provider.
    /// </summary>
    /// <typeparam name="T">The item</typeparam>
    public class CachingCollectionProvider<T> : ICollectionProvider<T>
        where T : class
    {
        #region Fields

        /// <summary>
        /// The cache.
        /// </summary>
        private readonly ICache cache;

        /// <summary>
        /// The provider.
        /// </summary>
        private readonly ICollectionProvider<T> provider;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CachingCollectionProvider{T}" /> class.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="cache">The cache.</param>
        /// <exception cref="System.ArgumentNullException">provider
        /// or
        /// cache</exception>
        /// <exception cref="ArgumentNullException">provider
        /// or
        /// {D255958A-8513-4226-94B9-080D98F904A1}cache</exception>
        public CachingCollectionProvider(ICollectionProvider<T> provider, ICache cache)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }

            if (cache == null)
            {
                throw new ArgumentNullException("cache");
            }

            this.provider = provider;
            this.cache = cache;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the collection.
        /// </summary>
        public ICollection<T> Collection
        {
            get
            {
                return this.GetCollection();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get collection.
        /// </summary>
        /// <returns>
        /// The Collection of items.
        /// </returns>
        private ICollection<T> GetCollection()
        {
            var cacheField = typeof(T).FullName;

            if (this.cache[cacheField] == null)
            {
                this.cache[cacheField] = this.provider.Collection;
            }

            return this.cache[cacheField] as ICollection<T>;
        }

        #endregion
    }
}