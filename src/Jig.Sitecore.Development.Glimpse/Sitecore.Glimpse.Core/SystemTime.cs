namespace Jig.Sitecore.Development.Glimpse
{
    using System;

    /// <summary>
    /// The system time.
    /// </summary>
    public static class SystemTime
    {
        #region Static Fields

        /// <summary>
        /// The now.
        /// </summary>
        public static readonly Func<DateTime> Now = () => DateTime.Now;

        #endregion
    }
}