﻿namespace Jig.Sitecore.Development.Glimpse
{
    using System;

    using global::Glimpse.Core.Extensibility;
    using global::Glimpse.Core.Tab.Assist;

    using Jig.Sitecore.Development.Glimpse.Infrastructure;

    /// <summary>
    /// The sitecore tab.
    /// </summary>
    public class SitecoreTab : TabBase, IDocumentation
    {
        #region Fields

        /// <summary>
        /// The sitecore request.
        /// </summary>
        private readonly ISitecoreRequest sitecoreRequest;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SitecoreTab" /> class.
        /// </summary>
        public SitecoreTab()
            : this(new SitecoreRequest())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SitecoreTab" /> class.
        /// </summary>
        /// <param name="sitecoreRequest">The sitecore request.</param>
        public SitecoreTab(ISitecoreRequest sitecoreRequest)
        {
            this.sitecoreRequest = sitecoreRequest;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the documentation uri.
        /// </summary>
        public string DocumentationUri
        {
            get
            {
                return Constants.Wiki.Url;
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public override string Name
        {
            get
            {
                return "Sitecore";
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The <see cref="object" />.</returns>
        public override object GetData(ITabContext context)
        {
            try
            {
                var sitecoreData = this.sitecoreRequest.GetData();

                if (!sitecoreData.HasData())
                {
                    return null;
                }

                var itemSummary = new ItemSummary(sitecoreData).Create();

                if (string.IsNullOrEmpty(itemSummary))
                {
                    return null;
                }

                var plugin = Plugin.Create("Item", itemSummary);

                var itemSection = new ItemSection(sitecoreData).Create();
                var contextSection = new ContextSection(sitecoreData).Create();
                var serverSection = new ServerSection(sitecoreData).Create();

                if (itemSection != null)
                {
                    plugin.AddRow().Column("Item").Column(itemSection).Selected();
                }

                if (contextSection != null)
                {
                    plugin.AddRow().Column("Context").Column(contextSection).Quiet();
                }

                if (serverSection != null)
                {
                    plugin.AddRow().Column("Server").Column(serverSection);
                }

                return plugin;
            }
            catch (Exception ex)
            {
                return new { Exception = ex };
            }
        }

        #endregion
    }
}