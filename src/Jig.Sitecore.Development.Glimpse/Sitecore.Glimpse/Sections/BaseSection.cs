﻿namespace Jig.Sitecore.Development.Glimpse
{
    using System.Collections.Generic;

    using global::Glimpse.Core.Tab.Assist;

    /// <summary>
    /// The base section.
    /// </summary>
    public abstract class BaseSection
    {
        #region Fields

        /// <summary>
        /// The request data.
        /// </summary>
        protected readonly RequestData RequestData;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseSection" /> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        protected BaseSection([global::Sitecore.NotNull]RequestData requestData)
        {
            this.RequestData = requestData;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>The <see cref="TabSection" />.</returns>
        public abstract TabSection Create();

        #endregion

        #region Methods

        /// <summary>
        /// The display fields.
        /// </summary>
        /// <param name="fields">The fields.</param>
        /// <param name="section">The section.</param>
        protected static void DisplayFields(KeyValuePair<string, object>[] fields, TabSection section)
        {
            foreach (var field in fields)
            {
                section.AddRow().Column(field.Key).Column(field.Value);
            }
        }

        /// <summary>
        /// The create section.
        /// </summary>
        /// <param name="dataKey">The data key.</param>
        /// <returns>The <see cref="TabSection" />.</returns>
        protected TabSection CreateSection(DataKey dataKey)
        {
            var fieldList = (FieldList)this.RequestData[dataKey];

            if (fieldList == null)
            {
                return null;
            }

            var section = new TabSection("Property", "Value");

            DisplayFields(fieldList.Fields, section);

            return section;
        }

        #endregion
    }
}