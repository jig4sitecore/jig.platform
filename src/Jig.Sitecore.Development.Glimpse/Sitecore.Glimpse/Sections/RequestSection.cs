﻿namespace Jig.Sitecore.Development.Glimpse
{
    using System.Linq;

    using global::Glimpse.Core.Tab.Assist;

    /// <summary>
    /// The request section.
    /// </summary>
    public class RequestSection : BaseSection
    {
        #region Constants

        /// <summary>
        /// The query string key.
        /// </summary>
        private const string QueryStringKey = "QueryString";

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestSection"/> class.
        /// </summary>
        /// <param name="requestData">
        /// The request data.
        /// </param>
        public RequestSection(RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>The <see cref="TabSection" />.</returns>
        public override TabSection Create()
        {
            var fieldList = (FieldList)this.RequestData[DataKey.Request];

            if (fieldList == null)
            {
                return null;
            }

            var section = new TabSection("Request Property", "Value");

            BaseSection.DisplayFields(fieldList.Fields.Where(x => x.Key != QueryStringKey).ToArray(), section);

            ParseQueryString(fieldList, section);

            return section;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The parse query string.
        /// </summary>
        /// <param name="fieldList">The field list.</param>
        /// <param name="section">The section.</param>
        private static void ParseQueryString(FieldList fieldList, TabSection section)
        {
            var queryStringFields = (FieldList)fieldList.Fields.SingleOrDefault(x => x.Key == QueryStringKey).Value;

            if (queryStringFields != null)
            {
                var queryStringSection = new TabSection("key", "value");

                BaseSection.DisplayFields(queryStringFields.Fields, queryStringSection);

                section.Section(QueryStringKey, queryStringSection);
            }
        }

        #endregion
    }
}