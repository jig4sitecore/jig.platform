namespace Jig.Sitecore.Development.Glimpse
{
    using global::Glimpse.Core.Tab.Assist;

    /// <summary>
    /// The server section.
    /// </summary>
    public class ServerSection : BaseSection
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerSection"/> class.
        /// </summary>
        /// <param name="requestData">
        /// The request data.
        /// </param>
        public ServerSection(RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="TabSection"/>.
        /// </returns>
        public override TabSection Create()
        {
            var section = new TabSection(string.Empty, this.GetSectionHeader());

            var licenseSection = new LicenseSection(this.RequestData).Create();
            if (licenseSection != null)
            {
                section.AddRow().Column("License").Column(licenseSection);
            }

            var userListSection = new UserListSection(this.RequestData).Create();
            if (userListSection != null)
            {
                section.AddRow().Column("Current Users").Column(userListSection);
            }

            var servicesSection = new ServicesSection(this.RequestData).Create();
            if (servicesSection != null)
            {
                section.AddRow().Column("Sitecore Services").Column(servicesSection);
            }

            var controllersSection = new ControllersSection(this.RequestData).Create();
            if (controllersSection != null)
            {
                section.AddRow().Column("Controllers").Column(controllersSection);
            }

            return section;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get section header.
        /// </summary>
        /// <returns>The <see cref="string" />.</returns>
        private string GetSectionHeader()
        {
            var versionInfo = (string)this.RequestData[DataKey.VersionInfo];

            return (!string.IsNullOrEmpty(versionInfo)) ? versionInfo : "Value";
        }

        #endregion
    }
}