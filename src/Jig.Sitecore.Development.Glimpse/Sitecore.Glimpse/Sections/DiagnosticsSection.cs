namespace Jig.Sitecore.Development.Glimpse
{
    using global::Glimpse.Core.Tab.Assist;

    /// <summary>
    /// The diagnostics section.
    /// </summary>
    public class DiagnosticsSection : BaseSection
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DiagnosticsSection"/> class.
        /// </summary>
        /// <param name="requestData">
        /// The request data.
        /// </param>
        public DiagnosticsSection(RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="TabSection"/>.
        /// </returns>
        public override TabSection Create()
        {
            return this.CreateSection(DataKey.Diagnostics);
        }

        #endregion
    }
}