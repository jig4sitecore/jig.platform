namespace Jig.Sitecore.Development.Glimpse
{
    using global::Glimpse.Core.Tab.Assist;

    /// <summary>
    /// The user section.
    /// </summary>
    public class UserSection : BaseSection
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UserSection"/> class.
        /// </summary>
        /// <param name="requestData">
        /// The request data.
        /// </param>
        public UserSection(RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="TabSection"/>.
        /// </returns>
        public override TabSection Create()
        {
            return this.CreateSection(DataKey.User);
        }

        #endregion
    }
}