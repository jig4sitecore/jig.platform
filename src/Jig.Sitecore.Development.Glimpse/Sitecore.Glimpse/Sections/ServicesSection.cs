﻿namespace Jig.Sitecore.Development.Glimpse
{
    using System.Linq;

    using global::Glimpse.Core.Tab.Assist;

    using Jig.Sitecore.Development.Glimpse.Model;

    /// <summary>
    /// The services section.
    /// </summary>
    public class ServicesSection : BaseSection
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ServicesSection" /> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public ServicesSection(RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>The <see cref="TabSection" />.</returns>
        public override TabSection Create()
        {
            var services = (SitecoreService[])this.RequestData[DataKey.Services];

            if ((services == null) || (!services.Any()))
            {
                return null;
            }

            var section = new TabSection(
                "Controller", 
                "Url", 
                "ES", 
                "Authorize", 
                "CSRF Protection", 
                "Definition", 
                "Metadata");

            foreach (var service in services)
            {
                section.AddRow()
                    .Column(service.Name)
                    .Column(service.Url)
                    .Column(service.IsEntityService ? "Yes" : "No")
                    .Column(service.Authorise ? "Yes" : "No")
                    .Column(service.CsrfProtection.ToString())
                    .Column(service.Definition)
                    .Column(service.Metadata)
                    .WarnIf(service.CorsEnabled);
            }

            return section;
        }

        #endregion
    }
}