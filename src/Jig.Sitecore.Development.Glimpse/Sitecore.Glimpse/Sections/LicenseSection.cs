﻿namespace Jig.Sitecore.Development.Glimpse
{
    using global::Glimpse.Core.Tab.Assist;

    /// <summary>
    /// The license section.
    /// </summary>
    public class LicenseSection : BaseSection
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseSection" /> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public LicenseSection(RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>The <see cref="TabSection" />.</returns>
        public override TabSection Create()
        {
            var fields = (FieldList)this.RequestData[DataKey.License];

            if (fields == null || fields.Fields.Length == 0)
            {
                return null;
            }

            var section = new TabSection("Property", "Value");

            foreach (var field in fields.Fields)
            {
                section.AddRow().Column(field.Key).Column(field.Value);
            }

            return section;
        }

        #endregion
    }
}