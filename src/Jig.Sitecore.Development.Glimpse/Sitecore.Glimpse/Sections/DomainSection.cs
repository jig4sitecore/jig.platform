namespace Jig.Sitecore.Development.Glimpse
{
    using global::Glimpse.Core.Tab.Assist;

    /// <summary>
    /// The domain section.
    /// </summary>
    public class DomainSection : BaseSection
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DomainSection"/> class.
        /// </summary>
        /// <param name="requestData">
        /// The request data.
        /// </param>
        public DomainSection(RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="TabSection"/>.
        /// </returns>
        public override TabSection Create()
        {
            return this.CreateSection(DataKey.Domain);
        }

        #endregion
    }
}