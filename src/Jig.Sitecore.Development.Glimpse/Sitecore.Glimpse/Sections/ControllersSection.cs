﻿namespace Jig.Sitecore.Development.Glimpse
{
    using System.Linq;

    using global::Glimpse.Core.Tab.Assist;

    using Jig.Sitecore.Development.Glimpse.Model;

    /// <summary>
    /// The controllers section.
    /// </summary>
    public class ControllersSection : BaseSection
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ControllersSection" /> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public ControllersSection(RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>The <see cref="TabSection" />.</returns>
        public override TabSection Create()
        {
            var controllers = (Controller[])this.RequestData[DataKey.Controllers];

            if ((controllers == null) || (!controllers.Any()))
            {
                return null;
            }

            var section = new TabSection("Controller", "Type", "Authorise", "CSRF Protection", "Definition");

            foreach (var controller in controllers)
            {
                section.AddRow()
                    .Column(controller.Name)
                    .Column(controller.ControllerType.ToString())
                    .Column(controller.Authorise ? "Yes" : "No")
                    .Column(controller.CsrfProtection.ToString())
                    .Column(controller.Definition);
            }

            return section;
        }

        #endregion
    }
}