﻿namespace Jig.Sitecore.Development.Glimpse
{
    using global::Glimpse.Core.Tab.Assist;

    /// <summary>
    /// The item section.
    /// </summary>
    public class ItemSection : BaseSection
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemSection" /> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public ItemSection(RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>The <see cref="TabSection" />.</returns>
        public override TabSection Create()
        {
            var section = new TabSection("Item", "Value");

            var itemSection = this.CreateSection(DataKey.Item);
            if (itemSection != null)
            {
                section.AddRow().Column("Properties").Column(itemSection);
            }

            var itemTemplate = new ItemTemplateSection(this.RequestData).Create();
            if (itemTemplate != null)
            {
                section.AddRow().Column("Template").Column(itemTemplate);
            }

            var itemVisualization = new ItemVisualizationSection(this.RequestData).Create();
            if (itemVisualization != null)
            {
                section.AddRow().Column("Visualization").Column(itemVisualization);
            }

            return section;
        }

        #endregion
    }
}