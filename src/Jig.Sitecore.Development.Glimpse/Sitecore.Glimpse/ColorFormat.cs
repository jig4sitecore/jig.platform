namespace Jig.Sitecore.Development.Glimpse
{
    /// <summary>
    /// The color format.
    /// </summary>
    internal class ColorFormat
    {
        #region Constants

        /// <summary>
        /// The highlight.
        /// </summary>
        public const string Highlight = "blue";

        /// <summary>
        /// The lowlight.
        /// </summary>
        public const string Lowlight = "grey";

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The colorize.
        /// </summary>
        /// <param name="colour">The colour.</param>
        /// <param name="data">The data.</param>
        /// <returns>The <see cref="string" />.</returns>
        public static string Colorize(string colour, string data)
        {
            return string.Format("<span style=\"colour:{0};\">{1}</span>", colour, data);
        }

        #endregion
    }
}