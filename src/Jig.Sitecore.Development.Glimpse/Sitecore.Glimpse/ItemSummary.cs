﻿namespace Jig.Sitecore.Development.Glimpse
{
    /// <summary>
    /// The item summary.
    /// </summary>
    public class ItemSummary
    {
        #region Fields

        /// <summary>
        /// The sitecore data.
        /// </summary>
        private readonly RequestData sitecoreData;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemSummary" /> class.
        /// </summary>
        /// <param name="sitecoreData">The sitecore data.</param>
        public ItemSummary(RequestData sitecoreData)
        {
            this.sitecoreData = sitecoreData;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>The <see cref="string" />.</returns>
        public string Create()
        {
            var fieldList = this.GetKeyValue(DataKey.Item);

            if (fieldList == null)
            {
                return null;
            }

            var fullPath = fieldList.GetField("Full Path");
            var templateName = fieldList.GetField("Template Name");

            if (fullPath == null || templateName == null)
            {
                return null;
            }

            return string.Format("{0} [ {1} ]", fullPath, templateName);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get key value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The <see cref="FieldList" />.</returns>
        private FieldList GetKeyValue(DataKey key)
        {
            return (FieldList)this.sitecoreData.GetKeyValue(key);
        }

        #endregion
    }
}