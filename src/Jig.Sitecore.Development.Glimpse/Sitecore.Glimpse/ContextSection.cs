﻿namespace Jig.Sitecore.Development.Glimpse
{
    using global::Glimpse.Core.Tab.Assist;

    /// <summary>
    /// The context section.
    /// </summary>
    public class ContextSection : BaseSection
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextSection" /> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public ContextSection(RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>The <see cref="TabSection" />.</returns>
        public override TabSection Create()
        {
            var section = new TabSection("Context", "Value");

            var siteSection = new SiteSection(this.RequestData).Create();
            if (siteSection != null)
            {
                section.AddRow().Column("Site").Column(siteSection);
            }

            var databaseSection = new DatabaseSection(this.RequestData).Create();
            if (databaseSection != null)
            {
                section.AddRow().Column("Database").Column(databaseSection);
            }

            var deviceSection = new DeviceSection(this.RequestData).Create();
            if (deviceSection != null)
            {
                section.AddRow().Column("Device").Column(deviceSection);
            }

            var domainSection = new DomainSection(this.RequestData).Create();
            if (domainSection != null)
            {
                section.AddRow().Column("Domain").Column(domainSection);
            }

            var languageSection = new LanguageSection(this.RequestData).Create();
            if (languageSection != null)
            {
                section.AddRow().Column("Language").Column(languageSection);
            }

            var cultureSection = new CultureSection(this.RequestData).Create();
            if (cultureSection != null)
            {
                section.AddRow().Column("Culture").Column(cultureSection);
            }

            var userSection = new UserSection(this.RequestData).Create();
            if (userSection != null)
            {
                section.AddRow().Column("User").Column(userSection);
            }

            var requestSection = new RequestSection(this.RequestData).Create();
            if (requestSection != null)
            {
                section.AddRow().Column("Request").Column(requestSection);
            }

            var diagnosticsSection = new DiagnosticsSection(this.RequestData).Create();
            if (diagnosticsSection != null)
            {
                section.AddRow().Column("Diagnostics").Column(diagnosticsSection);
            }

            var pageModeSection = new PageModeSection(this.RequestData).Create();
            if (pageModeSection != null)
            {
                section.AddRow().Column("PageMode").Column(pageModeSection);
            }

            return section;
        }

        #endregion
    }
}