namespace Jig.Sitecore.Development.Glimpse.Analytics
{
    using global::Glimpse.Core.Tab.Assist;

    using Jig.Sitecore.Development.Glimpse.Model.Analytics;

    /// <summary>
    /// The page views section.
    /// </summary>
    public class PageViewsSection : BaseSection
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PageViewsSection" /> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public PageViewsSection(RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>The <see cref="TabSection" />.</returns>
        public override TabSection Create()
        {
            var lastPages = (PageHolder[])this.RequestData[DataKey.LastPages];

            if ((lastPages == null) || (lastPages.Length == 0))
            {
                return null;
            }

            var section = new TabSection("#", "Id", "Timestamp", "Url");

            foreach (var pageHolder in lastPages)
            {
                section.AddRow()
                    .Column(pageHolder.Num)
                    .Column(pageHolder.Id)
                    .Column(pageHolder.Date)
                    .Column(pageHolder.Url);
            }

            return section;
        }

        #endregion
    }
}