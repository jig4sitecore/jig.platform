﻿namespace Jig.Sitecore.Development.Glimpse.Analytics
{
    using System;

    using global::Glimpse.Core.Extensibility;
    using global::Glimpse.Core.Tab.Assist;

    using Jig.Sitecore.Development.Glimpse.Infrastructure;

    /// <summary>
    /// The sitecore analytics tab.
    /// </summary>
    public class SitecoreAnalyticsTab : TabBase, IDocumentation
    {
        #region Fields

        /// <summary>
        /// The sitecore request.
        /// </summary>
        private readonly ISitecoreRequest sitecoreRequest;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SitecoreAnalyticsTab"/> class.
        /// </summary>
        public SitecoreAnalyticsTab()
            : this(new SitecoreAnalyticsForRequest())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SitecoreAnalyticsTab" /> class.
        /// </summary>
        /// <param name="sitecoreRequest">The sitecore request.</param>
        public SitecoreAnalyticsTab(ISitecoreRequest sitecoreRequest)
        {
            this.sitecoreRequest = sitecoreRequest;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the documentation uri.
        /// </summary>
        public string DocumentationUri
        {
            get
            {
                return Constants.Wiki.Url;
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public override string Name
        {
            get
            {
                return "Sitecore Analytics";
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The <see cref="object" />.</returns>
        public override object GetData(ITabContext context)
        {
            try
            {
                var sitecoreData = this.sitecoreRequest.GetData();

                if (!sitecoreData.HasData())
                {
                    return null;
                }

                var analyicsOverviewSection = new AnalyicsOverviewSection(sitecoreData).Create();
                var analticsSummary = new AnalticsSummary(sitecoreData).Create();

                if (string.IsNullOrEmpty(analticsSummary) || (analyicsOverviewSection == null))
                {
                    return null;
                }

                var plugin = Plugin.Create("Visitor", analticsSummary);
                plugin.AddRow().Column("Overview").Column(analyicsOverviewSection).Selected();

                var profilesSection = new ProfilesSection(sitecoreData).Create();
                var goalsSection = new GoalsSection(sitecoreData).Create();
                var pageViewsSection = new PageViewsSection(sitecoreData).Create();

                if (profilesSection != null)
                {
                    plugin.Section("Profiles", profilesSection);
                }

                if (goalsSection != null)
                {
                    plugin.AddRow().Column("Goals").Column(goalsSection).Info();
                }

                if (pageViewsSection != null)
                {
                    plugin.AddRow().Column("Page Views").Column(pageViewsSection).Quiet();
                }

                return plugin;
            }
            catch (Exception ex)
            {
                return new { Exception = ex };
            }
        }

        #endregion
    }
}