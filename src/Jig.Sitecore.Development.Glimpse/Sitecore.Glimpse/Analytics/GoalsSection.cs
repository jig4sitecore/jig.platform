namespace Jig.Sitecore.Development.Glimpse.Analytics
{
    using global::Glimpse.Core.Tab.Assist;

    using Jig.Sitecore.Development.Glimpse.Model.Analytics;

    /// <summary>
    /// The goals section.
    /// </summary>
    public class GoalsSection : BaseSection
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GoalsSection" /> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public GoalsSection([global::Sitecore.NotNull]RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>The <see cref="TabSection" />.</returns>
        public override TabSection Create()
        {
            var goals = (Goal[])this.RequestData[DataKey.Goals];

            if ((goals == null) || (goals.Length == 0))
            {
                return null;
            }

            var section = new TabSection("Timestamp", "Name");

            foreach (var goal in goals)
            {
                section.AddRow().Column(goal.Timestamp).Column(goal.Name);
            }

            return section;
        }

        #endregion
    }
}