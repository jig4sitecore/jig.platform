﻿namespace Jig.Sitecore.Development.Glimpse.Analytics
{
    using global::Glimpse.Core.Tab.Assist;

    /// <summary>
    /// The analyics overview section.
    /// </summary>
    public class AnalyicsOverviewSection : BaseSection
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AnalyicsOverviewSection" /> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public AnalyicsOverviewSection(RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>The <see cref="TabSection" />.</returns>
        public override TabSection Create()
        {
            var isNewVisitor = this.RequestData[DataKey.IsNewVisitor];
            var engagementValue = this.RequestData[DataKey.EngagementValue];
            var trafficType = this.RequestData[DataKey.TrafficType];

            if ((isNewVisitor == null) || (engagementValue == null) || (trafficType == null))
            {
                return null;
            }

            var section = new TabSection("Overview", "Value");

            section.AddRow().Column("New vs. Returning").Column(isNewVisitor);
            section.AddRow().Column("Engagement Value").Column(engagementValue);
            section.AddRow().Column("Traffic Type").Column(trafficType);

            var campaign = this.RequestData[DataKey.Campaign];
            if (campaign != null)
            {
                section.AddRow().Column("Campaign").Column(campaign);
            }

            return section;
        }

        #endregion
    }
}