namespace Jig.Sitecore.Development.Glimpse.Analytics
{
    using global::Glimpse.Core.Tab.Assist;

    using Jig.Sitecore.Development.Glimpse.Model.Analytics;

    /// <summary>
    /// The profiles section.
    /// </summary>
    public class ProfilesSection : BaseSection
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProfilesSection" /> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public ProfilesSection(RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>The <see cref="TabSection" />.</returns>
        public override TabSection Create()
        {
            var profiles = (Profile[])this.RequestData[DataKey.Profiles];

            if ((profiles == null) || (profiles.Length == 0))
            {
                return null;
            }

            var section = new TabSection();

            section.AddRow().Column("Profile name").Column("Pattern Matched").Column("Values");
            foreach (var profile in profiles)
            {
                section.AddRow().Column(profile.Name).Column(profile.PatternCard).Column(profile.Values);
            }

            return section;
        }

        #endregion
    }
}