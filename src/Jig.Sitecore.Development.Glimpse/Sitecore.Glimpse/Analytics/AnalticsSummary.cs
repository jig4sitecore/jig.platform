﻿namespace Jig.Sitecore.Development.Glimpse.Analytics
{
    using System.Linq;

    using Jig.Sitecore.Development.Glimpse.Model.Analytics;

    /// <summary>
    /// The analtics summary.
    /// </summary>
    public class AnalticsSummary
    {
        #region Fields

        /// <summary>
        /// The sitecore data.
        /// </summary>
        private readonly RequestData sitecoreData;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AnalticsSummary"/> class.
        /// </summary>
        /// <param name="sitecoreData">
        /// The sitecore data.
        /// </param>
        public AnalticsSummary([global::Sitecore.NotNull]RequestData sitecoreData)
        {
            this.sitecoreData = sitecoreData;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string Create()
        {
            const string DefaultInsight = "Insight";

            var profiles = (Profile[])this.sitecoreData[DataKey.Profiles];

            if ((profiles == null) || (profiles.Length == 0))
            {
                return DefaultInsight;
            }

            var matchedProfiles = profiles.Where(p => !string.IsNullOrEmpty(p.PatternCard));

            var enumerable = matchedProfiles as Profile[] ?? matchedProfiles.ToArray();
            if (!enumerable.Any())
            {
                return DefaultInsight;
            }

            return enumerable.Select(p => p.PatternCard).Aggregate((acu, ele) => acu + (", " + ele));
        }

        #endregion
    }
}