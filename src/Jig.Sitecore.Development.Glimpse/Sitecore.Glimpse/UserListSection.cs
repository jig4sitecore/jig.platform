﻿namespace Jig.Sitecore.Development.Glimpse
{
    using System.Linq;

    using global::Glimpse.Core.Tab.Assist;

    using Jig.Sitecore.Development.Glimpse.Model;

    /// <summary>
    /// The user list section.
    /// </summary>
    public class UserListSection : BaseSection
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UserListSection" /> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public UserListSection(RequestData requestData)
            : base(requestData)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>The <see cref="TabSection" />.</returns>
        public override TabSection Create()
        {
            var users = (LoggedInUser[])this.RequestData[DataKey.UserList];

            if ((users == null) || (!users.Any()))
            {
                return null;
            }

            var section = new TabSection("Username", "Session ID", "Admin", "Created", "Last Request");

            foreach (var user in users)
            {
                var row =
                    section.AddRow()
                        .Column(user.Name)
                        .Column(user.SessionId)
                        .Column(user.IsAdmin ? "Yes" : "No")
                        .Column(user.Created)
                        .Column(user.LastRequest);

                if (user.IsInactive())
                {
                    row.ApplyRowStyle("warn");
                }
            }

            return section;
        }

        #endregion
    }
}