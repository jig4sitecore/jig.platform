﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Jig.Sitecore.Development.Glimpse.Infrastructure.Reflection;
    using Jig.Sitecore.Development.Glimpse.Model;

    using Newtonsoft.Json;
    using global::Sitecore.Services.Core;
    using global::Sitecore.Services.Core.Configuration;
    using global::Sitecore.Services.Infrastructure.Services;

    /// <summary>
    /// The sitecore services.
    /// </summary>
    public class SitecoreServices : ICollectionProvider<SitecoreService>
    {
        #region Fields

        /// <summary>
        /// The controller name generator.
        /// </summary>
        private readonly IControllerNameGenerator controllerNameGenerator;

        /// <summary>
        /// The meta data builder.
        /// </summary>
        private readonly IMetaDataBuilder metaDataBuilder;

        /// <summary>
        /// The services configuration.
        /// </summary>
        private readonly IServicesConfiguration servicesConfiguration;

        /// <summary>
        /// The type provider.
        /// </summary>
        private readonly global::Jig.Sitecore.Development.Glimpse.ITypeProvider typeProvider;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SitecoreServices" /> class.
        /// </summary>
        /// <param name="typeProvider">The type provider.</param>
        /// <param name="controllerNameGenerator">The controller name generator.</param>
        /// <param name="metaDataBuilder">The meta data builder.</param>
        /// <param name="servicesConfiguration">The services configuration.</param>
        /// <exception cref="System.ArgumentNullException">typeProvider
        /// or
        /// controllerNameGenerator
        /// or
        /// metaDataBuilder
        /// or
        /// servicesConfiguration</exception>
        /// <exception cref="ArgumentNullException">typeProvider
        /// or
        /// controllerNameGenerator
        /// or
        /// metaDataBuilder
        /// or
        /// servicesConfiguration</exception>
        public SitecoreServices(
            global::Jig.Sitecore.Development.Glimpse.ITypeProvider typeProvider, 
            IControllerNameGenerator controllerNameGenerator, 
            IMetaDataBuilder metaDataBuilder, 
            IServicesConfiguration servicesConfiguration)
        {
            if (typeProvider == null)
            {
                throw new ArgumentNullException("typeProvider");
            }

            if (controllerNameGenerator == null)
            {
                throw new ArgumentNullException("controllerNameGenerator");
            }

            if (metaDataBuilder == null)
            {
                throw new ArgumentNullException("metaDataBuilder");
            }

            if (servicesConfiguration == null)
            {
                throw new ArgumentNullException("servicesConfiguration");
            }

            this.typeProvider = typeProvider;
            this.controllerNameGenerator = controllerNameGenerator;
            this.metaDataBuilder = metaDataBuilder;
            this.servicesConfiguration = servicesConfiguration;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the collection.
        /// </summary>
        public ICollection<SitecoreService> Collection
        {
            get
            {
                return this.typeProvider.Types.Select(this.BuildSitecoreService).Where(x => x != null).ToArray();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The format json metadata.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The <see cref="string" />.</returns>
        private static string FormatJsonMetadata(string value)
        {
            var metadataObject = JsonConvert.DeserializeObject<Dictionary<string, object>>(value);

            return JsonConvert.SerializeObject(metadataObject, Formatting.Indented);
        }

        /// <summary>
        /// The remove controller suffix.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>The <see cref="string" />.</returns>
        private static string RemoveControllerSuffix(string name)
        {
            return name.Remove(name.Length - "Controller".Length);
        }

        /// <summary>
        /// The build sitecore service.
        /// </summary>
        /// <param name="controllerType">The controller type.</param>
        /// <returns>The <see cref="SitecoreService" />.</returns>
        private SitecoreService BuildSitecoreService(Type controllerType)
        {
            var controller = new TypeViewer(controllerType);

            var service = new SitecoreService
                              {
                                  Name = RemoveControllerSuffix(controllerType.FullName), 
                                  Url = this.GetRouteFromType(controllerType), 
                                  Definition = controller.ToJson(), 
                                  CsrfProtection = controller.CheckForMitigations(ControllerType.WebApi), 
                                  Authorise = controller.CheckForAuthorize(ControllerType.WebApi)
                              };

            var entityService = controllerType.GetGenericInterface(typeof(IEntityService<>));

            if (entityService != null)
            {
                var pocoObject = entityService.GetGenericArguments()[0];

                service.IsEntityService = true;
                service.Metadata = this.GetMetadata(pocoObject);
            }

            return service;
        }

        /// <summary>
        /// The get metadata.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The <see cref="string" />.</returns>
        private string GetMetadata(Type type)
        {
            try
            {
                var metadata = this.metaDataBuilder.Parse(type);

                return FormatJsonMetadata(metadata);
            }
            catch (Exception exception)
            {
                return exception.Message;
            }
        }

        /// <summary>
        /// The get route from type.
        /// </summary>
        /// <param name="controllerType">The controller type.</param>
        /// <returns>The <see cref="string" />.</returns>
        private string GetRouteFromType(Type controllerType)
        {
            if (ServicesControllerAttribute.IsPresentOn(controllerType))
            {
                var name = this.controllerNameGenerator.GetName(controllerType);

                return string.Concat(
                    this.servicesConfiguration.Configuration.Services.Routes.RouteBase, 
                    name.Replace('.', '/'));
            }

            return "See Routes tab for details";
        }

        #endregion
    }
}