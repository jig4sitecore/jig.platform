namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using global::Sitecore.SecurityModel.License;
    using global::Sitecore.Xml;

    /// <summary>
    /// The license reader.
    /// </summary>
    internal class LicenseReader
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get info.
        /// </summary>
        /// <returns>The <see cref="LicenseInfo" />.</returns>
        public LicenseInfo GetInfo()
        {
            var license = License.VerifiedLicense();
            if (license == null)
            {
                return null;
            }

            var licenseNode = license.SelectSingleNode("/verifiedlicense/license");
            var licenseInfo = new LicenseInfo
                                  {
                                      Id = XmlUtil.GetChildValue("id", licenseNode),
                                      Expiration = global::Sitecore.DateUtil.IsoDateToDateTime(XmlUtil.GetChildValue("expiration", licenseNode)),
                                      Version = int.Parse(XmlUtil.GetChildValue("version", licenseNode)),
                                      Licensee = XmlUtil.GetChildValue("licensee", licenseNode),
                                      Country = XmlUtil.GetChildValue("country", licenseNode)
                                  };

            return licenseInfo;
        }

        #endregion
    }
}