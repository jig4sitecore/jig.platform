﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using System;
    using Jig.Sitecore.Development.Glimpse.Model.Analytics;
    using global::Sitecore.Data.Items;

    /// <summary>
    /// The SitecoreRepository interface.
    /// </summary>
    public interface ISitecoreRepository
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get item.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>The <see cref="Item" />.</returns>
        Item GetItem(string itemId);

        /// <summary>
        /// The get item.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>The <see cref="Item" />.</returns>
        Item GetItem(Guid itemId);

        /// <summary>
        /// The get pattern cards.
        /// </summary>
        /// <returns>
        /// The PatternCard.
        /// </returns>
        PatternCard[] GetPatternCards();

        /// <summary>
        /// The is goal.
        /// </summary>
        /// <param name="pageEventDefinitionId">The page event definition id.</param>
        /// <returns>The <see cref="bool" />.</returns>
        bool IsGoal(Guid pageEventDefinitionId);

        #endregion
    }
}