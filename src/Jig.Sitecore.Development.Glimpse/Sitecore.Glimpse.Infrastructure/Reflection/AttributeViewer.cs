﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure.Reflection
{
    using System;

    /// <summary>
    /// The attribute viewer.
    /// </summary>
    public class AttributeViewer
    {
        #region Fields

        /// <summary>
        /// The type.
        /// </summary>
        private readonly Type type;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeViewer" /> class.
        /// </summary>
        /// <param name="type">The type.</param>
        public AttributeViewer(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            this.type = type;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the base.
        /// </summary>
        public AttributeViewer Base
        {
            get
            {
                return !this.type.BaseType.IsRootAttribute() ? new AttributeViewer(this.type.BaseType) : null;
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.type.Name.RemoveFromEnd("Attribute");
            }
        }

        /// <summary>
        /// Gets the underlying type.
        /// </summary>
        public Type UnderlyingType
        {
            get
            {
                return this.type;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The should serialize base.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeBase()
        {
            return this.Base != null;
        }

        #endregion
    }
}