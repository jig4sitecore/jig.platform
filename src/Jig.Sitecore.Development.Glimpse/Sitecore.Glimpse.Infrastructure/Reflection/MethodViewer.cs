﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure.Reflection
{
    using System;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// The method viewer.
    /// </summary>
    public class MethodViewer
    {
        #region Fields

        /// <summary>
        /// The method info.
        /// </summary>
        private readonly MethodInfo methodInfo;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MethodViewer" /> class.
        /// </summary>
        /// <param name="methodInfo">The method info.</param>
        public MethodViewer([global::Sitecore.NotNull]MethodInfo methodInfo)
        {
            if (methodInfo == null)
            {
                throw new ArgumentNullException("methodInfo");
            }

            this.methodInfo = methodInfo;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the attributes.
        /// </summary>
        [global::Sitecore.NotNull]
        public AttributeViewer[] Attributes
        {
            get
            {
                return
                    this.methodInfo.GetCustomAttributes(false)
                        .Select(x => x.GetType())
                        .Where(x => !x.IsRootAttribute())
                        .Select(x => new AttributeViewer(x))
                        .ToArray();
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        [global::Sitecore.NotNull]
        public string Name
        {
            get
            {
                return this.methodInfo.Name;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The should serialize attributes.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeAttributes()
        {
            return this.Attributes.Length > 0;
        }

        #endregion
    }
}