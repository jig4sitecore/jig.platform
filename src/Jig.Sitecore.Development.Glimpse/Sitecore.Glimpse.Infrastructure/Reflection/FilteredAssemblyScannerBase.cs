namespace Jig.Sitecore.Development.Glimpse.Infrastructure.Reflection
{
    using System;
    using System.Linq;

    /// <summary>
    /// The filtered assembly scanner base.
    /// </summary>
    internal abstract class FilteredAssemblyScannerBase : ITypeProvider
    {
        #region Fields

        /// <summary>
        /// The type provider.
        /// </summary>
        private readonly ITypeProvider typeProvider;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FilteredAssemblyScannerBase"/> class.
        /// </summary>
        /// <param name="typeProvider">
        /// The type provider.
        /// </param>
        protected FilteredAssemblyScannerBase(ITypeProvider typeProvider)
        {
            this.typeProvider = typeProvider;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the types.
        /// </summary>
        public IQueryable<Type> Types
        {
            get
            {
                return this.typeProvider.Types.Where(this.Filter).AsQueryable();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The filter.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        protected abstract bool Filter(Type type);

        #endregion
    }
}