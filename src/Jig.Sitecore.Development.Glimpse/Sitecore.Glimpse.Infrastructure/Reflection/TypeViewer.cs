﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure.Reflection
{
    using System;
    using System.Linq;
    using System.Reflection;
    using Newtonsoft.Json;

    /// <summary>
    /// The type viewer.
    /// </summary>
    public class TypeViewer
    {
        #region Fields

        /// <summary>
        /// The type.
        /// </summary>
        private readonly Type type;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TypeViewer" /> class.
        /// </summary>
        /// <param name="type">The type.</param>
        public TypeViewer([global::Sitecore.NotNull]Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            this.type = type;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the attributes.
        /// </summary>
        public AttributeViewer[] Attributes
        {
            get
            {
                return
                    this.type.GetCustomAttributes(false)
                        .Select(x => x.GetType())
                        .Where(x => !x.IsRootAttribute())
                        .Select(x => new AttributeViewer(x))
                        .ToArray();
            }
        }

        /// <summary>
        /// Gets the base.
        /// </summary>
        public TypeViewer Base
        {
            get
            {
                // ReSharper disable AssignNullToNotNullAttribute
                return !this.type.IsRootType() ? new TypeViewer(this.type.BaseType) : null;
                // ReSharper restore AssignNullToNotNullAttribute
            }
        }

        /// <summary>
        /// Gets the methods.
        /// </summary>
        public MethodViewer[] Methods
        {
            get
            {
                return
                    this.type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly)
                        .Where(mi => !IsPropertyAccessor(mi))
                        .Select(mi => new MethodViewer(mi))
                        .ToArray();
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.type.FullName;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The has class attribute.
        /// </summary>
        /// <param name="typeName">The type name.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public bool HasClassAttribute(string typeName)
        {
            if (this.Attributes.ContainsType(typeName))
            {
                return true;
            }

            return this.Base != null && this.Base.HasClassAttribute(typeName);
        }

        /// <summary>
        /// The has method attribute.
        /// </summary>
        /// <param name="typeName">The type name.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public bool HasMethodAttribute(string typeName)
        {
            if (this.Methods.Any(method => method.Attributes.ContainsType(typeName)))
            {
                return true;
            }

            return this.Base != null && this.Base.HasMethodAttribute(typeName);
        }

        /// <summary>
        /// The should serialize attributes.
        /// </summary>
        /// <returns>The <see cref="bool" />.</returns>
        public bool ShouldSerializeAttributes()
        {
            return this.Attributes.Length > 0;
        }

        /// <summary>
        /// The should serialize base.
        /// </summary>
        /// <returns>The <see cref="bool" />.</returns>
        public bool ShouldSerializeBase()
        {
            return this.Base != null;
        }

        /// <summary>
        /// The should serialize methods.
        /// </summary>
        /// <returns>The <see cref="bool" />.</returns>
        public bool ShouldSerializeMethods()
        {
            return this.Methods.Length > 0;
        }

        /// <summary>
        /// The to json.
        /// </summary>
        /// <returns>The <see cref="string" />.</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The is property accessor.
        /// </summary>
        /// <param name="methodInfo">The method info.</param>
        /// <returns>The <see cref="bool" />.</returns>
        private static bool IsPropertyAccessor(MethodInfo methodInfo)
        {
            return methodInfo.IsSpecialName
                   && (methodInfo.Name.StartsWith("set_") || methodInfo.Name.StartsWith("get_"));
        }

        #endregion
    }
}