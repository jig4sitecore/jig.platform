namespace Jig.Sitecore.Development.Glimpse.Infrastructure.Reflection
{
    using System;

    using global::Sitecore.Services.Core;
    using global::Sitecore.Services.Infrastructure.Web.Http;

    /// <summary>
    /// The services controller assembly scanner.
    /// </summary>
    internal class ServicesControllerAssemblyScanner : FilteredAssemblyScannerBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ServicesControllerAssemblyScanner" /> class.
        /// </summary>
        /// <param name="typeProvider">The type provider.</param>
        public ServicesControllerAssemblyScanner(global::Jig.Sitecore.Development.Glimpse.ITypeProvider typeProvider)
            : base(typeProvider)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// The filter.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The <see cref="bool" />.</returns>
        protected override bool Filter(Type type)
        {
            if (type.IsAbstract)
            {
                return false;
            }

            return ServicesControllerAttribute.IsPresentOn(type) || ServicesApiController.IsServicesController(type);
        }

        #endregion
    }
}