namespace Jig.Sitecore.Development.Glimpse.Infrastructure.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// The assembly scanner.
    /// </summary>
    internal class AssemblyScanner : ITypeProvider
    {
        #region Static Fields

        /// <summary>
        /// The site assemblies.
        /// </summary>
        private static Assembly[] siteAssemblies;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the types.
        /// </summary>
        public IQueryable<Type> Types
        {
            get
            {
                return GetSiteAssemblies().SelectMany(GetTypes).AsQueryable();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get site assemblies.
        /// </summary>
        /// <returns>
        /// The collection of the Site Assemblies.
        /// </returns>
        private static IEnumerable<Assembly> GetSiteAssemblies()
        {
            return siteAssemblies ?? (siteAssemblies = AppDomain.CurrentDomain.GetAssemblies());
        }

        /// <summary>
        /// The get types.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>The type from assembly.</returns>
        private static IEnumerable<Type> GetTypes(Assembly assembly)
        {
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException)
            {
                return new Type[] { };
            }
        }

        #endregion
    }
}