﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using global::Sitecore.Analytics;

    /// <summary>
    /// The TrackerBuilder interface.
    /// </summary>
    public interface ITrackerBuilder
    {
        #region Public Properties

        /// <summary>
        /// Gets the tracker.
        /// </summary>
        ITracker Tracker { get; }

        #endregion
    }
}