﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using System.Collections.Generic;
    using System.Linq;
    using Jig.Sitecore.Development.Glimpse.Model;

    /// <summary>
    /// The current users.
    /// </summary>
    internal class CurrentUsers : ICollectionProvider<LoggedInUser>
    {
        #region Public Properties

        /// <summary>
        /// Gets the collection.
        /// </summary>
        public ICollection<LoggedInUser> Collection
        {
            get
            {
                return global::Sitecore.Web.Authentication.DomainAccessGuard.Sessions.Select(GetSitecoreUser).ToArray();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get sitecore user.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <returns>The <see cref="LoggedInUser" />.</returns>
        private static LoggedInUser GetSitecoreUser(global::Sitecore.Web.Authentication.DomainAccessGuard.Session session)
        {
            var sitecoreUser = global::Sitecore.Security.Accounts.User.FromName(session.UserName, false);

            return new LoggedInUser(
                session.SessionID,
                session.UserName,
                session.Created,
                session.LastRequest,
                sitecoreUser.IsAdministrator);
        }

        #endregion
    }
}