﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using Jig.Sitecore.Development.Glimpse.Caching;
    using Jig.Sitecore.Development.Glimpse.Model;

    /// <summary>
    /// The caching sitecore services.
    /// </summary>
    public class CachingSitecoreServices : CachingCollectionProvider<SitecoreService>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CachingSitecoreServices" /> class.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="cache">The cache.</param>
        public CachingSitecoreServices(ICollectionProvider<SitecoreService> provider, ICache cache)
            : base(provider, cache)
        {
        }

        #endregion
    }
}