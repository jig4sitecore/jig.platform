﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    /// <summary>
    /// The trace logger.
    /// </summary>
    public class TraceLogger : ILog
    {
        #region Public Methods and Operators

        /// <summary>
        /// The write.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void Write(string message)
        {
            System.Diagnostics.Trace.Write(message);
        }

        #endregion
    }
}