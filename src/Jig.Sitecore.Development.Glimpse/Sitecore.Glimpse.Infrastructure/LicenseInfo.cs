namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using System;

    /// <summary>
    /// The license info.
    /// </summary>
    internal class LicenseInfo
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the expiration.
        /// </summary>
        public DateTime Expiration { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the licensee.
        /// </summary>
        public string Licensee { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        public int Version { get; set; }

        #endregion
    }
}