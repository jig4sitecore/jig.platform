namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    /// <summary>
    /// The product version.
    /// </summary>
    public sealed class ProductVersion
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the major part.
        /// </summary>
        public int MajorPart { get; set; }

        /// <summary>
        /// Gets or sets the minor part.
        /// </summary>
        public int MinorPart { get; set; }

        #endregion
    }
}