﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Jig.Sitecore.Development.Glimpse.Model.Analytics;

    /// <summary>
    /// The sitecore analytics for request.
    /// </summary>
    public class SitecoreAnalyticsForRequest : ISitecoreRequest
    {
        #region Fields

        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILog logger;

        /// <summary>
        /// The sitecore repository.
        /// </summary>
        private readonly ISitecoreRepository sitecoreRepository;

        /// <summary>
        /// The tracker builder.
        /// </summary>
        private readonly ITrackerBuilder trackerBuilder;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SitecoreAnalyticsForRequest" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="sitecoreRepository">The sitecore repository.</param>
        /// <param name="trackerBuilder">The tracker builder.</param>
        public SitecoreAnalyticsForRequest(
            ILog logger,
            ISitecoreRepository sitecoreRepository,
            ITrackerBuilder trackerBuilder)
        {
            this.logger = logger;
            this.sitecoreRepository = sitecoreRepository;
            this.trackerBuilder = trackerBuilder;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SitecoreAnalyticsForRequest"/> class.
        /// </summary>
        public SitecoreAnalyticsForRequest()
            : this(
                new TraceLogger(),
                new CachingSitecoreRepository(new SitecoreRepository()),
                new SitecoreContextTrackerBuilder())
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get data.
        /// </summary>
        /// <returns>The <see cref="RequestData" />.</returns>
        public RequestData GetData()
        {
            try
            {
                return this.GetAnalyticsData();
            }
            catch (Exception exception)
            {
                this.logger.Write(
                    string.Format("Failed to load Sitecore Analytics Glimpse data - {0}", exception.Message));
            }

            return new RequestDataNotLoaded();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get analytics data.
        /// </summary>
        /// <returns>
        /// The <see cref="RequestData"/>.
        /// </returns>
        private RequestData GetAnalyticsData()
        {
            var tracker = this.trackerBuilder.Tracker;

            if (tracker.Interaction != null)
            {
                var data = new RequestData();

                data.Add(DataKey.Profiles, this.GetProfiles());
                data.Add(DataKey.LastPages, this.GetLastPages(5));
                data.Add(DataKey.Goals, this.GetGoals(5));
                data.Add(DataKey.Campaign, this.GetCampaign());
                data.Add(DataKey.TrafficType, this.GetTrafficType());
                data.Add(DataKey.EngagementValue, this.GetEngagementValue());
                data.Add(DataKey.IsNewVisitor, this.GetVisitType());

                return data;
            }

            return null;
        }

        /// <summary>
        /// The get campaign.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetCampaign()
        {
            var campaignId = this.trackerBuilder.Tracker.Interaction.CampaignId;

            if (campaignId != null)
            {
                var campaign = this.sitecoreRepository.GetItem(campaignId.ToString());
                if (campaign != null)
                {
                    return campaign.Name;
                }

                return campaignId.ToString();
            }

            return null;
        }

        /// <summary>
        /// The get engagement value.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetEngagementValue()
        {
            return this.trackerBuilder.Tracker.Interaction.Value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// The get goals.
        /// </summary>
        /// <param name="numberOfGoals">
        /// The number of goals.
        /// </param>
        /// <returns>
        /// The Goal collection.
        /// </returns>
        private Goal[] GetGoals(int numberOfGoals)
        {
            var tracker = this.trackerBuilder.Tracker;

            if (tracker.Interaction != null)
            {
                var pageNo = tracker.Interaction.CurrentPage.VisitPageIndex;

                var goalList = new List<Goal>();

                while (goalList.Count < numberOfGoals && pageNo > 0)
                {
                    var page = tracker.Interaction.GetPage(pageNo);
                    var goals = page.PageEvents.Select(ped => new Goal
                                                                  {
                                                                      Name = ped.Name,
                                                                      Timestamp = ped.DateTime
                                                                  });
                    goalList.AddRange(goals);
                    pageNo--;
                }

                return goalList.Take(numberOfGoals).ToArray();
            }

            return null;
        }

        /// <summary>
        /// The get last pages.
        /// </summary>
        /// <param name="numberOfPages">The number of pages.</param>
        /// <returns>The PageHolder.</returns>
        private PageHolder[] GetLastPages(int numberOfPages)
        {
            var pages =
                this.trackerBuilder.Tracker.Interaction.GetPages()
                    .OrderByDescending(p => p.DateTime)
                    .Skip(1)
                    .Take(numberOfPages)
                    .Select(x => new PageHolder(x.VisitPageIndex, x.Item.Id, x.DateTime, x.Url.ToString()))
                    .ToArray();

            return pages;
        }

        /// <summary>
        /// The get matched pattern cards.
        /// </summary>
        /// <returns>
        /// The Matched Pattern Cards.
        /// </returns>
        private IEnumerable<Guid?> GetMatchedPatternCards()
        {
            var profileNames = this.trackerBuilder.Tracker.Interaction.Profiles.GetProfileNames();

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var profileName in profileNames)
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                var id = this.trackerBuilder.Tracker.Interaction.Profiles[profileName].PatternId;

                yield return id;
            }
        }

        /// <summary>
        /// The get profiles.
        /// </summary>
        /// <returns>The profiles.</returns>
        private IEnumerable<Profile> GetProfiles()
        {
            var analyticsProfiles = this.trackerBuilder.Tracker.Interaction.Profiles;

            var returnList = new List<Profile>();

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var profileName in analyticsProfiles.GetProfileNames())
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                var profile = analyticsProfiles[profileName];
                returnList.Add(
                    new Profile
                        {
                            Name = profileName,
                            PatternCard = profile.PatternLabel,
                            Values = profile.ToString()
                        });
            }

            return returnList.ToArray();
        }

        /// <summary>
        /// The get traffic type.
        /// </summary>
        /// <returns>
        /// The trafic type.
        /// </returns>
        private string GetTrafficType()
        {
            // todo get through analytics items
            var trafficTypes = this.sitecoreRepository.GetItem(Constants.Sitecore.Analytics.Templates.TrafficTypes);

            var items =
                trafficTypes.Axes.GetDescendants()
                    .FirstOrDefault(
                        p =>
                        p.Fields["Value"].Value
                        == this.trackerBuilder.Tracker.Interaction.TrafficType.ToString(CultureInfo.InvariantCulture));

            return items != null ? items.Name : null;
        }

        /// <summary>
        /// The get visit type.
        /// </summary>
        /// <returns>
        /// The visit type.
        /// </returns>
        private string GetVisitType()
        {
            var visitCount = this.trackerBuilder.Tracker.Interaction.ContactVisitIndex;
            return visitCount > 1 ? "returning" : "new";
        }

        #endregion
    }
}