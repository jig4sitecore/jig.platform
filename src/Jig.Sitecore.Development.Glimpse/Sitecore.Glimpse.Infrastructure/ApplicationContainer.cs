namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Web.Http.Dispatcher;
    using Jig.Sitecore.Development.Glimpse.Infrastructure.Caching;
    using Jig.Sitecore.Development.Glimpse.Infrastructure.Reflection;
    using Jig.Sitecore.Development.Glimpse.Model;
    using global::Sitecore.Services.Core;
    using global::Sitecore.Services.Core.Configuration;
    using global::Sitecore.Services.Core.Diagnostics;
    using global::Sitecore.Services.Core.MetaData;
    using global::Sitecore.Services.Infrastructure.Services;
    using global::Sitecore.Services.Infrastructure.Sitecore.Configuration;
    using global::Sitecore.Services.Infrastructure.Sitecore.Diagnostics;

    /// <summary>
    /// The application container.
    /// </summary>
    public static class ApplicationContainer
    {
        #region Static Fields

        /// <summary>
        /// The site assemblies.
        /// </summary>
        private static Assembly[] siteAssemblies;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The controllers.
        /// </summary>
        /// <returns>
        /// The collection of the controlers.
        /// </returns>
        public static IEnumerable<Controller> Controllers()
        {
            return new Controllers(GetAssemblyScanner(), GetServicesControllerScanner()).Collection;
        }

        /// <summary>
        /// The current users.
        /// </summary>
        /// <returns>
        /// The collection of the curent users.
        /// </returns>
        public static IEnumerable<LoggedInUser> CurrentUsers()
        {
            return new CurrentUsers().Collection;
        }

        /// <summary>
        /// The resolve logger.
        /// </summary>
        /// <returns>
        /// The <see cref="ILogger"/>.
        /// </returns>
        public static ILogger ResolveLogger()
        {
            return new SitecoreLogger();
        }

        /// <summary>
        /// The sitecore service.
        /// </summary>
        /// <returns>
        /// The collection of sitecore services.
        /// </returns>
        public static IEnumerable<SitecoreService> SitecoreService()
        {
            var typeProvider = GetServicesControllerScanner();

            var nameGenerator = new NamespaceQualifiedUniqueNameGenerator(
                DefaultHttpControllerSelector.ControllerSuffix);

            IServicesConfiguration servicesConfiguration = new ServicesSettingsConfigurationProvider();

            var internalService = new SitecoreServices(
                typeProvider, 
                nameGenerator, 
                ResolveMetaDataBuilder(), 
                servicesConfiguration);

            return new CachingSitecoreServices(internalService, new WebCacheAdapter()).Collection;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get assembly scanner.
        /// </summary>
        /// <returns>The <see cref="ITypeProvider" />.</returns>
        private static global::Jig.Sitecore.Development.Glimpse.ITypeProvider GetAssemblyScanner()
        {
            return new AssemblyScanner();
        }

        /// <summary>
        /// The get services controller scanner.
        /// </summary>
        /// <returns>The <see cref="ServicesControllerAssemblyScanner" />.</returns>
        private static ServicesControllerAssemblyScanner GetServicesControllerScanner()
        {
            return new ServicesControllerAssemblyScanner(GetAssemblyScanner());
        }

        /// <summary>
        /// The get site assemblies.
        /// </summary>
        /// <returns>
        /// The collection of the site assemblies/>.
        /// </returns>
        private static Assembly[] GetSiteAssemblies()
        {
            return siteAssemblies ?? (siteAssemblies = AppDomain.CurrentDomain.GetAssemblies());
        }

        /// <summary>
        /// The resolve meta data builder.
        /// </summary>
        /// <returns>
        /// The <see cref="IMetaDataBuilder"/>.
        /// </returns>
        private static IMetaDataBuilder ResolveMetaDataBuilder()
        {
            var genericTypesToMapToArray = new List<string> { "List`1", "IEnumerable`1" };

            var entityParser = new EntityParser(
                new JavascriptTypeMapper(), 
                genericTypesToMapToArray, 
                ResolveValidationMetaDataProvider());

            return new MetaDataBuilder(entityParser);
        }

        /// <summary>
        /// The resolve validation meta data provider.
        /// </summary>
        /// <returns>
        /// The <see cref="IValidationMetaDataProvider"/>.
        /// </returns>
        private static IValidationMetaDataProvider ResolveValidationMetaDataProvider()
        {
            var logger = ResolveLogger();

            return new AssemblyScannerValidationMetaDataProvider(
                new ValidationMetaDataTypeProvider(GetSiteAssemblies()), 
                logger);
        }

        #endregion
    }
}