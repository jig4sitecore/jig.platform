﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Jig.Sitecore.Development.Glimpse.Infrastructure.Reflection;
    using Jig.Sitecore.Development.Glimpse.Model;

    /// <summary>
    /// The controllers.
    /// </summary>
    internal class Controllers : ICollectionProvider<Controller>
    {
        #region Static Fields

        /// <summary>
        /// The type mapper.
        /// </summary>
        private static readonly IDictionary<Type, ControllerType> TypeMapper = new Dictionary<Type, ControllerType>
                                                                                   {
                                                                                       {
                                                                                           typeof(System.Web.Http.ApiController), 
                                                                                           ControllerType
                                                                                           .WebApi
                                                                                       }, 
                                                                                       {
                                                                                           typeof(System.Web.Mvc.Controller), 
                                                                                           ControllerType
                                                                                           .Mvc
                                                                                       }
                                                                                   };

        #endregion

        #region Fields

        /// <summary>
        /// The services type provider.
        /// </summary>
        private readonly ITypeProvider servicesTypeProvider;

        /// <summary>
        /// The type provider.
        /// </summary>
        private readonly ITypeProvider typeProvider;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Controllers" /> class.
        /// </summary>
        /// <param name="typeProvider">The type provider.</param>
        /// <param name="servicesTypeProvider">The services type provider.</param>
        /// <exception cref="System.ArgumentNullException">typeProvider
        /// or
        /// servicesTypeProvider</exception>
        /// <exception cref="ArgumentNullException">typeProvider
        /// or
        /// {D255958A-8513-4226-94B9-080D98F904A1}servicesTypeProvider</exception>
        public Controllers(ITypeProvider typeProvider, ITypeProvider servicesTypeProvider)
        {
            if (typeProvider == null)
            {
                throw new ArgumentNullException("typeProvider");
            }

            if (servicesTypeProvider == null)
            {
                throw new ArgumentNullException("servicesTypeProvider");
            }

            this.typeProvider = typeProvider;
            this.servicesTypeProvider = servicesTypeProvider;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the collection.
        /// </summary>
        public ICollection<Controller> Collection
        {
            get
            {
                var services = this.servicesTypeProvider.Types.ToArray();

                var controllers =
                    this.typeProvider.Types.Where(x => !x.IsAbstract)
                        .Where(x => !services.Contains(x))
                        .Select(x => new ControllerWrapper { Type = x, ControllerType = GetControllerType(x) })
                        .Where(x => x.ControllerType != null);

                return controllers.Select(x => BuildController(x)).ToArray();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The build controller.
        /// </summary>
        /// <param name="wrapper">The wrapper.</param>
        /// <returns>The <see cref="Controller" />.</returns>
        private static Controller BuildController(ControllerWrapper wrapper)
        {
            System.Diagnostics.Debug.Assert(wrapper.ControllerType.HasValue, "The controller must have value");

            var typeViewer = new TypeViewer(wrapper.Type);

            return new Controller(
                wrapper.Type.FullName, 
                wrapper.ControllerType.Value, 
                typeViewer.ToJson(), 
                typeViewer.CheckForMitigations(wrapper.ControllerType.Value), 
                typeViewer.CheckForAuthorize(wrapper.ControllerType.Value));
        }

        /// <summary>
        /// The get controller type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The ControllerType.</returns>
        private static ControllerType? GetControllerType(Type type)
        {
            foreach (var controllerType in TypeMapper.Keys)
            {
                if (IsSameOrSubclass(controllerType, type))
                {
                    return TypeMapper[controllerType];
                }
            }

            return null;
        }

        /// <summary>
        /// The is same or subclass.
        /// </summary>
        /// <param name="potentialBase">The potential base.</param>
        /// <param name="potentialDescendant">The potential descendant.</param>
        /// <returns>The <see cref="bool" />.</returns>
        private static bool IsSameOrSubclass(Type potentialBase, Type potentialDescendant)
        {
            return potentialDescendant.IsSubclassOf(potentialBase) || potentialDescendant == potentialBase;
        }

        #endregion
    }
}