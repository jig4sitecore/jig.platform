namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using System;

    using Jig.Sitecore.Development.Glimpse.Model;

    /// <summary>
    /// The controller wrapper.
    /// </summary>
    internal class ControllerWrapper
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the controller type.
        /// </summary>
        public ControllerType? ControllerType { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public Type Type { get; set; }

        #endregion
    }
}