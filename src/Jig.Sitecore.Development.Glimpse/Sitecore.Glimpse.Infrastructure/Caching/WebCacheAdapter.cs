namespace Jig.Sitecore.Development.Glimpse.Infrastructure.Caching
{
    using System;
    using System.Web.Caching;

    using Jig.Sitecore.Development.Glimpse.Caching;

    /// <summary>
    /// The web cache adapter.
    /// </summary>
    public sealed class WebCacheAdapter : ICache
    {
        #region Fields

        /// <summary>
        /// The cache.
        /// </summary>
        private readonly Cache cache;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WebCacheAdapter" /> class.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">No HttpContext, unable to use the web cache</exception>
        /// <exception cref="InvalidOperationException"></exception>
        public WebCacheAdapter()
        {
            if (System.Web.HttpContext.Current != null)
            {
                this.cache = System.Web.HttpContext.Current.Cache;
            }
            else
            {
                throw new InvalidOperationException("No HttpContext, unable to use the web cache");
            }
        }

        #endregion

        #region Explicit Interface Indexers

        /// <summary>
        /// The i cache.this.
        /// </summary>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object ICache.this[string fieldName]
        {
            get
            {
                return this.cache[fieldName];
            }

            set
            {
                this.cache[fieldName] = value;
            }
        }

        #endregion
    }
}