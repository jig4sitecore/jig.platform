﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using System;
    using System.Linq;
    using Jig.Sitecore.Development.Glimpse.Model.Analytics;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;

    /// <summary>
    /// The sitecore repository.
    /// </summary>
    public class SitecoreRepository : ISitecoreRepository
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get item.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>The <see cref="Item" />.</returns>
        public Item GetItem(string itemId)
        {
            return global::Sitecore.Context.Database.GetItem(new ID(itemId));
        }

        /// <summary>
        /// The get item.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>The <see cref="Item" />.</returns>
        public Item GetItem(Guid itemId)
        {
            return global::Sitecore.Context.Database.GetItem(new ID(itemId));
        }

        /// <summary>
        /// The get pattern cards.
        /// </summary>
        /// <returns>The PatternCard cards.</returns>
        public PatternCard[] GetPatternCards()
        {
            var profileItem = this.GetItem(Constants.Sitecore.MarketingCenter.Profiles);

            var selectItems =
                profileItem.Axes.SelectItems(
                    string.Format(".//*[@@templateid = '{0}']", Constants.Sitecore.Analytics.Templates.PatternCard));

            if (selectItems != null)
            {
                return
                    selectItems.Select(
                        x => new PatternCard { Id = x.ID.Guid, Name = x.Name, Dimension = this.GetProfileDimension(x) })
                        .ToArray();
            }

            return new PatternCard[] { };
        }

        /// <summary>
        /// The is goal.
        /// </summary>
        /// <param name="pageEventDefinitionId">
        /// The page event definition id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsGoal(Guid pageEventDefinitionId)
        {
            return this.GetItem(pageEventDefinitionId).Fields["IsGoal"].Value == "1";
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get profile dimension.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetProfileDimension(Item item)
        {
            if (item.TemplateID == new ID(Constants.Sitecore.Analytics.Templates.Profile))
            {
                return item.Name;
            }

            return item.Parent == null ? null : this.GetProfileDimension(item.Parent);
        }

        #endregion
    }
}