﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using System;
    using Jig.Sitecore.Development.Glimpse.Model.Analytics;
    using global::Sitecore.Data.Items;

    /// <summary>
    /// The caching sitecore repository.
    /// </summary>
    internal class CachingSitecoreRepository : ISitecoreRepository
    {
        #region Static Fields

        /// <summary>
        /// The pattern cards.
        /// </summary>
        private static PatternCard[] patternCards;

        #endregion

        #region Fields

        /// <summary>
        /// The wrapped repository.
        /// </summary>
        private readonly ISitecoreRepository wrappedRepository;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CachingSitecoreRepository"/> class.
        /// </summary>
        /// <param name="wrappedRepository">
        /// The wrapped repository.
        /// </param>
        public CachingSitecoreRepository(ISitecoreRepository wrappedRepository)
        {
            this.wrappedRepository = wrappedRepository;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get item.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>The <see cref="Item" />.</returns>
        public Item GetItem(string itemId)
        {
            return this.wrappedRepository.GetItem(itemId);
        }

        /// <summary>
        /// The get item.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>The <see cref="Item" />.</returns>
        public Item GetItem(Guid itemId)
        {
            return this.wrappedRepository.GetItem(itemId);
        }

        /// <summary>
        /// The get pattern cards.
        /// </summary>
        /// <returns>
        /// The PatternCard collection/>.
        /// </returns>
        public PatternCard[] GetPatternCards()
        {
            return patternCards ?? (patternCards = this.wrappedRepository.GetPatternCards());
        }

        /// <summary>
        /// The is goal.
        /// </summary>
        /// <param name="pageEventDefinitionId">The page event definition id.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public bool IsGoal(Guid pageEventDefinitionId)
        {
            // TODO add caching to this call rather than pass thru
            return this.wrappedRepository.IsGoal(pageEventDefinitionId);
        }

        #endregion
    }
}