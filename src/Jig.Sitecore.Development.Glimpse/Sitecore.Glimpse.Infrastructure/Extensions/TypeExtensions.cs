﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using System;
    using System.Linq;
    using System.Web.Http;
    using System.Web.Http.Filters;

    using global::Sitecore.Services.Infrastructure.Services;
    using global::Sitecore.Services.Infrastructure.Web.Http;

    /// <summary>
    /// The type extensions.
    /// </summary>
    public static class TypeExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get generic interface.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="interfaceType">The interface type.</param>
        /// <returns>The <see cref="Type" />.</returns>
        public static Type GetGenericInterface(this Type type, Type interfaceType)
        {
            return
                type.GetInterfaces()
                    .Where(i => i.IsGenericType)
                    .FirstOrDefault(i => i.GetGenericTypeDefinition() == interfaceType);
        }

        /// <summary>
        /// The is root attribute.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public static bool IsRootAttribute(this Type type)
        {
            var rootTypes = new[]
                                {
                                    typeof(object), typeof(Attribute), typeof(FilterAttribute), 
                                    typeof(ActionFilterAttribute), typeof(ActionNameAttribute), 
                                    typeof(AuthorizationFilterAttribute), typeof(System.Web.Mvc.ActionFilterAttribute), 
                                    typeof(System.Web.Mvc.AcceptVerbsAttribute), typeof(System.Web.Mvc.FilterAttribute), 
                                    typeof(System.Web.Mvc.ActionMethodSelectorAttribute)
                                };

            return (type.BaseType == null) || rootTypes.Contains(type);
        }

        /// <summary>
        /// The is root type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public static bool IsRootType(this Type type)
        {
            var rootTypes = new[]
                                {
                                    typeof(object), typeof(System.Web.Http.ApiController), typeof(ServicesApiController), 
                                    typeof(EntityServiceBase<>), typeof(System.Web.Mvc.Controller)
                                };

            return (type.BaseType == null) || rootTypes.Any(x => x.Name == type.Name)
                    || rootTypes.Any(x => x.Name == type.BaseType.Name);
        }

        #endregion
    }
}
