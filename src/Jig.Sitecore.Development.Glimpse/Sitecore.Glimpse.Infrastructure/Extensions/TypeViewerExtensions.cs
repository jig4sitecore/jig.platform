﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using Jig.Sitecore.Development.Glimpse.Infrastructure.Reflection;
    using Jig.Sitecore.Development.Glimpse.Model;

    /// <summary>
    /// The type viewer extensions.
    /// </summary>
    public static class TypeViewerExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The check for authorize.
        /// </summary>
        /// <param name="typeViewer">The type viewer.</param>
        /// <param name="controllerType">The controller type.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public static bool CheckForAuthorize(this TypeViewer typeViewer, ControllerType controllerType)
        {
            var webApiAuthorizeAttributeName = typeof(System.Web.Http.AuthorizeAttribute).Name;

            var mvcAuthorizeAttributeName = typeof(System.Web.Mvc.AuthorizeAttribute).Name;

            var attributeToDetect = (controllerType == ControllerType.Mvc)
                                        ? mvcAuthorizeAttributeName
                                        : webApiAuthorizeAttributeName;

            var classValidation = typeViewer.HasClassAttribute(attributeToDetect);
            var methodValidation = typeViewer.HasMethodAttribute(attributeToDetect);

            return classValidation || methodValidation;
        }

        /// <summary>
        /// The check for mitigations.
        /// </summary>
        /// <param name="typeViewer">The type viewer.</param>
        /// <param name="controllerType">The controller type.</param>
        /// <returns>The <see cref="Csrf" />.</returns>
        public static Csrf CheckForMitigations(this TypeViewer typeViewer, ControllerType controllerType)
        {
            const string WebApiAntiForgeryAttributeName = "ValidateHttpAntiForgeryTokenAttribute";

            var mvcAntiForgeryAttributeName = typeof(System.Web.Mvc.ValidateAntiForgeryTokenAttribute).Name;

            var attributeToDetect = (controllerType == ControllerType.Mvc)
                                        ? mvcAntiForgeryAttributeName
                                        : WebApiAntiForgeryAttributeName;

            var classValidation = typeViewer.HasClassAttribute(attributeToDetect);
            var methodValidation = typeViewer.HasMethodAttribute(attributeToDetect);

            return classValidation ? Csrf.Class : methodValidation ? Csrf.Method : Csrf.None;
        }

        #endregion
    }
}