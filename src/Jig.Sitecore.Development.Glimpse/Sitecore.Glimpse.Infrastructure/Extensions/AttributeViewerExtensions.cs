﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using System.Collections.Generic;

    using Jig.Sitecore.Development.Glimpse.Infrastructure.Reflection;

    /// <summary>
    /// The attribute viewer extensions.
    /// </summary>
    public static class AttributeViewerExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The contains type.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        /// <param name="typeName">The type name.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public static bool ContainsType(this IEnumerable<AttributeViewer> attributes, string typeName)
        {
            foreach (var attribute in attributes)
            {
                if (attribute.UnderlyingType.Name == typeName)
                {
                    return true;
                }

                var attributeBase = attribute.Base;

                while (attributeBase != null)
                {
                    if (attributeBase.UnderlyingType.Name == typeName)
                    {
                        return true;
                    }

                    attributeBase = attributeBase.Base;
                }
            }

            return false;
        }

        #endregion
    }
}