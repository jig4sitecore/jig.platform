﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using global::Sitecore.Analytics;

    /// <summary>
    /// The sitecore context tracker builder.
    /// </summary>
    public class SitecoreContextTrackerBuilder : ITrackerBuilder
    {
        #region Public Properties

        /// <summary>
        /// Gets the tracker.
        /// </summary>
        public ITracker Tracker
        {
            get
            {
                return global::Sitecore.Analytics.Tracker.Current;
            }
        }

        #endregion
    }
}