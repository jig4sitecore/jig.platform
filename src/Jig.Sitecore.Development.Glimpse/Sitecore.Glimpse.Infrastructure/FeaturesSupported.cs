﻿namespace Jig.Sitecore.Development.Glimpse.Infrastructure
{
    using System.Diagnostics;

    using global::Sitecore.Data.Items;

    /// <summary>
    /// The features supported.
    /// </summary>
    internal static class FeaturesSupported
    {
        #region Static Fields

        /// <summary>
        /// The version.
        /// </summary>
        private static ProductVersion version;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether Sitecore support for Clones introduced in 6.3
        /// </summary>
        /// <value><c>true</c> if clones supported; otherwise, <c>false</c>.</value>
        public static bool Clones
        {
            get
            {
                if (Version.MajorPart >= 7)
                {
                    return true;
                }

                return Version.MajorPart >= 6 && Version.MinorPart >= 3;
            }
        }

        /// <summary>
        /// Gets a value indicating whether services client Sitecore support services client introduced in 7.5
        /// </summary>
        /// <value><c>true</c> if services client Sitecore support services client introduced in 7.5; otherwise, <c>false</c>.</value>
        public static bool ServicesClient
        {
            get
            {
                if (Version.MajorPart >= 8)
                {
                    return true;
                }

                return Version.MajorPart >= 7 && Version.MinorPart >= 5;
            }
        }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        public static ProductVersion Version
        {
            get
            {
                return version ?? (version = GetVersionInfo());
            }

            set
            {
                version = value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get version info.
        /// </summary>
        /// <returns>
        /// The <see cref="ProductVersion"/>.
        /// </returns>
        private static ProductVersion GetVersionInfo()
        {
            var fileVersion = FileVersionInfo.GetVersionInfo(typeof(Item).Assembly.Location);
            return new ProductVersion
                       {
                           MajorPart = fileVersion.ProductMajorPart, 
                           MinorPart = fileVersion.ProductMinorPart
                       };
        }

        #endregion
    }
}