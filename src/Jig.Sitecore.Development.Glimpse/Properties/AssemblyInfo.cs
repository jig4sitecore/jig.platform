﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: Guid("9a5cb337-5bf1-4a40-b9f3-da218a755ffa")]
[assembly: AssemblyTitle("Jig.Sitecore.Development.Glimpse")]

[assembly: AssemblyCompany("Jig4Sitecore Team")]
[assembly: AssemblyProduct("Jig4Sitecore Framework")]
[assembly: AssemblyCopyright("Copyright © Jig4Sitecore Team")]
[assembly: AssemblyTrademark("Jig4Sitecore Team")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyVersion("2.0.0")]
[assembly: AssemblyFileVersion("2.0.0")]
