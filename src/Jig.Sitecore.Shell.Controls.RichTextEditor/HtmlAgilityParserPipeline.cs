﻿namespace Jig.Sitecore.Shell.Controls.RichTextEditor
{
    using System;
    using Jig.Sitecore.Pipelines;
    using Jig.Sitecore.Shell.Controls.RichTextEditor.Pipelines.SaveRichTextContent;
    using global::Sitecore;
    using global::Sitecore.Shell.Controls.RichTextEditor.Pipelines.SaveRichTextContent;

    /// <summary>
    /// The html Agility Parser Pipeline pipeline.
    /// </summary>
    public sealed class HtmlAgilityParserPipeline : ISaveRichTextContent
    {
        /// <summary>
        /// Gets or sets the skip template identifier.
        /// </summary>
        /// <value>The skip template identifier.</value>
        [CanBeNull]
        public string SkipTemplateId { get; set; }

        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process(
            SaveRichTextContentArgs args)
        {
            if (!args.Aborted)
            {
                if (!string.IsNullOrWhiteSpace(this.SkipTemplateId) && args.ProcessorItem != null)
                {
                    var templateId = args.ProcessorItem.InnerItem.TemplateID.Guid;
                    foreach (
                        var value in
                            this.SkipTemplateId.Split(new[] { ',', ';', '|' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        Guid id;
                        if (Guid.TryParse(value, out id) && templateId == id)
                        {
                            return;
                        }
                    }
                }

                /* Run new pipeline to avoid multiple parsing and to string operations */
                var htmlArgs = new HtmlAgilityParserPipelineArgs(args.Content)
                                   {
                                       ProcessorItem = args.ProcessorItem
                                   };

                var html = htmlArgs.Run();

                args.Content = html;
            }
        }

        #endregion
    }
}