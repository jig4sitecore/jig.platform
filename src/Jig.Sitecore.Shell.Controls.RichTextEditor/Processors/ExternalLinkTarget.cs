﻿namespace Jig.Sitecore.Shell.Controls.RichTextEditor.Processors
{
    using System;

    using Jig.Sitecore.Pipelines;

    using global::Sitecore;

    /// <summary>
    /// The external link target to blank
    /// </summary>
    public sealed class ExternalLinkTarget : IHtmlAgilityParserProcessor
    {
        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process([NotNull]HtmlAgilityParserPipelineArgs args)
        {
            if (!args.Aborted)
            {
                /* Set external links target="_blank"  */
                var tags = args.HtmlNodeCollection.Elements("a");
                foreach (var tag in tags)
                {
                    var hrefAttr = tag.Attributes["href"];
                    if (hrefAttr != null && !string.IsNullOrWhiteSpace(hrefAttr.Value)
                        && (hrefAttr.Value.StartsWith("http", StringComparison.InvariantCultureIgnoreCase) || hrefAttr.Value.StartsWith("//", StringComparison.InvariantCultureIgnoreCase)))
                    {
                        /* External Link */
                        var targetAttr = tag.Attributes["target"];
                        if (targetAttr == null)
                        {
                            tag.Attributes.Add("target", "_blank");
                        }
                        else
                        {
                            if (string.IsNullOrWhiteSpace(targetAttr.Value))
                            {
                                targetAttr.Value = "_blank";
                            }
                        }

                        /* Set Title Attribute */
                        var titleAttr = tag.Attributes["title"];
                        if (titleAttr == null)
                        {
                            tag.Attributes.Add("title", tag.InnerText);
                        }
                        else
                        {
                            if (string.IsNullOrWhiteSpace(titleAttr.Value))
                            {
                                titleAttr.Value = tag.InnerText;
                            }
                        }
                    }
                }
            }
        }

        #endregion
    }
}