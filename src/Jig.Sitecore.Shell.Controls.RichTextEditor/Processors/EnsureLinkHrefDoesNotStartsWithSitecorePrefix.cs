﻿namespace Jig.Sitecore.Shell.Controls.RichTextEditor.Processors
{
    using System;

    using Jig.Sitecore.Pipelines;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    /// <summary>
    /// Class Ensure Link Href Does Not Starts With Sitecore Prefix. This class cannot be inherited.
    /// </summary>
    /// <remarks>This processor takes care of CMS misconfiguration/content author mistakes sometimes then links inserted not as <![CDATA[e <a href="/sitecore/content/home">Home</a>]]> but e <a href="~/link.aspx?_id=110D559FDEA542EA9C1C8A5DF7E70EF9&amp;_z=z">Home</a>  </remarks>
    public sealed class EnsureLinkHrefDoesNotStartsWithSitecorePrefix : IHtmlAgilityParserProcessor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnsureLinkHrefDoesNotStartsWithSitecorePrefix"/> class.
        /// </summary>
        public EnsureLinkHrefDoesNotStartsWithSitecorePrefix()
        {
            this.ExcludeStartsWith = "/sitecore/content";
        }

        /// <summary>
        /// Gets or sets the exclude starts with.
        /// </summary>
        [NotNull]
        public string ExcludeStartsWith { get; set; }

        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process([NotNull]HtmlAgilityParserPipelineArgs args)
        {
            if (!args.Aborted)
            {
                /* Set external links target="_blank"  */
                var tags = args.HtmlNodeCollection.Elements("a");
                foreach (var tag in tags)
                {
                    var hrefAttr = tag.Attributes["href"];
                    if (hrefAttr != null && !string.IsNullOrWhiteSpace(hrefAttr.Value)
                        && hrefAttr.Value.StartsWith(this.ExcludeStartsWith, StringComparison.InvariantCultureIgnoreCase))
                    {
                        /* ~/link.aspx?_id={ID}&amp;_z=z */
                        var database = global::Sitecore.Context.Database;
                        if (database != null)
                        {
                            try
                            {
                                var item = database.GetItem(hrefAttr.Value);
                                if (item != null)
                                {
                                    hrefAttr.Value = string.Format("~/link.aspx?_id={0}&amp;_z=z", item.ID.Guid.ToString("N"));
                                }
                            }
                            catch (Exception exception)
                            {
                                Log.Error(exception.Message, this);
                                hrefAttr.Value = "#" + hrefAttr.Value;
                            }
                        }
                    }
                }
            }
        }

        #endregion
    }
}