﻿namespace Jig.Sitecore.Shell.Controls.RichTextEditor.Processors
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    using global::HtmlAgilityPack;

    using Jig.Sitecore.Pipelines;

    using global::Sitecore;

    /// <summary>
    /// The strip html tags processor.
    /// </summary>
    public sealed class SanitizeHtmlTags : IHtmlAgilityParserProcessor
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SanitizeHtmlTags"/> class. 
        /// </summary>
        public SanitizeHtmlTags()
        {
            this.AllowedTags = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the allowed tags.
        /// </summary>
        [NotNull]
        public HashSet<string> AllowedTags { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add allowed tags.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        public void AddAllowedTags([NotNull] XmlNode node)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (node != null && node.Attributes != null && node.Name.Equals("htmlTag", StringComparison.InvariantCultureIgnoreCase))
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                /* <htmlTag name="div"/> */
                var attr = node.Attributes["name"];
                if (attr != null && !string.IsNullOrWhiteSpace(attr.Value) 
                    && (!string.Equals(attr.Name, "script", StringComparison.InvariantCultureIgnoreCase) || !string.Equals(attr.Name, "styles", StringComparison.InvariantCultureIgnoreCase)))
                {
                    /* Don't allow scripts or styles in rich text field. It is not below here at any circumstance. Period */
                    this.AllowedTags.Add(attr.Value.ToLowerInvariant());
                }
            }
        }

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process(HtmlAgilityParserPipelineArgs args)
        {
            if (!args.Aborted)
            {
                foreach (HtmlNode node in args.HtmlNodeCollection)
                {
                    if (!this.AllowedTags.Contains(node.Name))
                    {
                        node.Remove();
                    }
                }
            }
        }

        #endregion
    }
}