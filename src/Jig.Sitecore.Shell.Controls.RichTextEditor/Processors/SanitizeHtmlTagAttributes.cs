﻿namespace Jig.Sitecore.Shell.Controls.RichTextEditor.Processors
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;

    using global::HtmlAgilityPack;

    using Jig.Sitecore.Pipelines;

    using global::Sitecore;

    /// <summary>
    /// The strip html attributes processor.
    /// </summary>
    public sealed class SanitizeHtmlTagAttributes : IHtmlAgilityParserProcessor
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SanitizeHtmlTagAttributes"/> class.
        /// </summary>
        public SanitizeHtmlTagAttributes()
        {
            this.AllowedAttributes = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the allowed attributes.
        /// </summary>
        [NotNull]
        public HashSet<string> AllowedAttributes { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add allowed attributes.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        public void AddAttributes([NotNull] XmlNode node)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (node != null && node.Attributes != null && node.Name.Equals("htmlAttribute", StringComparison.InvariantCultureIgnoreCase))
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                /* <htmlAttribute name="id"/> */
                var attr = node.Attributes["name"];
                if (attr != null && !string.IsNullOrWhiteSpace(attr.Value))
                {
                    this.AllowedAttributes.Add(attr.Value.ToLowerInvariant());
                }
            }
        }

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process(HtmlAgilityParserPipelineArgs args)
        {
            if (!args.Aborted)
            {
                foreach (HtmlNode node in args.HtmlNodeCollection)
                {
                    if (node.Attributes.Count > 0)
                    {
                        var attributesNames = node.Attributes.Select(x => x.Name).ToArray();

                        foreach (string attributeName in attributesNames)
                        {
                            if (attributeName.StartsWith("data-", StringComparison.InvariantCultureIgnoreCase))
                            {
                                continue;
                            }

                            if (!this.AllowedAttributes.Contains(attributeName))
                            {
                                var attribute = node.Attributes[attributeName];
                                attribute.Remove();
                            }
                        }
                    }
                }
            }
        }

        #endregion
    }
}