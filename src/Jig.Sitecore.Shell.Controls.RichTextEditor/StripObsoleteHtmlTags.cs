﻿namespace Jig.Sitecore.Shell.Controls.RichTextEditor
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using Jig.Sitecore.Shell.Controls.RichTextEditor.Pipelines.SaveRichTextContent;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Shell.Controls.RichTextEditor.Pipelines.SaveRichTextContent;

    /// <summary>
    /// The strip obsolete html tags.
    /// </summary>
    public sealed class StripObsoleteHtmlTags : ISaveRichTextContent
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="StripObsoleteHtmlTags"/> class.
        /// </summary>
        public StripObsoleteHtmlTags()
        {
            this.HtmlTags = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the obsolete html tags.
        /// </summary>
        [NotNull]
        public HashSet<string> HtmlTags { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add obsolete html tags.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        public void AddHtmlTags([NotNull] XmlNode node)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (node != null && node.Attributes != null && node.Name.Equals("htmlTag", StringComparison.InvariantCultureIgnoreCase))
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse

                /* <htmlTag name="font"/> */
                var attr = node.Attributes["name"];
                if (attr != null && !string.IsNullOrWhiteSpace(attr.Value))
                {
                    this.HtmlTags.Add(attr.Value.ToLowerInvariant());
                }
            }
        }

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process(
            SaveRichTextContentArgs args)
        {
            if (!args.Aborted)
            {
                var oldHtml = args.Content;
                var html = new StringBuilder(args.Content);
                foreach (var tag in this.HtmlTags)
                {
                    var pattern = "<" + tag + "([^>]*[^/])>";
                    try
                    {
                        var regex = new Regex(
                            pattern,
                            RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline);

                        MatchCollection matchCollection = regex.Matches(oldHtml);
                        foreach (Match match in matchCollection)
                        {
                            if (match.Success)
                            {
                                html.Replace(match.Value, string.Empty);
                                html.Replace("</" + tag + ">", string.Empty);
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        Log.Error(exception.Message, exception, this);
                    }
                }

                args.Content = html.ToString();
            }
        }

        #endregion
    }
}