﻿namespace Jig.Sitecore.Shell.Controls.RichTextEditor.Pipelines.SaveRichTextContent
{
    using global::Sitecore;
    using global::Sitecore.Shell.Controls.RichTextEditor.Pipelines.SaveRichTextContent;

    /// <summary>
    /// The SaveRichTextContent interface.
    /// </summary>
    public interface ISaveRichTextContent
    {
        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">The args.</param>
        void Process([NotNull] SaveRichTextContentArgs args);

        #endregion
    }
}