﻿namespace Jig.Sitecore.Shell.Controls.RichTextEditor
{
    using System.Text;
    using System.Web;

    using Jig.Sitecore.Pipelines;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Pipelines;

    /// <summary>
    /// The html agility parser.
    /// </summary>
    public static class HtmlAgilityParser
    {
        #region Public Methods and Operators

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="pipelineArgs">The args.</param>
        /// <returns>The formated and cleaned html</returns>
        [NotNull]
        public static string Run([NotNull] this HtmlAgilityParserPipelineArgs pipelineArgs)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (pipelineArgs != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                /*  NOTE:
                 *  The code below could be re-used in other pipelines. 
                 *  However to avoid to dependencies sometimes is better duplicate code 
                 */

                var pipelineName = typeof(HtmlAgilityParser).FullName;

                CorePipeline pipeline = CorePipelineFactory.GetPipeline(pipelineName, string.Empty);
                if (pipeline == null)
                {
                    var msg = new StringBuilder(128);
                    msg.Append("Could not get pipeline: ").AppendLine(pipelineName);
                    msg.AppendLine("Missing config file: /App_Config/Include/_shell/Jig.Sitecore.Shell.Controls.RichTextEditor.config");

                    throw new HttpException(msg.ToString());
                }

                pipeline.Run(pipelineArgs);

                var errors = pipelineArgs.GetMessages(PipelineMessageFilter.Errors);
                foreach (var error in errors)
                {
                    Log.Error(error.Text, typeof(HtmlAgilityParser));
                }

                errors = pipelineArgs.GetMessages(PipelineMessageFilter.Warnings);
                foreach (var error in errors)
                {
                    Log.Warn(error.Text, typeof(HtmlAgilityParser));
                }

                return pipelineArgs.Html;
            }

            return string.Empty;
        }

        #endregion
    }
}
