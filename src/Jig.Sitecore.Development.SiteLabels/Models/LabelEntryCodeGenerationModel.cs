﻿namespace Jig.Sitecore.Development.SiteLabels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Jig.Web.CodeGeneration;
    using global::Sitecore;

    using global::Sitecore.Data;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;

    /// <summary>
    /// The Label Entry Code generation Model
    /// </summary>
    public class LabelEntryCodeGenerationModel
    {
        /// <summary>
        /// The label entry template id.
        /// </summary>
        private static readonly ID LabelEntryTemplateId = new ID("84A72B63-3AFD-446A-A566-5F077ACCAB9E");

           #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LabelEntryCodeGenerationModel" /> class.
        /// </summary>
        /// <param name="item">The label item.</param>
        /// <param name="namespace">The namespace.</param>
        /// <param name="requestUri">The request URI.</param>
        public LabelEntryCodeGenerationModel([NotNull] Item item, [NotNull] string @namespace, [NotNull] Uri requestUri)
        {
            global::Sitecore.Diagnostics.Assert.IsNotNull(item, "item != null");
            this.Item = item;
            this.Item.Fields.ReadAll();

            this.ClassName = item.Name.AsClassName() + "Item";

            this.Namespace = @namespace;
            this.RequestUri = requestUri;

            var descendants = item.Axes.GetDescendants();
            this.LabelEntries = descendants.Where(x => x.TemplateID == LabelEntryTemplateId).ToArray();
        }

        #endregion

        /// <summary>
        /// Enum FieldTypes
        /// </summary>
        [Flags]
        public enum FieldTypes
        {
            /// <summary>
            /// The own fields
            /// </summary>
            OwnFields,

            /// <summary>
            /// The system fields
            /// </summary>
            SystemFields,

            /// <summary>
            /// All fields
            /// </summary>
            All
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the class name.
        /// </summary>
        [NotNull]
        public string ClassName { get; set; }

        /// <summary>
        /// Gets or sets the label item.
        /// </summary>
        [NotNull]
        public Item Item { get; protected set; }

        /// <summary>
        /// Gets or sets the namespace.
        /// </summary>
        [NotNull]
        public string Namespace { get; protected set; }

        /// <summary>
        /// Gets or sets the request URI.
        /// </summary>
        /// <value>
        /// The request URI.
        /// </value>
        [NotNull]
        [UsedImplicitly]
        public Uri RequestUri { get; protected set; }

        /// <summary>
        /// Gets the label entries.
        /// </summary>
        [NotNull]
        public IList<Item> LabelEntries { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get item fields.
        /// </summary>
        /// <param name="fieldTypes">The field types.</param>
        /// <returns>The IList of fields</returns>
        [NotNull]
        public IEnumerable<Field> GetItemFieldCollection(FieldTypes fieldTypes = FieldTypes.OwnFields)
        {
            var itemFieldCollection = this.Item.Fields;
            var fields = this.GetItemFieldCollection(fieldTypes, itemFieldCollection);
            return fields.OrderBy(x => x.Name);
        }

        /// <summary>
        /// Gets the item template field collection.
        /// </summary>
        /// <param name="fieldTypes">The field types.</param>
        /// <returns>Item Template fields</returns>
        [NotNull]
        public IEnumerable<TemplateFieldItem> GetItemTemplateFieldCollection(FieldTypes fieldTypes = FieldTypes.OwnFields)
        {
            var template = this.Item.Database.GetTemplate(this.Item.ID);
            if (template != null)
            {
                var itemFieldCollection = template.Fields;
                var fields = this.GetItemFieldCollection(fieldTypes, itemFieldCollection);
                return fields.OrderBy(x => x.Name);
            }

            return new TemplateFieldItem[] { };
        }

        /// <summary>
        /// Gets the item field collection.
        /// </summary>
        /// <param name="fieldTypes">The field types.</param>
        /// <param name="itemFieldCollection">The item field collection.</param>
        /// <returns>The filtered field collection</returns>
        [NotNull]
        protected IEnumerable<Field> GetItemFieldCollection(FieldTypes fieldTypes, [NotNull] IEnumerable<Field> itemFieldCollection)
        {
            var fields = new List<Field>();

            foreach (Field field in itemFieldCollection)
            {
                if (field == null)
                {
                    continue;
                }

                if (fieldTypes == FieldTypes.OwnFields)
                {
                    if (field.HasBlobStream || field.Name.StartsWith("_"))
                    {
                        continue;
                    }

                    fields.Add(field);
                    continue;
                }

                if (fieldTypes == FieldTypes.SystemFields)
                {
                    if (field.Name.StartsWith("__"))
                    {
                        fields.Add(field);
                    }

                    continue;
                }

                if (fieldTypes == (FieldTypes.OwnFields | FieldTypes.SystemFields))
                {
                    if (!field.HasBlobStream)
                    {
                        fields.Add(field);
                    }

                    continue;
                }

                /* Add Blob Fields too*/
                fields.Add(field);
            }

            return fields;
        }

        /// <summary>
        /// Gets the item field collection.
        /// </summary>
        /// <param name="fieldTypes">The field types.</param>
        /// <param name="itemFieldCollection">The item field collection.</param>
        /// <returns>The filtered field collection</returns>
        [NotNull]
        protected IEnumerable<TemplateFieldItem> GetItemFieldCollection(FieldTypes fieldTypes, [NotNull] IEnumerable<TemplateFieldItem> itemFieldCollection)
        {
            var fields = new List<TemplateFieldItem>();

            foreach (var field in itemFieldCollection)
            {
                if (field == null)
                {
                    continue;
                }

                if (fieldTypes == FieldTypes.SystemFields)
                {
                    if (field.Name.StartsWith("__"))
                    {
                        fields.Add(field);
                    }

                    continue;
                }

                if (fieldTypes == FieldTypes.OwnFields)
                {
                    if (field.Name.StartsWith("_"))
                    {
                        continue;
                    }

                    fields.Add(field);
                    continue;
                }

                fields.Add(field);
            }

            return fields;
        }

        #endregion
    }
}