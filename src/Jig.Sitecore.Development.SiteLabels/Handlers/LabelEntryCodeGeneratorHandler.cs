﻿namespace Jig.Sitecore.Development.SiteLabels
{
    /// <summary>
    /// The label class code generator handler.
    /// </summary>
    public sealed class LabelEntryCodeGeneratorHandler : LabelEntryBaseCodeGeneratorHandler<LabelEntryGenerationView>
    {
    }
}