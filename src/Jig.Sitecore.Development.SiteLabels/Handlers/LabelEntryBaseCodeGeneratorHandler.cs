namespace Jig.Sitecore.Development.SiteLabels
{
    using System;
    using System.Diagnostics;
    using System.Web;
    using global::Sitecore.Configuration;

    using global::Sitecore.Data;

    /// <summary>
    /// The Base Code Generator Handler.
    /// </summary>
    /// <typeparam name="TView">The type of the view.</typeparam>
    public abstract class LabelEntryBaseCodeGeneratorHandler<TView> : IHttpHandler where TView : Jig.Web.CodeGeneration.Views.CodeGeneratorView<Jig.Sitecore.Development.SiteLabels.LabelEntryCodeGenerationModel>, new()
    {
        /// <summary>
        /// Gets a value indicating whether is reusable.
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// The process request.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Buffer = true;
            context.Response.BufferOutput = true;
            context.Response.Clear();
            try
            {
                if (!context.Request.IsLocal)
                {
                    var ex = new HttpException("The service available for local request only for security reasons!");
                    Trace.TraceError(ex.ToString());
                    throw ex;
                }

                if (context.Request.QueryString.Count == 0)
                {
                    var url = context.Request.Url.AbsolutePath.ToLowerInvariant();
                    var ex = new HttpException(@"The expected request formats: 
                                                                    " + url + @"?id={Required:Guid}&namespace={Required:C# Namespace}
                                                                    " + url + @"?id={Required:Guid}&namespace={Required:C# Namespace}&className={Recommended or Best Guess}&databaseName={Optional:Default is master}
                                            ");

                    Trace.TraceError(ex.ToString());
                    throw ex;
                }

                var itemId = context.Request.QueryString["id"];
                if (string.IsNullOrWhiteSpace(itemId))
                {
                    var ex = new HttpException("The item ID is not provided !");

                    Trace.TraceError(ex.ToString());
                    throw ex;
                }

                Guid guid;
                if (!Guid.TryParse(itemId.Trim(' ', '{', '}'), out guid))
                {
                    var ex = new HttpException("The item ID is not valid GUID !");

                    Trace.TraceError(ex.ToString());
                    throw ex;
                }

                var databaseName = context.Request.QueryString["databaseName"];
                if (string.IsNullOrWhiteSpace(databaseName))
                {
                    databaseName = "master";
                }

                var database = Factory.GetDatabase(databaseName.Trim());
                if (database == null)
                {
                    var ex = new HttpException("The database with name " + databaseName + " not found!");

                    Trace.TraceError(ex.ToString());
                    throw ex;
                }

                var item = database.GetItem(new ID(guid));
                if (item == null)
                {
                    var ex = new HttpException("The Item with ID " + guid + " not found!");

                    Trace.TraceError(ex.ToString());
                    throw ex;
                }

                var ns = context.Request.QueryString["namespace"];
                if (string.IsNullOrWhiteSpace(ns))
                {
                    var ex = new HttpException("The namespace parameter is required to generate code!");

                    Trace.TraceError(ex.ToString());
                    throw ex;
                }

                var model = new Jig.Sitecore.Development.SiteLabels.LabelEntryCodeGenerationModel(item, ns.Trim(), context.Request.Url);

                var className = context.Request.QueryString["className"];
                model.ClassName = !string.IsNullOrWhiteSpace(className) ? className : "SiteLabels";

                var template = new TView
                {
                    Model = model
                };

                var code = template.Render();

                context.Response.Write(code);
            }
            finally
            {
                context.ApplicationInstance.CompleteRequest();
            }
        }
    }
}