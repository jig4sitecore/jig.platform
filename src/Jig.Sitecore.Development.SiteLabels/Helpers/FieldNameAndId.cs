﻿namespace Jig.Sitecore.Development.SiteLabels
{
    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    /// <summary>
    /// Class FieldNameAndId.
    /// </summary>
    public class FieldNameAndId
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FieldNameAndId" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="id">The identifier.</param>
        public FieldNameAndId([NotNull] string name, [NotNull] global::Sitecore.Data.ID id)
        {
            Assert.IsNotNullOrEmpty(name, "name");
            Assert.IsNotNull(id, "id");
            this.Name = name;
            this.Id = id;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        [NotNull]
        public string Name { get; private set; }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [NotNull]
        public global::Sitecore.Data.ID Id { get; private set; }
    }
}