﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.Twitter
{
    using Jig.Web.Html;

    /// <summary>
    /// The twitter card tag.
    /// </summary>
    public partial class TwitterCardTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            var content = this.Content.Value;
            var tag = new global::Jig.Web.Html.TwitterCardTag(content);

            return tag;
        }

        #endregion
    }
}