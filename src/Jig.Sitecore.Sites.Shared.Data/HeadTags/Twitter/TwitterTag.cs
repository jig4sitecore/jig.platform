﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.Twitter
{
    using Jig.Web.Html;

    /// <summary>
    /// The twitter tag.
    /// </summary>
    public partial class TwitterTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            var name = this.PartialName.Value;
            var content = this.Content.Value;
            var tag = new global::Jig.Web.Html.TwitterTag(name, content);

            return tag;
        }

        #endregion
    }
}