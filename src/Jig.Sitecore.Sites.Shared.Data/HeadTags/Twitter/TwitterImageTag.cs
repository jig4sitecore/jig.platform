﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.Twitter
{
    using Jig.Web.Html;

    using global::Sitecore.Resources.Media;

    /// <summary>
    /// The twitter image tag.
    /// </summary>
    public partial class TwitterImageTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            if (this.Href.IsMediaLink)
            {
                var mediaItem = this.Href.GetMediaItem();
                if (mediaItem != null)
                {
                    var options = new MediaUrlOptions
                    {
                        LowercaseUrls = true,
                        AbsolutePath = false,
                        IncludeExtension = true,
                        UseItemPath = false,
                        RequestExtension = mediaItem.Extension,
                        AlwaysIncludeServerUrl = true
                    };

                    var url = mediaItem.GetMediaItemUrl(options);
                    var tag = new global::Jig.Web.Html.TwitterImageTag(url);

                    return tag;
                }
            }
            else
            {
                var url = this.Href.GetLinkUrl();
                var tag = new global::Jig.Web.Html.TwitterImageTag(url);

                return tag;
            }

            return null;
        }

        #endregion
    }
}