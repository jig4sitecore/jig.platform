﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags
{
    using global::System;
    using global::Sitecore;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;

    /// <summary>
    /// The head tag type base.
    /// </summary>
    public partial class HeadTagTypeBase
    {
        /// <summary>
        /// Gets the head tag type instance.
        /// </summary>
        /// <typeparam name="TType">The type of the type.</typeparam>
        /// <param name="type">The type.</param>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The GetHeadTags Tag instance
        /// </returns>
        [CanBeNull]
        public static TType GetTypeInstance<TType>([NotNull] global::System.Type type, [NotNull] Item item) where TType : class
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (type != null && item != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                try
                {
                    var instance = Activator.CreateInstance(type, item);

                    return instance as TType;
                }
                catch (global::System.Exception exception)
                {
                    Log.Error(exception.Message, exception);
#if DEBUG
                    throw;
#endif
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The Type from </returns>
        [CanBeNull]
        public static global::System.Type GetType([NotNull]string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                try
                {
                    var type = global::System.Type.GetType(value);
                    return type;
                }
                catch (global::System.Exception exception)
                {
                    Log.Error(exception.Message, exception);
#if DEBUG
                    throw;
#endif
                }
            }

            return null;
        }

        #region Public Methods and Operators

        /// <summary>
        /// Gets the type of the head tag.
        /// </summary>
        /// <returns>The head tag type</returns>
        [CanBeNull]
        public global::System.Type GetHeadTagType()
        {
            var type = GetType(this.Type.Value);
            return type;
        }

        #endregion
    }
}