﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.GooglePlus
{
    using Jig.Web.Html;

    /// <summary>
    /// The google plus publisher tag.
    /// </summary>
    public partial class GooglePlusPublisherTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            var url = this.Href.GetLinkUrl();
            var tag = new global::Jig.Web.Html.GooglePlusPublisherTag(url);

            return tag;
        }

        #endregion
    }
}