﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.GooglePlus
{
    using Jig.Web.Html;

    /// <summary>
    /// The google plus name tag.
    /// </summary>
    public partial class GooglePlusNameTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            var url = this.Content.Value;
            var tag = new global::Jig.Web.Html.GooglePlusNameTag(url);

            return tag;
        }

        #endregion
    }
}