﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.GooglePlus
{
    using Jig.Web.Html;

    /// <summary>
    /// The google plus item property tag.
    /// </summary>
    public partial class GooglePlusItemPropertyTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            string name = this.PartialName.Value;
            string content = this.Content.Value;

            var tag = new global::Jig.Web.Html.GooglePlusItemPropertyTag(name, content);

            return tag;
        }

        #endregion
    }
}