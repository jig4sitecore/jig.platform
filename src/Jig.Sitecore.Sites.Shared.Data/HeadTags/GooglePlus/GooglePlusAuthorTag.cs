﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.GooglePlus
{
    using Jig.Web.Html;

    /// <summary>
    /// The google plus author tag.
    /// </summary>
    public partial class GooglePlusAuthorTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            var url = this.Href.GetLinkUrl();
            var tag = new global::Jig.Web.Html.GooglePlusAuthorTag(url);

            return tag;
        }

        #endregion
    }
}