﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.GooglePlus
{
    using Jig.Web.Html;

    /// <summary>
    /// The google plus description tag.
    /// </summary>
    public partial class GooglePlusDescriptionTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            var content = this.Content.Value;
            var tag = new global::Jig.Web.Html.GooglePlusDescriptionTag(content);

            return tag;
        }

        #endregion
    }
}