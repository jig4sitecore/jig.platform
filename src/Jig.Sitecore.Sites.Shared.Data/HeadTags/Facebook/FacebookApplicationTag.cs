﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.Facebook
{
    using Jig.Web.Html;

    /// <summary>
    /// The facebook application tag.
    /// </summary>
    public partial class FacebookApplicationTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            var content = this.Content.Value;
            var tag = new global::Jig.Web.Html.FacebookApplicationTag(content);

            return tag;
        }

        #endregion
    }
}