﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.ContentPages
{
    using Jig.Sitecore.Sites.Shared.Data.Pages;

    using Jig.Web.Html;

    /// <summary>
    /// The open graph url tag.
    /// </summary>
    public partial class OpenGraphImageTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            var item = global::Sitecore.Context.Item;
            if (item != null)
            {
                var content = item[ContentPage.FieldNames.BrowserTitle];

                var tag = new global::Jig.Web.Html.OpenGraphTitleTag(content);

                return tag;
            }

            return null;
        }

        #endregion
    }
}