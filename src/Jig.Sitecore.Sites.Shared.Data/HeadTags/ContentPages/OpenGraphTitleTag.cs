﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.ContentPages
{
    using Jig.Sitecore.Sites.Shared.Data.Pages;
    using Jig.Web.Html;

    /// <summary>
    /// The page title tag.
    /// </summary>
    /// <remarks>Difference this OpenGraphTitleTag from Jig.Sitecore.Sites.Shared.Data.HeadTags.OpenGraphOpenGraphTitleTag is content field source</remarks>
    public partial class OpenGraphTitleTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            var item = global::Sitecore.Context.Item;
            if (item != null)
            {
                var content = item[ContentPage.FieldNames.BrowserTitle];

                var tag = new global::Jig.Web.Html.OpenGraphTitleTag(content);

                return tag;
            }

            return null;
        }

        #endregion
    }
}