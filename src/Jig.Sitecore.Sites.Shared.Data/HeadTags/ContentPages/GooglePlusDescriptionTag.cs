﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.ContentPages
{
    using Jig.Sitecore.Sites.Shared.Data.Pages;

    using Jig.Web.Html;

    /// <summary>
    /// The google plus description tag.
    /// </summary>
    public partial class GooglePlusDescriptionTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            var item = global::Sitecore.Context.Item;
            if (item != null)
            {
                var content = item[ContentPage.FieldNames.PageMetaDescription];

                var tag = new global::Jig.Web.Html.GooglePlusDescriptionTag(content);

                return tag;
            }

            return null;
        }

        #endregion
    }
}