﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.ContentPages
{
    using Jig.Sitecore.Sites.Shared.Data.Pages;

    using Jig.Web.Html;

    /// <summary>
    /// The google plus name tag.
    /// </summary>
    public partial class GooglePlusNameTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            var item = global::Sitecore.Context.Item;
            if (item != null)
            {
                var content = item[ContentPage.FieldNames.BrowserTitle];

                var tag = new global::Jig.Web.Html.GooglePlusNameTag(content);

                return tag;
            }

            return null;
        }

        #endregion
    }
}