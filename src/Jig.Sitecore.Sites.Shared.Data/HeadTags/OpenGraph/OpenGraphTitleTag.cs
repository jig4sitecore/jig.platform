﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.OpenGraph
{
    using Jig.Web.Html;

    /// <summary>
    /// The open graph title tag.
    /// </summary>
    public partial class OpenGraphTitleTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            var content = this.Content.Value;
            var tag = new global::Jig.Web.Html.OpenGraphTitleTag(content);

            return tag;
        }

        #endregion
    }
}