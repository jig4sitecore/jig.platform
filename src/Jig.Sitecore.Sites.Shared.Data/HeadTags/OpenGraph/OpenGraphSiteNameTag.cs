﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.OpenGraph
{
    using Jig.Web.Html;

    /// <summary>
    /// The open graph site name tag.
    /// </summary>
    public partial class OpenGraphSiteNameTag : ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instance.
        /// </returns>
        public IHeadTag GetHeadTag()
        {
            var content = this.Content.Value;
            var tag = new global::Jig.Web.Html.OpenGraphSiteNameTag(content);

            return tag;
        }

        #endregion
    }
}