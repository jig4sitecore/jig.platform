﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags.OpenGraph
{
    using global::Sitecore;

    /// <summary>
    /// The profile last name tag.
    /// </summary>
    public partial class ProfileLastNameTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The render html.
        /// </summary>
        /// <returns>
        /// The rendered html output.
        /// </returns>
        [NotNull]
        public string Render()
        {
            var content = this.Content.Value;
            var tag = new Jig.Web.Html.ProfileLastNameTag(content);

            return tag;
        }

        #endregion
    }
}