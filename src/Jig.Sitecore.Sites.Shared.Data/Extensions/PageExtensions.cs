﻿namespace Jig.Sitecore.Sites.Shared.Data
{
    using global::System.Collections.Generic;
    using global::System.Linq;
    
    using Jig.Sitecore.Sites.Shared.Data.HeadTags;
    using Jig.Sitecore.Sites.Shared.Data.Pages;
    using Jig.Web.Html;
    using global::Sitecore;

    /// <summary>
    /// The page extensions.
    /// </summary>
    public static partial class PageExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get page head tags.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="datasource">The datasource.</param>
        /// <returns>
        /// The Collection of the  Head Tags instances
        /// </returns>
        [NotNull]
        public static IEnumerable<ICreateHeadTag> GetPageHeadTagsInstances([NotNull] this IContentPage page, [NotNull] string datasource = "/_subcontent/_headTags/*")
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (page != null && !string.IsNullOrWhiteSpace(datasource))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var query = page.InnerItem.Paths.FullPath + datasource;

                /* Get Items */
                var items = page.InnerItem.Database.SelectItems(query);
                if (items != null)
                {
                    var tags = CreateHeadTagsTypeInstance(items);
                    return tags.ToArray();
                }
            }

            return new ICreateHeadTag[] { };
        }

        /// <summary>
        /// Gets the page head tags.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="datasource">The datasource.</param>
        /// <returns>The Collection of the  Head Tags</returns>
        /// <remarks>The each page could have multiple sitecore head items and of them could create 1+ IHeadTag (example Image => link, with, height)</remarks>
        [NotNull]
        public static IEnumerable<IHeadTag> GetPageHeadTags([NotNull] this IContentPage page, [NotNull] string datasource = "/_subcontent/_headTags/*")
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (page != null && !string.IsNullOrWhiteSpace(datasource))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var collection = page.GetPageHeadTagsInstances(datasource).ToArray();

                var headTags = CreateHeadTags(collection).ToArray();
                return headTags;
            }

            return new IHeadTag[] { };
        }

        /// <summary>
        /// Gets the page head tags.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="tagFolderId">The tag folder identifier.</param>
        /// <returns>The Collection of the  Head Tags</returns>
        /// <remarks>The each page could have multiple sitecore head items and of them could create 1+ IHeadTag (example Image => link, with, height)</remarks>
        [NotNull]
        public static IEnumerable<IHeadTag> GetPageHeadTags([NotNull] this IContentPage page, [NotNull] global::Sitecore.Data.ID tagFolderId)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (page != null && !tagFolderId.IsNull)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var tagFolder = page.Database.GetItem(tagFolderId, page.Language);
                if (tagFolder != null)
                {
                    var descendants = tagFolder.Axes.GetDescendants();
                    if (descendants != null && descendants.Length > 0)
                    {
                        var collection = CreateHeadTagsTypeInstance(descendants).ToArray();
                        var headTags = CreateHeadTags(collection).ToArray();

                        return headTags;
                    }
                }
            }

            return new IHeadTag[] { };
        }

        /// <summary>
        /// Gets the page head tags.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="tagFolder">The tag folder.</param>
        /// <returns>The Collection of the  Head Tags</returns>
        /// <remarks>The each page could have multiple sitecore head items and of them could create 1+ IHeadTag (example Image =&gt; link, with, height)</remarks>
        [NotNull]
        public static IEnumerable<IHeadTag> GetPageHeadTags([NotNull] this IContentPage page, [NotNull] global::Sitecore.Data.Items.Item tagFolder)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (page != null && tagFolder != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var descendants = tagFolder.Axes.GetDescendants();
                if (descendants != null && descendants.Length > 0)
                {
                    var collection = CreateHeadTagsTypeInstance(descendants).ToArray();
                    var headTags = CreateHeadTags(collection).ToArray();

                    return headTags;
                }
            }

            return new IHeadTag[] { };
        }

        /// <summary>
        /// Creates the head tags.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns>The head Tag instances</returns>
        [NotNull]
        private static IEnumerable<ICreateHeadTag> CreateHeadTagsTypeInstance([NotNull]IEnumerable<global::Sitecore.Data.Items.Item> items)
        {
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (global::Sitecore.Data.Items.Item item in items)
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                var value = item[HeadTagTypeBase.FieldNames.Type];
                var type = HeadTagTypeBase.GetType(value);
                if (type == null)
                {
                    continue;
                }

                var instance = HeadTagTypeBase.GetTypeInstance<ICreateHeadTag>(type, item);
                if (instance != null)
                {
                    yield return instance;
                }
            }
        }

        /// <summary>
        /// Creates the head tags.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns>The Head tag collection</returns>
        [NotNull]
        private static IEnumerable<IHeadTag> CreateHeadTags([NotNull]IEnumerable<ICreateHeadTag> collection)
        {
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var item in collection)
            // ReSharper restore LoopCanBeConvertedToQuery
            {
                var tag = item.GetHeadTag();
                if (tag != null)
                {
                    yield return tag;
                }
            }
        }

        #endregion
    }
}