﻿namespace Jig.Sitecore.Sites.Shared.Data
{
    using global::Sitecore;

    /// <summary>
    /// The item page.
    /// </summary>
    public static partial class ItemExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets the best navigation title.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <returns>The menu title or Display .</returns>
        [NotNull]
        public static string GetBestNavigationTitle([NotNull] this global::Sitecore.Data.Items.Item page)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (page == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return string.Empty;
            }

            // ReSharper restore HeuristicUnreachableCode
            var pageMenuTitle = page[Jig.Sitecore.Sites.Shared.Data.Pages.ContentPage.FieldNames.PageMenuTitle];
            if (!string.IsNullOrEmpty(pageMenuTitle))
            {
                return pageMenuTitle;
            }

            return page.DisplayName;
        }

        /// <summary>
        /// Determines the browser title based on all title fields available on the item.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <returns>
        /// The browser title.
        /// </returns>
        [NotNull]
        public static string GetBestBrowserTitle([NotNull] this global::Sitecore.Data.Items.Item page)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (page == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return string.Empty;
            }

            // ReSharper restore HeuristicUnreachableCode
            var title = page[Jig.Sitecore.Sites.Shared.Data.Pages.ContentPage.FieldNames.BrowserTitle];
            if (!string.IsNullOrEmpty(title))
            {
                return title;
            }

            title = page[Jig.Sitecore.Sites.Shared.Data.Pages.ContentPage.FieldNames.PageMenuTitle];
            if (!string.IsNullOrEmpty(title))
            {
                return title;
            }

            return page.DisplayName;
        }

        #endregion
    }
}
