﻿namespace Jig.Sitecore.Sites.Shared.Data
{
    using global::System;
    using global::System.Collections.Generic;
    using global::System.Linq;

    using Jig.Sitecore.Data;
    using Jig.Sitecore.Sites.Shared.Data.Navigation;
    using Jig.Sitecore.Sites.Shared.Data.Pages;
    using global::Sitecore;
    using global::Sitecore.Data.Items;

    using global::System.Xml.Linq;

    /// <summary>
    /// The navigation link.
    /// </summary>
    public static partial class NavigationLinkExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The gets best navigation title.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <returns>
        /// The <see cref="string" />.
        /// </returns>
        [NotNull]
        public static string GetBestNavigationTitle([NotNull] this INavigationLink link)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return string.Empty;
            }

            // ReSharper restore HeuristicUnreachableCode
            var title = link.Link.GetBestNavigationTitle();
            return title;
        }

        /// <summary>
        /// The get children links.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <returns>
        /// The  Link Collection.
        /// </returns>
        [NotNull]
        public static IList<INavigationLink> GetChildrenNavigationLinks([NotNull] this INavigationLink link)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link != null && link.InnerItem.HasChildren)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var links = link.InnerItem.Children.As<INavigationLink>().ToArray();
                return links;
            }

            return new INavigationLink[] { };
        }

        /// <summary>
        /// Gets the link page.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <returns>
        /// The link page
        /// </returns>
        [CanBeNull]
        public static IPage GetPage([NotNull] this INavigationLink link)
        {
            var page = link.Link.GetLinkPage();
            return page;
        }

        /// <summary>
        /// Inspects the Link's properties to determine the best inner text
        /// for the A tag.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <returns>
        /// The inner text for the A tag.
        /// </returns>
        [NotNull]
        public static string GetLinkText([NotNull] this INavigationLink link)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return string.Empty;
            }

            // ReSharper restore HeuristicUnreachableCode
            var text = link.Link.GetLinkText();
            return text;
        }

        /// <summary>
        /// Inspects the Link's properties to determine the best URL.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <returns>
        /// The URL for the rendering.
        /// </returns>
        [NotNull]
        public static string GetUrl([NotNull] this INavigationLink link)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return string.Empty;
            }

            // ReSharper restore HeuristicUnreachableCode
            var url = link.Link.GetLinkUrl();
            return url;
        }

        /// <summary>
        /// Gets the parent link.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <returns>
        /// The Parent link
        /// </returns>
        [CanBeNull]
        public static INavigationLink GetParentLink([NotNull] this INavigationLink link)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return null;
            }

            // ReSharper restore HeuristicUnreachableCode
            var parent = link.InnerItem.Parent.As<INavigationLink>();
            return parent;
        }

        /// <summary>
        /// Determines whether [is active link] [the specified page item].
        /// </summary>
        /// <param name="link">The link.</param>
        /// <param name="pageItem">The page item.</param>
        /// <returns>
        /// True if link target item matches Page
        /// </returns>
        public static bool IsActiveLink([NotNull] this INavigationLink link, [NotNull] IPage pageItem)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link == null || pageItem != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                IPage linkPage = link.GetPage();

                /* Check: The link could be external */
                if (linkPage != null)
                {
                    var active = linkPage.InnerItem == pageItem.InnerItem;
                    return active;
                }
            }

            return false;
        }

        /// <summary>
        /// Determines if one item is an ancestor of another by comparing their paths.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <param name="item">The presumed ancestor.</param>
        /// <returns>
        /// True if the descendant path starts with the ancestor path.
        /// </returns>
        // ReSharper disable CodeAnnotationAnalyzer
        public static bool IsAncestorItem(this INavigationLink link, [NotNull] StandardTemplate item)
        // ReSharper restore CodeAnnotationAnalyzer
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link == null || item == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return false;
            }

            var targetItem = link.Link.TargetItem;
            if (targetItem != null)
            {
                var ancestor = targetItem.As<StandardTemplate>(link.InnerItem.Language);
                if (ancestor == null)
                {
                    return false;
                }

                var contextPath = item.InnerItem.Paths.FullPath;
                var ancestorPath = ancestor.InnerItem.Paths.FullPath;
                var value = contextPath.StartsWith(ancestorPath, StringComparison.InvariantCultureIgnoreCase);

                return value;
            }

            return false;
        }

        /// <summary>
        /// Determines whether the specified link is ancestor.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <param name="item">The item.</param>
        /// <returns>True if the descendant path starts with the ancestor path.</returns>
        public static bool IsAncestorItem([NotNull] this INavigationLink link, [NotNull] Item item)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link == null || item == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return false;
            }

            // ReSharper restore HeuristicUnreachableCode
            var ancestor = link.Link.TargetItem;
            if (ancestor == null)
            {
                return false;
            }

            var contextPath = item.Paths.FullPath;
            var ancestorPath = ancestor.Paths.FullPath;
            var value = contextPath.StartsWith(ancestorPath, StringComparison.InvariantCultureIgnoreCase);

            return value;
        }

        /// <summary>
        /// Determines whether the specified link is content item.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <param name="item">The item.</param>
        /// <returns>True if the descendant path equals with the content item.</returns>
        public static bool IsContextItem([NotNull] this INavigationLink link, [CanBeNull] Item item)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link == null || item == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return false;
            }

            var linkItem = link.Link.TargetItem;
            if (linkItem == null)
            {
                return false;
            }

            var contextPath = item.Paths.FullPath;
            var linkPath = linkItem.Paths.FullPath;
            var value = contextPath.Equals(linkPath, StringComparison.InvariantCultureIgnoreCase);

            return value;
        }

        /// <summary>
        /// The render navigation link. This convenience extension method method.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <param name="attributes">The attributes.</param>
        /// <returns>
        /// The Html output.
        /// </returns>
        [NotNull]
        public static MvcHtmlString Render([NotNull] this INavigationLink link, [NotNull] params XAttribute[] attributes)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var html = link.Link.Render(attributes);
                return html;
            }

            return string.Empty;
        }

        /// <summary>
        /// The render navigation link. This convenience extension method method.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <param name="attributes">The attributes.</param>
        /// <returns>
        /// The Html output.
        /// </returns>
        [NotNull]
        public static MvcHtmlString Render([NotNull] this INavigationLink link, [NotNull] IEnumerable<XAttribute> attributes)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var html = link.Link.Render(attributes);
                return html;
            }

            return string.Empty;
        }

        #endregion
    }
}
