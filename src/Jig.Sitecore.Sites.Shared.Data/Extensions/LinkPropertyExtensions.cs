﻿namespace Jig.Sitecore.Sites.Shared.Data
{
    using global::System.Collections.Generic;
    using global::System.Collections.ObjectModel;
    using global::System.Linq;
    using Jig.Sitecore.Data.Fields;
    using Jig.Sitecore.Sites.Shared.Data.Navigation;
    using Jig.Sitecore.Sites.Shared.Data.Pages;
    using global::Sitecore;

    /// <summary>
    /// The link property extensions.
    /// </summary>
    public static partial class LinkPropertyExtensions
    {
        /// <summary>
        /// The gets best navigation title.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <returns>
        /// The <see cref="string" />.
        /// </returns>
        [NotNull]
        // ReSharper disable CodeAnnotationAnalyzer
        public static string GetBestNavigationTitle(this LinkProperty link)
        // ReSharper restore CodeAnnotationAnalyzer
        {
            if (link == null || !link.HasValue)
            {
                return string.Empty;
            }

            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            if (!string.IsNullOrEmpty(link.Text))
            {
                return link.Text;
            }

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link.IsInternal && link.TargetItem != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var page = link.TargetItem.As<IContentPage>(link.Language);
                if (page != null)
                {
                    var title = page.GetBestNavigationTitle();

                    return title;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the link page.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <returns>
        /// The link page
        /// </returns>
        [CanBeNull]
        public static IPage GetLinkPage([NotNull] this LinkProperty link)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link == null || !link.HasValue)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return null;
            }

            var targetItem = link.TargetItem;
            if (targetItem != null)
            {
                var page = targetItem.As<IPage>(link.Language);
                return page;
            }

            return null;
        }

        /// <summary>
        /// Inspects the Link's properties to determine the best inner text
        /// for the A tag.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <returns>
        /// The inner text for the A tag.
        /// </returns>
        [NotNull]
        public static string GetLinkText([NotNull] this LinkProperty link)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link == null || !link.HasValue)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return string.Empty;
            }

            if (!string.IsNullOrEmpty(link.Text))
            {
                return link.Text;
            }

            if (link.IsInternal)
            {
                if (link.TargetItem != null)
                {
                    var page = link.TargetItem.As<IContentPage>(link.Language);

                    if (page != null)
                    {
                        return page.GetBestNavigationTitle();
                    }
                }

                return "#not-found";
            }

            return link.DisplayName;
        }

        /// <summary>
        /// The get children links.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <returns>
        /// The  Link Collection.
        /// </returns>
        // ReSharper disable ReturnTypeCanBeEnumerable.Global
        [NotNull]
        public static IList<INavigationLink> GetChildrenNavigationLinks([NotNull] this LinkProperty link)
        // ReSharper restore ReturnTypeCanBeEnumerable.Global
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (link == null || !link.HasValue)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return new Collection<INavigationLink>();
            }

            var links = link.Item.Children.As<INavigationLink>().ToArray();
            return links;
        }
    }
}