﻿namespace Jig.Sitecore.Sites
{
    using System;
    using Jig.Sitecore.Sites.Shared.Data.System.Interfaces;
    using global::Sitecore;

    /// <summary>
    /// Class IHostnameExtensions.
    /// </summary>
    public static class HostNameExtensions
    {
        /// <summary>
        /// Matches the specified hostname.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="hostname">The hostname.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool MatchUriHost([NotNull] this IHostname item, [NotNull] string hostname)
        {
            /* Direct match */
            if (string.Equals(hostname, item.Hostname.Value, StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            /* Partial match */
            return UriHelpers.MatchUriHost(hostname, item.Hostname.Value);
        }

        /// <summary>
        /// Matches the specified hostname.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="hostname">The hostname.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool MatchUriHost([NotNull] this IHostname item, [NotNull] Uri hostname)
        {
            /* Direct match */
            if (string.Equals(hostname.Host, item.Hostname.Value, StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            /* Partial match */

            return UriHelpers.MatchUriHost(hostname, item.Hostname.Value);
        }
    }
}