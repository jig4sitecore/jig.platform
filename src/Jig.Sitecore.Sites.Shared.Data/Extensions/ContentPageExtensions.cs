﻿namespace Jig.Sitecore.Sites.Shared.Data
{
    using global::Sitecore;

    /// <summary>
    /// The content page.
    /// </summary>
    public static partial class ContentPageExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get best navigation title.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <returns>
        /// The menu title or Display .
        /// </returns>
        [NotNull]
        public static string GetBestNavigationTitle([NotNull] this Jig.Sitecore.Sites.Shared.Data.Pages.IContentPage page)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (page == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return string.Empty;
            }

            // ReSharper restore HeuristicUnreachableCode
            if (!string.IsNullOrEmpty(page.PageMenuTitle))
            {
                return page.PageMenuTitle;
            }

            return page.DisplayName;
        }

        /// <summary>
        /// Determines the browser title based on all title fields available on the item.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <returns>
        /// The browser title.
        /// </returns>
        [NotNull]
        public static string GetBestBrowserTitle([NotNull] this Jig.Sitecore.Sites.Shared.Data.Pages.IContentPage page)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (page == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return string.Empty;
            }

            // ReSharper restore HeuristicUnreachableCode
            if (!string.IsNullOrEmpty(page.BrowserTitle))
            {
                return page.BrowserTitle.Value;
            }

            if (!string.IsNullOrEmpty(page.PageMenuTitle))
            {
                return page.PageMenuTitle.Value;
            }

            return page.DisplayName;
        }

        #endregion
    }
}
