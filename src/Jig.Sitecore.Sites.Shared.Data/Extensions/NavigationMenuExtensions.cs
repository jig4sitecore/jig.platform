﻿namespace Jig.Sitecore.Sites.Shared.Data
{
    using global::System.Collections.Generic;
    using global::System.Linq;
    using Jig.Sitecore.Sites.Shared.Data.Navigation;
    using global::Sitecore;

    /// <summary>
    /// The navigation menu.
    /// </summary>
    public static partial class NavigationMenuExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get children navigation links.
        /// </summary>
        /// <param name="menu">The menu.</param>
        /// <returns>
        /// The Collection of children links
        /// </returns>
        [NotNull]
        public static IEnumerable<INavigationLink> GetChildrenLinks([NotNull] this INavigationMenu menu)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (menu != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var links = menu.InnerItem.Children.As<INavigationLink>().ToArray();
                return links;
            }

            return new INavigationLink[] { };
        }

        #endregion
    }
}
