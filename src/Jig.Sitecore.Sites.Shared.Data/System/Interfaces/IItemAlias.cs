﻿namespace Jig.Sitecore.Sites.Shared.Data.System.Interfaces
{
    using global::Sitecore;
    using global::Sitecore.Pipelines.HttpRequest;

    /// <summary>
    /// The ItemAlias interface.
    /// </summary>
    public partial interface IItemAlias
    {
        #region Public Methods and Operators

        /// <summary>
        /// Determines whether the specified arguments is match.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns><c>true</c> if the specified arguments is match; otherwise, <c>false</c>.</returns>
        bool IsMatch([NotNull] HttpRequestArgs args);

        #endregion
    }
}