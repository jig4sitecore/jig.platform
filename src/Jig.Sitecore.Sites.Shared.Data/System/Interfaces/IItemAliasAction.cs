﻿namespace Jig.Sitecore.Sites.Shared.Data.System.Interfaces
{
    using global::Sitecore;

    using global::Sitecore.Pipelines.HttpRequest;

    /// <summary>
    /// The ItemAliasAction interface.
    /// </summary>
    public partial interface IItemAliasAction
    {
        #region Public Methods and Operators

        /// <summary>
        /// Executes the specified item alias.
        /// </summary>
        /// <param name="itemAlias">The item alias.</param>
        /// <param name="args">The arguments.</param>
        void Execute([NotNull] IItemAlias itemAlias, [NotNull] HttpRequestArgs args);

        #endregion
    }
}