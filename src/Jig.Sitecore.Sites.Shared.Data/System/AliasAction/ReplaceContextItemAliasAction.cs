﻿namespace Jig.Sitecore.Sites.Shared.Data.System.AliasAction
{
    using Jig.Sitecore.Sites.Shared.Data.System.Interfaces;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Pipelines.HttpRequest;

    /// <summary>
    /// The replace context item alias action.
    /// </summary>
    public partial class ReplaceContextItemAliasAction
    {
        #region Public Methods and Operators

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="itemAlias">The item alias.</param>
        /// <param name="args">The args.</param>
        public void Execute([NotNull] IItemAlias itemAlias, [NotNull] HttpRequestArgs args)
        {
            var targetItem = itemAlias.TargetItem.TargetItem;
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (targetItem == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                Log.Warn("Replace Context Item Action - targetItem = null", this);
                return;
            }

            // ReSharper restore HeuristicUnreachableCode
            Log.Debug("Replace Context Item Action - Item ID: " + targetItem.ID, this);

            Context.Item = targetItem;
        }

        #endregion
    }
}