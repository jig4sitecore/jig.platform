﻿namespace Jig.Sitecore.Sites.Shared.Data.System.AliasAction
{
    using Jig.Sitecore.Sites.Shared.Data.System.Interfaces;
    using Jig.Web;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Links;

    using global::Sitecore.Pipelines.HttpRequest;

    /// <summary>
    /// The redirect item alias action.
    /// </summary>
    public partial class RedirectItemAliasAction
    {
        #region Public Methods and Operators

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="itemAlias">The item alias.</param>
        /// <param name="args">The args.</param>
        public void Execute([NotNull] IItemAlias itemAlias, [NotNull] HttpRequestArgs args)
        {
            var item = itemAlias.TargetItem.TargetItem;
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                Log.Debug("Replace Context Item Action - item = null", this);
                return;
            }

            // ReSharper restore HeuristicUnreachableCode
            var url = LinkManager.GetItemUrl(item);

            args.Context.Response.Clear();
            args.Context.Response.StatusCode = this.StatusCode.Value.ConvertAsInt(302);
            args.Context.Response.RedirectLocation = url;
            args.Context.Response.End();

            args.AbortPipeline();
        }

        #endregion
    }
}