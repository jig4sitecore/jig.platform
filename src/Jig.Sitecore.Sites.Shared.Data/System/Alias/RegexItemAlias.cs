﻿namespace Jig.Sitecore.Sites.Shared.Data.System.Alias
{
    using global::System;

    using global::System.Text.RegularExpressions;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Pipelines.HttpRequest;

    /// <summary>
    /// The regex item alias.
    /// </summary>
    public partial class RegexItemAlias
    {
        #region Public Methods and Operators

        /// <summary>
        /// The is match.
        /// </summary>
        /// <param name="args">The args.</param>
        /// <returns>The <see cref="bool" />.</returns>
        public bool IsMatch(HttpRequestArgs args)
        {
            Uri requestUrl = args.Context.Request.Url;
            var rightParts = requestUrl.GetComponents(UriComponents.PathAndQuery, UriFormat.Unescaped);

            try
            {
                var regex = new Regex(this.Expression.Value, RegexOptions.Singleline | RegexOptions.IgnoreCase);

                if (regex.IsMatch(rightParts))
                {
                    Log.Debug("Regex Item Alias - Expression: " + this.Expression.Value, this);
                    Log.Debug("Regex Item Alias - Match ID: " + this.ID, this);
                    return true;
                }
            }
            catch
            {
                Log.Warn("Regex Item Alias - Invalid Regex -> " + this.Expression.Value, this);
                return false;
            }

            return false;
        }

        #endregion
    }
}