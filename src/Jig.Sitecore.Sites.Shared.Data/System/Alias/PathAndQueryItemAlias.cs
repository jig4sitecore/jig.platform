﻿namespace Jig.Sitecore.Sites.Shared.Data.System.Alias
{
    using global::System;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Pipelines.HttpRequest;

    /// <summary>
    /// The path and query item alias.
    /// </summary>
    public partial class PathAndQueryItemAlias
    {
        #region Public Methods and Operators

        /// <summary>
        /// Determines whether the specified arguments is match.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns><c>true</c> if the specified arguments is match; otherwise, <c>false</c>.</returns>
        public bool IsMatch(HttpRequestArgs args)
        {
            Uri requestUrl = args.Context.Request.Url;
            Uri sourceUrl = this.GetAbsoluteUrI(this.PathAndQuery.Value, requestUrl);

            var result = Uri.Compare(
                sourceUrl, 
                requestUrl, 
                UriComponents.Path | UriComponents.Query, 
                UriFormat.Unescaped, 
                StringComparison.InvariantCultureIgnoreCase);

            if (result != 0)
            {
                return false;
            }

            Log.Debug("Path And Query Item Alias - Match ID: " + this.ID, this);
            Log.Debug("Path And Query Item Alias - Value: " + this.PathAndQuery.Value, this);
            return true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the absolute ur i.
        /// </summary>
        /// <param name="relativeUrl">The relative URL.</param>
        /// <param name="requestUrl">The request URL.</param>
        /// <returns>The aboslute Uri.</returns>
        [NotNull]
        private Uri GetAbsoluteUrI([NotNull] string relativeUrl, [NotNull] Uri requestUrl)
        {
            var baseUrl = requestUrl.GetComponents(UriComponents.Scheme | UriComponents.Host, UriFormat.Unescaped);

            // Ensure the relative target URL starts with a slash.
            relativeUrl = relativeUrl.TrimStart('/').Insert(0, "/");

            return new Uri(string.Concat(baseUrl, relativeUrl));
        }

        #endregion
    }
}