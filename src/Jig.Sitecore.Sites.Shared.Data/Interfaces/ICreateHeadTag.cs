﻿namespace Jig.Sitecore.Sites.Shared.Data.HeadTags
{
    using Jig.Web.Html;
    using global::Sitecore;

    /// <summary>
    /// The CreateHeadTag interface.
    /// </summary>
    public interface ICreateHeadTag
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get head tag.
        /// </summary>
        /// <returns>
        /// The IHeadTag instances.
        /// </returns>
        [CanBeNull]
        IHeadTag GetHeadTag();

        #endregion
    }
}