﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: Guid("BF746F2A-FFCB-4CFB-AEC5-59FEED10667F")]
[assembly: AssemblyTitle("Jig4Sitecore Framework")]
[assembly: AssemblyDescription("The library extending the Sitecore CMS.")]

[assembly: AssemblyCompany("Jig4Sitecore Team")]
[assembly: AssemblyProduct("Jig4Sitecore Framework")]
[assembly: AssemblyCopyright("Copyright © Jig4Sitecore Team 2014")]
[assembly: AssemblyTrademark("Jig4Sitecore Team")]

[assembly: ComVisible(false)]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyVersion("2.0.0")]
[assembly: AssemblyFileVersion("2.0.0")]

[assembly: System.Runtime.CompilerServices.Dependency("Jig.Sitecore.Abstractions", System.Runtime.CompilerServices.LoadHint.Always)]
[assembly: System.Runtime.CompilerServices.Dependency("Jig.Sitecore.StandardTemplate", System.Runtime.CompilerServices.LoadHint.Always)]
[assembly: System.Runtime.CompilerServices.Dependency("Jig.Sitecore.Data.Fields", System.Runtime.CompilerServices.LoadHint.Always)]
