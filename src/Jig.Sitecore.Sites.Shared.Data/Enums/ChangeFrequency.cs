namespace Jig.Sitecore.Sites.Shared.Data.Pages
{
    using global::System;
    using global::System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Specifies the frequency with which a page's content changes. 
    /// </summary>
    /// <seealso href="http://sitemaps.org/protocol.php"/>
    [SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue", Justification = "The Enumerator mapped to original SiteMap protocol values")]
    [Serializable, Flags]
    public enum ChangeFrequency : byte
    {
        /// <summary>
        /// Page content Ignored.
        /// </summary>
        Ignore = 0,

        /// <summary>
        /// Page content changes hourly.
        /// </summary>
        Hourly = 1,

        /// <summary>
        /// Page content changes daily.
        /// </summary>
        Daily = 2,

        /// <summary>
        /// Page content changes weekly.
        /// </summary>
        Weekly = 4,

        /// <summary>
        /// Page content changes monthly.
        /// </summary>
        Monthly = 8,

        /// <summary>
        /// Page content changes yearly.
        /// </summary>
        Yearly = 16,

        /// <summary>
        /// Page content always changes.
        /// </summary>
        Always = 32,

        /// <summary>
        /// Page content never changes.
        /// </summary>
        Never = 64,
    }
}