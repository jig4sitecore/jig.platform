﻿namespace Jig.Sitecore.HttpHandlers
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Web;

    using Jig.Sitecore.Pipelines;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Pipelines;

    /// <summary>
    /// Generates a sitemap.xml file for the sitecore site requested by the hostname component
    /// of the current request.
    /// </summary>
    /// <example>
    /// Add to the IIS7 handlers section<![CDATA[
    /// <add name="sitemap" path="sitemap.xml" verb="GET,PROPFIND" preCondition="integratedMode" type="Jig.Sitecore.HttpHandlers.SitemapXmlHttpHandler, Jig.Sitecore.HttpHandlers.SitemapXml"/>]]>
    /// </example>
    public class SitemapXmlHttpHandler : IHttpHandler
    {
        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether this instance of the handler is reusable.
        /// Required by IHttpHandler, not used by developers in this case.
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Required by IHttpHandler, this is called by ASP.NET when an appropriate URL match is found.
        /// </summary>
        /// <param name="context">
        /// The current request context.
        /// </param>
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                var response = context.Response;
                response.Clear();
                response.ContentType = "text/xml";
                response.Buffer = true;

                /* Enable output compression */
                if (!string.IsNullOrEmpty(context.Request.ServerVariables["SERVER_SOFTWARE"]))
                {
                    context.Request.ServerVariables["IIS_EnableDynamicCompression"] = "1";
                }

                /* Write output header */
                response.Write(@"<?xml version=""1.0"" encoding=""UTF-8""?>" + Environment.NewLine);

                try
                {
                    using (var args = new SiteMapPipelineArgs(new HttpContextWrapper(context)))
                    {
                        /* 
                            <configuration xmlns:patch="http://www.sitecore.net/xmlconfig/">
                                <sitecore>
                                    <pipelines>
                                        <sitemapXml>
                                            <processor type="{Type FullName}, {Assembly}" />
                                            <processor type="{Type FullName}, {Assembly}" />
                                            <processor type="{Type FullName}, {Assembly}" />
                                        </sitemapXml>
                                    </pipelines>
                                </sitecore>
                            </configuration>
                         */
                        var pipelineName = typeof(SitemapXmlHttpHandler).FullName;

                        CorePipeline pipeline = CorePipelineFactory.GetPipeline(pipelineName, string.Empty);
                        if (pipeline == null)
                        {
                            var msg = new StringBuilder(128);
                            msg.Append("Could not get pipeline: ").AppendLine(pipelineName);
                            msg.AppendLine("Missing config file: /App_Config/Include/_web/Jig.Sitecore.HttpHandlers.SitemapXml.config");

                            throw new HttpException(msg.ToString());
                        }

                        pipeline.Run(args);
                        
                        /* Logging the errors and info messages */
                        var errors = args.GetMessages(PipelineMessageFilter.Errors);
                        foreach (var error in errors)
                        {
                            Log.Error(error.Text, typeof(SiteMapPipelineArgs));
                        }

                        errors = args.GetMessages(PipelineMessageFilter.Warnings);
                        foreach (var error in errors)
                        {
                            Log.Warn(error.Text, typeof(SiteMapPipelineArgs));
                        }

                        /* Write the output xml */
                        var attr = string.Join(" ", args.SitemapNamespaces);
                        response.Write("<urlset " + attr + ">" + Environment.NewLine);

                        if (!errors.Any() && !args.Suspended)
                        {
                            /* Get output */
                            var output = args.Output.InnerWriter.ToString();
                            response.Write(output);
                        }

                        response.Write("</urlset>" + Environment.NewLine);
                    }
                }
                catch (Exception exception)
                {
                    Log.Error(exception.Message, exception, typeof(SiteMapPipelineArgs));
                    Trace.TraceError(exception.ToString());

                    response.Write("<!-- " + exception.Message + "-->");
                }
            }
            finally
            {
                context.Response.Flush();
                context.ApplicationInstance.CompleteRequest();
            }
        }

        #endregion
    }
}