﻿namespace Jig.Sitecore.HttpHandlers.SitemapXml
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using global::Sitecore;

    /// <summary>
    /// The internal extensions.
    /// </summary>
    internal static partial class InternalExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Given a comma-delimited string, separates it into its component parts.
        /// </summary>
        /// <param name="values">The string to parse.</param>
        /// <returns>
        /// A list of values.
        /// </returns>
        [NotNull]
        public static IList<string> ConvertToList([NotNull] this string values)
        {
            var output = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);

            if (!string.IsNullOrEmpty(values))
            {
                var separated = values.Split(new[] { ',', '|' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var value in separated)
                {
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        output.Add(value.Trim());
                    }
                }
            }

            return output.ToArray();
        }

        #endregion
    }
}