﻿namespace Jig.Sitecore.HttpHandlers.RobotTextFiles
{
    using Jig.Sitecore.Pipelines;

    /// <summary>
    /// Class ResolveContextDatabase. This class cannot be inherited.
    /// </summary>
    public sealed class ResolveContextDatabase : IRobotTextFileProcessor
    {
        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process(RobotTextFilePipelineArgs args)
        {
            if (args.Database == null)
            {
                args.Database = global::Sitecore.Context.Database;
            }
        }
    }
}