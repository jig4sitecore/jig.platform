﻿namespace Jig.Sitecore.HttpHandlers.RobotTextFiles
{
    using Jig.Sitecore.Pipelines;
    using global::Sitecore.Sites;

    /// <summary>
    /// Class ResolveSiteContext. This class cannot be inherited.
    /// </summary>
    public sealed class ResolveSiteContext : IRobotTextFileProcessor
    {
        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process(RobotTextFilePipelineArgs args)
        {
            if (args.SiteContext == null && args.Context.Request.Url != null)
            {
                args.SiteContext = global::Sitecore.Context.Site
                                   ?? SiteContextFactory.GetSiteContext(
                                       args.Context.Request.Url.Host,
                                       args.Context.Request.Url.AbsolutePath.ToLowerInvariant());
            }
        }
    }
}