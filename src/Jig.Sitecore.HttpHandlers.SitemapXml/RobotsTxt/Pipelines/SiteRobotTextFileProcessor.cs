﻿namespace Jig.Sitecore.HttpHandlers.RobotTextFiles
{
    using System.Linq;
    using Jig.Sitecore.Context;
    using Jig.Sitecore.Pipelines;
    using Jig.Sitecore.Sites;
    using Jig.Sitecore.Sites.Shared.Data.Folders;
    using Jig.Sitecore.Sites.Shared.Data.System;
    using global::Sitecore;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;

    /// <summary>
    /// The shared site map processor.
    /// </summary>
    public sealed class SiteRobotTextFileProcessor : IRobotTextFileProcessor, ISitesToIgnore
    {
        /// <summary>
        /// Gets or sets a comma-delimited list of Site names that should cause the Processor to ignore the request.
        /// </summary>
        /// <value>The sites to ignore.</value>
        [UsedImplicitly]
        public string SitesToIgnore { get; set; }

        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process(RobotTextFilePipelineArgs args)
        {
            /*
                # robots.txt for http://example.com/

                User-agent: *
                Disallow: /
                Allow: /under-construction
             * */
            if (!args.Aborted && args.SiteContext != null && args.Database != null)
            {
                if (this.IsMatchSitesToIgnore(args.SiteContext))
                {
                    return;
                }

                Item rootItem = args.Database.GetItem(args.SiteContext.RootPath);
                if (rootItem == null)
                {
                    Log.Warn("Could not resolve the site root path: " + args.SiteContext.RootPath, this);
                    return;
                }

                var configFolder = rootItem.GetChildren().As<SiteConfigurationFolder>().FirstOrDefault();
                if (configFolder == null)
                {
                    Log.Warn("Could not resolve the site config folder: " + args.SiteContext.RootPath, this);
                    return;
                }

                var robotTextFileFolder = configFolder.GetFirstOrDefaultChildAs<RobotTextFileFolder>();
                if (robotTextFileFolder == null)
                {
                    Log.Warn("Could not resolve the site robot text file folder: " + args.SiteContext.RootPath, this);
                    return;
                }

                if (args.Context.Request.Url != null)
                {
                    var hostName = args.Context.Request.Url.Host;

                    var children = robotTextFileFolder.GetChildren<RobotTextFile>();

                    // ReSharper disable LoopCanBeConvertedToQuery
                    foreach (RobotTextFile robotTextFile in children)
                        // ReSharper restore LoopCanBeConvertedToQuery
                    {
                        if (robotTextFile.MatchUriHost(hostName))
                        {
                            args.Output.WriteLineNoTabs(robotTextFile.Code);
                            break;
                        }
                    }
                }
            }
        }

        #endregion
    }
}
