﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: Guid("FF4D2192-7110-460E-8D3F-96F32A6D7942")]
[assembly: AssemblyTitle("Jig4Sitecore Framework")]
[assembly: AssemblyDescription("The library extending the Sitecore CMS.")]

[assembly: AssemblyCompany("Jig4Sitecore Team")]
[assembly: AssemblyProduct("Jig4Sitecore Framework")]
[assembly: AssemblyCopyright("Copyright © Jig4Sitecore Team 2014")]
[assembly: AssemblyTrademark("Jig4Sitecore Team")]

[assembly: ComVisible(false)]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyVersion("2.0.0")]
[assembly: AssemblyFileVersion("2.0.0")]
