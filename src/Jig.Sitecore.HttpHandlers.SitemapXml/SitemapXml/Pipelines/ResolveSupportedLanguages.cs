﻿namespace Jig.Sitecore.HttpHandlers.SitemapXml
{
    using Jig.Sitecore.Pipelines;

    /// <summary>
    /// Class Resolve Supported Languages. This class cannot be inherited.
    /// </summary>
    public sealed class ResolveSupportedLanguages : ISiteMapProcessor
    {
        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process(SiteMapPipelineArgs args)
        {
            if (!args.Aborted && args.SiteContext != null && args.Database != null)
            {
                var languages = args.SiteContext.SiteInfo.GetSupportedLanguages(args.Database);
                foreach (var languageName in languages)
                {
                    global::Sitecore.Globalization.Language language;
                    if (global::Sitecore.Globalization.Language.TryParse(languageName, out language))
                    {
                        args.SupportedLanguages.Add(language);
                    }
                }
            }
        }
    }
}