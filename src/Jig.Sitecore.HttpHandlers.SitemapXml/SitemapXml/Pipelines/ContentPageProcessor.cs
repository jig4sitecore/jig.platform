﻿namespace Jig.Sitecore.HttpHandlers.SitemapXml
{
    using System;
    using System.Diagnostics;
    using System.Linq;

    using Jig.Sitecore.Context;
    using Jig.Sitecore.Pipelines;
    using Jig.Sitecore.Sites.Shared.Data.Pages;
    using global::Sitecore;
    using global::Sitecore.Data.Items;

    /// <summary>
    /// The shared site map processor.
    /// </summary>
    public sealed class ContentPageProcessor : ISiteMapProcessor, ISitesToIgnore
    {
        /// <summary>
        /// Gets or sets a comma-delimited list of Site names that should cause the Processor to ignore the request.
        /// </summary>
        /// <value>The sites to ignore.</value>
        [UsedImplicitly]
        public string SitesToIgnore { get; set; }

        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process(SiteMapPipelineArgs args)
        {
            /*
                <url>
                    <loc>https://www.{domain}.com/{page-url}</loc>
                    <link rel="alternate" hreflang="de" href="https://www.{domain}.com/de/{page-url}"/>
                    <link rel="alternate" hreflang="fr" href="https://www.{domain}.com/fr/{page-url}"/>
                    <link rel="alternate" hreflang="es" href="https://www.{domain}.com/es/{page-url}"/>
                    <link rel="alternate" hreflang="it" href="https://www.{domain}.com/it/{page-url}"/>
                    <link rel="alternate" hreflang="ja" href="https://www.{domain}.com/ja/{page-url}"/>
                    <link rel="alternate" hreflang="nl" href="https://www.{domain}.com/nl/{page-url}"/>
                    <link rel="alternate" hreflang="pt" href="https://www.{domain}.com/pt/{page-url}"/>
                    <changefreq>Daily</changefreq>
                    <lastmod>2015-02-03</lastmod>
                    <priority>0.5</priority>
                </url>
             * */
            if (!args.Aborted && args.SiteContext != null && args.Database != null && args.SupportedLanguages.Count > 0)
            {
                if (this.IsMatchSitesToIgnore(args.SiteContext))
                {
                    return;
                }

                foreach (var language in args.SupportedLanguages)
                {
                    Item homeItem = args.Database.GetItem(args.SiteContext.ContentStartItem, language);
                    if (homeItem != null)
                    {
                        var homePage = homeItem.As<IContentPage>();
                        if (homePage != null)
                        {
                            this.AddSitemapNode(args, homePage);
                        }

                        /* TODO: Verify that it returns all descendants and not first 100 and check maybe its a way to improve performance */
                        var descendants = homeItem.Axes.GetDescendants().As<IContentPage>();
                        foreach (var descendant in descendants)
                        {
                            this.AddSitemapNode(args, descendant);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adds the sitemap node.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <param name="page">The page.</param>
        private void AddSitemapNode([NotNull] SiteMapPipelineArgs args, [NotNull] IContentPage page)
        {
            Debug.Assert(args.SiteContext != null, "args.SiteContext != null");

            if (page.IncludeInSiteMap.Checked)
            {
                var url = page.InnerItem.GetUrl();
                if (string.IsNullOrWhiteSpace(url))
                {
                    return;
                }

                ChangeFrequency siteMapChangeFrequency;
                if (!Enum.TryParse(page.SiteMapChangeFrequency.Value, out siteMapChangeFrequency)
                    || siteMapChangeFrequency == ChangeFrequency.Ignore)
                {
                    return;
                }

                double siteMapPriority;
                if (!double.TryParse(page.SiteMapPriority.Value, out siteMapPriority))
                {
                    siteMapPriority = 0.5d;
                }

                var request = args.Context.Request;
                if (request != null && request.Url != null)
                {
                    var uriBuilder = new UriBuilder
                                         {
                                             Scheme = args.ForceToHttps ? "https" : request.Url.Scheme,
                                             Host = request.Url.Host,
                                             Path = url
                                         };

                    var node = new SiteMapNode
                                   {
                                       Location = uriBuilder.Uri,
                                       ChangeFrequency = siteMapChangeFrequency,
                                       Priority = siteMapPriority,
                                       LastModification = page.InnerItem.Statistics.Updated
                                   };

                    /* Filter default language */
                    var supportedLanguages =
                        args.SupportedLanguages.Where(x => args.SiteContext != null && !string.Equals(x.Name, args.SiteContext.Language, StringComparison.InvariantCultureIgnoreCase)).ToArray();
                   
                    var urlOptions = global::Sitecore.Context.Site.GetDefaultUrlOptions();

                    urlOptions.SiteResolving = global::Sitecore.Configuration.Settings.Rendering.SiteResolving;

                    foreach (var language in supportedLanguages)
                    {
                        /* Conversion also checks for empty items and versions as well */
                        var languagePage = page.InnerItem.Database.GetItem(page.ID, language).As<IContentPage>();
                        if (languagePage != null)
                        {
                            urlOptions.Language = language;

                            var pageUrl = languagePage.GetUrl(urlOptions);
                            if (string.IsNullOrWhiteSpace(pageUrl))
                            {
                                continue;
                            }

                            /* <link rel="alternate" hreflang="{language}" href="https://www.{domain}.com/{language}/{page-url}"/> */
                            node.SiteMapNodeLinks.Add(new SiteMapNodeLink
                                                          {
                                                              HrefLangAttr = language.Name,
                                                              HrefAttr = pageUrl
                                                          });
                        }
                    }

                    args.Output.Write(node + Environment.NewLine);
                }
            }
        }

        #endregion
    }
}