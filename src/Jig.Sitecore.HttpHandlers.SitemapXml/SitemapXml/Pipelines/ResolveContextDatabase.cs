﻿namespace Jig.Sitecore.HttpHandlers.SitemapXml
{
    using Jig.Sitecore.Pipelines;

    /// <summary>
    /// Class ResolveContextDatabase. This class cannot be inherited.
    /// </summary>
    public sealed class ResolveContextDatabase : ISiteMapProcessor
    {
        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process(SiteMapPipelineArgs args)
        {
            if (args.Database == null)
            {
                args.Database = global::Sitecore.Context.Database;
            }
        }
    }
}