﻿namespace Jig.Sitecore.HttpHandlers.SitemapXml
{
    using System;
    using System.Xml.Linq;

    using global::Sitecore;

    /// <summary>
    /// Class SiteMapNodeLink.
    /// </summary>
    /// <remarks>Should output something like this 
    /// <![CDATA[
    ///     <link rel="alternate" hreflang="{language}" href="https://www.{domain}.com/{language}/{page-url}"/>
    /// ]]>
    /// </remarks>
    [Serializable]
    public sealed class SiteMapNodeLink
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SiteMapNodeLink"/> class.
        /// </summary>
        public SiteMapNodeLink()
        {
            this.Inner = new XElement(
            "link",
            new XAttribute(AttributeNames.RelAttr, "alternate"),
            new XAttribute(AttributeNames.HrefLangAttr, string.Empty),
            new XAttribute(AttributeNames.HrefAttr, string.Empty));
        }

        /// <summary>
        /// Gets the inner xml element.
        /// </summary>
        [NotNull]  
        public XElement Inner { get; private set; }

        /// <summary>
        /// Gets or sets the href attribute.
        /// </summary>
        /// <value>The href attribute.</value>
        [NotNull] 
        public string HrefAttr
        {
            get
            {
                return this.GetAttributeValue(AttributeNames.HrefAttr);
            }

            set
            {
                this.SetAttributeValue(AttributeNames.HrefAttr, value);
            }
        }

        /// <summary>
        /// Gets or sets the href language attribute.
        /// </summary>
        /// <value>The href language attribute.</value>
        [NotNull]  
        public string HrefLangAttr
        {
            get
            {
                return this.GetAttributeValue(AttributeNames.HrefLangAttr);
            }

            set
            {
                this.SetAttributeValue(AttributeNames.HrefLangAttr, value);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        /// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
        public bool IsValid
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.HrefAttr) && !string.IsNullOrWhiteSpace(this.HrefLangAttr);
            }
        }

        /// <summary>
        /// The get attribute value.
        /// </summary>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <returns>
        /// The attribute value
        /// </returns>
        [NotNull]
        public string GetAttributeValue([NotNull] XName attributeName)
        {
            var attribute = this.Inner.Attribute(attributeName);
            if (attribute != null)
            {
                return attribute.Value;
            }

            return string.Empty;
        }

        /// <summary>
        /// The set attribute value.
        /// </summary>
        /// <param name="attributeName">The name.</param>
        /// <param name="attributeValue">The value.</param>
        public void SetAttributeValue([NotNull] XName attributeName, [NotNull] string attributeValue)
        {
            if (!string.IsNullOrWhiteSpace(attributeValue))
            {
                var attribute = this.Inner.Attribute(attributeName);
                if (attribute != null)
                {
                    attribute.Value = attributeValue;
                }
                else
                {
                    attribute = new XAttribute(attributeName, attributeValue);
                    this.Inner.Add(attribute);
                }
            }
        }

        /// <summary>
        /// Class AttributeNames.
        /// </summary>
        private static class AttributeNames
        {
            /// <summary>
            /// The relative attribute
            /// </summary>
            public static readonly XName RelAttr = XName.Get("rel");

            /// <summary>
            /// The href attribute
            /// </summary>
            public static readonly XName HrefAttr = XName.Get("href");

            /// <summary>
            /// The href language attribute
            /// </summary>
            public static readonly XName HrefLangAttr = XName.Get("hreflang");
        }
    }
}