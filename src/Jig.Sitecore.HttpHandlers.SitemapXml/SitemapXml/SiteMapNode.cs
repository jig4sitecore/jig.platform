﻿namespace Jig.Sitecore.HttpHandlers.SitemapXml
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Xml.Linq;
    using Jig.Sitecore.Sites.Shared.Data.Pages;
    using global::Sitecore;

    /// <summary>
    /// The site map node.
    /// </summary>
    public partial class SiteMapNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SiteMapNode"/> class.
        /// </summary>
        public SiteMapNode()
        {
            this.SiteMapNodeLinks = new HashSet<SiteMapNodeLink>();
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the change frequency.
        /// </summary>
        /// <value>
        /// The change frequency.
        /// </value>
        public ChangeFrequency ChangeFrequency { get; set; }

        /// <summary>
        /// Gets or sets the LastModification.
        /// </summary>
        /// <value>The LastModification.</value>
        public DateTime? LastModification { get; set; }

        /// <summary>
        /// Gets or sets the Location.
        /// </summary>
        /// <value>The Location.</value>
        [CanBeNull]
        public Uri Location { get; set; }

        /// <summary>
        /// Gets or sets the Priority.
        /// </summary>
        /// <value>The Priority.</value>
        public double? Priority { get; set; }

        /// <summary>
        /// Gets the site map node links.
        /// </summary>
        /// <value>The site map node links.</value>
        [UsedImplicitly]
        [NotNull] 
        public ICollection<SiteMapNodeLink> SiteMapNodeLinks { get; private set; }

        #endregion

        /// <summary>
        /// Gets a flag indicating whether object is valid or not
        /// </summary>
        /// <returns>True if valid otherwise false</returns>
        public bool Validate()
        {
            if (this.Location == null)
            {
                return false;
            }

            if (this.ChangeFrequency == ChangeFrequency.Ignore)
            {
                return false;
            }

            if (this.Priority.HasValue && !(this.Priority >= 0 && this.Priority <= 1))
            {
                return false;
            }

            if (this.LastModification.HasValue && (this.LastModification.Value.Date < DateTime.UtcNow.Date.AddYears(-10) || this.LastModification.Value.Date > DateTime.UtcNow.Date))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var input = string.Concat(this.Location);
                if (!string.IsNullOrEmpty(input))
                {
                    return input.ToUpper(CultureInfo.InvariantCulture).GetHashCode();
                }

                return string.Empty.GetHashCode();
            }
        }

        /// <summary>
        /// Gets the site map node.
        /// </summary>
        /// <returns>The site map node</returns>
        [NotNull] 
        public XElement GetSiteMapNode()
        {
            /*
                <url>
                    <loc>https://www.{domain}.com/{page-url}</loc>
                    <link rel="alternate" hreflang="{language}" href="https://www.{domain}.com/{language}/{page-url}"/>
                    <link rel="alternate" hreflang="fr" href="https://www.{domain}.com/fr/{page-url}"/>
                    <link rel="alternate" hreflang="es" href="https://www.{domain}.com/es/{page-url}"/>
                    <link rel="alternate" hreflang="it" href="https://www.{domain}.com/it/{page-url}"/>
                    <link rel="alternate" hreflang="ja" href="https://www.{domain}.com/ja/{page-url}"/>
                    <link rel="alternate" hreflang="nl" href="https://www.{domain}.com/nl/{page-url}"/>
                    <link rel="alternate" hreflang="pt" href="https://www.{domain}.com/pt/{page-url}"/>
                    <changefreq>Daily</changefreq>
                    <lastmod>2015-02-03</lastmod>
                    <priority>0.5</priority>
                </url>
            */
            var root = new XElement("url");

            root.Add(new XElement("loc", string.Concat(this.Location)));

            foreach (var link in this.SiteMapNodeLinks)
            {
                if (link.IsValid)
                {
                    root.Add(link.Inner);
                }
            }

            root.Add(new XElement("changefreq", this.ChangeFrequency));

            if (this.LastModification.HasValue)
            {
                root.Add(new XElement("lastmod", this.LastModification.Value.ToString("yyyy-MM-dd")));
            }

            if (this.Priority.HasValue)
            {
                root.Add(new XElement("priority", this.Priority));
            }

            return root;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this.GetSiteMapNode().ToString();
        }
    }
}