﻿namespace Jig.Sitecore.Development.WebControlView.Processors
{
    using System.Text;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;

    /// <summary>
    /// The generate web control view.
    /// </summary>
    public sealed class GenerateWebControlView : ICodeGenerationProcessor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GenerateWebControlView"/> class.
        /// </summary>
        public GenerateWebControlView()
        {
            this.SourceFolderPath = string.Empty;
        }

        /// <summary>
        /// Gets or sets the source folder path.
        /// </summary>
        [NotNull]
        public string SourceFolderPath { get; [UsedImplicitly] set; }

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process([NotNull] CodeGenerationArgs args)
        {
            Assert.IsNotNull(args.Model, "Model");
            Assert.IsNotNull(args.ZipWriter, "ZipWriter");
            var template = new WebControlViewTemplate
                               {
                                   Model = args.Model
                               };

            string code = template.Render();

            var codeBytes = Encoding.Default.GetBytes(code);
            var path = string.Concat(this.SourceFolderPath, args.Model.Namespace, '/', args.Model.ViewName, '/', args.Model.ViewName, ".cs");

            args.ZipWriter.AddEntry(path, codeBytes);
        }
    }
}