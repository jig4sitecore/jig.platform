﻿namespace Jig.Sitecore.Development.WebControlView.Processors
{
    using global::Sitecore;
    using global::Sitecore.Web.UI.Sheer;

    /// <summary>
    /// The download zip.
    /// </summary>
    public sealed class DownloadZip : ICodeGenerationProcessor
    {
        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process([NotNull] CodeGenerationArgs args)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (args.ZipWriter != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                args.ZipWriter.Dispose();
            }

            SheerResponse.Eval("window.open('{0}', 'download');", args.DownloadUrl);
        }
    }
}