﻿namespace Jig.Sitecore.Development.WebControlView.Processors
{
    using System.IO;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.IO;

    using global::Sitecore.Zip;

    /// <summary>
    /// The initialize zip writer.
    /// </summary>
    public sealed class InitializeZipWriter : ICodeGenerationProcessor
    {
        /// <summary>
        /// Gets or sets the download folder path.
        /// </summary>
        [global::Sitecore.CanBeNull]
        [global::Sitecore.UsedImplicitly]
        public string DownloadFolderPath { get; set; }

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process([NotNull] CodeGenerationArgs args)
        {
            Assert.IsNotNullOrEmpty(this.DownloadFolderPath, "DownloadFolderPath");
            Assert.IsNotNull(args.Model, "Model");

            var folder = FileUtil.MapPath(this.DownloadFolderPath);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            var file = string.Concat(folder, "\\", args.Model.ModelName, ".zip");

            args.DownloadUrl = string.Concat(this.DownloadFolderPath, args.Model.ModelName, ".zip");

            args.ZipWriter = new ZipWriter(file);
        }
    }
}