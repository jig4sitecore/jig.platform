﻿namespace Jig.Sitecore.Development.WebControlView.Processors
{
    using System.Text;

    using Jig.Sitecore.Development.WebControlView;

    using global::Sitecore;

    using global::Sitecore.Diagnostics;

    /// <summary>
    /// The generate razor view.
    /// </summary>
    public sealed class GenerateRazorView : ICodeGenerationProcessor
    {
        /// <summary>
        /// Gets or sets the source folder path.
        /// </summary>
        [NotNull]
        [UsedImplicitly]
        public string SourceFolderPath { get; set; }

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">The args.</param>
        public void Process([NotNull] CodeGenerationArgs args)
        {
            Assert.IsNotNull(args.Model, "Model");
            Assert.IsNotNull(args.ZipWriter, "ZipWriter");

            var template = new RazorControlViewTemplate
                               {
                                   Model = args.Model
                               };

            string code = template.Render();
            var codeBytes = Encoding.Default.GetBytes(code);
            var path = string.Concat(this.SourceFolderPath, args.Model.Namespace, '/', args.Model.ViewName, '/', args.Model.ViewName, ".cshtml");

            args.ZipWriter.AddEntry(path, codeBytes);
        }
    }
}