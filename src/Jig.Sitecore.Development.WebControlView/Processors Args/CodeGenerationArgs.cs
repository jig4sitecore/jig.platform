﻿namespace Jig.Sitecore.Development.WebControlView
{
    using System;

    using global::Sitecore;

    using global::Sitecore.Web.UI.Sheer;

    using global::Sitecore.Zip;

    /// <summary>
    /// The code generation args.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Justification = "No Native resources"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable", Justification = "The serialization never used any more. Legacy"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2240:ImplementISerializableCorrectly", Justification = "No unmanaged resources were allocated")]
    public class CodeGenerationArgs : ClientPipelineArgs, IDisposable
    {
        /// <summary>
        /// The disposed.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Gets or sets the zip writer.
        /// </summary>
        [CanBeNull]
        [UsedImplicitly]
        public ZipWriter ZipWriter { get; set; }

        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        [UsedImplicitly]
        [CanBeNull]
        public ViewTemplateModel Model { get; set; }

        /// <summary>
        /// Gets or sets the download url.
        /// </summary>
        [UsedImplicitly]
        [CanBeNull]
        public string DownloadUrl { get; set; }

        /// <summary>
        /// The dispose.
        /// </summary>
        void IDisposable.Dispose()
        {
            this.Dispose();
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        protected override void Dispose()
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (!this.disposed && this.ZipWriter != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                this.disposed = true;
                this.ZipWriter.Dispose();
            }

            base.Dispose();
        }
    }
}