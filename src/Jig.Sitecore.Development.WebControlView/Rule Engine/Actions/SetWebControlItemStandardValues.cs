﻿namespace Jig.Sitecore.Rules.Actions
{
    using System.Runtime.InteropServices;

    using Jig.Sitecore.CustomItems;
    using Jig.Sitecore.Development.WebControlView;

    using global::Sitecore;

    using global::Sitecore.Rules;

    /// <summary>
    /// Set WebControl Item Standard Values. This class cannot be inherited.
    /// </summary>
    /// <typeparam name="TRuleContext">The type of the t rule context.</typeparam>
    [UsedImplicitly]
    [Guid("3BE72D75-8638-46F4-963F-A7B2DD4C3372")]
    public sealed class SetWebControlItemStandardValues<TRuleContext> : Jig.Sitecore.Rules.Actions.RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        #region Methods

        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        protected override void ApplyRule([NotNull] TRuleContext ruleContext)
        {
            WebControlItem webControlItem = ruleContext.Item;
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (webControlItem != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                webControlItem.InnerItem.SetWebControlStandardValues();
            }
        }

        #endregion
    }
}