//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Jig.Sitecore.Development.WebControlView {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    
    public partial class WebControlViewTemplate : Jig.Web.CodeGeneration.Views.CodeGeneratorView<Jig.Sitecore.Development.WebControlView.ViewTemplateModel> {


         
        protected override void RenderView(HtmlTextWriter writer) {
writer.Write("namespace ");

writer.Write(this.Model.Namespace);

writer.Write(" \r\n{\r\n    using System.Runtime.InteropServices;\r\n    using System.Web.UI;\r\n    us" +
"ing Jig.Sitecore;\r\n    using Jig.Web.Html;\r\n    using global::Sitecore;\r\n\r\n    /" +
"// <summary>\r\n    /// The ");

writer.Write(this.Model.ViewDisplayName);

writer.Write(" view\r\n    ///     <para>Template Path: ");

                     writer.Write(this.Model.ModelFullPath);

writer.Write("</para>\r\n    ///     <para>Rendering Path: ");

                      writer.Write(this.Model.ViewFullPath);

writer.Write("</para>\r\n    /// </summary>\r\n    [Guid(\"");

writer.Write(this.Model.Id);

writer.Write("\")]\r\n");

writer.Write("    ");

writer.Write(this.Model.GetViewAttributeTag());

writer.Write("\r\n");

writer.Write("    ");

writer.Write(this.Model.GetToolboxDataTag());

writer.Write("\r\n    public partial class ");

             writer.Write(this.Model.ViewName);

writer.Write(" : Jig.Sitecore.Views.WebForms.WebControlView<");

                                                                               writer.Write(this.Model.ModelFullName);

writer.Write(@">
    {
        /// <summary>
        /// Called from Sitecore's DoRender() contract, this method will be executed if the control
        /// is not cached and the control has data to render, unless in Page Editor, in which case
        /// the logic will execute regardless.
        /// </summary>
        /// ");

writer.Write(this.Model.GetParamOutputTag("The HtmlTextWriter for the response."));

writer.Write(@"
        protected override void RenderView([NotNull] HtmlTextWriter output)
        {
            var attributes = this.GetParametersAsXAttributes();
            using (output.RenderSection(this.CssClass, attributes: attributes.Values))
            {
                /* Default Behavior - Renders partial view from generated class */
                this.RenderPartialView(output);
            }
        }
    }
}");

        }
    }
}
