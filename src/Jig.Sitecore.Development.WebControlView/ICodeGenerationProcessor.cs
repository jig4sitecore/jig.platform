﻿namespace Jig.Sitecore.Development.WebControlView.Processors
{
    using global::Sitecore;

    /// <summary>
    /// The code generation processor.
    /// </summary>
    public interface ICodeGenerationProcessor
    {
        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        void Process([NotNull] CodeGenerationArgs args);
    }
}