﻿namespace Jig.Sitecore.Development.WebControlView
{
    using System.Linq;

    using Jig.Sitecore.CustomItems;
    using Jig.Sitecore.Sites.Shared.Data.Folders;
    using Jig.Web.CodeGeneration;

    using global::Sitecore;

    using global::Sitecore.Data;

    using global::Sitecore.Data.Items;

    using global::Sitecore.SecurityModel;

    /// <summary>
    /// The web control item extensions.
    /// </summary>
    public static class WebControlItemExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Sets the web control standard values.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="datasourceTemplateId">The datasource template identifier.</param>
        public static void SetWebControlStandardValues([NotNull] this Item item, [CanBeNull] ID datasourceTemplateId = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item != null)
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                var settingFolder = item.Axes.GetAncestors().As<SiteRenderingFolder>().FirstOrDefault();
                if (settingFolder == null)
                {
                    return;
                }

                WebControlItem webControlItem = item;

                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (webControlItem == null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                // ReSharper disable HeuristicUnreachableCode
                {
                    return;
                }

                /* ReSharper restore HeuristicUnreachableCode */

                using (new SecurityDisabler())
                {
                    webControlItem.BeginEdit();

                    if (!webControlItem.Name.EndsWith("View"))
                    {
                        var name = webControlItem.Name;
                        webControlItem.InnerItem.Name = name.AsClassName() + "View";
                        webControlItem.Appearance.DisplayName = name;
                    }

                    webControlItem.Tag.Value = webControlItem.Name;

                    if (string.IsNullOrWhiteSpace(webControlItem.Namespace.Value)
                        && !string.IsNullOrWhiteSpace(settingFolder.Namespace))
                    {
                        webControlItem.Namespace.Value = settingFolder.Namespace;
                    }

                    if (string.IsNullOrWhiteSpace(webControlItem.Assembly.Value)
                        && !string.IsNullOrWhiteSpace(settingFolder.Assembly))
                    {
                        webControlItem.Assembly.Value = settingFolder.Assembly;
                    }

                    if (string.IsNullOrWhiteSpace(webControlItem.TagPrefix.Value)
                        && !string.IsNullOrWhiteSpace(settingFolder.TagPrefix))
                    {
                        webControlItem.TagPrefix.Value = settingFolder.TagPrefix;
                    }

                    if (string.IsNullOrWhiteSpace(webControlItem.DatasourceLocation.Value)
                        && !string.IsNullOrWhiteSpace(settingFolder.DatasourceLocation.Value))
                    {
                        webControlItem.DatasourceLocation.Value = settingFolder.DatasourceLocation.Value;
                    }

                    if (string.IsNullOrWhiteSpace(webControlItem.ParametersTemplate.Value)
                        && !string.IsNullOrWhiteSpace(settingFolder.ParametersTemplate.Value))
                    {
                        webControlItem.ParametersTemplate.Value = settingFolder.ParametersTemplate.Value;
                    }

                    if (string.IsNullOrWhiteSpace(webControlItem.Placeholder.Value)
                    && !string.IsNullOrWhiteSpace(settingFolder.Placeholder.Value))
                    {
                        webControlItem.Placeholder.Value = settingFolder.Placeholder.Value;
                    }

                    webControlItem.Cacheable.Value = settingFolder.Cacheable.Checked ? "1" : "0";
                    webControlItem.VaryByData.Value = settingFolder.VaryByData.Checked
                                                                                  ? "1"
                                                                                  : "0";
                    webControlItem.ClearOnIndexUpdate.Value =
                        settingFolder.ClearOnIndexUpdate.Checked ? "1" : "0";

                    webControlItem.DatasourceTemplate.Value = string.Concat(datasourceTemplateId);

                    webControlItem.EndEdit();
                }
            }
        }

        #endregion
    }
}