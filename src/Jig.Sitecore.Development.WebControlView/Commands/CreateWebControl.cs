﻿namespace Jig.Sitecore.Development.WebControlView.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Text.RegularExpressions;

    using Jig.Sitecore.CustomItems;
    using Jig.Sitecore.Development.WebControlView;
    using Jig.Sitecore.Sites.Shared.Data.Folders;
    using Jig.Web.CodeGeneration;
    using global::Sitecore;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Shell.Framework.Commands;
    using global::Sitecore.Web.UI.Sheer;

    /// <summary>
    /// The create web control command
    /// </summary>
    public partial class CreateWebControl : Command
    {
        /// <summary>
        /// Executes the command in the specified context.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public override void Execute([NotNull] CommandContext context)
        {
            Assert.IsNotNull(context, "context");
            if (context.Items.Length == 1)
            {
                var parameters = new NameValueCollection();
                parameters["items"] = this.SerializeItems(context.Items);
                Context.ClientPage.Start(this, "Run", parameters);
            }
        }

        /// <summary>
        /// Queries the state.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The Command State</returns>
        public override CommandState QueryState([NotNull] CommandContext context)
        {
            if (context.Items.Length != 1)
            {
                return CommandState.Hidden;
            }

            var item = context.Items[0];

            if (item.TemplateID != TemplateIDs.Template)
            {
                return CommandState.Hidden;
            }

            if (
                !item.Paths.FullPath.StartsWith(
                    global::Jig.Sitecore.Constants.Paths.Sites.Templates,
                    StringComparison.InvariantCultureIgnoreCase))
            {
                return CommandState.Disabled;
            }

            return base.QueryState(context);
        }

        /// <summary>
        /// Runs the pipeline.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <remarks>this will be run from button in ribbon</remarks>
        protected virtual void Run([NotNull] ClientPipelineArgs args)
        {
            Assert.IsNotNull(args, "args");
            SheerResponse.CheckModified(false);

            var currentItem = this.DeserializeItems(args.Parameters["items"])[0];

            TemplateItem currentTemplate = currentItem.Database.GetTemplate(currentItem.ID);
            var fullPath = currentTemplate.InnerItem.Paths.FullPath;

            if (
                !fullPath.StartsWith(
                    global::Jig.Sitecore.Constants.Paths.Sites.Templates,
                    StringComparison.InvariantCultureIgnoreCase))
            {
                SheerResponse.Alert("Path does not follow convention, please refer to Jig sitecore guide.", false);
                return;
            }

            var list =
                Regex.Replace(
                    fullPath,
                    global::Jig.Sitecore.Constants.Paths.Sites.Templates,
                    string.Empty,
                    RegexOptions.IgnoreCase).Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

            if (list.Length == 0)
            {
                SheerResponse.Alert("Path does not follow convention, please refer to Jig sitecore guide.", false);
                return;
            }

            var siteName = list[0];

            var webControl =
                currentItem.Database.SelectSingleItem(
                    global::Jig.Sitecore.Constants.Paths.Sites.Renderings + "/" + siteName + "//*[@@name='" + currentItem.Name
                    + "View']");

            if (webControl != null)
            {
                SheerResponse.Alert("Control item already exists", false);
                Context.ClientPage.SendMessage(this, string.Format("item:refresh(id={0})", webControl.ID));
                return;
            }

            var renderingsFolder = currentItem.Database.GetItem(global::Jig.Sitecore.Constants.Id.Sites.Renderings);

            if (renderingsFolder == null)
            {
                SheerResponse.Alert("Rendering folder 'CFCACFC6-39CA-4BE1-B961-94037203B2FC' does not exist.", false);
                return;
            }

            SiteRenderingFolder siteRenderingsFolder = renderingsFolder.GetChildren().FirstOrDefault(c => string.Equals(c.Name, siteName, StringComparison.InvariantCultureIgnoreCase));

            if (siteRenderingsFolder == null)
            {
                SheerResponse.Alert("Site Rendering folder with name '" + siteName + "'  does not exist.", false);
                return;
            }

            var webControlItem = siteRenderingsFolder.InnerItem.Add(
                  currentItem.Name,
                  currentItem.Database.GetTemplate(typeof(WebControlItem)));

            webControlItem.SetWebControlStandardValues(currentTemplate.ID);

            /*
             *  Model:
             *      Jig.Sitecore.Sites.Shared.Data
             *      /sitecore/templates/Sites/Shared/Widgets/Person
             *  Result:
             *      Jig.Sitecore.Sites.Shared.Data.Widgets
             */
            string modelFullNameOffset =
                currentItem.Paths.FullPath.Replace("/sitecore/templates/Sites/", string.Empty)
                    .Replace(siteName + "/", string.Empty)
                    .Replace(currentItem.Name, string.Empty)
                    .Trim(' ', '/', '_', '-');

            var modelFullName = siteRenderingsFolder.ModelNamespace + "." + modelFullNameOffset.Split('/').AsNamespace();

            var codeGenerationArgs = new CodeGenerationArgs
            {
                Model = new ViewTemplateModel
                    {
                        Id = webControlItem.ID.Guid.ToString("D"),
                        Namespace = siteRenderingsFolder.Namespace,
                        ModelNamespace = modelFullName,
                        ModelName = currentItem.Name.AsClassName(),
                        ModelTemplateId = currentItem.Template.ID.Guid.ToString("D"),
                        ViewDisplayName = webControlItem.DisplayName,
                        ViewName = webControlItem.Name.AsClassName(),
                        ViewFullPath = webControlItem.Paths.FullPath,
                        Properties = this.GetPropertyNames(currentTemplate),
                        ModelFullPath = currentItem.Paths.FullPath
                    }
            };

            Context.ClientPage.Start("jig.ui.codeGeneration", codeGenerationArgs);
        }

        /// <summary>
        /// Gets the property names.
        /// </summary>
        /// <param name="templateItem">The template item.</param>
        /// <returns>The list of property names</returns>
        protected virtual IList<string> GetPropertyNames(TemplateItem templateItem)
        {
            var dictionary = new Dictionary<string, int>();

            foreach (var field in templateItem.OwnFields)
            {
                if (field.Key.StartsWith("_"))
                {
                    continue;
                }

                var key = field.Name.AsPropertyName();
                dictionary[key] = field.Sortorder;
            }

            foreach (TemplateItem baseTemplateItem in templateItem.BaseTemplates)
            {
                foreach (var field in baseTemplateItem.OwnFields)
                {
                    if (field.Key.StartsWith("_"))
                    {
                        continue;
                    }

                    var key = field.Name.AsPropertyName();
                    dictionary[key] = field.Sortorder;
                }
            }

            return dictionary.ToArray().OrderBy(x => x.Value).Select(x => x.Key).ToArray();
        }
    }
}