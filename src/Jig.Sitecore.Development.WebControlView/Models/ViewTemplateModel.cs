﻿namespace Jig.Sitecore.Development.WebControlView
{
    using System.Collections.Generic;

    using global::Sitecore;

    /// <summary>
    /// The model.
    /// </summary>
    public sealed class ViewTemplateModel
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewTemplateModel"/> class.
        /// </summary>
        // ReSharper disable NotNullMemberIsNotInitialized
        public ViewTemplateModel()
        // ReSharper restore NotNullMemberIsNotInitialized
        {
            this.Properties = new List<string>();
            this.RenderPartialView = "RenderPartialView";
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the guid attribute.
        /// </summary>
        [NotNull]
        [UsedImplicitly]
        public string Id { get; set; }

        /// <summary>
        /// Gets the model full name.
        /// </summary>
        [UsedImplicitly]
        [NotNull]
        public string ModelFullName
        {
            get
            {
                return this.ModelNamespace + "." + this.ModelName;
            }
        }

        /// <summary>
        /// Gets or sets the model name.
        /// </summary>
        [UsedImplicitly]
        [NotNull]
        public string ModelName { get; set; }

        /// <summary>
        /// Gets or sets the model namespace.
        /// </summary>
        [UsedImplicitly]
        [NotNull]
        public string ModelNamespace { get; set; }

        /// <summary>
        /// Gets or sets the model template identifier.
        /// </summary>
        /// <value>The model template identifier.</value>
        [UsedImplicitly]
        [NotNull]
        public string ModelTemplateId { get; set; }

        /// <summary>
        /// Gets or sets the namespace.
        /// </summary>
        [UsedImplicitly]
        [NotNull]
        public string Namespace { get; set; }

        /// <summary>
        /// Gets or sets the properties.
        /// </summary>
        [UsedImplicitly]
        [NotNull]
        public IList<string> Properties { get; set; }

        /// <summary>
        /// Gets or sets the render partial view.
        /// </summary>
        /// <value>The render partial view.</value>
        [UsedImplicitly]
        [NotNull]
        public string RenderPartialView { get; set; }

        /// <summary>
        /// Gets or sets the view display name.
        /// </summary>
        [UsedImplicitly]
        [NotNull]
        public string ViewDisplayName { get; set; }

        /// <summary>
        /// Gets or sets the view full path.
        /// </summary>
        /// <value>The view full path.</value>
        [UsedImplicitly]
        [NotNull]
        public string ViewFullPath { get; set; }

        /// <summary>
        /// Gets or sets the view name.
        /// </summary>
        [UsedImplicitly]
        [NotNull]
        public string ViewName { get; set; }

        /// <summary>
        /// Gets or sets the model path.
        /// </summary>
        /// <value>The model path.</value>
        [UsedImplicitly]
        [NotNull]
        public string ModelFullPath { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the parameter output tag.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The param output tag
        /// </returns>
        [NotNull]
        public string GetParamOutputTag([NotNull] string value)
        {
            /* <param name="output">The HtmlTextWriter for the response.</param> */
            var tag = string.Concat("<param name=\"output\">", value, "</param>");
            return tag;
        }

        /// <summary>
        /// Gets the toolbox data tag.
        /// </summary>
        /// <returns>The ss toolbox tag</returns>
        [NotNull]
        public string GetToolboxDataTag()
        {
            var tag = string.Concat(
                @"[ToolboxData(@""<{0}:",
                this.ViewName,
                @" runat=""""server""""></{0}:",
                this.ViewName,
                @">"")]");

            return tag;
        }

        /// <summary>
        /// Gets the view attribute tag.
        /// </summary>
        /// <returns>The View Tag</returns>
        [NotNull]
        public string GetViewAttributeTag()
        {
            var tag = string.Format(
                @"[View(""{0}"", DisplayName = ""{1}"", TemplateId = ""{2}"")]",
                this.Id,
                this.ViewDisplayName,
                this.ModelTemplateId);

            return tag;
        }

        #endregion
    }
}