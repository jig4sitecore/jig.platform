﻿namespace Jig.Sitecore.Sites.Shared.Views
{
    using System.Web.UI;
    using Jig.Sitecore.Sites.Shared.Data;
    using Jig.Sitecore.Sites.Shared.Data.Navigation;

    using Jig.Web.Html;
    using global::Sitecore;

    /// <summary>
    /// The navigation view.
    /// </summary>
    [ToolboxData("<{0}:SimpleNavigationView runat=\"server\"></{0}:SimpleNavigationView>")]
    public partial class SimpleNavigationView : NavigationMenuBase
    {
        #region Methods

        /// <summary>
        /// Renders the view.
        /// </summary>
        /// <param name="output">The output.</param>
        protected override void RenderView([NotNull] HtmlTextWriter output)
        {
            /*
              <ul>
                <li><a href="#">Legal Info</a></li>
                <li><a href="#">Site Map</a></li>
               </ul>            
            */
            var attributes = this.GetParametersAsXAttributes();
            using (output.RenderUl(cssClass: this.CssClass, attributes: attributes.Values))
            {
                // ReSharper disable AssignNullToNotNullAttribute
                foreach (INavigationLink linkItem in this.Model.GetChildrenLinks())
                // ReSharper restore AssignNullToNotNullAttribute
                {
                    string link = linkItem.Render();
                    if (string.IsNullOrWhiteSpace(link))
                    {
                        var code = this.GetErrorCode(linkItem);
                        output.WriteLine(code);
                        continue;
                    }

                    using (output.RenderLi())
                    {
                        output.WriteLine(link);
                    }
                }
            }
        }

        #endregion
    }
}