namespace Jig.Sitecore.Sites.Shared.Views
{
    using Jig.Sitecore.Sites.Shared.Data.Navigation;

    /// <summary>
    /// The navigation menu base.
    /// </summary>
    public abstract class NavigationMenuBase : NavigationMenuBase<INavigationMenu>
    {
    }
}