namespace Jig.Sitecore.Sites.Shared.Views
{
    using Jig.Sitecore.Sites.Shared.Data.Navigation;

    /// <summary>
    /// The navigation link.
    /// </summary>
    public class NavigationLink : NavigationLink<INavigationLink>
    {
    }
}