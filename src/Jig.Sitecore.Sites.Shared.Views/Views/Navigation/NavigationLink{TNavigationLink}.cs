namespace Jig.Sitecore.Sites.Shared.Views
{
    using System.Web.UI;
    using System.Xml.Linq;

    using Jig.Sitecore.Sites.Shared.Data.Navigation;
    using Jig.Sitecore.Views.WebForms;
    using global::Sitecore;

    /// <summary>
    /// The navigation link.
    /// </summary>
    /// <typeparam name="TNavigationLink">The type of the navigation link.</typeparam>
    public abstract class NavigationLink<TNavigationLink> : WebControlView<TNavigationLink>
        where TNavigationLink : class, INavigationLink
    {
        #region Methods

        /// <summary>
        /// Gets the error code.
        /// </summary>
        /// <param name="linkItem">
        /// The link item.
        /// </param>
        /// <returns>
        /// The Error Code
        /// </returns>
        [NotNull]
        protected virtual XElement GetErrorCode([NotNull] INavigationLink linkItem)
        {
            var code = new XElement("code", string.Empty);

            code.Add(new XAttribute("class", "error"));
            code.Add(new XAttribute("data-error-message", "Could not resolve link!"));
            code.Add(new XAttribute("data-item-id", linkItem.ID.Guid.ToString("D")));
            code.Add(new XAttribute("data-target-item-id", linkItem.ID.Guid.ToString("D")));
            code.Add(new XAttribute("data-type-fullname", typeof(INavigationLink).FullName));

            return code;
        }

        /// <summary>
        /// Renders the page editor view.
        /// </summary>
        /// <param name="output">
        /// The output.
        /// </param>
        protected override void RenderPageEditorView(HtmlTextWriter output)
        {
            /* No Html */
        }

        /// <summary>
        /// Called from Sitecore's DoRender() contract, this method will be executed if the control
        /// is not cached and the control has data to render, unless in Page Editor, in which case
        /// the logic will execute regardless.
        /// </summary>
        /// <param name="output">
        /// The HtmlTextWriter for the response.
        /// </param>
        protected override void RenderView(HtmlTextWriter output)
        {
            var attributes = this.GetParametersAsXAttributes();
            var navigationLink = this.Model;
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (navigationLink != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var html = navigationLink.Link.Render(
                    innerHtml: string.Empty,
                    cssClass: this.CssClass,
                    attributes: attributes.Values);

                output.Write(html);
            }
        }

        #endregion
    }
}