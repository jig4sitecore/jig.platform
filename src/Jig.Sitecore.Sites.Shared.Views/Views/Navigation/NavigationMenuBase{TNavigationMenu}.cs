namespace Jig.Sitecore.Sites.Shared.Views
{
    using System.Web.UI;
    using System.Xml.Linq;

    using Jig.Sitecore.Sites.Shared.Data.Navigation;
    using global::Sitecore;

    /// <summary>
    /// The navigation menu base.
    /// </summary>
    /// <typeparam name="TNavigationMenu">The type of the navigation menu.</typeparam>
    public abstract class NavigationMenuBase<TNavigationMenu> : Jig.Sitecore.Views.WebForms.WebControlView<TNavigationMenu>
        where TNavigationMenu : class, INavigationMenu
    {
        #region Methods

        /// <summary>
        /// Gets the error code.
        /// </summary>
        /// <param name="linkItem">
        /// The link item.
        /// </param>
        /// <returns>
        /// The Error Code 
        /// </returns>
        [NotNull]
        protected virtual XElement GetErrorCode([NotNull] INavigationLink linkItem)
        {
            var code = new XElement("code", string.Empty);

            code.Add(new XAttribute("class", "error"));
            code.Add(new XAttribute("data-error-message", "Could not resolve link!"));
            code.Add(new XAttribute("data-item-id", linkItem.ID.Guid.ToString("D")));
            code.Add(new XAttribute("data-target-item-id", linkItem.ID.Guid.ToString("D")));
            code.Add(new XAttribute("data-type-fullname", typeof(INavigationLink).FullName));

            return code;
        }

        /// <summary>
        /// Renders the page editor view.
        /// </summary>
        /// <param name="output">
        /// The output.
        /// </param>
        protected override void RenderPageEditorView(HtmlTextWriter output)
        {
            /* No Html */
        }

        #endregion
    }
}