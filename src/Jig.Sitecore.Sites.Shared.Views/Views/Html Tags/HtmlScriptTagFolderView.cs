﻿namespace Jig.Sitecore.Sites.Shared.Views
{
    using System;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Web.UI;
    using Jig.Sitecore;
    using Jig.Sitecore.Sites.Shared.Data.Folders;
    using Jig.Sitecore.Sites.Shared.Data.HtmlTags;
    using Jig.Sitecore.Sites.Shared.Data.System;
    using Jig.Sitecore.Views.WebForms;
    using Jig.Web;
    using Jig.Web.Html;
    using global::Sitecore.Data;

    /// <summary>
    /// The Html Script Tag Folder view
    ///     <para>Template Path: /sitecore/templates/Sites/Shared/Folders/Html Script Tag Folder</para>
    ///     <para>Rendering Path: /sitecore/layout/Renderings/Sites/Shared/HtmlScriptTagFolderView</para>
    /// </summary>
    [Guid("fa4b33dc-8d44-44c9-a1a9-84e365ff9c83")]
    [View("fa4b33dc-8d44-44c9-a1a9-84e365ff9c83",
        DisplayName = "Html Script Tag Folder",
        Path = "/sitecore/layout/Renderings/Sites/Shared/HtmlScriptTagFolderView",
        TemplateId = "ab86861a-6030-46c5-b394-e8f99e8b87db")]
    [ToolboxData(@"<{0}:HtmlScriptTagFolderView runat=""server""></{0}:HtmlScriptTagFolderView>")]
    public partial class HtmlScriptTagFolderView :
        WebControlView<HtmlScriptTagFolder>
    {
        /// <summary>
        /// Sets the static files hostname folder.
        /// </summary>
        /// <value>The static files hostname folder.</value>
        /// <remarks>Used by /sitecore/templates/Sites/Shared/Rendering Parameters/Html Tag Folder Rendering Parameters/Datasource/StaticFilesHostnameFolder</remarks>
        public string StaticFilesHostnameFolder
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    Guid id;
                    if (Guid.TryParse(value, out id))
                    {
                        this.StaticFilesHostnameFolderId = id;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the static files hostname folder identifier.
        /// </summary>
        /// <value>The static files hostname folder identifier.</value>
        public Guid? StaticFilesHostnameFolderId { get; set; }

        /// <summary>
        /// Called from Sitecore's DoRender() contract, this method will be executed if the control
        /// is not cached and the control has data to render, unless in Page Editor, in which case
        /// the logic will execute regardless.
        /// </summary>
        /// <param name="output">The HtmlTextWriter for the response.</param>
        protected override void RenderView([global::Sitecore.NotNull] HtmlTextWriter output)
        {
            string hostname = this.ResolveStaticFileHostName();

            var children = this.Model.GetChildren<IHtmlScriptTag>();
            foreach (IHtmlScriptTag child in children)
            {
                if (!child.SourceAttributeValue.HasValue)
                {
                    continue;
                }

                var src = this.ResolveScriptScrAttribute(hostname, child.SourceAttributeValue);

                var script = new Script
                {
                    Src = src,
                    Type = child.TypeAttributeValue.Value
                };

                /* Add sitecore attributes */
                var attributes = this.GetParametersAsXAttributes();
                script.Add(attributes);

                var html = new StringBuilder(128);
                if (!string.IsNullOrWhiteSpace(child.ConditionalCommentValue))
                {
                    html.Append("<!--[").Append(child.ConditionalCommentValue.Value).Append("]>");
                    html.Append(script);
                    html.Append(@"<![endif]-->");
                }
                else
                {
                    html.Append(script);
                }

                string htmlOutput = html.ToString();
                output.WriteLineNoTabs(htmlOutput);
            }
        }

        /// <summary>
        /// Resolves the name of the static file host.
        /// </summary>
        /// <returns>SThe static file host name</returns>
        [global::Sitecore.NotNull]
        protected string ResolveStaticFileHostName()
        {
            if (this.StaticFilesHostnameFolderId.HasValue)
            {
                var root = this.Database.GetItem(new ID(this.StaticFilesHostnameFolderId.Value));
                if (root != null)
                {
                    var hostname = this.Context.Request.Url.Host.ToLowerInvariant();
                    var collection = this.Model.Children.As<StaticFileHostName>(this.Language);
                    foreach (StaticFileHostName item in collection)
                    {
                        if (item.MatchUriHost(hostname))
                        {
                            string path = item.StaticFileHostNameWithOptionalPath.Value;
                            if (!string.IsNullOrWhiteSpace(path))
                            {
                                return path;
                            }
                        }
                    }

                    var rootFolder = root.As<StaticFilesHostnameFolder>();
                    if (rootFolder != null && rootFolder.StaticFileHostNameWithOptionalPath.HasValue)
                    {
                        return rootFolder.StaticFileHostNameWithOptionalPath.Value;
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Resolves the script SCR attribute.
        /// </summary>
        /// <param name="hostname">The hostname.</param>
        /// <param name="sourceAttributeValue">The child.</param>
        /// <returns>The attribute value</returns>
        [global::Sitecore.NotNull]
        protected string ResolveScriptScrAttribute([global::Sitecore.NotNull] string hostname, [global::Sitecore.NotNull] string sourceAttributeValue)
        {
            var src = (sourceAttributeValue.StartsWith(
                                 "http",
                                 StringComparison.InvariantCultureIgnoreCase)
                                             || sourceAttributeValue.StartsWith(
                                                 "//",
                                                 StringComparison.InvariantCultureIgnoreCase) || string.IsNullOrWhiteSpace(hostname))
                                                 ? sourceAttributeValue :
                                                   string.Concat("//", hostname, sourceAttributeValue.EnsureStartsWith("/"));

            return src;
        }
    }
}