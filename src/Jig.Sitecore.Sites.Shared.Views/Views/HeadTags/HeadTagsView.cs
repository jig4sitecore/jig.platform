﻿namespace Jig.Sitecore.Sites.Shared.Views
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Web.UI;
    using Jig.Sitecore.Sites.Shared.Data;
    using Jig.Sitecore.Sites.Shared.Data.Pages;
    using Jig.Sitecore.Views.WebForms;
    using Jig.Web.Html;
    using global::Sitecore;
    using global::Sitecore.Data;

    /// <summary>
    /// The content page head tags.
    /// </summary>
    [DefaultProperty("Text")]
    [ToolboxData("<{0}::HeadTagsView runat=server></{0}::HeadTagsView>")]
    public class HeadTagsView : WebControlView<IContentPage>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HeadTagsView"/> class.
        /// </summary>
        public HeadTagsView()
        {
            this.FolderRelativePath = "/site-settings/headtags";
        }

        /// <summary>
        /// Gets or sets the folder relative path.
        /// </summary>
        /// <value>The folder relative path.</value>
        [NotNull]
        public string FolderRelativePath { get; set; }

        /// <summary>
        /// Gets or sets the site wide data source.
        /// </summary>
        /// <value>
        /// The site wide data source.
        /// </value>
        public Guid? RootFolderDataSource { get; [UsedImplicitly] set; }

        #region Methods

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            // ReSharper disable UseNullPropagation
            if (this.Model == null)
            // ReSharper restore UseNullPropagation
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return;
            }
            // ReSharper restore HeuristicUnreachableCode

            /* The control should be on the Page  */
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (this.RootFolderDataSource.HasValue)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var tags = this.Model.GetPageHeadTags(new ID(this.RootFolderDataSource.Value)).ToArray();
                using (var contextItemTags = this.Context.GetHtmlHeadTags())
                {
                    contextItemTags.AddRange(tags);
                }
            }
            else
            {
                var folder = this.ResolveRootFolder();
                if (folder != null)
                {
                    var tags = this.Model.GetPageHeadTags(folder).ToArray();
                    using (var contextItemTags = this.Context.GetHtmlHeadTags())
                    {
                        contextItemTags.AddRange(tags);
                    }
                }
            }

            /* This is not suppose to be editable on page. Its global settings as well  */
            this.Visible = global::Sitecore.Context.PageMode.IsNormal;
        }

        /// <summary>
        /// Resolves the root folder data source.
        /// </summary>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        [CanBeNull]
        protected global::Sitecore.Data.Items.Item ResolveRootFolder()
        {
            var site = global::Sitecore.Context.Site;
            if (site != null)
            {
                /* /sitecore/content/sites/{Site Name}/site-settings/HeadTags */
                var fullPath = site.RootPath + this.FolderRelativePath;
                var item = this.Database.GetItem(fullPath);
                return item;
            }

            return null;
        }

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (this.Model != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var tags = this.Model.GetPageHeadTags().ToArray();
                using (var contextItemTags = this.Context.GetHtmlHeadTags())
                {
                    contextItemTags.AddRange(tags);
                }
            }
        }

        /// <summary>
        /// Renders the view.
        /// </summary>
        /// <param name="output">The output.</param>
        protected override void RenderView([NotNull] HtmlTextWriter output)
        {
            var tags = this.Context.GetHtmlHeadTags();
            tags.RenderControl(output);
        }

        #endregion
    }
}