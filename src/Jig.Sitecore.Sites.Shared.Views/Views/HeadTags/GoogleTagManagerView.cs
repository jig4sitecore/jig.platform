﻿namespace Jig.Sitecore.Sites.Shared.Views
{
    using System;
    using System.Web.UI;
    using Jig.Sitecore.Sites.Shared.Data.Folders;
    using Jig.Sitecore.Sites.Shared.Data.Metadata;
    using Jig.Sitecore.Views.WebForms;
    using Jig.Web.Html;
    using global::Sitecore;

    /// <summary>
    /// The Google Tag Manager view.
    /// </summary>
    [ToolboxData("<{0}:GoogleTagManagerView runat=\"server\"></{0}:GoogleTagManagerView>")]
    public partial class GoogleTagManagerView :
        WebControlView<IGoogleTagManagerScriptFolder>
    {
        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            /* This is not suppose to be editable on page. Its global settings  */
            this.Visible = global::Sitecore.Context.PageMode.IsNormal;
        }

        #region Methods

        /// <summary>
        /// The render page editor view.
        /// </summary>
        /// <param name="output">The output.</param>
        protected override void RenderPageEditorView([NotNull] HtmlTextWriter output)
        {
            output.Comment("The Google Tag Manager View goes here ...");
        }

        /// <summary>
        /// The render view.
        /// </summary>
        /// <param name="output">The output.</param>
        protected override void RenderView([NotNull] HtmlTextWriter output)
        {
            /* ReSharper restore HeuristicUnreachableCode */
            var value = this.ResolveGoogleTagManagerScriptValue();
            output.Write(value);
        }

        /// <summary>
        /// Resolves the google tag manager script value.
        /// </summary>
        /// <returns> google tag script value</returns>
        [NotNull]
        protected string ResolveGoogleTagManagerScriptValue()
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (this.Model == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return string.Empty;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            if (this.Model.HasChildren)
            {
                var hostname = this.Context.Request.Url;
                var collection = this.Model.Children.As<IGoogleTagManagerScript>(this.Language);
                foreach (IGoogleTagManagerScript item in collection)
                {
                    if (item.MatchUriHost(hostname))
                    {
                        string html = item.Script;
                        if (!string.IsNullOrWhiteSpace(html))
                        {
                            return html;
                        }
                    }
                }
            }

            string googleCode = this.Model.Script.Value;
            if (!string.IsNullOrWhiteSpace(googleCode))
            {
                return googleCode;
            }

            return string.Empty;
        }
        #endregion
    }
}