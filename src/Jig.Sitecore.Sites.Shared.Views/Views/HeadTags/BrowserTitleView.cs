﻿namespace Jig.Sitecore.Sites.Shared.Views
{
    using System.Web.UI;

    /// <summary>
    /// The Browser Title view.
    /// </summary>
    [ToolboxData("<{0}:BrowserTitleView runat=\"server\"></{0}:BrowserTitleView>")]
    public partial class BrowserTitleView :
        Control
    {
        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);

            /* This way the modification of browser are allowed in another controls  */
            var item = global::Sitecore.Context.Item;
            if (item != null)
            {
                // <head runat="server">
                //     <title>{Insert the page title here.}</title>
                // </head>
                string browserTitle = item[Jig.Sitecore.Sites.Shared.Data.Metadata.PageInformationAndMetadata.FieldNames.BrowserTitle];
                if (!string.IsNullOrWhiteSpace(this.Page.Title))
                {
                    browserTitle += " | " + this.Page.Title;
                }

                this.Page.Title = browserTitle;
            }

            this.Visible = global::Sitecore.Context.PageMode.IsNormal;
        }
    }
}