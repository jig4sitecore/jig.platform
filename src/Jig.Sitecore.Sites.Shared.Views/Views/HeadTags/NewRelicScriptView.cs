﻿namespace Jig.Sitecore.Sites.Shared.Views
{
    using System;
    using System.Web.UI;

    using Jig.Sitecore.Sites.Shared.Data.Folders;
    using Jig.Sitecore.Sites.Shared.Data.Metadata;
    using Jig.Sitecore.Views.WebForms;
    using Jig.Web.Html;

    using global::Sitecore;

    /// <summary>
    /// The NewRelic Script View.
    /// </summary>
    [ToolboxData("<{0}:NewRelicScriptView runat=\"server\"></{0}:NewRelicScriptView>")]
    public partial class NewRelicScriptView : WebControlView<INewRelicScriptFolder>
    {
        #region Methods

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            /* This is not suppose to be editable on page. Its global settings  */
            this.Visible = global::Sitecore.Context.PageMode.IsNormal;
        }

        /// <summary>
        /// The render page editor view.
        /// </summary>
        /// <param name="output">The output.</param>
        protected override void RenderPageEditorView([NotNull] HtmlTextWriter output)
        {
            output.Comment("The NewRelic Script goes here ...");
        }

        /// <summary>
        /// Called from Sitecore's DoRender() contract, this method will be executed if the control
        /// is not cached and the control has data to render, unless in Page Editor, in which case
        /// the logic will execute regardless.
        /// </summary>
        /// <param name="output">The HtmlTextWriter for the response.</param>
        protected override void RenderView([NotNull] HtmlTextWriter output)
        {
            var value = this.ResolveNewRelicScriptValue();
            output.Write(value);
        }

        /// <summary>
        /// Resolves the new relic script value.
        /// </summary>
        /// <returns>The NewRelic Script Value</returns>
        [NotNull]
        protected string ResolveNewRelicScriptValue()
        {
            if (this.Model.HasChildren)
            {
                var hostname = this.Context.Request.Url.Host;
                var collection = this.Model.Children.As<NewRelicScript>(this.Language);
                foreach (NewRelicScript item in collection)
                {
                    if (item.MatchUriHost(hostname))
                    {
                        string html = item.Script.Value;
                        if (!string.IsNullOrWhiteSpace(html))
                        {
                            return html;
                        }
                    }
                }
            }

            var code = this.Model.Script.Value;
            if (!string.IsNullOrWhiteSpace(code))
            {
                return code;
            }

            return string.Empty;
        }

        #endregion
    }
}