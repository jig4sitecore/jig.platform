﻿namespace Jig.Sitecore.Sites.Shared.Views
{
    using System.Web.UI;
    using Jig.Sitecore.Sites.Shared.Data.Pages;
    using Jig.Web.Html;
    using global::Sitecore;

    /// <summary>
    /// The PageDescription view.
    /// </summary>
    [ToolboxData("<{0}:PageMetaDescriptionView runat=\"server\"></{0}:PageMetaDescriptionView>")]
    public partial class PageMetaDescriptionView :
        Jig.Sitecore.Views.WebForms.WebControlView<IContentPage>
    {
        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);

            /* <meta name="description" content="This is an example of a meta description. This will often show up in search results."> */
            var contentPage = this.Model;
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (contentPage != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var content = contentPage.PageMetaDescription;
                if (!string.IsNullOrWhiteSpace(content))
                {
                    this.Page.MetaDescription = content;
                }
            }
        }

        /// <summary>
        /// The render page editor view.
        /// </summary>
        /// <param name="output">The output.</param>
        protected override void RenderPageEditorView([NotNull] HtmlTextWriter output)
        {
            output.Comment("The Meta Description goes here ...");
        }

        /// <summary>
        /// The render view.
        /// </summary>
        /// <param name="output">The output.</param>
        protected override void RenderView(HtmlTextWriter output)
        {
            /* <meta name="description" content="This is an example of a meta description. This will often show up in search results."> */
            var content = this.Page.MetaDescription;
            if (!string.IsNullOrWhiteSpace(content))
            {
                string tag = new Meta
                                 {
                                     Name = "description",
                                     Content = content
                                 };

                output.Write(tag);
            }
        }
    }
}