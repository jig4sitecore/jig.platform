﻿namespace Jig.Sitecore.Sites.Shared.Views
{
    using System.Web.UI;
    using Jig.Sitecore.Sites.Shared.Data.Folders;
    using Jig.Sitecore.Sites.Shared.Data.Metadata;
    using Jig.Sitecore.Views.WebForms;
    using Jig.Web.Html;

    using global::Sitecore;

    /// <summary>
    /// The google site verification view.
    /// </summary>
    [ToolboxData("<{0}:GoogleSiteVerificationView runat=\"server\"></{0}:GoogleSiteVerificationView>")]
    public partial class GoogleSiteVerificationView :
        WebControlView<IGoogleSiteVerificationCodeFolder>
    {
        #region Methods

        /// <summary>
        /// The render view.
        /// </summary>
        /// <param name="output">The output.</param>
        protected override void RenderView([NotNull] HtmlTextWriter output)
        {
            string value = this.ResolveGoogleSiteVerificationValue();
            output.Write(value);
        }

        /// <summary>
        /// Resolves the google site verification value.
        /// </summary>
        /// <returns>The Google Site verification</returns>
        [NotNull]
        protected string ResolveGoogleSiteVerificationValue()
        {
            if (this.Model.HasChildren) // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var hostname = this.Context.Request.Url;
                var collection = this.Model.Children.As<IGoogleSiteVerificationCode>(this.Language);
                foreach (IGoogleSiteVerificationCode item in collection)
                {
                    if (item.MatchUriHost(hostname))
                    {
                        string html = item.Code.Value;
                        if (!string.IsNullOrWhiteSpace(html))
                        {
                            /* <meta name="google-site-verification" content="FYwKwI6_j9-pO4FnrN4TU5IiDQu3Uv0fhe4OziCIvm0" /> */
                            return new Meta { Name = "google-site-verification", Content = html.Trim() };
                        }
                    }
                }
            }

            string googleCode = this.Model.Code.Value;
            if (!string.IsNullOrWhiteSpace(googleCode))
            {
                /* <meta name="google-site-verification" content="FYwKwI6_j9-pO4FnrN4TU5IiDQu3Uv0fhe4OziCIvm0" /> */
                return new Meta { Name = "google-site-verification", Content = googleCode.Trim() };
            }

            return string.Empty;
        }

        #endregion
    }
}