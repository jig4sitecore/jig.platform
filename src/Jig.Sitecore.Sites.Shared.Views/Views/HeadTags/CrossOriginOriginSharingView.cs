﻿namespace Jig.Sitecore.Sites.Shared.Views
{
    using System;
    using System.Collections.Generic;
    using Jig.Sitecore.Sites.Shared.Data.Folders;
    using Jig.Sitecore.Sites.Shared.Data.System;
    using Jig.Sitecore.Sites.Shared.Data.System.Interfaces;
    using global::Sitecore;

    /// <summary>
    /// Class CrossOriginOriginSharingView.
    /// </summary>
    public class CrossOriginOriginSharingView : Jig.Sitecore.Views.WebForms.WebControlView<ICorsHeaderFolder>
    {
        #region Methods

        /// <summary>
        /// Adds the cross origin origin sharing headers.
        /// </summary>
        /// <param name="headers">The headers.</param>
        protected void AddCrossOriginOriginSharingHeaders([NotNull] ICorsHeaders headers)
        {
            if (headers.AccessControlAllowOrigin.HasValue)
            {
                this.Context.Response.Headers["Access-Control-Allow-Origin"] = headers.AccessControlAllowOrigin.Value;
            }

            if (headers.AccessControlAllowMethods.HasValue)
            {
                this.Context.Response.Headers["Access-Control-Allow-Methods"] = headers.AccessControlAllowMethods.Value;
            }

            if (headers.AccessControlAllowHeaders.HasValue)
            {
                this.Context.Response.Headers["Access-Control-Allow-Headers"] = headers.AccessControlAllowHeaders.Value;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load" /> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (this.Model == null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                // ReSharper disable HeuristicUnreachableCode
            {
                return;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            if (this.Model.HasChildren)
            {
                var collection = this.Model.Children.As<ICorsHeader>();
                ICorsHeaders headerItem = this.ResolveCrossOriginOriginSharingItem(collection);
                if (headerItem != null)
                {
                    this.AddCrossOriginOriginSharingHeaders(headerItem);
                    return;
                }
            }

            ICorsHeaders headers = this.Model;
            this.AddCrossOriginOriginSharingHeaders(headers);

            /* This is not suppose to be editable on page. Its global settings  */
            this.Visible = global::Sitecore.Context.PageMode.IsNormal;
        }

        /// <summary>
        /// Resolves the cross origin origin sharing item.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns>Cross Origin Origin Sharing item.</returns>
        [CanBeNull]
        protected ICorsHeader ResolveCrossOriginOriginSharingItem([NotNull] IEnumerable<ICorsHeader> collection)
        {
            var hostname = this.Context.Request.Url;
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (ICorsHeader item in collection)
                // ReSharper restore LoopCanBeConvertedToQuery
            {
                if (item.MatchUriHost(hostname))
                {
                    return item;
                }
            }

            return null;
        }

        #endregion
    }
}