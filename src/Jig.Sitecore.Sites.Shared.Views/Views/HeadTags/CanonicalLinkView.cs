﻿namespace Jig.Sitecore.Sites.Shared.Views
{
    using System;
    using System.Web.UI;
    using Jig.Sitecore.Sites.Shared.Data.Pages;
    using Jig.Web.Html;
    using global::Sitecore.Links;

    /// <summary>
    /// The canonical link view.
    /// </summary>
    [ToolboxData("<{0}:CanonicalLinkView runat=\"server\"></{0}:CanonicalLinkView>")]
    public partial class CanonicalLinkView : Control
    {
        #region Methods

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            /* This approach allows to overrided/modified the link anywhere until "Render" is called */
            var contentPage = global::Sitecore.Context.Item.As<IContentPage>();
            if (contentPage != null)
            {
                var site = global::Sitecore.Context.Site;
                var language = contentPage.Language;

                var options = site.GetDefaultUrlOptions();
                options.Language = language;

                var href = LinkManager.Provider.GetItemUrl(contentPage.InnerItem, options);
                if (!string.IsNullOrWhiteSpace(href))
                {
                    using (var contextItemTags = this.Context.GetHtmlHeadTags())
                    {
                        contextItemTags.AddRange(new CanonicalLinkTag(href));
                    }
                }
            }

            this.Visible = global::Sitecore.Context.PageMode.IsNormal;
        }

        #endregion
    }
}