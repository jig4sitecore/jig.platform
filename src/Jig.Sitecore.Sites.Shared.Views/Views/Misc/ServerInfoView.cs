﻿namespace Jig.Sitecore.Sites.Shared.Views
{
    using System.Web.UI;

    /// <summary>
    /// Class ServerInfoView. This class cannot be inherited.
    /// </summary>
     [ToolboxData("<{0}:ServerInfoView runat=\"server\"></{0}:ServerInfoView>")] 
    public sealed class ServerInfoView : System.Web.UI.Control
    {
        /// <summary>
        /// Outputs server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter" /> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> object that receives the control content.</param>
        public override void RenderControl(HtmlTextWriter writer)
        {
            writer.WriteLineNoTabs("<!-- BEGIN: ServerInfoView -->");
            writer.WriteLineNoTabs(string.Format("    <!-- Server IP:  {0} -->", this.Context.Request.ServerVariables["LOCAL_ADDR"]));
            writer.WriteLineNoTabs(string.Format("    <!-- Client IP:  {0} -->", this.Context.Request.UserHostAddress));
            writer.WriteLineNoTabs(string.Format("    <!-- Server:  {0} -->", this.Context.Server.MachineName));
            writer.WriteLineNoTabs(string.Format("    <!-- Lang:  {0} -->", global::Sitecore.Context.Language.Name));
            writer.WriteLineNoTabs("<!-- END: ServerInfoView -->");
        }
    }
}