﻿namespace Jig.Sitecore.Sites.Shared.Views
{
    using System;
    using System.Web.UI;

    using Jig.Sitecore.Sites.Shared.Data.Widgets;

    using Jig.Web.Html;
    using global::Sitecore;

    /// <summary>
    /// The custom error view.
    /// </summary>
    [ToolboxData("<{0}:CustomErrorView runat=\"server\"></{0}:CustomErrorView>")]
    public partial class CustomErrorView : Jig.Sitecore.Views.WebForms.WebControlView<ICustomErrors>
    {
        #region Methods

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load" /> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Context.Response.TrySkipIisCustomErrors = true;
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (this.Model != null && this.Model.StatusCode.HasValue)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                int number;
                if (int.TryParse(this.Model.StatusCode, out number) && number > 200)
                {
                    this.Context.Response.StatusCode = number;
                }
                else
                {
                    global::Sitecore.Diagnostics.Log.Warn("Couldn't parse Status Code value: " + this.Model.StatusCode, this);

                    this.Context.Response.StatusCode = 404;
                }
            }
        }

        /// <summary>
        /// The render view.
        /// </summary>
        /// <param name="output">
        /// The output.
        /// </param>
        protected override void RenderView([NotNull] HtmlTextWriter output)
        {
            var customErrors = this.Model;
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (customErrors != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var msg = customErrors.Message;

                /* The additional H4 attributes like css class or data-attributes could be passed as Sitecore parameters in CMS  */
                var attributes = this.GetParametersAsXAttributes();

                using (output.RenderDiv(cssClass: "page-custom-error", attributes: attributes.Values))
                {
                    output.WriteLine(msg);
                }
            }
        }

        #endregion
    }
}