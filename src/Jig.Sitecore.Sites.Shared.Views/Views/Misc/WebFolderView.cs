﻿namespace Jig.Sitecore.Sites.Shared.Views
{
    using System;
    using System.Linq;
    using System.Web.UI;
    using Jig.Sitecore.Sites.Shared.Data.Folders;

    /// <summary>
    /// The Web Folder (redirect to child page).
    /// </summary>
    /// <remarks>The purpose of this control is to handle situation for the "/{page}/{web folder}/" request situations. The control will redirect to select page or first child</remarks>
    [ToolboxData("<{0}:WebFolderView runat=\"server\"></{0}:WebFolderView>")]
    public partial class WebFolderView : Control
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WebFolderView"/> class.
        /// </summary>
        public WebFolderView()
        {
            this.DefaultRedirectUri = "/";
        }

        /// <summary>
        /// Gets or sets the redirect URI.
        /// </summary>
        /// <value>The redirect URI.</value>
        public string DefaultRedirectUri { get; set; }

        #region Methods

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">
        /// The <see cref="T:System.EventArgs"/> object that contains the event data.
        /// </param>
        protected override void OnLoad(EventArgs e)
        {
            var folder = global::Sitecore.Context.Item.As<IWebFolder>();
            if (folder != null)
            {
                this.DefaultRedirectUri = this.ResolveDefaultRedirectUri(folder);
            }

            this.Context.Response.Redirect(this.DefaultRedirectUri, false);
        }

        /// <summary>
        /// Resolves the default redirect URI.
        /// </summary>
        /// <param name="folder">The folder.</param>
        /// <returns>The default uri</returns>
        protected string ResolveDefaultRedirectUri(IWebFolder folder)
        {
            var targetItem = folder.Link.TargetItem;
            if (targetItem != null && targetItem.HasLayout())
            {
                var uri = targetItem.GetUrl();
                return uri;
            }

            var item = folder.GetChildren().FirstOrDefault(x => x.HasLayout());
            if (item != null)
            {
                var uri = item.GetUrl();
                return uri;
            }

            return this.DefaultRedirectUri;
        }

        #endregion
    }
}