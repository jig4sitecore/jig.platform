﻿namespace Jig.Sitecore.Rules.Processors
{
    using global::Sitecore;

    using global::Sitecore.Configuration;

    using global::Sitecore.Data;

    using global::Sitecore.Pipelines.Upload;

    /// <summary>
    /// The ui upload run rules processor.
    /// </summary>
    public class UiUploadRunRulesProcessor : global::Jig.Sitecore.Rules.Processors.IUiUpload
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the rule folder id.
        /// </summary>
        [CanBeNull]
        [UsedImplicitly]
        public string RuleFolderId { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Processes the specified upload arguments.
        /// </summary>
        /// <param name="args">
        /// The arguments.
        /// </param>
        public void Process([NotNull] UploadArgs args)
        {
            if (args.Aborted || args.ProcessorItem == null)
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(this.RuleFolderId))
            {
                return;
            }

            if (!Settings.Rules.ItemEventHandlers.RulesSupported(args.ProcessorItem.Database))
            {
                return;
            }

            ID id;
            if (ID.TryParse(this.RuleFolderId, out id))
            {
                RuleManager.RunRules(args, id);
            }
        }

        #endregion
    }
}