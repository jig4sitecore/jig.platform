﻿namespace Jig.Sitecore.Rules.Pipelines
{
    using global::Sitecore;
    using global::Sitecore.Diagnostics;

    using global::Sitecore.Pipelines.Upload;

    /// <summary>
    /// The ui upload rule context.
    /// </summary>
    public sealed class UiUploadRuleContext : global::Sitecore.Rules.RuleContext, global::Jig.Sitecore.Rules.IPipelineArgsRuleContext<UploadArgs>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UiUploadRuleContext"/> class.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public UiUploadRuleContext([NotNull] UploadArgs args)
        {
            Assert.IsNotNull(args, "args");
            this.Args = args;
            this.Item = args.ProcessorItem.InnerItem;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the args.
        /// </summary>
        [NotNull]
        public UploadArgs Args { get; private set; }

        #endregion
    }
}