﻿namespace Jig.Sitecore.Rules
{
    using System;

    using Jig.Sitecore.Rules.Pipelines;

    using global::Sitecore;

    using global::Sitecore.Configuration;

    using global::Sitecore.Data;

    using global::Sitecore.Data.Items;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Pipelines.ResolveRenderingDatasource;

    using global::Sitecore.Pipelines.Upload;

    using global::Sitecore.Rules;

    using global::Sitecore.SecurityModel;

    /// <summary>
    /// The rule manager.
    /// </summary>
    public static partial class RuleManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// The run rules.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="rulesFolder">The rules folder.</param>
        public static void RunRules([NotNull] Item item, [NotNull] ID rulesFolder)
        {
            Assert.ArgumentNotNull(item, "item");
            Assert.ArgumentNotNull(rulesFolder, "rulesFolder");
            try
            {
                if (!Settings.Rules.ItemEventHandlers.RulesSupported(item.Database))
                {
                    return;
                }

                Item parentItem;
                using (new SecurityDisabler())
                {
                    parentItem = item.Database.GetItem(rulesFolder);
                    if (parentItem == null)
                    {
                        return;
                    }
                }

                RuleList<RuleContext> rules = RuleFactory.GetRules<RuleContext>(parentItem, "Rule");
                if (rules == null || rules.Count == 0)
                {
                    return;
                }

                var ruleContext = new RuleContext
                {
                    Item = item
                };

                rules.Run(ruleContext);
            }
            catch (Exception exception)
            {
                global::Sitecore.Diagnostics.Log.Error(exception.Message, exception, typeof(RuleManager));
            }
        }

        /// <summary>
        /// The run rules.
        /// </summary>
        /// <param name="args">The args.</param>
        /// <param name="rulesFolder">The rules folder.</param>
        public static void RunRules(
            [NotNull] ResolveRenderingDatasourceArgs args,
            [NotNull] ID rulesFolder)
        {
            Assert.ArgumentNotNull(args, "args");
            Assert.ArgumentNotNull(rulesFolder, "rulesFolder");

            try
            {
                if (!Settings.Rules.ItemEventHandlers.RulesSupported(args.ProcessorItem.Database))
                {
                    return;
                }

                Item parentItem;
                using (new SecurityDisabler())
                {
                    parentItem = args.ProcessorItem.Database.GetItem(rulesFolder);
                    if (parentItem == null)
                    {
                        return;
                    }
                }

                var rules = RuleFactory.GetRules<ResolveRenderingDatasourceRuleContext>(parentItem, "Rule");
                if (rules == null || rules.Count == 0)
                {
                    return;
                }

                var ruleContext = new ResolveRenderingDatasourceRuleContext(args);

                rules.Run(ruleContext);
            }
            catch (Exception exception)
            {
                global::Sitecore.Diagnostics.Log.Error(exception.Message, exception, typeof(RuleManager));
            }
        }

        /// <summary>
        /// The run rules.
        /// </summary>
        /// <param name="args">The args.</param>
        /// <param name="rulesFolder">The rules folder.</param>
        public static void RunRules(
            [NotNull] UploadArgs args,
            [NotNull] ID rulesFolder)
        {
            Assert.ArgumentNotNull(args, "args");
            Assert.ArgumentNotNull(rulesFolder, "rulesFolder");

            try
            {
                if (!Settings.Rules.ItemEventHandlers.RulesSupported(args.ProcessorItem.Database))
                {
                    return;
                }

                Item parentItem;
                using (new SecurityDisabler())
                {
                    parentItem = args.ProcessorItem.Database.GetItem(rulesFolder);
                    if (parentItem == null)
                    {
                        return;
                    }
                }

                var rules = RuleFactory.GetRules<UiUploadRuleContext>(parentItem, "Rule");
                if (rules == null || rules.Count == 0)
                {
                    return;
                }

                var ruleContext = new UiUploadRuleContext(args);

                rules.Run(ruleContext);
            }
            catch (Exception exception)
            {
                global::Sitecore.Diagnostics.Log.Error(exception.Message, exception, typeof(RuleManager));
            }
        }

        #endregion
    }
}