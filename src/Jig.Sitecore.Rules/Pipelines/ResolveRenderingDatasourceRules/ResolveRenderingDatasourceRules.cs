﻿namespace Jig.Sitecore.Rules.Pipelines
{
    using Jig.Sitecore.Pipelines;

    using global::Sitecore;

    using global::Sitecore.Configuration;

    using global::Sitecore.Data;
    using global::Sitecore.Pipelines.ResolveRenderingDatasource;

    /// <summary>
    /// The pipeline ResolveRenderingDatasource extension
    /// </summary>
    [System.Runtime.InteropServices.GuidAttribute("D743348C-08F0-4508-9CDD-59B498CDAC46")]
    public sealed class ResolveRenderingDatasourceRules : IResolveRenderingDatasource
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the rule folder id.
        /// </summary>
        [UsedImplicitly]
        [CanBeNull]
        public string RuleFolderId { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The process.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public void Process(ResolveRenderingDatasourceArgs args)
        {
            if (args.ProcessorItem == null
                || !Settings.Rules.ItemEventHandlers.RulesSupported(args.ProcessorItem.Database))
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(this.RuleFolderId))
            {
                return;
            }

            ID id;
            if (ID.TryParse(this.RuleFolderId, out id))
            {
                RuleManager.RunRules(args, id);
            }
        }

        #endregion
    }
}