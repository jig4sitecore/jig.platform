﻿namespace Jig.Sitecore.Rules.Pipelines
{
    using global::Sitecore;

    using global::Sitecore.Pipelines.ResolveRenderingDatasource;

    /// <summary>
    /// The resolve rendering datasource.
    /// </summary>
    public sealed class ResolveRenderingDatasourceRuleContext : PipelineArgsRuleContext<ResolveRenderingDatasourceArgs>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ResolveRenderingDatasourceRuleContext"/> class.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public ResolveRenderingDatasourceRuleContext([NotNull] ResolveRenderingDatasourceArgs args)
            : base(args)
        {
        }

        #endregion
    }
}