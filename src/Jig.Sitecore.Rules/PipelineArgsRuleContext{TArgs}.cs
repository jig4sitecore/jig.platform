﻿namespace Jig.Sitecore.Rules
{
    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Rules;

    /// <summary>
    /// The pipeline args rule context.
    /// </summary>
    /// <typeparam name="TArgs">The type of the arguments.</typeparam>
    public abstract class PipelineArgsRuleContext<TArgs> : RuleContext, IPipelineArgsRuleContext<TArgs>
        where TArgs : global::Sitecore.Pipelines.PipelineArgs
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineArgsRuleContext{TArgs}"/> class.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected PipelineArgsRuleContext([NotNull] TArgs args)
        {
            Assert.IsNotNull(args, "args");
            this.Args = args;
            this.Item = args.ProcessorItem.InnerItem;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the args.
        /// </summary>
        [NotNull]
        public TArgs Args { get; protected set; }

        #endregion
    }
}