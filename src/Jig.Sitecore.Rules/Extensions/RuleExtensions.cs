﻿namespace Jig.Sitecore
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using global::Sitecore;
    using global::Sitecore.Configuration;
    using global::Sitecore.Data.Events;

    using global::Sitecore.Data.Items;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.SecurityModel;

    /// <summary>
    /// The rule extensions.
    /// </summary>
    internal static class RuleExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Rename the item, unless it is a standard values item
        /// or the start item for any of the managed Web sites.
        /// </summary>
        /// <param name="item">The item to rename.</param>
        /// <param name="newName">The new name for the item.</param>
        public static void RenameItem([NotNull] this Item item, [NotNull] string newName)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (item == null || string.IsNullOrWhiteSpace(newName))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return;
            }

            try
            {
                if (item.Template.StandardValues != null && item.ID == item.Template.StandardValues.ID)
                {
                    return;
                }

                if (
                    Factory.GetSiteInfoList()
                        .Any(
                            site =>
                            string.Compare(
                                site.RootPath + site.StartItem,
                                item.Paths.FullPath,
                                StringComparison.OrdinalIgnoreCase) == 0))
                {
                    return;
                }

                var proposedNameBuilder = new StringBuilder(newName.Length);
                foreach (char letter in newName.Trim(' ', '_', ',', '.', ';', '|'))
                {
                    proposedNameBuilder.Append(char.IsLetterOrDigit(letter) ? letter : ' ');
                }

                proposedNameBuilder.Replace("   ", " ");
                proposedNameBuilder.Replace("  ", " ");

                var proposedValidName = newName.Trim();

                if (!ItemUtil.IsItemNameValid(proposedValidName))
                {
                    proposedValidName = ItemUtil.ProposeValidItemName(proposedValidName);
                }

                using (new SecurityDisabler())
                {
                    using (new EditContext(item))
                    {
                        using (new EventDisabler())
                        {
                            var displayName = item.Appearance.DisplayName;

                            item.Appearance.DisplayName = string.IsNullOrWhiteSpace(displayName)
                                                              ? proposedValidName.GetProposeValidItemDisplayName()
                                                              : displayName;

                            item.Name = proposedValidName;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error("RenameItem failed.", exception);
                Trace.TraceError(exception.ToString());
            }
        }

        /// <summary>
        /// Gets the display name.
        /// </summary>
        /// <param name="newName">The new name.</param>
        /// <returns>The proposed display name</returns>
        [NotNull]
        public static string GetProposeValidItemDisplayName([NotNull] this string newName)
        {
            if (string.IsNullOrWhiteSpace(newName))
            {
                return string.Empty;
            }

            var tokens = newName.Split(new[] { ' ', '_', '-', '.' }, StringSplitOptions.RemoveEmptyEntries);
            var nameBuilder = new StringBuilder(newName.Length);
            foreach (var token in tokens)
            {
                char letter = token[0];
                char.ToUpperInvariant(letter);
                nameBuilder.Append(letter);
                if (token.Length > 1)
                {
                    string part = token.Substring(1);
                    nameBuilder.Append(part);
                }

                nameBuilder.Append(' ');
            }

            return nameBuilder.ToString().Trim();
        }

        #endregion
    }
}