﻿namespace Jig.Sitecore.Rules.Conditions
{
    using System;
    using System.Runtime.InteropServices;
    using System.Text.RegularExpressions;
    using System.Web;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Rules.Conditions;

    /// <summary>
    /// Class QueryStringCondition. This class cannot be inherited.
    /// </summary>
    /// <typeparam name="TRuleContext">The type of the t rule context.</typeparam>
    [UsedImplicitly]
    [Guid("C830E05D-8CBF-47B3-B607-E5EA69F715DC")]
    public sealed class QueryStringCondition<TRuleContext> : StringOperatorCondition<TRuleContext>
        where TRuleContext : global::Sitecore.Rules.RuleContext
    {
        // ReSharper disable NotNullMemberIsNotInitialized

        /// <summary>
        /// Gets or sets the name of the query string.
        /// </summary>
        /// <value>The name of the query string.</value>
        [UsedImplicitly]
        [NotNull]
        public string QueryStringName { get; set; }

        /// <summary>
        /// Gets or sets the query string value.
        /// </summary>
        /// <value>The query string value.</value>
        [UsedImplicitly]
        [NotNull]
        public string QueryStringValue { get; set; }
        // ReSharper restore NotNullMemberIsNotInitialized

        /// <summary>
        /// The execute rule.
        /// </summary>
        /// <param name="ruleContext">The rule context.</param>
        /// <returns><c>True</c>, if the condition succeeds, otherwise <c>false</c>.</returns>
        protected override bool ExecuteRule(TRuleContext ruleContext)
        {
            Assert.IsNotNullOrEmpty(this.QueryStringName, "QueryStringName");
            Assert.IsNotNullOrEmpty(this.QueryStringValue, "QueryStringValue");

            var context = HttpContext.Current;
            if (context == null)
            {
                return false;
            }

            var queryStringValue = context.Request.QueryString[this.QueryStringName];

            if (string.IsNullOrWhiteSpace(queryStringValue))
            {
                return false;
            }

            switch (this.GetOperator())
            {
                case StringConditionOperator.Equals:
                    return string.Equals(this.QueryStringValue, queryStringValue);

                case StringConditionOperator.NotEqual:
                    return !string.Equals(this.QueryStringValue, queryStringValue);

                case StringConditionOperator.CaseInsensitivelyEquals:
                    return string.Equals(this.QueryStringValue, queryStringValue, StringComparison.InvariantCultureIgnoreCase);

                case StringConditionOperator.NotCaseInsensitivelyEquals:
                    return string.Equals(this.QueryStringValue, queryStringValue, StringComparison.InvariantCultureIgnoreCase);

                /* Case insensitive Contains implementation for query string */
                case StringConditionOperator.Contains:
                    return queryStringValue.IndexOf(this.QueryStringValue, StringComparison.InvariantCultureIgnoreCase) > 0;

                case StringConditionOperator.StartsWith:
                    return queryStringValue.StartsWith(this.QueryStringValue, StringComparison.InvariantCultureIgnoreCase);

                case StringConditionOperator.EndsWith:

                    return queryStringValue.EndsWith(this.QueryStringValue, StringComparison.InvariantCultureIgnoreCase);

                case StringConditionOperator.MatchesRegularExpression:

                    var regex = new Regex(this.QueryStringValue, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);
                    Match match = regex.Match(queryStringValue);

                    return match.Success;

                default:

                    return false;
            }
        }
    }
}
