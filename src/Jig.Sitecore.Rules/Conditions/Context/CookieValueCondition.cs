﻿namespace Jig.Sitecore.Rules.Conditions
{
    using System;
    using System.Runtime.InteropServices;
    using System.Text.RegularExpressions;
    using System.Web;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Rules;
    using global::Sitecore.Rules.Conditions;

    /// <summary>
    /// Class CookieValueCondition. This class cannot be inherited.
    /// </summary>
    /// <typeparam name="TRuleContext">The type of the t rule context.</typeparam>
    [UsedImplicitly]
    [Guid("1E58B300-94EC-4677-9E54-AEEA3E4D32DE")]
    public sealed class CookieValueCondition<TRuleContext> : StringOperatorCondition<TRuleContext>
        where TRuleContext : RuleContext
    {
        // ReSharper disable NotNullMemberIsNotInitialized

        /// <summary>
        /// Gets or sets the name of the cookie.
        /// </summary>
        /// <value>The name of the cookie.</value>
        [UsedImplicitly]
        [NotNull]
        public string CookieName { get; set; }

        /// <summary>
        /// Gets or sets the cookie value.
        /// </summary>
        /// <value>The cookie value.</value>
        [UsedImplicitly]
        [NotNull]
        public string CookieValue { get; set; }
        // ReSharper restore NotNullMemberIsNotInitialized

        /// <summary>
        /// The execute rule.
        /// </summary>
        /// <param name="ruleContext">The rule context.</param>
        /// <returns><c>True</c>, if the condition succeeds, otherwise <c>false</c>.</returns>
        protected override bool ExecuteRule(TRuleContext ruleContext)
        {
            Assert.IsNotNullOrEmpty(this.CookieName, "CookieName");
            Assert.IsNotNullOrEmpty(this.CookieValue, "CookieValue");

            var context = HttpContext.Current;
            if (context == null)
            {
                return false;
            }

            var cookie = context.Request.Cookies[this.CookieName];
            if (cookie == null || string.IsNullOrWhiteSpace(cookie.Value))
            {
                return false;
            }

            switch (this.GetOperator())
            {
                case StringConditionOperator.Equals:
                    return string.Equals(this.CookieValue, cookie.Value);

                case StringConditionOperator.NotEqual:
                    return !string.Equals(this.CookieValue, cookie.Value);

                case StringConditionOperator.CaseInsensitivelyEquals:
                    return string.Equals(this.CookieValue, cookie.Value, StringComparison.InvariantCultureIgnoreCase);

                case StringConditionOperator.NotCaseInsensitivelyEquals:
                    return string.Equals(this.CookieValue, cookie.Value, StringComparison.InvariantCultureIgnoreCase);

                /* Case insensitive Contains implementation for cookie value */
                case StringConditionOperator.Contains:
                    return cookie.Value.IndexOf(this.CookieValue, StringComparison.InvariantCultureIgnoreCase) > 0;

                case StringConditionOperator.StartsWith:
                    return cookie.Value.StartsWith(this.CookieValue, StringComparison.InvariantCultureIgnoreCase);

                case StringConditionOperator.EndsWith:

                    return cookie.Value.EndsWith(this.CookieValue, StringComparison.InvariantCultureIgnoreCase);

                case StringConditionOperator.MatchesRegularExpression:

                    var regex = new Regex(this.CookieValue, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);
                    Match match = regex.Match(cookie.Value);

                    return match.Success;

                default:

                    return false;
            }
        }
    }
}
