﻿namespace Jig.Sitecore.Rules.Conditions
{
    using System.Linq;
    using System.Runtime.InteropServices;
    using global::Sitecore;
    using global::Sitecore.Rules;

    /// <summary>
    /// Rules engine condition to determine if an item has
    /// layout details for any device.
    /// </summary>
    /// <typeparam name="TRuleContext">Type providing rule context.</typeparam>
    /// <credit>
    /// The original code and idea is from <seealso href="http://marketplace.sitecore.net/en/Modules/Item_Naming_rules.aspx" /></credit>
    [UsedImplicitly]
    [Guid("1AEE16C7-F2C1-4C27-9790-63A4F0E3D2AF")]
    public sealed class HasLayoutDetailsForAnyDevice<TRuleContext> : OperatorCondition<TRuleContext>
        where TRuleContext : RuleContext
    {
        #region Methods

        /// <summary>
        /// Executes the rule.
        /// </summary>
        /// <param name="ruleContext">The rule context.</param>
        /// <returns>True if the item has layout details for any device, otherwise False.</returns>
        protected override bool ExecuteRule([NotNull] TRuleContext ruleContext)
        {
            var devices = ruleContext.Item.Database.Resources.Devices.GetAll();
            var result = devices.Any(compare => ruleContext.Item.Visualization.GetLayout(compare) != null);
            return result;
        }

        #endregion
    }
}