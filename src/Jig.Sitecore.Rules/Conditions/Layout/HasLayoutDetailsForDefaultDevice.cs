﻿namespace Jig.Sitecore.Rules.Conditions
{
    using System.Linq;
    using System.Runtime.InteropServices;
    using global::Sitecore;
    using global::Sitecore.Rules;

    /// <summary>
    /// Rules engine condition to determine if an item has
    /// layout details for the default device.
    /// </summary>
    /// <typeparam name="TRuleContext">Type providing rule context.</typeparam>
    /// <credit>
    /// The original code and idea is from <seealso href="http://marketplace.sitecore.net/en/Modules/Item_Naming_rules.aspx" /></credit>
    [UsedImplicitly]
    [Guid("DA428AF5-7D24-42BF-841E-B9E1A3460DDD")]
    public sealed class HasLayoutDetailsForDefaultDevice<TRuleContext> : OperatorCondition<TRuleContext>
        where TRuleContext : RuleContext
    {
        #region Methods

        #endregion

        /// <summary>
        /// Executes the rule.
        /// </summary>
        /// <param name="ruleContext">The rule context.</param>
        /// <returns>True if the item has layout details for the default device, otherwise False.</returns>
        protected override bool ExecuteRule(TRuleContext ruleContext)
        {
            var devices = ruleContext.Item.Database.Resources.Devices.GetAll();
            var result =
                (from compare in devices
                 where compare.IsDefault
                 select ruleContext.Item.Visualization.GetLayout(compare) != null).FirstOrDefault();

            return result;
        }
    }
}