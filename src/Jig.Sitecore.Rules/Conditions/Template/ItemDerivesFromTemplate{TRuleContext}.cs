﻿namespace Jig.Sitecore.Rules.Conditions
{
    using System.Linq;
    using System.Runtime.InteropServices;
    using global::Sitecore;
    using global::Sitecore.Data;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Rules;

    /// <summary>
    /// Rule that walks the supplied Item's template inheritance hierarchy looking
    /// for a match to the TemplateID provided by the rule context.
    /// </summary>
    /// <typeparam name="TRuleContext">
    /// Instance of Sitecore.Rules.Conditions.RuleContext.
    /// </typeparam>
    [UsedImplicitly]
    [Guid("0E144D1F-E2E7-4E50-A00B-CD7B7DBEED20")]
    public sealed class ItemDerivesFromTemplate<TRuleContext> : WhenCondition<TRuleContext>
        where TRuleContext : RuleContext
    {
        #region Fields

        /// <summary>
        /// The template Id.
        /// </summary>
        private ID templateId;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemDerivesFromTemplate{TRuleContext}"/> class. 
        /// Initializes a new instance of the ItemDerivesFromTemplate class.
        /// </summary>
        public ItemDerivesFromTemplate()
        {
            this.templateId = ID.Null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemDerivesFromTemplate{TRuleContext}"/> class. 
        /// Initializes a new instance of the ItemDerivesFromTemplate class.
        /// </summary>
        /// <param name="templateId">
        /// The template id.
        /// </param>
        public ItemDerivesFromTemplate([NotNull] ID templateId)
        {
            Assert.ArgumentNotNull(templateId, "templateId");
            this.templateId = templateId;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the template id.
        /// </summary>
        [NotNull]
        public ID TemplateId
        {
            get
            {
                return this.templateId;
            }

            set
            {
                Assert.ArgumentNotNull(value, "value");
                this.templateId = value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The execute rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        /// <returns>
        /// <c>True</c>, if the condition succeeds, otherwise <c>false</c>.
        /// </returns>
        protected override bool ExecuteRule(TRuleContext ruleContext)
        {
            var item = ruleContext.Item;
            if (item == null)
            {
                return false;
            }

            if (item.TemplateID == this.TemplateId)
            {
                return true;
            }

            /* Don't go too deep for performance reasons */
            if (
                item.Template.BaseTemplates.Any(
                    x => x.ID == this.TemplateId || x.BaseTemplates.Any(y => y.ID == this.TemplateId)))
            {
                return true;
            }

            return false;
        }

        #endregion
    }
}