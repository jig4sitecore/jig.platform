﻿namespace Jig.Sitecore.Rules.Conditions
{
    using System.Runtime.InteropServices;

    using global::Sitecore;
    using global::Sitecore.Diagnostics;

    /// <summary>
    /// Class DatabaseNameCondition. This class cannot be inherited.
    /// </summary>
    /// <typeparam name="TRuleContext">The type of the rule context.</typeparam>
    [UsedImplicitly]
    [Guid("0F8263E4-84A4-405B-8D9C-D338EE04E5DA")]
    public sealed class ItemDatabaseNameCondition<TRuleContext> : StringOperatorCondition<TRuleContext>
        where TRuleContext : global::Sitecore.Rules.RuleContext
    {
        /// <summary>
        /// Gets or sets the database name to compare.
        /// </summary>
        /// <value>The database name.</value>
        [UsedImplicitly]
        [CanBeNull]
        public string Value { get; set; }

        /// <summary>
        /// The execute rule.
        /// </summary>
        /// <param name="ruleContext">The rule context.</param>
        /// <returns><c>True</c>, if the condition succeeds, otherwise <c>false</c>.</returns>
        protected override bool ExecuteRule(TRuleContext ruleContext)
        {
            if (string.IsNullOrWhiteSpace(this.Value))
            {
                Log.Warn("The item database name to compare is missing in rule ", this);
            }

            var item = ruleContext.Item;
            var result = this.Compare(item.Database.Name, this.Value);

            return result;
        }
    }
}
