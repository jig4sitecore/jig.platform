﻿namespace Jig.Sitecore.Rules.Conditions
{
    using global::Sitecore.Rules;

    /// <summary>
    /// The rule check to have minimum item version for each language
    /// </summary>
    /// <typeparam name="TRuleContext">The type of the rule context.</typeparam>
    public sealed class HasMinimumNumberOfItemVersionsForLanguage<TRuleContext> :
        OperatorCondition<TRuleContext>
        where TRuleContext : RuleContext
    {
        /// <summary>
        /// Backs the MinVersions property.
        /// </summary>
        private int minVersions;

        /// <summary>
        /// The minimum updated days
        /// </summary>
        private int minUpdatedDays;

        /// <summary>
        /// Gets or sets a value that indicates 
        /// the minimum number of versions to retain. 
        /// Set by rule parameters, or defaults to 5. 
        /// </summary>
        public int MinVersions
        {
            get
            {
                return this.minVersions < 5 || this.minVersions > 10 ? 5 : this.minVersions;
            }

            set
            {
                this.minVersions = value;
            }
        }

        /// <summary>
        /// Gets or sets a value that indicates the minimum age of versions to 
        /// remove, in days. Actions will not process Versions updated within this number of days.
        /// </summary>
        public int MinUpdatedDays
        {
            get
            {
                return this.minUpdatedDays > 1 ? this.minUpdatedDays : 1;
            }

            set
            {
                this.minUpdatedDays = value > 1 ? value : 1;
            }
        }

        /// <summary>
        /// The execute rule.
        /// </summary>
        /// <param name="ruleContext">The rule context.</param>
        /// <returns>
        ///   <c>True</c>, if the condition succeeds, otherwise <c>false</c>.
        /// </returns>
        protected override bool ExecuteRule(TRuleContext ruleContext)
        {
            global::Sitecore.Data.Items.Item item = ruleContext.Item.Database.GetItem(
              ruleContext.Item.ID,
              ruleContext.Item.Language);

            if (item == null || item.Versions.Count == 0)
            {
                return false;
            }

            if (item.Versions.Count < this.MinVersions)
            {
                return false;
            }

            // to prevent the while loop from reaching MinVersions,
            // only process this number of items
            int limit = item.Versions.Count - this.MinVersions;
            int i = 0;

            while (item.Versions.Count > this.MinVersions && i < limit)
            {
                global::Sitecore.Data.Items.Item version = item.Versions.GetVersions()[i++];
                global::Sitecore.Diagnostics.Assert.IsNotNull(version, "version");

                if (this.MinUpdatedDays < 1
                  || version.Statistics.Updated.AddDays(this.MinUpdatedDays) < System.DateTime.Now)
                {
                    return true;
                }
            }

            return false;
        }
    }
}