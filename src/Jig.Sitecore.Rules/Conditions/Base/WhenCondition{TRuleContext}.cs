﻿namespace Jig.Sitecore.Rules.Conditions
{
    using System;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;

    /// <summary>
    /// The base class for the rule engine
    /// </summary>
    /// <typeparam name="TRuleContext">
    /// Instance of Sitecore.Rules.Conditions.RuleContext.
    /// </typeparam>
    [UsedImplicitly]
    public abstract class WhenCondition<TRuleContext> : global::Sitecore.Rules.Conditions.WhenCondition<TRuleContext>
        where TRuleContext : global::Sitecore.Rules.RuleContext
    {
        #region Methods

        /// <summary>
        /// Executes the specified rule context.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        /// <returns>
        /// <c>True</c>, if the condition succeeds, otherwise <c>false</c>.
        /// </returns>
        protected sealed override bool Execute([NotNull] TRuleContext ruleContext)
        {
            Assert.IsNotNull(ruleContext, "ruleContext");
            Assert.IsNotNull(ruleContext.Item, "ruleContext.Item");
            if (ruleContext.IsAborted)
            {
                return false;
            }

            try
            {
                Log.Debug("RuleAction " + this.GetType().Name + " started for " + ruleContext.Item.Name, this);

                bool result = this.ExecuteRule(ruleContext);
                return result;
            }
            catch (Exception exception)
            {
                Log.Error("RuleAction " + this.GetType().Name + " failed.", exception, this);
            }

            Log.Debug("RuleAction " + this.GetType().Name + " ended for " + ruleContext.Item.Name, this);

            return false;
        }

        /// <summary>
        /// The execute rule.
        /// </summary>
        /// <param name="ruleContext">The rule context.</param>
        /// <returns>
        /// <c>True</c>, if the condition succeeds, otherwise <c>false</c>.
        /// </returns>
        protected abstract bool ExecuteRule([NotNull] TRuleContext ruleContext);

        #endregion
    }
}