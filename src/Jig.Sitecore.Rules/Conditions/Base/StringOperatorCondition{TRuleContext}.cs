﻿namespace Jig.Sitecore.Rules.Conditions
{
    using System;
    using System.Diagnostics;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;

    /// <summary>
    /// The string operator condition.
    /// </summary>
    /// <typeparam name="TRuleContext">Rule context</typeparam>
    public abstract class StringOperatorCondition<TRuleContext> : global::Sitecore.Rules.Conditions.StringOperatorCondition<TRuleContext>
        where TRuleContext : global::Sitecore.Rules.RuleContext
    {
        /// <summary>
        /// Condition implementation.
        /// </summary>
        /// <param name="ruleContext">The rule context.</param>
        /// <returns>
        /// True if the item has layout details for the default device,
        /// otherwise False.
        /// </returns>
        protected override sealed bool Execute([NotNull] TRuleContext ruleContext)
        {
            Assert.IsNotNull(ruleContext, "ruleContext");
            Assert.IsNotNull(ruleContext.Item, "ruleContext.Item");
            if (ruleContext.IsAborted)
            {
                return false;
            }

            try
            {
                Log.Debug("RuleAction " + this.GetType().Name + " started for " + ruleContext.Item.Name, this);

                bool result = this.ExecuteRule(ruleContext);
                return result;
            }
            catch (Exception exception)
            {
                Log.Error("RuleAction " + this.GetType().Name + " failed.", exception, this);
                Trace.TraceError(exception.ToString());
            }

            Log.Debug("RuleAction " + this.GetType().Name + " ended for " + ruleContext.Item.Name, this);

            return false;
        }

        /// <summary>
        /// The execute rule.
        /// </summary>
        /// <param name="ruleContext">The rule context.</param>
        /// <returns>
        /// <c>True</c>, if the condition succeeds, otherwise <c>false</c>.
        /// </returns>
        protected abstract bool ExecuteRule([NotNull] TRuleContext ruleContext);
    }
}