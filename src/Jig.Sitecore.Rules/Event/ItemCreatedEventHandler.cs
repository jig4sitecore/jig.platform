﻿namespace Jig.Sitecore.Rules
{
    using System;
    using global::Sitecore;
    using global::Sitecore.Configuration;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Events;
    using global::Sitecore.Diagnostics;

    using global::Sitecore.Events;

    /// <summary>
    /// The item event handler.
    /// </summary>
    public sealed class ItemCreatedEventHandler
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the rule folder id.
        /// </summary>
        [UsedImplicitly]
        [CanBeNull]
        public string RuleFolderId { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The args.</param>
        private void RunRules([NotNull] object sender, [NotNull] EventArgs args)
        {
            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull(args, "args");

            if (string.IsNullOrWhiteSpace(this.RuleFolderId))
            {
                return;
            }

            var eventArgs = Event.ExtractParameter(args, 0) as ItemCreatedEventArgs;
            if (eventArgs == null)
            {
                return;
            }

            if (!Settings.Rules.ItemEventHandlers.RulesSupported(eventArgs.Item.Database))
            {
                return;
            }

            ID id;
            if (ID.TryParse(this.RuleFolderId, out id))
            {
                RuleManager.RunRules(eventArgs.Item, id);
            }
        }

        #endregion
    }
}