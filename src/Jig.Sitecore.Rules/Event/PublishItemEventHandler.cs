﻿namespace Jig.Sitecore.Rules
{
    using System;

    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Events;
    using global::Sitecore.Publishing;

    /// <summary>
    /// The item event handler.
    /// </summary>
    public sealed class PublishItemEventHandler
    {
        #region Public Properties

        /* ReSharper disable NotNullMemberIsNotInitialized */

        /// <summary>
        /// Gets or sets the rule folder id.
        /// </summary>
        [NotNull]
        [UsedImplicitly]
        public string RuleFolderId { get; set; }

        /* ReSharper restore NotNullMemberIsNotInitialized */

        #endregion

        #region Methods

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        private void RunRules([NotNull] object sender, [NotNull] EventArgs args)
        {
            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull(args, "args");

            if (string.IsNullOrWhiteSpace(this.RuleFolderId))
            {
                return;
            }

            var publisher = Event.ExtractParameter(args, 0) as Publisher;

            if (publisher != null)
            {
                // TODO: Implement call
            }
        }

        #endregion
    }
}