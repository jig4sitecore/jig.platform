﻿namespace Jig.Sitecore.Rules
{
    using System;
    using global::Sitecore;
    using global::Sitecore.Configuration;
    using global::Sitecore.Data;

    using global::Sitecore.Data.Items;

    using global::Sitecore.Diagnostics;

    using global::Sitecore.Events;

    /// <summary>
    /// The item event handler.
    /// </summary>
    public sealed class ItemEventHandler
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the rule folder id.
        /// </summary>
        [UsedImplicitly]
        [CanBeNull]
        public string RuleFolderId { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The args.</param>
        private void RunRules([NotNull] object sender, [NotNull] EventArgs args)
        {
            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull(args, "args");

            if (string.IsNullOrWhiteSpace(this.RuleFolderId))
            {
                return;
            }

            var item = Event.ExtractParameter(args, 0) as Item;
            if (item == null)
            {
                return;
            }

            if (!Settings.Rules.ItemEventHandlers.RulesSupported(item.Database))
            {
                return;
            }

            ID id;
            if (ID.TryParse(this.RuleFolderId, out id))
            {
                RuleManager.RunRules(item, id);
            }
        }

        #endregion
    }
}