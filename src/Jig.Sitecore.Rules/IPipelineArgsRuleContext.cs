﻿namespace Jig.Sitecore.Rules
{
    using global::Sitecore;

    using global::Sitecore.Data.Items;

    /// <summary>
    /// The PipelineArgsRuleContext interface.
    /// </summary>
    /// <typeparam name="TArgs">The type of the arguments.</typeparam>
    public interface IPipelineArgsRuleContext<out TArgs>
        where TArgs : class
    {
        #region Public Properties

        /// <summary>
        /// Gets the args.
        /// </summary>
        [NotNull]
        TArgs Args { get; }

        /// <summary>
        /// Gets the processor item.
        /// </summary>
        [NotNull]
        Item Item { get; }

        #endregion
    }
}