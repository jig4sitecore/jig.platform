﻿namespace Jig.Sitecore.Rules.Actions
{
    using System.Runtime.InteropServices;

    using global::Sitecore;
    using global::Sitecore.Rules;

    using global::Sitecore.SecurityModel;

    /// <summary>
    /// The set item field value.
    /// </summary>
    /// <typeparam name="TRuleContext">The type of the rule context.</typeparam>
    [UsedImplicitly]
    [Guid("E34375FD-9DB8-4026-B32A-CCDFDA52A4FD")]
    public sealed class SetItemFieldValue<TRuleContext> : Jig.Sitecore.Rules.Actions.RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the name of the field.
        /// </summary>
        /// <value>
        /// The name of the field.
        /// </value>
        [UsedImplicitly]
        [CanBeNull] 
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [UsedImplicitly]
        [CanBeNull] 
        public string Value { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        protected override void ApplyRule([NotNull] TRuleContext ruleContext)
        {
            if (string.IsNullOrWhiteSpace(this.Value))
            {
                return;
            }

            var templateItem = ruleContext.Item;
            string fieldValue = templateItem[this.FieldName];
            if (!string.IsNullOrWhiteSpace(fieldValue) || string.Equals(fieldValue, this.Value))
            {
                return;
            }

            using (new SecurityDisabler())
            {
                templateItem.Editing.BeginEdit();
                templateItem[this.FieldName] = this.Value;
                templateItem.Editing.EndEdit();
            }
        }

        #endregion
    }
}