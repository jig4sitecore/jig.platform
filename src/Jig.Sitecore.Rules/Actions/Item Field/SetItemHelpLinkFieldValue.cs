﻿namespace Jig.Sitecore.Rules.Actions
{
    using System.Runtime.InteropServices;
    using global::Sitecore;
    using global::Sitecore.Rules;
    using global::Sitecore.SecurityModel;

    /// <summary>
    /// The set item field value.
    /// </summary>
    /// <typeparam name="TRuleContext">The type of the rule context.</typeparam>
    [UsedImplicitly]
    [Guid("F807BB28-C3FD-4007-8CD2-13EFB61E3CA3")]
    public sealed class SetItemHelpLinkFieldValue<TRuleContext> : Jig.Sitecore.Rules.Actions.RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the field value.
        /// </summary>
        /// <value>
        /// The field value.
        /// </value>
        [CanBeNull]
        [UsedImplicitly]
        public string FieldValue { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        protected override void ApplyRule([NotNull] TRuleContext ruleContext)
        {
            if (string.IsNullOrWhiteSpace(this.FieldValue))
            {
                return;
            }

            var templateItem = ruleContext.Item;
            if (!string.IsNullOrWhiteSpace(templateItem.Appearance.HelpLink))
            {
                return;
            }

            using (new SecurityDisabler())
            {
                templateItem.Editing.BeginEdit();
                templateItem.Appearance.HelpLink = this.FieldValue;
                templateItem.Editing.EndEdit();
            }
        }

        #endregion
    }
}