﻿namespace Jig.Sitecore.Rules.Actions
{
    using System;
    using System.Globalization;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Text.RegularExpressions;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Rules;

    /// <summary>
    /// Rules engine action to replace invalid characters in item names.
    /// </summary>
    /// <typeparam name="TRuleContext">
    /// Type providing rule context.
    /// </typeparam>
    /// <credit>
    /// The original code and idea is from <seealso href="http://marketplace.sitecore.net/en/Modules/Item_Naming_rules.aspx"/></credit>
    [UsedImplicitly]
    [Guid("A951E10C-1D0E-4024-BA51-61D19C6F64ED")]
    public sealed class EnsureItemNameInvalidCharactersReplaced<TRuleContext> :
        Jig.Sitecore.Rules.Actions.RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EnsureItemNameInvalidCharactersReplaced{TRuleContext}"/> class.
        /// </summary>
        public EnsureItemNameInvalidCharactersReplaced()
        {
            this.ReplaceWith = string.Empty;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the regular expression used to validate each character
        /// in item names.
        /// </summary>
        [CanBeNull]
        public string MatchPattern { get; [UsedImplicitly] set; }

        /// <summary>
        /// Gets or sets the string with which to replace invalid characters
        /// in item names.
        /// </summary>
        [CanBeNull]
        public string ReplaceWith { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        protected override void ApplyRule([NotNull] TRuleContext ruleContext)
        {
            if (string.IsNullOrEmpty(this.MatchPattern) || this.ReplaceWith == null)
            {
                var message = string.Concat("The ", this.GetType().FullName, " property the MatchPattern is null or empty!");
                Log.Warn(message, this);
                System.Diagnostics.Trace.TraceInformation(message);

                return;
            }

            if (this.ReplaceWith == null)
            {
                var message = string.Concat("The ", this.GetType().FullName, " property the ReplaceWith is null!");
                Log.Warn(message, this);
                System.Diagnostics.Trace.TraceInformation(message);

                return;
            }

            var patternMatcher = new Regex(this.MatchPattern);

            var nameBuilder = new StringBuilder(64);
            foreach (char letter in ruleContext.Item.Name)
            {
                if (patternMatcher.IsMatch(letter.ToString(CultureInfo.InvariantCulture)))
                {
                    nameBuilder.Append(letter);
                }
                else if (!string.IsNullOrEmpty(this.ReplaceWith))
                {
                    nameBuilder.Append(this.ReplaceWith);
                }
            }

            string newName = nameBuilder.ToString();
            if (string.Equals(newName, ruleContext.Item.Name))
            {
                return;
            }

            while (newName.StartsWith(this.ReplaceWith))
            {
                newName = newName.Substring(this.ReplaceWith.Length, newName.Length - this.ReplaceWith.Length);
            }

            while (newName.EndsWith(this.ReplaceWith))
            {
                newName = newName.Substring(0, newName.Length - this.ReplaceWith.Length);
            }

            string sequence = this.ReplaceWith + this.ReplaceWith;

            while (newName.Contains(sequence))
            {
                newName = newName.Replace(sequence, this.ReplaceWith);
            }

            /* Remarks this edge case. It should never be executed in real environment */
            if (string.IsNullOrEmpty(newName))
            {
                string dateTime = global::Sitecore.DateUtil.ToIsoDate(DateTime.Now).ToLowerInvariant();
                newName = "item-" + dateTime;
            }

            if (ruleContext.Item.Name != newName)
            {
                ruleContext.Item.RenameItem(newName);
            }
        }

        #endregion
    }
}