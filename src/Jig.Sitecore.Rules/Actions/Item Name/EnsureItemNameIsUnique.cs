﻿namespace Jig.Sitecore.Rules.Actions
{
    using System;
    using System.Runtime.InteropServices;
    using global::Sitecore;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Rules;

    /// <summary>
    /// Rules engine action to ensure unique item names under a parent
    /// by replacing trailing characters with a date/time stamp.
    /// </summary>
    /// <typeparam name="TRuleContext">Type providing rule context.</typeparam>
    /// <credit>
    /// The original code and idea is from <seealso href="http://marketplace.sitecore.net/en/Modules/Item_Naming_rules.aspx" /></credit>
    [UsedImplicitly]
    [Guid("3E76C52C-133D-40F3-8B38-421F56E1019A")]
    public sealed class EnsureItemNameIsUnique<TRuleContext> : 
        Jig.Sitecore.Rules.Actions.RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        #region Methods

        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">The rule context.</param>
        protected override void ApplyRule([NotNull] TRuleContext ruleContext)
        {
            bool unique;

            do
            {
                unique = true;

                foreach (Item child
                    in ruleContext.Item.Parent.Children)
                {
                    if (child.ID.Equals(ruleContext.Item.ID) || !child.Key.Equals(ruleContext.Item.Key))
                    {
                        continue;
                    }

                    unique = false;
                    string dateTime = global::Sitecore.DateUtil.ToIsoDate(DateTime.Now).ToLowerInvariant();
                    string newName = ruleContext.Item.Name + "-" + dateTime;

                    ruleContext.Item.RenameItem(newName);
                    break;
                }
            }
            while (!unique);
        }

        #endregion
    }
}