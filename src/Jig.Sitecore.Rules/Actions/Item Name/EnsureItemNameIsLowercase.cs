﻿namespace Jig.Sitecore.Rules.Actions
{
    using System.Runtime.InteropServices;
    using global::Sitecore;
    using global::Sitecore.Rules;

    /// <summary>
    /// Rules engine action to lowercase item names.
    /// </summary>
    /// <typeparam name="TRuleContext">
    /// Type providing rule context.
    /// </typeparam>
    /// <credit>
    /// The original code and idea is from <seealso href="http://marketplace.sitecore.net/en/Modules/Item_Naming_rules.aspx"/></credit>
    [UsedImplicitly]
    [Guid("94D909C0-934D-4E66-905E-6604615F9800")]
    public sealed class EnsureItemNameIsLowercase<TRuleContext> :
        Jig.Sitecore.Rules.Actions.RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        #region Methods

        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        protected override void ApplyRule(TRuleContext ruleContext)
        {
            string newName = ruleContext.Item.Name.ToLowerInvariant();

            if (ruleContext.Item.Name != newName)
            {
                ruleContext.Item.RenameItem(newName);
            }
        }

        #endregion
    }
}