﻿namespace Jig.Sitecore.Rules.Actions
{
    using System.Runtime.InteropServices;
    using global::Sitecore;
    using global::Sitecore.Rules;

    /// <summary>
    /// Rules engine action to ensure item names ends with.
    /// </summary>
    /// <typeparam name="TRuleContext">Type providing rule context.</typeparam>
    [UsedImplicitly]
    [Guid("2A066BD4-9D3C-4799-B324-7D00B4CEF7DB")]
    public sealed class EnsureItemNameEndsWith<TRuleContext> :
        RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        /// <summary>
        /// Gets or sets the suffix.
        /// </summary>
        /// <value>The suffix.</value>
        [CanBeNull]
        public string Suffix { get; [UsedImplicitly] set; }

        #region Methods

        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">The rule context.</param>
        protected override void ApplyRule([NotNull] TRuleContext ruleContext)
        {
            if (!string.IsNullOrWhiteSpace(this.Suffix))
            {
                if (!ruleContext.Item.Name.EndsWith(this.Suffix))
                {
                    string newName = ruleContext.Item.Name + this.Suffix;
                    ruleContext.Item.RenameItem(newName);
                }
            }
        }

        #endregion
    }
}