﻿namespace Jig.Sitecore.Rules.Actions
{
    using System;
    using System.Diagnostics;
    using global::Sitecore;
    using global::Sitecore.Diagnostics;

    /// <summary>
    /// The rule action.
    /// </summary>
    /// <typeparam name="TRuleContext">The type of the rule context.</typeparam>
    [UsedImplicitly]
    public abstract class RuleAction<TRuleContext> : global::Sitecore.Rules.Actions.RuleAction<TRuleContext>
        where TRuleContext : global::Sitecore.Rules.RuleContext
    {
        #region Public Methods and Operators

        /// <summary>
        /// Action implementation.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        public override sealed void Apply([NotNull] TRuleContext ruleContext)
        {
            Assert.IsNotNull(ruleContext, "ruleContext");
            Assert.IsNotNull(ruleContext.Item, "ruleContext.Item");
            if (ruleContext.IsAborted)
            {
                return;
            }

            try
            {
                var msg = "RuleAction " + this.GetType().Name + " started for " + ruleContext.Item.Name;

                System.Diagnostics.Trace.TraceInformation(msg);

                this.ApplyRule(ruleContext);
            }
            catch (Exception exception)
            {
                var msg = "RuleAction " + this.GetType().Name + " failed.";
                Log.Error(msg, exception, this);
                Trace.TraceError(msg);
            }

            var message = "RuleAction " + this.GetType().Name + " ended for " + ruleContext.Item.Name;

            System.Diagnostics.Trace.TraceInformation(message);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        protected abstract void ApplyRule([NotNull] TRuleContext ruleContext);

        #endregion
    }
}