﻿namespace Jig.Sitecore.Rules.Actions
{
    using System.Globalization;

    using global::Sitecore;

    using global::Sitecore.Data;

    using global::Sitecore.Data.Items;

    using global::Sitecore.Rules;

    using global::Sitecore.SecurityModel;

    /// <summary>
    /// Rules action to create Alphabetical hierarchy.
    /// </summary>
    /// <typeparam name="TRuleContext">
    /// Rules Context.
    /// </typeparam>
    /// <see href="https://github.com/sitecorerick/constellation.sitecore/blob/master/Rules/Actions/MoveToDateFolder.cs"/>
    /// <code>Move Item to appropriate alphabetical folder. Using [FolderTemplateId,Template,,this template]</code>
    /// <remarks>The Item must implement IsFiledAlphabetically template</remarks>
    [System.Runtime.InteropServices.GuidAttribute("AD88553C-2C0F-4F0B-A06B-0B1E7199D916")]
    public sealed class MoveToAlphabeticalFolder<TRuleContext> : RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the Folder template.
        /// </summary>
        [CanBeNull]
        public string FolderTemplateId { get; [UsedImplicitly] set; }

        #endregion

        #region Methods

        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        protected override void ApplyRule([NotNull] TRuleContext ruleContext)
        {
            if (string.IsNullOrWhiteSpace(this.FolderTemplateId))
            {
                return;
            }

            var item = ruleContext.Item;
            if (item.Parent == null)
            {
                return;
            }

            ID templateId;
            if (!ID.TryParse(this.FolderTemplateId, out templateId))
            {
                return;
            }

            if (ruleContext.Item.TemplateID == templateId)
            {
                return;
            }

            char firstLetter = char.ToUpperInvariant(item.Name[0]);

            if (char.ToUpperInvariant(item.Parent.Name[0]).Equals(firstLetter) && item.Parent.TemplateID == templateId)
            {
                return;
            }

            using (new SecurityDisabler())
            {
                var rootFolder = this.GetRootItem(item);
                var alphaFolder = rootFolder.FindOrCreateChildItem(firstLetter.ToString(CultureInfo.InvariantCulture), new ID(this.FolderTemplateId));

                item.MoveTo(alphaFolder);
            }
        }

        /// <summary>
        /// Gets the root item for a given item.
        /// </summary>
        /// <param name="context">The context item.</param>
        /// <returns>
        /// The root item<see cref="Item" />.
        /// </returns>
        [NotNull]
        private Item GetRootItem([NotNull] Item context)
        {
            if (context.Parent.TemplateID != new ID(this.FolderTemplateId))
            {
                return context.Parent;
            }

            return this.GetRootItem(context.Parent);
        }

        #endregion
    }
}