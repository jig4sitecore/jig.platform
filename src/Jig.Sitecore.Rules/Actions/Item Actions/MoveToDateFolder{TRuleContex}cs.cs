﻿namespace Jig.Sitecore.Rules.Actions
{
    using System.Globalization;

    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Fields;

    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Rules;

    using global::Sitecore.SecurityModel;

    /// <summary>
    /// Rules action to create Alphabetical hierarchy.
    /// </summary>
    /// <typeparam name="TRuleContext">
    /// Rules Context.
    /// </typeparam>
    /// <see href="https://github.com/sitecorerick/constellation.sitecore/blob/master/Rules/Actions/MoveToDateFolder.cs"/>
    /// <code>
    /// Move Item to appropriate date folder. Using [FolderTemplateId,Template,,this template]. Folder depth is [FolderDepth,,,OnlyYear YearAndMonth or YearAndMonthAndDay]
    /// </code> 
    [System.Runtime.InteropServices.GuidAttribute("5E19448F-5F58-4900-98DE-6EA355B2E1C0")]
    public sealed class MoveToDateFolder<TRuleContext> : RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        /// <summary>
        /// The date field name
        /// </summary>
        private string dateFieldName = "__Created";

        #region Enums

        /// <summary>
        /// The date sort options.
        /// </summary>
        public enum DateSortOptions
        {
            /// <summary>
            /// The only year.
            /// </summary>
            OnlyYear,

            /// <summary>
            /// The year and month.
            /// </summary>
            YearAndMonth,

            /// <summary>
            /// The year and month and day.
            /// </summary>
            YearAndMonthAndDay
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the folder depth.
        /// </summary>
        public DateSortOptions FolderDepth { get; [UsedImplicitly] set; }

        /// <summary>
        /// Gets or sets the Folder template.
        /// </summary>
        [NotNull]
        [UsedImplicitly]
        // ReSharper disable NotNullMemberIsNotInitialized
        public ID FolderTemplateId { get; set; }
        // ReSharper restore NotNullMemberIsNotInitialized

        /// <summary>
        /// Gets or sets the date field name.
        /// </summary>
        [NotNull]
        public string DateFieldName
        {
            get
            {
                return this.dateFieldName;
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.dateFieldName = value;
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">The rule context.</param>
        protected override void ApplyRule(TRuleContext ruleContext)
        {
            Assert.IsNotNull(this.FolderTemplateId, "FolderTemplateID");
            Assert.IsNotNullOrEmpty(this.DateFieldName, "DateFieldName");

            var item = ruleContext.Item;
            if (item.TemplateID == this.FolderTemplateId)
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(this.DateFieldName))
            {
                this.DateFieldName = "Release Date";
            }

            DateField field = item.Fields[this.DateFieldName];
            if (field == null || field.DateTime == System.DateTime.MinValue)
            {
                return;
            }

            var theYear = field.DateTime.ToString("yyyy", CultureInfo.InvariantCulture);
            var theMonth = field.DateTime.ToString("MM", CultureInfo.InvariantCulture);
            var theDay = field.DateTime.ToString("dd", CultureInfo.InvariantCulture);

            var folderLevel = this.GetSiteFolder(item);
            var oldFilePath = item.Paths.FullPath;
            var datePath = "/" + theYear;
            if (!oldFilePath.Contains(datePath))
            {
                this.MoveItem(theYear, item, folderLevel);
                oldFilePath = item.Paths.FullPath;
            }

            if (this.FolderDepth != DateSortOptions.OnlyYear)
            {
                datePath = datePath + "/" + theMonth;
                if (!oldFilePath.Contains(datePath))
                {
                    folderLevel = this.AdvanceFolderLevel(folderLevel, item);
                    this.MoveItem(theMonth, item, folderLevel);
                    oldFilePath = item.Paths.FullPath;
                }

                if (this.FolderDepth == DateSortOptions.YearAndMonthAndDay)
                {
                    datePath = datePath + "/" + theDay;
                    if (!oldFilePath.Contains(datePath))
                    {
                        folderLevel = this.AdvanceFolderLevel(folderLevel, item);
                        this.MoveItem(theDay, item, folderLevel);
                    }
                }
            }
        }

        /// <summary>
        /// Advances the folder level.
        /// </summary>
        /// <param name="folder">The folder.</param>
        /// <param name="item">The item.</param>
        /// <returns>
        /// Folder Item.
        /// </returns>
        [NotNull]
        private Item AdvanceFolderLevel([NotNull] Item folder, [NotNull] Item item)
        {
            foreach (Item child in folder.GetChildren())
            {
                if (child.Axes.IsAncestorOf(item))
                {
                    return child;
                }
            }

            return folder;
        }

        /// <summary>
        /// Gets the site folder for the item.
        /// </summary>
        /// <param name="item">The context item.</param>
        /// <returns>The site folder<see cref="Item" /> for the item.</returns>
        [NotNull]
        private Item GetSiteFolder([NotNull] Item item)
        {
            item = item.Parent;
            while (item.TemplateID == this.FolderTemplateId)
            {
                item = item.Parent;
            }

            return item;
        }

        /// <summary>
        /// Moves the item to a new folder.
        /// </summary>
        /// <param name="name">The name of the folder.</param>
        /// <param name="currentItem">The current item.</param>
        /// <param name="folderLevel">The folder Level.</param>
        private void MoveItem([NotNull] string name, [NotNull] Item currentItem, [NotNull] Item folderLevel)
        {
            using (new SecurityDisabler())
            {
                var newFolder = folderLevel.FindOrCreateChildItem(name, this.FolderTemplateId);
                currentItem.MoveTo(newFolder);
            }
        }

        #endregion
    }
}