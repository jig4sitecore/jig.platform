﻿namespace Jig.Sitecore.Rules.Actions
{
    using System;
    using System.Linq;
    using System.Runtime.InteropServices;
    using global::Sitecore;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Rules;
    using global::Sitecore.Text;

    /// <summary>
    /// Rules engine action to convert datasource location path to GUID.
    /// </summary>
    /// <typeparam name="TRuleContext">
    /// Type providing rule context.
    /// </typeparam>
    [UsedImplicitly]
    [Guid("98A06FAF-2398-44BC-8C17-55B0274529AD")]
    public sealed class EnsureDataSourceLocationPathIsConvertedToId<TRuleContext> :
        RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        #region Methods

        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        protected override void ApplyRule(TRuleContext ruleContext)
        {
            Item item = ruleContext.Item;

            if (item == null || !item.Database.Name.Equals("master"))
            {
                return;
            }

            Item existingItem = item.Database.GetItem(item.ID, item.Language, item.Version);
            if (existingItem == null)
            {
                return;
            }

            var field = existingItem.Fields[FieldNames.DatasourceLocation];
            if (field == null)
            {
                return;
            }

            /* 
             * Handle Cases:
             * ./_subcontent/
             * ./_subcontent/|{4E5B0EC7-325A-4D7A-A6C5-09404311C69E} 
             * ./_subcontent/|/sitecore/content/sites/Demo/shared-data
             */
            var oldFieldValue = field.Value;
            if (string.IsNullOrWhiteSpace(oldFieldValue))
            {
                return;
            }

            var newSource = new ListString();

            var sources = oldFieldValue.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray();
            foreach (var path in sources)
            {
                if (string.IsNullOrWhiteSpace(path)
                     || !oldFieldValue.StartsWith("/", StringComparison.InvariantCultureIgnoreCase) /* System Folders */)
                {
                    newSource.Add(path);
                    continue;
                }

                var templateItem = existingItem.Database.GetItem(path);
                if (templateItem == null)
                {
                    /* Could not resolve, lets leave to developer to decide what is wrong or wright */
                    newSource.Add(path);
                    continue;
                }

                newSource.Add(templateItem.ID.ToString());
            }

            var newFieldValue = newSource.ToString();
            if (!string.Equals(oldFieldValue, newFieldValue, StringComparison.InvariantCultureIgnoreCase))
            {
                existingItem.Editing.BeginEdit();
                field.Value = newFieldValue;
                existingItem.Editing.EndEdit();
            }
        }

        /// <summary>
        /// The FieldNames.
        /// </summary>
        public static class FieldNames
        {
            /// <summary>
            /// The datasource template
            /// </summary>
            // ReSharper disable StaticFieldInGenericType
            public static readonly global::Sitecore.Data.ID DatasourceLocation = new global::Sitecore.Data.ID("b5b27af1-25ef-405c-87ce-369b3a004016");
            // ReSharper restore StaticFieldInGenericType
        }

        #endregion
    }
}