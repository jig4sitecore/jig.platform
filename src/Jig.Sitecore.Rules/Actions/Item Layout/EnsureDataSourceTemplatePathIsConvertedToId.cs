﻿namespace Jig.Sitecore.Rules.Actions
{
    using System;
    using System.Runtime.InteropServices;
    using global::Sitecore;
    using global::Sitecore.Data;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Rules;

    /// <summary>
    /// Rules engine action to convert datasource template path to GUID.
    /// </summary>
    /// <typeparam name="TRuleContext">
    /// Type providing rule context.
    /// </typeparam>
    [UsedImplicitly]
    [Guid("74C5E35C-4A3C-452D-8436-87F6613110A6")]
    public sealed class EnsureDataSourceTemplatePathIsConvertedToId<TRuleContext> :
        RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        #region Methods

        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        protected override void ApplyRule(TRuleContext ruleContext)
        {
            Item item = ruleContext.Item;

            if (item == null || !item.Database.Name.Equals("master"))
            {
                return;
            }

            Item existingItem = item.Database.GetItem(item.ID, item.Language, item.Version);
            if (existingItem == null)
            {
                return;
            }
            
            var field = existingItem.Fields[FieldNames.DatasourceTemplate];
            if (field == null)
            {
                return;
            }

            var value = field.Value;
            if (string.IsNullOrWhiteSpace(value) 
                || value.StartsWith("{", StringComparison.InvariantCultureIgnoreCase) /* Guid already */
                || !value.StartsWith("/sitecore/templates/Sites/", StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }

            var templateItem = existingItem.Database.GetItem(value);
            if (templateItem == null)
            {
                return;
            }

            existingItem.Editing.BeginEdit();
            field.Value = templateItem.ID.ToString();
            existingItem.Editing.EndEdit();
        }

        /// <summary>
        /// The FieldNames.
        /// </summary>
        public static class FieldNames
        {
            /// <summary>
            /// The datasource template
            /// </summary>
            // ReSharper disable StaticFieldInGenericType
            public static readonly ID DatasourceTemplate = new ID("1a7c85e5-dc0b-490d-9187-bb1dbcb4c72f");
            // ReSharper restore StaticFieldInGenericType
        }

        #endregion
    }
}