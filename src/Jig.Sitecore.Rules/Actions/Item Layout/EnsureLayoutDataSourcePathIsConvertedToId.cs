﻿namespace Jig.Sitecore.Rules.Actions
{
    using System;
    using System.Runtime.InteropServices;
    using global::Sitecore;
    using global::Sitecore.Data.Fields;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Layouts;
    using global::Sitecore.Rules;

    /// <summary>
    /// Rules engine action to convert datasource path to GUID.
    /// </summary>
    /// <typeparam name="TRuleContext">
    /// Type providing rule context.
    /// </typeparam>
    [UsedImplicitly]
    [Guid("95E66D52-AD6E-4545-9FA6-F4C215C09730")]
    public sealed class EnsureLayoutDataSourcePathIsConvertedToId<TRuleContext> : 
        Jig.Sitecore.Rules.Actions.RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        #region Methods

        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        protected override void ApplyRule(TRuleContext ruleContext)
        {
            Item item = ruleContext.Item;

            if (item == null)
            {
                return;
            }

            var layoutField = new LayoutField(item.Fields[global::Sitecore.FieldIDs.LayoutField]);
            if (!layoutField.InnerField.HasValue)
            {
                return;
            }

            Item existingItem = item.Database.GetItem(item.ID, item.Language, item.Version);
            if (existingItem == null)
            {
                return;
            }

            var oldLayout = new LayoutField(existingItem.Fields[global::Sitecore.FieldIDs.LayoutField]);

            /*
             * NOTE:
             *      Check if control datasource with paths and if old and new values are equal exit
             */
            if (!oldLayout.Value.Contains(" ds=\"/")
                && (string.IsNullOrEmpty(layoutField.Value) || layoutField.Value == oldLayout.Value))
            {
                return;
            }

            LayoutDefinition layout = LayoutDefinition.Parse(layoutField.Value);
            foreach (DeviceDefinition device in layout.Devices)
            {
                if (device == null)
                {
                    continue;
                }

                foreach (RenderingDefinition rendering in device.Renderings)
                {
                    if (rendering == null)
                    {
                        continue;
                    }

                    if (!string.IsNullOrEmpty(rendering.Datasource) 
                        && !rendering.Datasource.StartsWith("{", StringComparison.InvariantCultureIgnoreCase))
                    {
                        Item datasourceItem = item.Database.GetItem(rendering.Datasource);
                        if (datasourceItem == null)
                        {
                            Log.Warn(
                                string.Format(
                                    "Could not find datasource item at path {0} while saving item {1}",
                                    rendering.Datasource,
                                    item.ID),
                                this);
                            continue;
                        }

                        rendering.Datasource = datasourceItem.ID.ToString();
                    }
                }
            }

            layoutField.Value = layout.ToXml();
        }

        #endregion
    }
}