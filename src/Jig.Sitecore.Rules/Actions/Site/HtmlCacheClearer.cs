﻿namespace Jig.Sitecore.Rules.Actions
{
    using System.Runtime.InteropServices;

    using global::Sitecore.Caching;
    using global::Sitecore.Configuration;
    using global::Sitecore.Data.Items;
    using global::Sitecore.Diagnostics;
    using global::Sitecore.Rules;
    using global::Sitecore.Sites;

    /// <summary>
    /// The html cache clearer.
    /// </summary>
    /// <typeparam name="TRuleContext">The type of the rule context.</typeparam>
    [Guid("F67746AF-2912-4B25-9F28-20DE52CC4D1D")]
    public sealed class HtmlCacheClearer<TRuleContext> : RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        #region Methods

        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">The rule context.</param>
        protected override void ApplyRule(TRuleContext ruleContext)
        {
            var sites = ruleContext.Item.Database.GetItem(Jig.Sitecore.Constants.Id.Sites.Content);
            if (sites == null || !sites.HasChildren)
            {
                return;
            }

            foreach (Item node in sites.Children)
            {
                string siteName = node.Name;
                if (siteName != null)
                {
                    SiteContext site = Factory.GetSite(siteName);
                    if (site != null)
                    {
                        HtmlCache htmlCache = CacheManager.GetHtmlCache(site);
                        if (htmlCache != null)
                        {
                            Log.Debug("Jig.Sitecore.Rules.Actions.HtmlCacheClearer=> " + siteName, this);
                            htmlCache.Clear();
                        }
                    }
                }
            }
        }

        #endregion
    }
}