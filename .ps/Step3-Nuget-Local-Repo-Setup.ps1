# Create folder "C:\Local.Nugets" if not exists
#
# Add ACL role Everyone with Full permissions
#
# Create folder "C:\Local.Nugets\git" if not exists
#
# Clone repo git@bitbucket.org:scwebcore/jig.platform.nugets.git (master always)

Try
{
    $scriptRoot = Split-Path (Resolve-Path $myInvocation.MyCommand.Path)
    
    . "$scriptRoot\Common.ps1"
    [xml]$config = Get-Script-Config

	$iisAppName = $config.Settings.SiteName

	$folderPrefix = $config.Settings.FolderPrefix
	$nugetFolder = $folderPrefix + $config.Settings.NugetFolderName + "\"
	$nugetGitFolder = $nugetFolder + "git\jig.platform"

	$gitUrl = $config.Settings.Git.NugetRepository.Url
	$gitBranch = $config.Settings.Git.NugetRepository.Branch

	$gitLocalFolder = $nugetGitFolder + "\" + $gitBranch

	Create-Folder $nugetFolder
	Create-Folder $nugetGitFolder
	Git-Clone-Pull $gitUrl $gitBranch $gitLocalFolder
}
Catch
{
	$ErrorMessage = $_.Exception.Message
	Log-Error $ErrorMessage
}

Pause