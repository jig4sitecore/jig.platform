# Required Params {SiteName} , {BranchName} 
# Optional Params {Git Repo url}
#
# Create folder "C:\Local.SqlServer" if not exists
#
# Add ACL role Everyone with Full permissions
#
# Create folder "C:\Local.SqlServer\git" if not exists
#
# Clone repo git@bitbucket.org:scwebcore/databases.sdn.sitecore.net.git with branch name {8-0-0-150223) 
# to folder "C:\Local.SqlServer\git\{BranchName}" if not exists
#
# Unzip "C:\Local.SqlServer\git\{BranchName}\Databases.zip" to the "C:\Local.SqlServer\Data\{SiteName}\{BranchName}\Databases\
Try
{
    $scriptRoot = Split-Path (Resolve-Path $myInvocation.MyCommand.Path)
    
    . "$scriptRoot\Common.ps1"
    [xml]$config = Get-Script-Config
    
    $serverName = Read-Host "Please enter your sql server location. ex.'SQLSERVER\INSTANCE' OR use default '(local)'"

    IF (!$serverName)
    {
        $serverName = $config.Settings.SQLServerName
    }

    $iisAppName = $config.Settings.SiteName

    $gitUrl = $config.Settings.Git.SqlRepository.Url
    $gitBranch = $config.Settings.Git.SqlRepository.Branch

    $gitBranchFolderName = $gitBranch.Replace("release/","")

    $folderPrefix = $config.Settings.FolderPrefix
    $sqlFolder = $folderPrefix + $config.Settings.SqlFolderName + "\"
    $sqlDataFolder = $sqlFolder + "Data\$iisAppName\$gitBranchFolderName\"
    $sqlGitFolder = $sqlFolder + "git\"

    $gitLocalFolder = $sqlGitFolder + $gitBranchFolderName
    $databaseZip = $gitLocalFolder + "\databases.zip"

    $databaseNames = @()

    foreach($database in $config.Settings.Databases.Database)
    {
	    $databaseNames += $database
    }

    Create-Folder $sqlFolder
    Set-FolderAclRule ($folderPrefix + $config.Settings.SqlFolderName)
    Create-Folder $sqlGitFolder

    Git-Clone-Pull $gitUrl $gitBranch $gitLocalFolder
    Create-Folder $sqlDataFolder

    $sh_app= New-Object -com Shell.Application 
    $zip_file = $sh_app.namespace($databaseZip) 
    $destination = $sh_app.namespace($sqlDataFolder)
    $destination.Copyhere($zip_file.items())

    [System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo") | Out-Null;

    $sql = New-Object Microsoft.SqlServer.Management.Smo.Server $serverName

    [System.Reflection.Assembly]::LoadWithPartialName("System") 

    foreach ($database in $databaseNames)
    {
        $files = New-Object System.Collections.Specialized.StringCollection 
	 
        $dataFileName = "$sqlDataFolder" + "Sitecore.$database.mdf";
        $logFileName = "$sqlDataFolder" + "Sitecore.$database.ldf";

        $files.Add("$dataFileName")
        $files.Add("$logFileName")
        $sqldbName = "$iisAppName.Sitecore.$database.$gitBranchFolderName"
	  
        Log-Info "Attaching $dataFileName to database $sqldbName"

        $sql.AttachDatabase("$sqldbName", $files)
	  
        Log-Info "Attached!"
    }
}
Catch
{
	$ErrorMessage = $_.Exception.Message
	Log-Error $ErrorMessage
}

Pause
