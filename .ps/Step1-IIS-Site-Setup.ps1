$scriptRoot = Split-Path (Resolve-Path $myInvocation.MyCommand.Path)

. "$scriptRoot\Common.ps1"
[xml]$config = Get-Script-Config

[System.Reflection.Assembly]::LoadFrom("C:\windows\system32\inetsrv\Microsoft.Web.Administration.dll") | out-null;
Import-Module WebAdministration

$iisAppName = $config.Settings.SiteName
$iisFullAppName = $config.Settings.SiteName + ".local" + $config.Settings.DomainSuffix
$iisAppPoolDotNetVersion = $config.Settings.DefaultRuntimeVersion
$siteDefaultPath = $config.Settings.InetpubLocation + $iisFullAppName
$iisDefaultPort = $config.Settings.IisPort
$domainSuffix = $config.Settings.DomainSuffix
$gitUrl = $config.Settings.Git.WebRepository.Url
$gitBranch = $config.Settings.Git.WebRepository.Branch

# Idea: Copy the Git repo localy and robocopy to wwwroot. No time waisted for branching and resetup.
$folderPrefix = $config.Settings.FolderPrefix
$sitecoreWebGitFolder = $folderPrefix + $config.Settings.SitecoreWebGitFolderName + "\" + [System.IO.Path]::GetFileNameWithoutExtension($gitUrl) + "\"
$gitLocalFolder = $sitecoreWebGitFolder + $gitBranch

$iisHeaders = @()

foreach ($h in $config.Settings.Hosts.Host)
{
    $iisHeaders += $iisAppName + $h + $domainSuffix
}
<#
    .DESCRIPTION
        Creates application pool in IIS
#>
Function Create-AppPool ($sitePoolName)
{  
	Log-Info "Begin - Create Site Pool Name: $sitePoolName"

    $serverManager = New-Object Microsoft.Web.Administration.ServerManager;
    
    # Remove old AppPool (if exists with the came name)
    if ($serverManager.ApplicationPools[$sitePoolName] -ne $NULL)
    {
        Log-Info "Old App Pool will be removed."
        $serverManager.ApplicationPools.Remove($serverManager.ApplicationPools[$sitePoolName])
    }
        
    $appPool = $serverManager.ApplicationPools.Add($sitePoolName);
    Log-Info "AppPool Created"

    $appPool.ManagedRuntimeVersion = $iisAppPoolDotNetVersion

    "Setting AppPool identity."	
    
    $appPool.ProcessModel.IdentityType = "ApplicationPoolIdentity"
 
    # This is development config
    $appPool.ProcessModel.IdleTimeout = [TimeSpan] "0.00:10:00"
    $appPool.Recycling.PeriodicRestart.time = [TimeSpan] "00:30:00"
    
    "AppPool identity set."  
    
    $serverManager.CommitChanges();   
    
    # Wait for the changes to apply
    Start-sleep -milliseconds 1500
	
	Log-Info "End - Create Site Pool Name" 
}

<#
    .DESCRIPTION
        Creates website in IIS
#>
Function Create-Site ($siteName)
{
    Log-Info "Begin - Create Website folder: $siteDefaultPath" 
    $serverManager = New-Object Microsoft.Web.Administration.ServerManager;
   
    # Remove old site (if exists with the same name)
    if ($serverManager.Sites[$siteName] -ne $NULL) 
    {
        "Old site will be removed."
        $serverManager.Sites.Remove($serverManager.Sites[$siteName])
    }

    $webSite = $serverManager.Sites.Add($siteName, "http", "*:${iisDefaultPort}:$($iisHeaders[0])", "$siteDefaultPath\website");

    for ($i = 1; $i -lt $iisHeaders.length; $i++) {
        $header = $iisHeaders[$i]
        $webSite.Bindings.Add("*:${iisDefaultPort}:$header","http");
    }

    $webSite.Applications[0].ApplicationPoolName = $siteName;

    Start-sleep -milliseconds 1000
    $serverManager.CommitChanges();    
    
    Log-Info "Website Created"
        
    # Wait for the changes to apply
    Start-sleep -milliseconds 1000
	
	Log-Info "End - End Website folder" 
}

<#
    .DESCRIPTION
        Creates entries to windows hosts file required by local IIS
#>
Function Create-WindowsHostsFileEntries([string] $siteName)
{
    $file = "C:\Windows\System32\drivers\etc\hosts"
    Log-Info "Beging - Updating host file $file..." 
    "`n`n# Site: $siteName.local.noclab.local" | Out-File $file -encoding ASCII -append 
    
    for ($i = 0; $i -lt $iisHeaders.length; $i++) {
        $header = $iisHeaders[$i]
        "127.0.0.1    $header" | Out-File $file -encoding ASCII -append 
        Log-Info "Added line: 127.0.0.0    $header"
    }
	
	 Log-Info "End - Updating host file" 
}

<#
    .DESCRIPTION
        Creates website in IIS Site Hostname bindings
#>
Function Create-SiteHostNameBinding ($iisAppName, $header, $iisPort)
{
    #add more bindings here as necessary
    New-WebBinding -Name $iisAppName -IP "*" -Port $iisPort -Protocol http -HostHeader $header
}

try{
	Create-Folder $sitecoreWebGitFolder
	Git-Clone-Pull $gitUrl $gitBranch $gitLocalFolder
    
	Create-Folder $siteDefaultPath

	Robocopy $gitLocalFolder $siteDefaultPath /e
    
	Create-AppPool $iisFullAppName 
	Create-Site $iisFullAppName
	Create-WindowsHostsFileEntries $iisAppName 
    
	Set-FolderAclRule $siteDefaultPath
}
catch{
	$ErrorMessage = $_.Exception.Message
	Log-Error $ErrorMessage
}

Pause