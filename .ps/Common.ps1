$scriptRoot = Split-Path (Resolve-Path $myInvocation.MyCommand.Path)
[xml]$config = get-content ($scriptRoot + "\setup.config")

$infoLog = $config.Settings.InfoLog + $config.Settings.SiteName + "-info.log"
$errorLog =$config.Settings.ErrorLog + $config.Settings.SiteName + "-error.log"

<#
	.DESCRIPTION
		Gets the configuration for the scripts
#>
Function Get-Script-Config
{
    [CmdletBinding()]
    PARAM()

    return $config
}

Function Log-Info
{
    [CmdletBinding()]
    PARAM($message)
    
    begin
    {
        $tempPreference = $ErrorActionPreference
        $ErrorActionPreference = "continue"
    }

    process
    {
		Write-Host $message -ForegroundColor Green
		$message | Out-File $infoLog -Append
    }

    end
    {
        $ErrorActionPreference = $tempPreference
    }
}

Function Log-Error
{
    [CmdletBinding()]
    PARAM($message)
    
    begin
    {
        $tempPreference = $ErrorActionPreference
        $ErrorActionPreference = "continue"
    }

    process
    {
		Write-Host $message -ForegroundColor DarkRed
        $message | Out-File $errorLog -Append
    }

    end
    {
        $ErrorActionPreference = $tempPreference
    }
}


<#
	.DESCRIPTION
		Creates Folder if it doesn't exist
#>
Function Create-Folder
{
    [CmdletBinding()]
    PARAM($path)

	if((Test-Path $path) -eq 0)
	{
		mkdir $path 
	}
}

<#
	.DESCRIPTION
		Creates Folder if it doesn't exist
#>
Function Git-Clone-Pull
{
    [CmdletBinding()]
    PARAM($gitUrl, $gitBranch, $gitPath)

	Log-Info "Begin - Git Clone/Pull $gitUrl $gitBranch $gitPath"
	Log-Info "        Git Url: $gitUrl"
	Log-Info "        Git Branch: $gitBranch"
	Log-Info "        Git Path: $gitPath"
	
    if((Test-Path $gitPath) -eq 0)
    {
        git clone $gitUrl -b $gitBranch --single-branch $gitPath
    }
    else
    {
        cd $gitPath
        git reset --hard
        git clean -d -x -f
        git pull origin $gitBranch
    }
	
	Log-Info "End - Git Clone/Pull"
}

<#
    .DESCRIPTION
        Set ACL rules for folder
#>
Function Set-FolderAclRule
{
    [CmdletBinding()]
    PARAM($folder)

    #add folder permissions for EVERYONE account on site folder (development only)
    Log-Info "Begin - Adding folder permissions for $folder"
	TRY
    {
        $acl = Get-Acl $folder 
        
        $rule = New-Object System.Security.AccessControl.FileSystemAccessRule("EVERYONE","FullControl", "ContainerInherit,ObjectInherit", "None", "Allow")
        $acl.AddAccessRule($rule)
        Set-Acl $folder $acl
    }
    CATCH
    {
        # NOT Critical Error
    	$ErrorMessage = $_.Exception.Message
	    Log-Error $ErrorMessage
    }
    
	Log-Info "End - Adding folder permissions"
}