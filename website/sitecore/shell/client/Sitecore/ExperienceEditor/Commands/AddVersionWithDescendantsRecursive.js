﻿define(["sitecore"], function (sitecore) {
    sitecore.Commands.AddVersionWithDescendantsRecursive =
    {
        canExecute: function (context) {
            return context.app.canExecute("Platform.ExperienceEditor.Language.CanExecute", context.currentContext);
        },
        execute: function (context) {
            context.app.disableButtonClickEvents();
            sitecore.ExperienceEditor.PipelinesUtil.generateRequestProcessor("Platform.ExperienceEditor.Language.AddVersionWithDescendantsRecursive", function () {
                window.top.location.reload();
            }).execute(context);
            context.app.enableButtonClickEvents();

        }
    };
});