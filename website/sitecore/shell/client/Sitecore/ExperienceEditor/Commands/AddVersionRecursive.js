﻿define(["sitecore"], function (sitecore) {
    sitecore.Commands.AddVersionRecursive =
    {
        canExecute: function (context) {
            return context.app.canExecute("Platform.ExperienceEditor.Language.CanExecute", context.currentContext);
        },
        execute: function (context) {
            context.app.disableButtonClickEvents();
            sitecore.ExperienceEditor.PipelinesUtil.generateRequestProcessor("Platform.ExperienceEditor.Language.AddVersionRecursive", function () {
                window.top.location.reload();
            }).execute(context);
            context.app.enableButtonClickEvents();

        }
    };
});