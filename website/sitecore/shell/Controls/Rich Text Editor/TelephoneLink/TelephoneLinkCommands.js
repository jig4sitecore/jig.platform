﻿var scEditor = null;
var scTool = null;

//Set the Id of your button into the RadEditorCommandList[]
RadEditorCommandList["InsertTelephoneLink"] = function (commandName, editor, args) {
    var d = Telerik.Web.UI.Editor.CommandList._getLinkArgument(editor);
    Telerik.Web.UI.Editor.CommandList._getDialogArguments(d, "A", editor, "DocumentManager");

    //Retrieve the html selected in the editor
    var html = editor.getSelectionHtml();

    scEditor = editor;

    //Call your custom dialog box
    editor.showExternalDialog(
  "/sitecore/shell/default.aspx?xmlcontrol=RichText.TelephoneLink&la=" + scLanguage,
  null, //argument
  600, //Height
  300, //Width
  scTelephoneLinkCallback, //callback
  null, // callback args
  "Insert Telephone Link",
  true, //modal
  Telerik.Web.UI.WindowBehaviors.Close, // behaviors
  false, //showStatusBar
  false //showTitleBar
   );
};

//The function called when the user close the dialog
function scTelephoneLinkCallback(sender, returnValue) {
    if (!returnValue) {
        return;
    }

    var d = scEditor.getSelection().getParentElement();

    if ($telerik.isFirefox && d.tagName === "A") {
        d.parentNode.removeChild(d);
    } else {
        scEditor.fire("Unlink");
    }

    var text = scEditor.getSelectionHtml();

    if (text === "" || text == null || ((text != null) && (text.length === 15) && (text.substring(2, 15).toLowerCase() === "<p>&nbsp;</p>"))) {
        text = returnValue.text;
    }
    else {
        // if selected string is a full paragraph, we want to insert the link inside the paragraph, and not the other way around.
        var regex = /^[\s]*<p>(.+)<\/p>[\s]*$/i;
        var match = regex.exec(text);
        if (match && match.length >= 2) {
            scEditor.pasteHtml("<p><a href=\"" + returnValue.url + "\">" + match[1] + "</a></p>", "DocumentManager");
            return;
        }
    }

    var cssClass = returnValue.cssClass ? " class=\"" + returnValue.cssClass + "\"" : "";

    scEditor.pasteHtml("<a href=\"" + returnValue.url + "\"" + cssClass +">" + text + "</a>", "DocumentManager");
}